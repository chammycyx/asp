
-- START CHANGE SCRIPT #1: 00001 [0.0.0.1] Create Sys Tables.sql

create table AUDIT_LOG (
    ID number(19) not null,
    HOSP_CODE varchar2(3) null,
    WORKSTORE_CODE varchar2(4) null,
    WORKSTATION_ID varchar2(100) null,
    USER_ID varchar2(20) null,
    APPLICATION_ID number(10) null,
    REQUEST_URI varchar2(500) null,
    TRAN_ID varchar2(50) null,
    ENTITY_NAME varchar2(100) null,
    MESSAGE_ID varchar2(5) null,
    MESSAGE varchar2(3000) not null,
    DETAIL_MESSAGE clob null,
    CREATE_DATE timestamp not null,
    PACKAGE_NAME varchar2(100) null,
    CLASS_NAME varchar2(100) null,
    METHOD_NAME varchar2(100) null,
    LINE_NUMBER varchar2(20) null,
    FILE_NAME varchar2(100) null,
    ELAPSE_TIME number(10) null,
    LOG_TYPE varchar2(1) not null,
    HOST_NAME varchar2(50) null,
    EXCEPTION_PACKAGE_NAME varchar2(100) null,
    EXCEPTION_CLASS_NAME varchar2(100) null,
    EXCEPTION_MESSAGE varchar2(1000) null,
    CAUSE_PACKAGE_NAME varchar2(100) null,
    CAUSE_CLASS_NAME varchar2(100) null,
    CAUSE_MESSAGE varchar2(1000) null,
    constraint PK_AUDIT_LOG primary key (ID) using index tablespace ASP_LOG_INDX_01)
partition by range (CREATE_DATE) (
    partition P_ASP_LOG_DATA_2017 values less than (maxvalue) tablespace P_ASP_LOG_DATA_2017);


create index I_AUDIT_LOG_01 on AUDIT_LOG (CREATE_DATE) local online;

create sequence SQ_AUDIT_LOG start with 10000000000000;

comment on table  AUDIT_LOG is 'Audit logs written by audit logger';
comment on column AUDIT_LOG.ID is 'Audit log id';
comment on column AUDIT_LOG.HOSP_CODE is 'Hospital code';
comment on column AUDIT_LOG.WORKSTORE_CODE is 'Workstore code';
comment on column AUDIT_LOG.WORKSTATION_ID is 'Workstation id';
comment on column AUDIT_LOG.USER_ID is 'Actioned user';
comment on column AUDIT_LOG.APPLICATION_ID is 'Application id';
comment on column AUDIT_LOG.REQUEST_URI is 'Access URL';
comment on column AUDIT_LOG.TRAN_ID is 'EclipseLink customizer - unit of work';
comment on column AUDIT_LOG.ENTITY_NAME is 'Java entity name';
comment on column AUDIT_LOG.MESSAGE_ID is 'System message code';
comment on column AUDIT_LOG.MESSAGE is 'Log message';
comment on column AUDIT_LOG.DETAIL_MESSAGE is 'Log message more than 1000 characters';
comment on column AUDIT_LOG.CREATE_DATE is 'Created date';
comment on column AUDIT_LOG.CLASS_NAME is 'Java class name';
comment on column AUDIT_LOG.METHOD_NAME is 'Java method name';
comment on column AUDIT_LOG.LINE_NUMBER is 'Line number of Java source code';
comment on column AUDIT_LOG.PACKAGE_NAME is 'Java package name';
comment on column AUDIT_LOG.FILE_NAME is 'Java file name';
comment on column AUDIT_LOG.ELAPSE_TIME is 'Elapsed time';
comment on column AUDIT_LOG.LOG_TYPE is 'Log type';
comment on column AUDIT_LOG.HOST_NAME is 'Workstation name';
comment on column AUDIT_LOG.EXCEPTION_PACKAGE_NAME is 'Exception package name';
comment on column AUDIT_LOG.EXCEPTION_CLASS_NAME is 'Exception class name';
comment on column AUDIT_LOG.EXCEPTION_MESSAGE is 'Exception message';
comment on column AUDIT_LOG.CAUSE_PACKAGE_NAME is 'Cause package name';
comment on column AUDIT_LOG.CAUSE_CLASS_NAME is 'Cause class name';
comment on column AUDIT_LOG.CAUSE_MESSAGE is 'Cause message';


create table SYSTEM_LOG (
    ID number(19) not null,
    HOSP_CODE varchar2(3) null,
    WORKSTORE_CODE varchar2(4) null,
    WORKSTATION_ID varchar2(100) null,
    USER_ID varchar2(20) null,
    APPLICATION_ID number(10) null,
    REQUEST_URI varchar2(500) null,
    TRAN_ID varchar2(50) null,
    ENTITY_NAME varchar2(100) null,
    MESSAGE_ID varchar2(5) null,
    MESSAGE varchar2(3000) not null,
    DETAIL_MESSAGE clob null,
    CREATE_DATE timestamp not null,
    PACKAGE_NAME varchar2(100) null,
    CLASS_NAME varchar2(100) null,
    METHOD_NAME varchar2(100) null,
    LINE_NUMBER varchar2(20) null,
    FILE_NAME varchar2(100) null,
    ELAPSE_TIME number(10) null,
    LOG_TYPE varchar2(1) not null,
    HOST_NAME varchar2(50) null,
    EXCEPTION_PACKAGE_NAME varchar2(100) null,
    EXCEPTION_CLASS_NAME varchar2(100) null,
    EXCEPTION_MESSAGE varchar2(1000) null,
    CAUSE_PACKAGE_NAME varchar2(100) null,
    CAUSE_CLASS_NAME varchar2(100) null,
    CAUSE_MESSAGE varchar2(1000) null,    
    constraint PK_SYSTEM_LOG primary key (ID) using index tablespace ASP_SLOG_INDX_01)
partition by range (CREATE_DATE) (
    partition P_ASP_SLOG_DATA_2017 values less than (maxvalue) tablespace P_ASP_SLOG_DATA_2017);

create index I_SYSTEM_LOG_01 on SYSTEM_LOG (CREATE_DATE) local;

create sequence SQ_SYSTEM_LOG start with 10000000000000;

comment on table  SYSTEM_LOG is 'System logs written by audit logger';
comment on column SYSTEM_LOG.ID is 'System log id';
comment on column SYSTEM_LOG.HOSP_CODE is 'Hospital code';
comment on column SYSTEM_LOG.WORKSTORE_CODE is 'Workstore code';
comment on column SYSTEM_LOG.WORKSTATION_ID is 'Workstation id';
comment on column SYSTEM_LOG.USER_ID is 'Actioned user';
comment on column SYSTEM_LOG.APPLICATION_ID is 'Application id';
comment on column SYSTEM_LOG.REQUEST_URI is 'Access URL';
comment on column SYSTEM_LOG.TRAN_ID is 'EclipseLink customizer - unit of work';
comment on column SYSTEM_LOG.ENTITY_NAME is 'Java entity name';
comment on column SYSTEM_LOG.MESSAGE_ID is 'System message code';
comment on column SYSTEM_LOG.MESSAGE is 'Log message';
comment on column SYSTEM_LOG.DETAIL_MESSAGE is 'Log message more than 1000 characters';
comment on column SYSTEM_LOG.CREATE_DATE is 'Created date';
comment on column SYSTEM_LOG.CLASS_NAME is 'Java class name';
comment on column SYSTEM_LOG.METHOD_NAME is 'Java method name';
comment on column SYSTEM_LOG.LINE_NUMBER is 'Line number of Java source code';
comment on column SYSTEM_LOG.PACKAGE_NAME is 'Java package name';
comment on column SYSTEM_LOG.FILE_NAME is 'Java file name';
comment on column SYSTEM_LOG.ELAPSE_TIME is 'Elapsed time';
comment on column SYSTEM_LOG.LOG_TYPE is 'Log type';
comment on column SYSTEM_LOG.HOST_NAME is 'Workstation name';
comment on column SYSTEM_LOG.EXCEPTION_PACKAGE_NAME is 'Exception package name';
comment on column SYSTEM_LOG.EXCEPTION_CLASS_NAME is 'Exception class name';
comment on column SYSTEM_LOG.EXCEPTION_MESSAGE is 'Exception message';
comment on column SYSTEM_LOG.CAUSE_PACKAGE_NAME is 'Cause package name';
comment on column SYSTEM_LOG.CAUSE_CLASS_NAME is 'Cause class name';
comment on column SYSTEM_LOG.CAUSE_MESSAGE is 'Cause message';


create table SYSTEM_MESSAGE (
    MESSAGE_CODE varchar2(5) not null,
    APPLICATION_ID number(10) not null,
    FUNCTION_ID number(10) not null,
    OPERATION_ID number(10) null,
    SEVERITY_CODE varchar2(1) not null,
    MAIN_MSG varchar2(500) not null,
    LOCALE varchar2(5) not null,
    SUPPL_MSG varchar2(500) null,
    DETAIL varchar2(500) null,
    BTN_YES_CAPTION varchar2(30) null,
    BTN_YES_SIZE varchar2(1) null,
    BTN_YES_SHORTCUT varchar2(1) null,
    BTN_NO_CAPTION varchar2(30) null,
    BTN_NO_SIZE varchar2(1) null,
    BTN_NO_SHORTCUT varchar2(1) null,
    BTN_CANCEL_CAPTION varchar2(30) null,
    BTN_CANCEL_SIZE varchar2(1) null,
    BTN_CANCEL_SHORTCUT varchar2(1) null,
    EFFECTIVE timestamp null,
    EXPIRY timestamp null,
    CREATE_USER varchar2(12) not null,
    CREATE_DATE timestamp not null,
    UPDATE_USER varchar2(12) not null,
    UPDATE_DATE timestamp not null,
    VERSION number(19) not null,
    constraint PK_SYSTEM_MESSAGE primary key (MESSAGE_CODE) using index tablespace ASP_INDX_01)
tablespace ASP_DATA_01;

comment on table  SYSTEM_MESSAGE is 'System messages';
comment on column SYSTEM_MESSAGE.MESSAGE_CODE is 'System message code';
comment on column SYSTEM_MESSAGE.APPLICATION_ID is 'Application id';
comment on column SYSTEM_MESSAGE.FUNCTION_ID is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.OPERATION_ID is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.SEVERITY_CODE is 'Severity code';
comment on column SYSTEM_MESSAGE.MAIN_MSG is 'System message content';
comment on column SYSTEM_MESSAGE.LOCALE is 'Locale';
comment on column SYSTEM_MESSAGE.SUPPL_MSG is 'Supplementary message';
comment on column SYSTEM_MESSAGE.DETAIL is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_YES_CAPTION is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_YES_SIZE is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_YES_SHORTCUT is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_NO_CAPTION is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_NO_SIZE is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_NO_SHORTCUT is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_CANCEL_CAPTION is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_CANCEL_SIZE is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.BTN_CANCEL_SHORTCUT is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.EFFECTIVE is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.EXPIRY is 'Column defined by CMS III Architecture - not used in PMS';
comment on column SYSTEM_MESSAGE.CREATE_USER is 'Created user';
comment on column SYSTEM_MESSAGE.CREATE_DATE is 'Created date';
comment on column SYSTEM_MESSAGE.UPDATE_USER is 'Last updated user';
comment on column SYSTEM_MESSAGE.UPDATE_DATE is 'Last updated date';
comment on column SYSTEM_MESSAGE.VERSION is 'Version to serve as optimistic lock value';

create table SEQ_TABLE (
    SEQ_NAME varchar2(100) not null,
    CURR_NUM number(19) not null,
    MIN_NUM number(19) not null,
    MAX_NUM number(19) not null,
    UPDATE_DATE timestamp not null,
    constraint PK_SEQ_TABLE primary key (SEQ_NAME) using index tablespace ASP_INDX_01)
tablespace ASP_DATA_01;

comment on table  SEQ_TABLE is 'Sequence table';
comment on column SEQ_TABLE.SEQ_NAME is 'Column used';
comment on column SEQ_TABLE.CURR_NUM is 'Current number';
comment on column SEQ_TABLE.MIN_NUM is 'Minimum number';
comment on column SEQ_TABLE.MAX_NUM is 'Maximum number';
comment on column SEQ_TABLE.UPDATE_DATE is 'Last updated date';



INSERT INTO CHANGELOG (change_number, complete_dt, applied_by, description)
 VALUES (1, CURRENT_TIMESTAMP, USER, '00001 [0.0.0.1] Create Sys Tables.sql');

COMMIT;

-- END CHANGE SCRIPT #1: 00001 [0.0.0.1] Create Sys Tables.sql


-- START CHANGE SCRIPT #2: 00002 [0.0.0.1] Create Data SystemMessage.sql

insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0001', '1022','0',null,'Q', 'Are you sure to logoff?', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0002', '1022','0',null,'I', 'Save successfully.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0003', '1022','0',null,'I', 'Delete successfully.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0004', '1022','0',null,'W', 'You have already opened the application.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0005', '1022','0',null,'W', 'Duplicate user code [#0].', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0006', '1022','0',null,'Q', 'Are you sure to mark selected record(s) as [Not for review]?', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0007', '1022','0',null,'W', 'Password must contain 6 to 12 characters.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0008', '1022','0',null,'Q', 'Are you sure to submit?', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0009', '1022','0',null,'I', 'Submit successfully.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0010', '1022','0',null,'Q', 'Are you sure to clear all radio buttons?', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0011', '1022','0',null,'W', 'Please input treatment.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0012', '1022','0',null,'W', 'Please input appropriate description.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0013', '1022','0',null,'W', 'Please input immediate concurrent feedback.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0014', '1022','0',null,'W', 'Please input immediate concurrent feedback information.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0015', '1022','0',null,'W', 'Please input switch criteria.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0016', '1022','0',null,'W', 'Please input switching criteria.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0017', '1022','0',null,'W', 'Please input switching criteria information.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0018', '1022','0',null,'W', 'Failed to update. The report [#0] is being [#1] by [#2] at [#3].', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0019', '1022','0',null,'I', 'Unlock successfully.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0020', '1022','0',null,'I', 'Unsubmit successfully.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0021', '1022','0',null,'I', 'Restore review successfully.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0022', '1022','0',null,'I', 'Update successfully.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0023', '1022','0',null,'I', 'Record not found.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0024', '1022','0',null,'W', 'Invalid report date range.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0025', '1022','0',null,'W', 'Please input report date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0026', '1022','0',null,'W', 'Please input HKID.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0027', '1022','0',null,'W', 'Please input SN.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0028', '1022','0',null,'W', 'Please select hospital.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0029', '1022','0',null,'Q', 'Are you sure to refresh selected record(s)?', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0030', '1022','0',null,'I', 'Refresh successfully.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0031', '1022','0',null,'I', 'Mark [Not for review] successfully.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0032', '1022','0',null,'W', 'Weight must be between 3 and 150.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0033', '1022','0',null,'W', 'Invalid first specimen date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0034', '1022','0',null,'W', 'Invalid first reference date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0035', '1022','0',null,'W', 'Invalid first report date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0036', '1022','0',null,'W', 'Invalid second specimen date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0037', '1022','0',null,'W', 'Invalid second reference date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0038', '1022','0',null,'W', 'Invalid second report date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0039', '1022','0',null,'W', 'Invalid third specimen date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0040', '1022','0',null,'W', 'Invalid third reference date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0041', '1022','0',null,'W', 'Invalid third report date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0042', '1022','0',null,'W', 'Invalid collection date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0043', '1022','0',null,'W', 'Invalid audit date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0044', '1022','0',null,'W', 'Intended duration must be between 0 and 9999.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0045', '1022','0',null,'W', 'HKPMI service is not available, please contact IT system support for assistance.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0046', '1022','0',null,'W', 'EPR service is not available, please contact IT system support for assistance.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0047', '1022','0',null,'W', 'Only new / edited record(s) can be refreshed.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0048', '1022','0',null,'W', 'Only new / edited record(s) can be marked as [Not for review].', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0049', '1022','0',null,'W', 'Body temp. must be between 25 and 50.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0050', '1022','0',null,'W', '[Not for review] record(s) cannot be exported.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0051', '1022','0',null,'W', 'Please select record(s).', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0052', '1022','0',null,'W', '[Submitted] / [Not for review] record(s) cannot be edited.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0053', '1022','0',null,'W', 'Invalid ICF date.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0054', '1022','0',null,'Q', '[#0] Program Audit form has been opened for SN [#1], all unsave information will be lost.  Are you sure to continue?', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0055', '1022','0',null,'W', 'Maximum [#0] reports can be selected for refresh.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION)  values ('0056', '1022','0',null,'I', 'Previous action still in progress.  Please retry later.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);





INSERT INTO CHANGELOG (change_number, complete_dt, applied_by, description)
 VALUES (2, CURRENT_TIMESTAMP, USER, '00002 [0.0.0.1] Create Data SystemMessage.sql');

COMMIT;

-- END CHANGE SCRIPT #2: 00002 [0.0.0.1] Create Data SystemMessage.sql


-- START CHANGE SCRIPT #3: 00003 [0.0.0.1] Create Asp Tables.sql

create table ASP_USER
(
	USER_CODE	varchar2(12) not null,
	CREATE_USER	varchar2(12) not null,
	CREATE_DATE	timestamp not null,
	UPDATE_USER	varchar2(12) not null,
	UPDATE_DATE	timestamp not null,
	VERSION		varchar2(19) not null,
	constraint PK_ASP_USER primary key (USER_CODE) using index tablespace ASP_INDX_01
)
tablespace ASP_DATA_01;

comment on column ASP_USER.USER_CODE is 'User code';
comment on column ASP_USER.CREATE_USER is 'Created user';
comment on column ASP_USER.CREATE_DATE is 'Created date';
comment on column ASP_USER.UPDATE_USER is 'Last updated user';
comment on column ASP_USER.UPDATE_DATE is 'Last updated date';
comment on column ASP_USER.VERSION is 'Version to serve as optimistic lock value';

create table ASP_USER_HOSP
(
	USER_CODE varchar2(12) not null,
	HOSP_CODE varchar2(3) not null,
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,
	constraint PK_ASP_USER_HOSP primary key (USER_CODE,HOSP_CODE) using index tablespace ASP_INDX_01
)
tablespace ASP_DATA_01;

comment on column ASP_USER_HOSP.USER_CODE is 'User code';
comment on column ASP_USER_HOSP.HOSP_CODE is 'Hospital code';
comment on column ASP_USER_HOSP.CREATE_USER is 'Created user';
comment on column ASP_USER_HOSP.CREATE_DATE is 'Created date';
comment on column ASP_USER_HOSP.UPDATE_USER is 'Last updated user';
comment on column ASP_USER_HOSP.UPDATE_DATE is 'Last updated date';
comment on column ASP_USER_HOSP.VERSION is 'Version to serve as optimistic lock value';

create table ASP_DISP_ITEM
	(
	ID number(19,0) not null,
	CASE_NUM varchar2(12),
	HOSP_CODE varchar2(3) not null,
	ITEM_CODE varchar2(6) not null,
	ITEM_NUM number(10,0),
	HKID varchar2(12),
	PAT_KEY varchar2(8),
	DISP_QTY number(19,4),
	DISP_DATE date,
	WARD_CODE varchar2(4),
	SPEC_CODE varchar2(4),
	DRUG_NAME varchar2(59),
	STRENGTH varchar2(59),
	FORM_CODE varchar2(3),
	FORM_DESC varchar2(30),
	FREQ_CODE varchar2(7),
	FREQ_DESC varchar2(42),
	DOSE_QTY varchar2(19),
	DOSE_UNIT varchar2(15),
	START_DATE date,
	BATCH_DATE date,
	ASP_TYPE varchar2(2) not null,
	STATUS varchar2(1) not null,
	ASP_DISP_RPT_ID number(19,0),
	PAT_HOSP_CODE varchar2(3),
	DISP_ORDER_ID number(19,0),
	PAT_NAME varchar2(48),
	DOB timestamp(6),
	SEX varchar2(1),
	CC_CODE varchar2(100),
	BED_NUM	varchar2(5),
	ADMISSION_DATE timestamp(6),
	DISCHARGE_DATE timestamp(6),
	PAS_SPEC_CODE varchar2(4),
	PAS_WARD_CODE varchar2(4),
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,	
	constraint PK_ASP_DISP_ITEM primary key (ID) using index tablespace ASP_INDX_01
	)
	partition by list (HOSP_CODE)
	(
		partition P_ASP_DATA_AHN values ('AHN') tablespace P_ASP_DATA_AHN,
		partition P_ASP_DATA_CMC values ('CMC') tablespace P_ASP_DATA_CMC,
		partition P_ASP_DATA_KH values ('KH') tablespace P_ASP_DATA_KH,
		partition P_ASP_DATA_KWH values ('KWH') tablespace P_ASP_DATA_KWH,
		partition P_ASP_DATA_NDH values ('NDH') tablespace P_ASP_DATA_NDH,
		partition P_ASP_DATA_PMH values ('PMH') tablespace P_ASP_DATA_PMH,
		partition P_ASP_DATA_PWH values ('PWH') tablespace P_ASP_DATA_PWH,
		partition P_ASP_DATA_PYN values ('PYN') tablespace P_ASP_DATA_PYN,
		partition P_ASP_DATA_QMH values ('QMH') tablespace P_ASP_DATA_QMH,
		partition P_ASP_DATA_QEH values ('QEH') tablespace P_ASP_DATA_QEH,
		partition P_ASP_DATA_RH values ('RH') tablespace P_ASP_DATA_RH,
		partition P_ASP_DATA_TKO values ('TKO') tablespace P_ASP_DATA_TKO,
		partition P_ASP_DATA_TWH values ('TWH') tablespace P_ASP_DATA_TWH,
		partition P_ASP_DATA_TMH values ('TMH') tablespace P_ASP_DATA_TMH,
		partition P_ASP_DATA_UCH values ('UCH') tablespace P_ASP_DATA_UCH,
		partition P_ASP_DATA_YCH values ('YCH') tablespace P_ASP_DATA_YCH,
		partition P_ASP_DATA_DKH values ('DKH') tablespace P_ASP_DATA_DKH,
		partition P_ASP_DATA_GH values ('GH') tablespace P_ASP_DATA_GH,
		partition P_ASP_DATA_TWE values ('TWE') tablespace P_ASP_DATA_TWE,
		partition P_ASP_DATA_NLT values ('NLT') tablespace P_ASP_DATA_NLT,
		partition P_ASP_DATA_BH values ('BH') tablespace P_ASP_DATA_BH,
		partition P_ASP_DATA_OLM values ('OLM') tablespace P_ASP_DATA_OLM,
		partition P_ASP_DATA_TPH values ('TPH') tablespace P_ASP_DATA_TPH,
		partition P_ASP_DATA_DEFAULT values (DEFAULT) tablespace P_ASP_DATA_DEFAULT
	);

comment on table ASP_DISP_ITEM is 'ASP Dispensing order items';
comment on column ASP_DISP_ITEM.ID is 'ID';
comment on column ASP_DISP_ITEM.CASE_NUM is 'Medication case number';
comment on column ASP_DISP_ITEM.HOSP_CODE is 'Disp. hospital code';
comment on column ASP_DISP_ITEM.ITEM_CODE is 'Item code';
comment on column ASP_DISP_ITEM.ITEM_NUM is 'Item number';
comment on column ASP_DISP_ITEM.HKID is 'HKID';
comment on column ASP_DISP_ITEM.PAT_KEY is 'Patient key';
comment on column ASP_DISP_ITEM.DISP_QTY is 'Dispensed quantity';
comment on column ASP_DISP_ITEM.DISP_DATE is 'Dispensing date';
comment on column ASP_DISP_ITEM.WARD_CODE is 'Ward code';
comment on column ASP_DISP_ITEM.SPEC_CODE is 'Specialty code';
comment on column ASP_DISP_ITEM.DRUG_NAME is 'Drug name';
comment on column ASP_DISP_ITEM.STRENGTH is 'Strength';
comment on column ASP_DISP_ITEM.FORM_CODE is 'Form code';
comment on column ASP_DISP_ITEM.FORM_DESC is 'Form description';
comment on column ASP_DISP_ITEM.FREQ_CODE is 'Frequency code';
comment on column ASP_DISP_ITEM.FREQ_DESC is 'Frequency description';
comment on column ASP_DISP_ITEM.DOSE_QTY is 'Dose quantity';
comment on column ASP_DISP_ITEM.DOSE_UNIT is 'Dose unit';
comment on column ASP_DISP_ITEM.START_DATE is 'Start Date';
comment on column ASP_DISP_ITEM.BATCH_DATE is 'Batch Date';
comment on column ASP_DISP_ITEM.ASP_TYPE is 'ASP Type (BG,IV)';
comment on column ASP_DISP_ITEM.STATUS is 'Status';
comment on column ASP_DISP_ITEM.ASP_DISP_RPT_ID is 'Asp Report Id';
comment on column ASP_DISP_ITEM.PAT_HOSP_CODE is 'Patient hospital code';
comment on column ASP_DISP_ITEM.DISP_ORDER_ID is 'Disp order id';
comment on column ASP_DISP_ITEM.CREATE_USER is 'Created user';
comment on column ASP_DISP_ITEM.CREATE_DATE is 'Created date';
comment on column ASP_DISP_ITEM.UPDATE_USER is 'Last updated user';
comment on column ASP_DISP_ITEM.UPDATE_DATE is 'Last updated date';
comment on column ASP_DISP_ITEM.VERSION is 'Version to serve as optimistic lock value';
comment on column ASP_DISP_ITEM.PAT_NAME is 'Patient name';
comment on column ASP_DISP_ITEM.DOB is 'Date of birth';
comment on column ASP_DISP_ITEM.SEX is 'Sex';
comment on column ASP_DISP_ITEM.CC_CODE is 'Cc code';
comment on column ASP_DISP_ITEM.BED_NUM is 'Bed number';
comment on column ASP_DISP_ITEM.ADMISSION_DATE is 'Admission date';
comment on column ASP_DISP_ITEM.DISCHARGE_DATE is 'Discharge date';
comment on column ASP_DISP_ITEM.PAS_SPEC_CODE is 'Pas specialty code';
comment on column ASP_DISP_ITEM.PAS_WARD_CODE is 'Pas ward code';

create table ASP_DISP_RPT
	(
	ID number(19,0) not null,
	HOSP_CODE varchar2(3) not null,
	RPT_TYPE varchar2(2) not null,
	SERIAL_NUM varchar2(13) not null,
	RPT_XML clob,
	RPT_DATE timestamp not null,
	HKID varchar2(12),
	PAS_SPEC_CODE varchar2(4),
	PAS_WARD_CODE varchar2(4),
	STATUS varchar2(1) not null,
	PAT_NAME varchar2(48),
	DRUG_NAME varchar2(59),
	DISP_DATE date not null,
	DOB timestamp(6),
	SEX varchar2(1),
	CC_CODE varchar2(100),
	PAT_KEY varchar2(8),
	ADMIN_STATUS varchar2(1) not null,
	DISCHARGE_FLAG number(1) not null,
	WORKSTATION_ID varchar2(19),
	ASP_DISP_ITEM_ID number(19,0) not null,
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,	
	constraint PK_ASP_DISP_RPT primary key (ID) using index tablespace ASP_INDX_01
	)
partition by list (HOSP_CODE)
	(
		partition P_ASP_DATA_AHN values ('AHN') tablespace P_ASP_DATA_AHN,
		partition P_ASP_DATA_CMC values ('CMC') tablespace P_ASP_DATA_CMC,
		partition P_ASP_DATA_KH values ('KH') tablespace P_ASP_DATA_KH,
		partition P_ASP_DATA_KWH values ('KWH') tablespace P_ASP_DATA_KWH,
		partition P_ASP_DATA_NDH values ('NDH') tablespace P_ASP_DATA_NDH,
		partition P_ASP_DATA_PMH values ('PMH') tablespace P_ASP_DATA_PMH,
		partition P_ASP_DATA_PWH values ('PWH') tablespace P_ASP_DATA_PWH,
		partition P_ASP_DATA_PYN values ('PYN') tablespace P_ASP_DATA_PYN,
		partition P_ASP_DATA_QMH values ('QMH') tablespace P_ASP_DATA_QMH,
		partition P_ASP_DATA_QEH values ('QEH') tablespace P_ASP_DATA_QEH,
		partition P_ASP_DATA_RH values ('RH') tablespace P_ASP_DATA_RH,
		partition P_ASP_DATA_TKO values ('TKO') tablespace P_ASP_DATA_TKO,
		partition P_ASP_DATA_TWH values ('TWH') tablespace P_ASP_DATA_TWH,
		partition P_ASP_DATA_TMH values ('TMH') tablespace P_ASP_DATA_TMH,
		partition P_ASP_DATA_UCH values ('UCH') tablespace P_ASP_DATA_UCH,
		partition P_ASP_DATA_YCH values ('YCH') tablespace P_ASP_DATA_YCH,
		partition P_ASP_DATA_DKH values ('DKH') tablespace P_ASP_DATA_DKH,
		partition P_ASP_DATA_GH values ('GH') tablespace P_ASP_DATA_GH,
		partition P_ASP_DATA_TWE values ('TWE') tablespace P_ASP_DATA_TWE,
		partition P_ASP_DATA_NLT values ('NLT') tablespace P_ASP_DATA_NLT,
		partition P_ASP_DATA_BH values ('BH') tablespace P_ASP_DATA_BH,
		partition P_ASP_DATA_OLM values ('OLM') tablespace P_ASP_DATA_OLM,
		partition P_ASP_DATA_TPH values ('TPH') tablespace P_ASP_DATA_TPH,
		partition P_ASP_DATA_DEFAULT values (DEFAULT) tablespace P_ASP_DATA_DEFAULT
	);

comment on column ASP_DISP_RPT.ID is 'ID';
comment on column ASP_DISP_RPT.HOSP_CODE is 'Hospital code';
comment on column ASP_DISP_RPT.RPT_TYPE is 'ASP Type (BG,IV)';
comment on column ASP_DISP_RPT.SERIAL_NUM is 'serial number';
comment on column ASP_DISP_RPT.RPT_XML is 'report details in xml';
comment on column ASP_DISP_RPT.RPT_DATE is 'report date';
comment on column ASP_DISP_RPT.HKID is 'HKID';
comment on column ASP_DISP_RPT.PAS_SPEC_CODE is 'pas specialty code';
comment on column ASP_DISP_RPT.PAS_WARD_CODE is 'pas ward code';
comment on column ASP_DISP_RPT.STATUS is 'status';
comment on column ASP_DISP_RPT.PAT_NAME is 'patient name';
comment on column ASP_DISP_RPT.DRUG_NAME is 'drug name';
comment on column ASP_DISP_RPT.DISP_DATE is 'Dispensing date';
comment on column ASP_DISP_RPT.DOB is 'Date of birth';
comment on column ASP_DISP_RPT.SEX is 'Sex';
comment on column ASP_DISP_RPT.CC_CODE is 'HKID CC code';
comment on column ASP_DISP_RPT.PAT_KEY is 'Patient key';
comment on column ASP_DISP_RPT.ADMIN_STATUS is 'admin status';
comment on column ASP_DISP_RPT.DISCHARGE_FLAG is 'discharge flag';
comment on column ASP_DISP_RPT.WORKSTATION_ID is 'workstation id';
comment on column ASP_DISP_RPT.ASP_DISP_ITEM_ID is 'Asp Disp Item Id';
comment on column ASP_DISP_RPT.CREATE_USER is 'Created user';
comment on column ASP_DISP_RPT.CREATE_DATE is 'Created date';
comment on column ASP_DISP_RPT.UPDATE_USER is 'Last updated user';
comment on column ASP_DISP_RPT.UPDATE_DATE is 'Last updated date';
comment on column ASP_DISP_RPT.VERSION is 'Version to serve as optimistic lock value';

alter table ASP_USER_HOSP add constraint FK_ASP_USER_HOSP_01 foreign key (USER_CODE) references ASP_USER (USER_CODE);
alter table ASP_DISP_ITEM add constraint FK_ASP_DISP_ITEM_01 foreign key (ASP_DISP_RPT_ID) references ASP_DISP_RPT (ID);
alter table ASP_DISP_RPT add constraint FK_ASP_DISP_RPT_01 foreign key (ASP_DISP_ITEM_ID) references ASP_DISP_ITEM (ID);

create index FK_ASP_USER_HOSP_01 on ASP_USER_HOSP (USER_CODE) tablespace ASP_INDX_01;
create index FK_ASP_DISP_ITEM_01 on ASP_DISP_ITEM (ASP_DISP_RPT_ID) tablespace ASP_INDX_01;
create index FK_ASP_DISP_RPT_01 on ASP_DISP_RPT (ASP_DISP_ITEM_ID) tablespace ASP_INDX_01;



INSERT INTO CHANGELOG (change_number, complete_dt, applied_by, description)
 VALUES (3, CURRENT_TIMESTAMP, USER, '00003 [0.0.0.1] Create Asp Tables.sql');

COMMIT;

-- END CHANGE SCRIPT #3: 00003 [0.0.0.1] Create Asp Tables.sql


-- START CHANGE SCRIPT #4: 00004 [0.0.0.1] Create Data SeqTable.sql

insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_RH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_PYN_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_QMH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_TWH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_UCH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_TKO_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_QEH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_KH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_PMH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_CMC_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_KWH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_YCH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_PWH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_AHN_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_NDH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_TMH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_SH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_DKH_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_GH_BG',0,1,99999999,sysdate); 
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_TWE_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_NLT_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_BH_BG',0,1,99999999,sysdate); 
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_OLM_BG',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_TPH_BG',0,1,99999999,sysdate);

insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_RH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_PYN_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_QMH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_TWH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_UCH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_TKO_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_QEH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_KH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_PMH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_CMC_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_KWH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_YCH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_PWH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_AHN_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_NDH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_TMH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_SH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_DKH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_GH_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_TWE_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_NLT_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_BH_IV',0,1,99999999,sysdate); 
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_OLM_IV',0,1,99999999,sysdate);
insert into SEQ_TABLE (SEQ_NAME,CURR_NUM,MIN_NUM,MAX_NUM,UPDATE_DATE) values ('RPT_SERIAL_NUM_TPH_IV',0,1,99999999,sysdate);


INSERT INTO CHANGELOG (change_number, complete_dt, applied_by, description)
 VALUES (4, CURRENT_TIMESTAMP, USER, '00004 [0.0.0.1] Create Data SeqTable.sql');

COMMIT;

-- END CHANGE SCRIPT #4: 00004 [0.0.0.1] Create Data SeqTable.sql


-- START CHANGE SCRIPT #5: 00005 [0.0.0.1] Create All Sequences.sql

create sequence SQ_ASP_DISP_ITEM increment by 50 start with 10000000000049;
create sequence SQ_ASP_DISP_RPT increment by 50 start with 10000000000049;


INSERT INTO CHANGELOG (change_number, complete_dt, applied_by, description)
 VALUES (5, CURRENT_TIMESTAMP, USER, '00005 [0.0.0.1] Create All Sequences.sql');

COMMIT;

-- END CHANGE SCRIPT #5: 00005 [0.0.0.1] Create All Sequences.sql


-- START CHANGE SCRIPT #11: 00011 [1.0.0.2] Create Data SystemMessage.sql

insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0057', '1022','0',null,'A','Enquiry|Retrieve|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|AspRptCriteria [#3]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0058', '1022','0',null,'A','Enquiry|Export|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|ScopeOfReport [#3]|AspDispRpt [#4]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0059', '1022','0',null,'A','Enquiry|Refresh|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|G6dpCode [#3]|AspDispRpt [#4]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0060', '1022','0',null,'A','Enquiry|Not for Review|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|AspDispRpt [#3]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);

insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0061', '1022','0',null,'A','Audit Form|Lock Form|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|AspDispRpt [#3]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0062', '1022','0',null,'A','Audit Form|Save Success|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|AspDispRpt [#3]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0063', '1022','0',null,'A','Audit Form|Submit Success|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|AspDispRpt [#3]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0064', '1022','0',null,'A','Audit Form|Unlock Form|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|AspDispRpt [#3]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);

insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0065', '1022','0',null,'A','Operation Report|Export|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|HospCode [#3]||RptType [#4]|DispDateFrom [#5]|DispDateTo [#6]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);

insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0066', '1022','0',null,'A','Rollback|Retrieve|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|AdminStatus [#3]|DispDateFrom [#4]|DispDateTo [#5]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0067', '1022','0',null,'A','Rollback|Reset|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|AspDispRpt [#3]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);

insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0068', '1022','0',null,'A','User Management|Retrieve User|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0069', '1022','0',null,'A','User Management|Retrieve User List|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0070', '1022','0',null,'A','User Management|Save|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|AspUser [#3]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0071', '1022','0',null,'A','User Management|Delete|UserId [#0]|WorkstationId [#1]|UpdateDate [#2]|AspUser [#3]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);

insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0072', '1022','0',null,'A','#0|Retrieve Hospital List By User|UserId [#1]|WorkstationId [#2]|UpdateDate [#3]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0073', '1022','0',null,'A','#0|Retrieve Hospital List|UserId [#1]|WorkstationId [#2]|UpdateDate [#3]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0074', '1022','0',null,'A','#0|Update Asp Hospital|UserId [#1]|WorkstationId [#2]|UpdateDate [#3]|AspHosp [#4]|', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);

insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0075', '1022','0',null,'I','This session has timed out for security reasons.  Please re-logon.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);




INSERT INTO CHANGELOG (change_number, complete_dt, applied_by, description)
 VALUES (11, CURRENT_TIMESTAMP, USER, '00011 [1.0.0.2] Create Data SystemMessage.sql');

COMMIT;

-- END CHANGE SCRIPT #11: 00011 [1.0.0.2] Create Data SystemMessage.sql


-- START CHANGE SCRIPT #31: 00031 (1.0.0.4) Create Data SystemMessage.sql

insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0076', '1022','0',null,'I','Save fail: Record has been unlocked by [#0].', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0077', '1022','0',null,'I','#0', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);
insert into SYSTEM_MESSAGE (MESSAGE_CODE, APPLICATION_ID, FUNCTION_ID, OPERATION_ID, SEVERITY_CODE, MAIN_MSG, LOCALE, SUPPL_MSG, DETAIL, BTN_YES_CAPTION, BTN_YES_SIZE, BTN_YES_SHORTCUT, BTN_NO_CAPTION, BTN_NO_SIZE, BTN_NO_SHORTCUT, BTN_CANCEL_CAPTION, BTN_CANCEL_SIZE, BTN_CANCEL_SHORTCUT, EFFECTIVE, EXPIRY, CREATE_USER, CREATE_DATE, UPDATE_USER, UPDATE_DATE, VERSION) values ('0078', '1022','0',null,'I','Exception is found during Refresh.', 'en_US', '' ,'', '', null, null, '',  null, null, '', null, null, null, null, 'itd',sysdate,'itd',sysdate,1);




INSERT INTO CHANGELOG (change_number, complete_dt, applied_by, description)
 VALUES (31, CURRENT_TIMESTAMP, USER, '00031 (1.0.0.4) Create Data SystemMessage.sql');

COMMIT;

-- END CHANGE SCRIPT #31: 00031 (1.0.0.4) Create Data SystemMessage.sql


-- START CHANGE SCRIPT #41: 00041 (1.0.0.5) Update Data SystemMessage.sql

update SYSTEM_MESSAGE set MAIN_MSG ='Alert Profile exception is found for following patient.',  SUPPL_MSG ='#0' where  MESSAGE_CODE=0078;




INSERT INTO CHANGELOG (change_number, complete_dt, applied_by, description)
 VALUES (41, CURRENT_TIMESTAMP, USER, '00041 (1.0.0.5) Update Data SystemMessage.sql');

COMMIT;

-- END CHANGE SCRIPT #41: 00041 (1.0.0.5) Update Data SystemMessage.sql

