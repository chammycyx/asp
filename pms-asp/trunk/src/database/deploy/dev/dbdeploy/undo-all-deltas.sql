
-- START UNDO OF CHANGE SCRIPT #41: 00041 (1.0.0.5) Update Data SystemMessage.sql

update SYSTEM_MESSAGE set MAIN_MSG ='Exception is found during Refresh.',  SUPPL_MSG ='' where  MESSAGE_CODE=0078;
--//


DELETE FROM CHANGELOG WHERE change_number = 41;

COMMIT;

-- END UNDO OF CHANGE SCRIPT #41: 00041 (1.0.0.5) Update Data SystemMessage.sql


-- START UNDO OF CHANGE SCRIPT #31: 00031 (1.0.0.4) Create Data SystemMessage.sql

delete SYSTEM_MESSAGE where MESSAGE_CODE = '0076';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0077';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0078';
--//


DELETE FROM CHANGELOG WHERE change_number = 31;

COMMIT;

-- END UNDO OF CHANGE SCRIPT #31: 00031 (1.0.0.4) Create Data SystemMessage.sql


-- START UNDO OF CHANGE SCRIPT #11: 00011 [1.0.0.2] Create Data SystemMessage.sql

delete SYSTEM_MESSAGE where MESSAGE_CODE = '0057';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0058';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0059';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0060';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0061';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0062';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0063';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0064';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0065';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0066';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0067';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0068';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0069';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0070';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0071';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0072';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0073';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0074';

delete SYSTEM_MESSAGE where MESSAGE_CODE = '0075';


--//


DELETE FROM CHANGELOG WHERE change_number = 11;

COMMIT;

-- END UNDO OF CHANGE SCRIPT #11: 00011 [1.0.0.2] Create Data SystemMessage.sql


-- START UNDO OF CHANGE SCRIPT #5: 00005 [0.0.0.1] Create All Sequences.sql



DELETE FROM CHANGELOG WHERE change_number = 5;

COMMIT;

-- END UNDO OF CHANGE SCRIPT #5: 00005 [0.0.0.1] Create All Sequences.sql


-- START UNDO OF CHANGE SCRIPT #4: 00004 [0.0.0.1] Create Data SeqTable.sql



DELETE FROM CHANGELOG WHERE change_number = 4;

COMMIT;

-- END UNDO OF CHANGE SCRIPT #4: 00004 [0.0.0.1] Create Data SeqTable.sql


-- START UNDO OF CHANGE SCRIPT #3: 00003 [0.0.0.1] Create Asp Tables.sql

drop index FK_ASP_DISP_RPT_01;
drop index FK_ASP_DISP_ITEM_01;
drop index FK_ASP_USER_HOSP_01;

drop table ASP_DISP_RPT;
drop table ASP_DISP_ITEM;
drop table ASP_USER_HOSP;
drop table ASP_USER;
--//


DELETE FROM CHANGELOG WHERE change_number = 3;

COMMIT;

-- END UNDO OF CHANGE SCRIPT #3: 00003 [0.0.0.1] Create Asp Tables.sql


-- START UNDO OF CHANGE SCRIPT #2: 00002 [0.0.0.1] Create Data SystemMessage.sql

delete SYSTEM_MESSAGE where MESSAGE_CODE = '0001';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0002';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0003';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0004';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0005';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0006';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0007';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0008';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0009';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0010';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0011';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0012';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0013';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0014';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0015';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0016';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0017';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0018';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0019';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0020';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0021';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0022';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0023';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0024';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0025';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0026';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0027';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0028';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0029';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0030';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0031';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0032';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0033';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0034';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0035';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0036';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0037';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0038';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0039';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0040';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0041';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0042';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0043';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0044';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0045';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0046';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0047';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0048';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0049';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0050';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0051';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0052';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0053';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0054';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0055';
delete SYSTEM_MESSAGE where MESSAGE_CODE = '0056';
--//


DELETE FROM CHANGELOG WHERE change_number = 2;

COMMIT;

-- END UNDO OF CHANGE SCRIPT #2: 00002 [0.0.0.1] Create Data SystemMessage.sql


-- START UNDO OF CHANGE SCRIPT #1: 00001 [0.0.0.1] Create Sys Tables.sql

drop index I_AUDIT_LOG_01;
drop index I_SYSTEM_LOG_01;

drop sequence SQ_AUDIT_LOG;
drop sequence SQ_SYSTEM_LOG;

drop table AUDIT_LOG;
drop table SYSTEM_LOG;
drop table SYSTEM_MESSAGE;
drop table SEQ_TABLE;
--//


DELETE FROM CHANGELOG WHERE change_number = 1;

COMMIT;

-- END UNDO OF CHANGE SCRIPT #1: 00001 [0.0.0.1] Create Sys Tables.sql

