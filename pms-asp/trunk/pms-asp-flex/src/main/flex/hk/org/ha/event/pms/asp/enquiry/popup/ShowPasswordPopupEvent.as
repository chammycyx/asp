package hk.org.ha.event.pms.asp.enquiry.popup
{
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowPasswordPopupEvent extends AbstractTideEvent 
	{	
		private var _callback:Function;
		
		public function ShowPasswordPopupEvent( callback:Function):void
		{
			super();
			_callback= callback;
		}
		public function get callback():Function {
			return _callback;
		}
	}
	
}