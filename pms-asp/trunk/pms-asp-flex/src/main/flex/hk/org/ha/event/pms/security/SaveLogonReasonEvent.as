package hk.org.ha.event.pms.security {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class SaveLogonReasonEvent extends AbstractTideEvent 
	{
		private var _reason:String;
		
		public function SaveLogonReasonEvent(reason:String):void 
		{
			super();
			_reason = reason;
		}        
		
		public function get reason():String 
		{
			return _reason;
		}
	}
}