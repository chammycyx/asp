package hk.org.ha.pms.asp.util
{
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.LinkBar;
	import mx.controls.LinkButton;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.PopUpManager;
	
	import hk.org.ha.fmk.pms.flex.components.message.ShowSystemMessagePopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	
	public class Toolbar extends LinkBar
	{	
		public var _retrieveFunc:Function = null;
		public var _addFunc:Function = null;
		public var _saveFunc:Function = null;
		public var _saveYesFunc:Function = null;
		public var _saveNoFunc:Function = null;
		public var _deleteFunc:Function = null;
		public var _deleteYesFunc:Function = null;
		public var _deleteNoFunc:Function = null;
		public var _clearFunc:Function = null;
		public var _printFunc:Function = null;
		public var _reprintFunc:Function = null;
		public var _exportFunc:Function = null;
		//enquiry 
		public var _refreshFunc:Function = null;
		public var _notforReviewFunc:Function = null;
		public var _resetFunc:Function = null;
		
							
		private var operationName:String;
		
		public var retrieveButton:LinkButton;
		public var addButton:LinkButton;
		public var saveButton:LinkButton;
		public var deleteButton:LinkButton;
		public var clearButton:LinkButton;
		public var printButton:LinkButton;
		public var reprintButton:LinkButton;
		public var exportButton:LinkButton;
		//enquiry 
		public var refreshButton:LinkButton;
		public var notForReviewButton:LinkButton;
		public var resetButton:LinkButton;
		
		
		private var btnList:ArrayCollection = new ArrayCollection();
		private var btnSeq:Array = new Array( "Add","Retrieve", "Clear", "Save", "Delete", "Print", "Reprint", "Export","Refresh","Not for Review","Reset");
		
		public function Toolbar() {             
			super();
		}
		
		public function hasInited():Boolean {
			if (this.numChildren == 0) {
				return false;
			} else {
				return true;
			}
		}
		
		public function init():void {
			for each(var btnName:String in btnSeq) {
				for each(var btn:LinkButton in btnList) {
					if (btnName == btn.label) {
						this.addChild(btn);
						break;
					}
				}
			}
		}
		
		public function set retrieveFunc(retrieveFunc:Function):void {
			if (retrieveButton == null) {
				this._retrieveFunc = retrieveFunc;
				retrieveButton = new LinkButton();
				retrieveButton.label = "Retrieve";
				retrieveButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(retrieveButton);
			}
		}
		
		public function set addFunc(addFunc:Function):void {
			if (addButton == null) {
				this._addFunc = addFunc;
				addButton = new LinkButton();
				addButton.label = "Add";
				addButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(addButton);
			}
		}
		
		public function set deleteFunc(deleteFunc:Function):void {
			if (deleteButton == null) {
				this._deleteFunc = deleteFunc;
				deleteButton = new LinkButton();
				deleteButton.label = "Delete";
				deleteButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(deleteButton);
			}
		}
		
		public function set saveFunc(saveFunc:Function):void {
			if (saveButton == null) {
				this._saveFunc = saveFunc;
				saveButton = new LinkButton();
				saveButton.label = "Save";
				saveButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(saveButton);
			}
		}
		
		public function set saveYesFunc(saveYesFunc:Function):void {
			if (saveButton == null) {
				this._saveYesFunc = saveYesFunc;
				saveButton = new LinkButton();
				saveButton.label = "Save";
				saveButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(saveButton);
			}
		}
		
		public function set deleteYesFunc(deleteYesFunc:Function):void {
			if (deleteButton == null) {
				this._deleteYesFunc = deleteYesFunc;
				deleteButton = new LinkButton();
				deleteButton.label = "Delete";
				deleteButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(deleteButton);
			}
		}
		
		public function set deleteNoFunc(deleteNoFunc:Function):void {
			if (deleteButton == null) {
				this._deleteNoFunc = deleteNoFunc;
				deleteButton = new LinkButton();
				deleteButton.label = "Delete";
				deleteButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(deleteButton);
			}
		}
		
		public function set clearFunc(clearFunc:Function):void {
			if (clearButton == null) {
				this._clearFunc = clearFunc;
				clearButton = new LinkButton();
				clearButton.label = "Clear";
				clearButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(clearButton);
			}
		}
		
		public function set printFunc(printFunc:Function):void {
			if (printButton == null) {
				this._printFunc = printFunc;
				printButton = new LinkButton();
				printButton.label = "Print";
				printButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(printButton);
			}
		}
		
		public function set reprintFunc(reprintFunc:Function):void {
			if (reprintButton == null) {
				this._reprintFunc = reprintFunc;
				reprintButton = new LinkButton();
				reprintButton.label = "Reprint";
				reprintButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(reprintButton);
			}
		}
		
		public function set exportFunc(exportFunc:Function):void {
			if (exportButton == null) {
				this._exportFunc = exportFunc;
				exportButton = new LinkButton();
				exportButton.label = "Export";
				exportButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(exportButton);
			}
		}
		
		public function set refreshFunc(refreshFunc:Function):void {
			if (refreshButton == null) {
				this._refreshFunc = refreshFunc;
				refreshButton = new LinkButton();
				refreshButton.label = "Refresh";
				refreshButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(refreshButton);
			}
		}
		
		public function set notForReviewFunc(notForReviewFunc:Function):void {
			if (notForReviewButton == null) {
				this._notforReviewFunc = notForReviewFunc;
				notForReviewButton = new LinkButton();
				notForReviewButton.label = "Not for Review";
				notForReviewButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(notForReviewButton);
			}
		}
		
		
		public function set resetFunc(resetFunc:Function):void {
			if (resetButton == null) {
				this._resetFunc = resetFunc;
				resetButton = new LinkButton();
				resetButton.label = "Reset";
				resetButton.addEventListener(MouseEvent.CLICK, itemClickHandler);
				btnList.addItem(resetButton);
			}
		}
		
				
		public function itemClickHandler(evt:MouseEvent):void {
			if (evt.currentTarget.label == "CAPD") {
				operationName = "_" + (evt.currentTarget.label).toLowerCase();
			} else {
				operationName = "_" + (evt.currentTarget.label).substring(0,1).toLowerCase() + (evt.currentTarget.label).substring(1);
				operationName = operationName.split(" ").join("");
			}
			if ((operationName == "_save" && _saveYesFunc != null) || operationName == "_delete") {
				confirmationMessagePopup(operationName);
			} else {
				this[operationName + "Func"]();
			}
		}
		
		private function confirmationMessagePopup(operationName:String):void {
			var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
			if (operationName == "_save") {
				msgProp.displayDesc = "<font size='16'><b>Are you sure you want to save?</b></font>";
				msgProp.messageTitle = "Save Confirmation";
			} else {
				msgProp.displayDesc = "<font size='16'><b>Are you sure you want to delete?</b></font>";
				msgProp.messageTitle = "Delete Confirmation";
			}
			msgProp.setYesNoButton = true;
			msgProp.setYesDefaultButton = false;
			msgProp.yesHandler = confirmHandler;
			msgProp.noHandler = cancelHandler;
			dispatchEvent(new ShowSystemMessagePopupEvent(msgProp));
		}
		
		private function confirmHandler(evt:MouseEvent):void {
			closePopupHandler(evt);
			if (operationName == "_save") {
				_saveYesFunc();
			} else {
				_deleteYesFunc();	
			}
		}
		
		private function cancelHandler(evt:MouseEvent):void {
			closePopupHandler(evt);
			if (operationName == "_save") {
				if (_saveNoFunc != null) {
					_saveNoFunc();
				}
			} else {
				if (_deleteNoFunc != null) {
					_deleteNoFunc();
				}
			}
		}
		
		private function closePopupHandler(evt:MouseEvent):void {
			var temp:UIComponent = evt.currentTarget as UIComponent; 
			PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);
		}
	}
}