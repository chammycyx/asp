package hk.org.ha.control.pms.asp.maint
{	
	import hk.org.ha.event.pms.asp.maint.RefreshUpdateAspHospListEvent;
	import hk.org.ha.event.pms.asp.maint.UpdateAspHospEvent;
	import hk.org.ha.model.pms.asp.biz.maint.AspHospServiceBean;
	
	import org.granite.tide.events.TideResultEvent;

	[Bindable]
	[Name("aspHospServiceCtl", restrict="false")]
	public class AspHospServiceCtl
	{

		[In]
		public var aspHospService:AspHospServiceBean;
		
		private var maintName:String;
		
		[Observer]
		public function updateAspHosp(evt:UpdateAspHospEvent):void
		{
			maintName = evt.maintName;
			
			var screen:String = maintName =="itemMaint"? "Antibiotics List Maintenance" : "Specialty Maintenance";
			
			aspHospService.updateAspHosp(evt.aspHosp,screen,UpdateAspHospEventResult);
		}
		
		public function UpdateAspHospEventResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshUpdateAspHospListEvent(maintName));
		}

	}
}