package hk.org.ha.event.pms.asp.operation {	
	
	import hk.org.ha.model.pms.asp.vo.operation.OperationRptCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveOperationRptListEvent extends AbstractTideEvent 
	{
		private var _operationRptCriteria:OperationRptCriteria;
										
		public function RetrieveOperationRptListEvent(operationRptCriteria:OperationRptCriteria):void 
		{
			super();
			_operationRptCriteria = operationRptCriteria;
		}
		
		public function get operationRptCriteria():OperationRptCriteria {
			return _operationRptCriteria;
		}
	}
}