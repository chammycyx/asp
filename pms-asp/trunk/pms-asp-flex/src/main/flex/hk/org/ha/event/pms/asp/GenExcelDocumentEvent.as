package hk.org.ha.event.pms.asp {
	
	import flash.events.Event;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class GenExcelDocumentEvent extends AbstractTideEvent 
	{						
		private var _rptKey:String;
		
		private var _rptPath:String;
		
		private var _rptFileName:String;
		
		private var _password:String;
		
		private var _passwordFlag:Boolean;
		
		private var _event:Event;
		
		public function GenExcelDocumentEvent(rptPath:String, rptKey:String, rptFileName:String, passwordFlag:Boolean=false,password:String=null, event:Event=null):void 
		{
			super();
			_rptKey = rptKey;
			_rptPath = rptPath;
			_rptFileName = rptFileName;
			_passwordFlag = passwordFlag;
			_password = password;
			_event = event;
		}

		public function get event():Event {
			return _event;
		}
		
		public function get rptKey():String {
			return _rptKey;
		}
		
		public function get rptPath():String {
			return _rptPath;
		}
		
		public function get rptFileName():String {
			return _rptFileName;
		}
		
		public function get password():String {
			return _password;
		}
		
		public function get passwordFlag():Boolean {
			return _passwordFlag;
		}
	}
}