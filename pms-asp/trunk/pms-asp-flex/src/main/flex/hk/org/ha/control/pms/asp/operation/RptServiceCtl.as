package hk.org.ha.control.pms.asp.operation
{		
	import mx.messaging.config.ServerConfig;
	
	import hk.org.ha.event.pms.asp.GenExcelDocumentEvent;
	import hk.org.ha.model.pms.asp.biz.operation.RptServiceBean;
	import hk.org.ha.model.pms.asp.persistence.ExtendedExternalInterface;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("RptServiceCtl", restrict="false")]
	public class RptServiceCtl
	{
		
		[In]
		public var rptService:RptServiceBean;
		
		[Observer]
		public function genExcelDocument(evt:GenExcelDocumentEvent):void
		{
			rptService.setRpt(evt.rptPath, evt.rptKey, evt.rptFileName,  evt.passwordFlag, evt.password, genExcelDocumentResult);
		}
		
		
		public function genExcelDocumentResult(evt:TideResultEvent):void
		{
			var url:String = "genOperationRpt.seam";
			
			var winName:String ="excel_win";
			
			if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
				url = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + url;
			}							
			ExtendedExternalInterface.call("window.open", url, winName, "" );  
		}	
		
		
//	
		
		
		
	}	
}
