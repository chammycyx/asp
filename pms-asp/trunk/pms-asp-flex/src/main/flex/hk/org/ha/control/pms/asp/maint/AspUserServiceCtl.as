package hk.org.ha.control.pms.asp.maint
{
	import hk.org.ha.event.pms.asp.maint.DeleteAspUserEvent;
	import hk.org.ha.event.pms.asp.maint.RefreshDeleteAspUserEvent;
	import hk.org.ha.model.pms.asp.biz.maint.AspUserServiceBean;
	
	import org.granite.tide.events.TideResultEvent;

	[Bindable]
	[Name("aspUserServiceCtl", restrict="false")]
	public class AspUserServiceCtl
	{
		[In]
		public var aspUserService:AspUserServiceBean;
		
		[Observer]
		public function deleteAspUser(evt:DeleteAspUserEvent):void
		{
			aspUserService.deleteAspUser(evt.aspUser, deleteAspUserResult);
		}
		
		public function deleteAspUserResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshDeleteAspUserEvent());
		}
		
	}
}