package hk.org.ha.event.pms.asp.enquiry.show
{
	import org.granite.tide.events.AbstractTideEvent;
		
	public class ShowEnqViewEvent extends AbstractTideEvent
	{
		private var _refresh:Boolean;	
		
		public function ShowEnqViewEvent(refresh:Boolean=false):void 
		{
			super();
			_refresh = refresh;	
		}
			
		public function get refresh():Boolean {
			return _refresh;
		}
	}
}