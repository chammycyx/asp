/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.asp.udt {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.asp.udt.AdmissionSource")]
    public class AdmissionSource extends Enum {

        public static const Home:AdmissionSource = new AdmissionSource("Home", _, "H", "Home");
        public static const Acute:AdmissionSource = new AdmissionSource("Acute", _, "A", "OAH");
        public static const OtherAcute:AdmissionSource = new AdmissionSource("OtherAcute", _, "U", "Other acute HA hospital");
        public static const Rehab:AdmissionSource = new AdmissionSource("Rehab", _, "E", "Other rehab/extened care hospital");
        public static const TPH:AdmissionSource = new AdmissionSource("TPH", _, "T", "Transfer from private hospital");
        public static const Others:AdmissionSource = new AdmissionSource("Others", _, "O", "Others");

        function AdmissionSource(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || Home.name), restrictor, (dataValue || Home.dataValue), (displayValue || Home.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [Home, Acute, OtherAcute, Rehab, TPH, Others];
        }

        public static function valueOf(name:String):AdmissionSource {
            return AdmissionSource(Home.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):AdmissionSource {
            return AdmissionSource(Home.dataConstantOf(dataValue));
        }
    }
}