/*
  GRANITE DATA SERVICES
  Copyright (C) 2011 GRANITE DATA SERVICES S.A.S.

  This file is part of Granite Data Services.

  Granite Data Services is free software; you can redistribute it and/or modify
  it under the terms of the GNU Library General Public License as published by
  the Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  Granite Data Services is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License
  for more details.

  You should have received a copy of the GNU Library General Public License
  along with this library; if not, see <http://www.gnu.org/licenses/>.
*/

package org.granite.util {
    
    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;
    import flash.utils.Dictionary;
    import flash.utils.getQualifiedClassName;

    /**
	 * 	ActionScript implementation of an Enum class 
	 * 
     * 	@author Franck WOLFF
     */
    public class Enum implements IExternalizable {
        
        private var _name:String;
        
//>> change by tommy 20100709
//        function Enum(name:String, restrictor:*) {
//            _name = (restrictor is Restrictor ? name : constantOf(name).name);
//        }

		private var _dataValue:String;
		
		private var _displayValue:String;
		
		function Enum(name:String, restrictor:*, dataValue:String = null, displayValue:String = null) {
			_name = (restrictor is Restrictor ? name : constantOf(name).name);
			if (dataValue != null) {
				_dataValue = dataValue;
				_displayValue = displayValue;
			} else {
				if (! restrictor is Restrictor) {
					var enum:Enum = constantOf(name);
					_dataValue = enum.dataValue;
					_displayValue = enum.displayValue;
				}
			}
		}
		
		public function get dataValue():String {
			return _dataValue;
		}
		
		public function get displayValue():String {
			return _displayValue;
		}
		
//<< change by tommy 20100709
        
        public function get name():String {
            return _name;
        }

        protected function getConstants():Array {
            throw new Error("Should be overriden");
        }
        
        protected function constantOf(name:String):Enum {
            for each (var o:* in getConstants()) {
                var enum:Enum = Enum(o);
                if (enum.name == name)
                    return enum;
            }
            throw new ArgumentError("Invalid " + getQualifiedClassName(this) + " value: " + name);
        }

//>> change by tommy 20110527
		protected function dataConstantOf(dataValue:String):Enum {
			for each (var o:* in getConstants()) {
				var enum:Enum = Enum(o);
				if (enum.dataValue == dataValue)
					return enum;
			}
			throw new ArgumentError("Invalid " + getQualifiedClassName(this) + " data value: " + dataValue);
		}
//<< change by tommy 20110527

        public function readExternal(input:IDataInput):void {
//>> change by tommy 20100709
            //_name = constantOf(input.readObject() as String).name;
			var enum:Enum = constantOf(input.readObject() as String);
			_name = enum.name;
			_dataValue = enum.dataValue;
			_displayValue = enum.displayValue;
//<< change by tommy 20100709
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_name);
        }
		
		public static function checkForConstant(o:*):* {
			return (o is Enum ? (o as Enum).constantOf((o as Enum).name) : o);
		}
		
		public static function normalize(tmp:Enum):Enum {
			return (tmp == null ? null : tmp.constantOf(tmp.name));
		}
        
        public static function readEnum(input:IDataInput):Enum {
            var tmp:Enum = input.readObject() as Enum;
            return normalize(tmp);
        }
        
        public function toString():String {
            return name;
        }

        public function equals(other:Enum):Boolean {
        	return other === this || (
        		other != null &&
        		getQualifiedClassName(this) == getQualifiedClassName(other) &&
        		other.name == this.name
        	);
        }
        
        
        protected static function get _():Restrictor {
            return new Restrictor();
        }
    }
}
class Restrictor {}
