<?xml version="1.0" encoding="UTF-8"?>

<project name="dbdeploy" default="default">

	<property environment="env"/>
	<property name="dbdeploy.home" value="${env.DBDEPLOY_HOME}"/>

	<property file="jdbc.properties" />
	<property name="db.driver" value="${jdbc.driverClassName}" />
    <property name="db.url" value="${jdbc.url}" />
    <property name="db.env" value="${jdbc.env}" />
	<property name="db.user" value="${jdbc.username}"/>
	<property name="db.pass" value="${jdbc.password}"/>
	<property name="db.app.user" value="${jdbc.app.username}"/>
	<property name="db.app.pass" value="${jdbc.app.password}"/>
	<property name="db.sup.user" value="${jdbc.sup.username}"/>
	<property name="encoding" value="UTF-8"/>
	    
	<path id="db.classpath">
        <fileset dir="${dbdeploy.home}/jdbc">
            <include name="*.jar"/>
        </fileset>
    </path>

    <path id="dbdeploy.classpath">
        <!-- include the dbdeploy-ant jar -->
        <fileset dir="${dbdeploy.home}">
            <include name="dbdeploy-ant-*.jar"/>
        </fileset>

        <!-- the dbdeploy task also needs the database driver jar on the classpath -->
        <path refid="db.classpath" />

    </path>

	<!-- Define an ANT task -->
    <taskdef name="dbdeploy" classname="com.dbdeploy.AntTarget" classpathref="dbdeploy.classpath"/>

    <target name="default" depends="test-connect"/>

    <target name="create-changelog-table">
        <sql driver="${db.driver}" url="${db.url}" encoding="${encoding}"
             userid="${db.user}" password="${db.pass}" print="true" classpathref="db.classpath" onerror="continue">
        	CREATE TABLE CHANGELOG (
        	  CHANGE_NUMBER INTEGER NOT NULL,
        	  COMPLETE_DT TIMESTAMP NOT NULL,
        	  APPLIED_BY VARCHAR2(100) NOT NULL,
        	  DESCRIPTION VARCHAR2(500) NOT NULL,
        	  CONSTRAINT PK_CHANGELOG PRIMARY KEY (CHANGE_NUMBER)
        	);       	
        </sql>
    </target>

    <target name="update-database" description="generate and apply a sql upgrade script">
        <antcall target="generate-database"/>
        <antcall target="apply-database"/>
    </target>

    <target name="generate-database" description="generate a sql upgrade script">
        <mkdir dir="./dbdeploy"/>    	
        <!-- use dbdeploy to generate the change script -->
        <dbdeploy driver="${db.driver}" url="${db.url}" encoding="${encoding}"
                  userid="${db.user}" password="${db.pass}"		  		  
                  dir="./deltas"
                  outputfile="./dbdeploy/all-deltas.sql"
                  undoOutputfile="./dbdeploy/undo-all-deltas.sql"
                  dbms="ora"
                  changelogTableName="CHANGELOG"/>
    </target>
	
    <target name="apply-database" description="apply a sql upgrade script">
        <!-- now apply the changescript to the database -->
        <sql driver="${db.driver}" url="${db.url}" encoding="${encoding}"
             userid="${db.user}" password="${db.pass}" onerror="abort" classpathref="db.classpath">
            <fileset file="./dbdeploy/all-deltas.sql"/>
        </sql>
    </target>

    <target name="undo-database">
		<!-- undo the last dbdeploy changes -->
        <sql driver="${db.driver}" url="${db.url}" encoding="${encoding}"
             userid="${db.user}" password="${db.pass}"
             src="./dbdeploy/undo-all-deltas.sql"
             onerror="abort" classpathref="db.classpath">
        </sql>
    </target>
	
	<target name="select-changelog">
        <sql driver="${db.driver}" url="${db.url}" encoding="${encoding}"
             userid="${db.user}" password="${db.pass}" print="true" classpathref="db.classpath" onerror="continue">
            SELECT * FROM CHANGELOG ORDER BY CHANGE_NUMBER;
        </sql>
    </target>
	
	<target name="test-connect">
        <sql driver="${db.driver}" url="${db.url}" encoding="${encoding}"
             userid="${db.user}" password="${db.pass}" print="true" classpathref="db.classpath" onerror="continue">
            SELECT * FROM DUAL;
        </sql>
    </target>
	
	<target name="create-extra">
        <sql driver="${db.driver}" url="${db.url}" encoding="${encoding}"
             userid="${db.user}" password="${db.pass}" print="true" classpathref="db.classpath" onerror="abort" delimitertype="row">
		    <fileset dir="./extra" erroronmissingdir="false">
		    	<include name="*.sql"/>
		    </fileset>
		    <fileset dir="./extra/${db.env}" erroronmissingdir="false">
		    	<include name="*.sql"/>
		    </fileset>
        </sql>
    </target>
	
	<target name="update-privs-synonyms">
		<antcall target="update-privs-synonyms-app-user"/>
		<antcall target="update-privs-sup-user"/>
	</target>

	<target name="update-privs-synonyms-app-user">
		<condition property="user.not.avaliable">
			<equals arg1="${db.app.user}" arg2=""/>
		</condition>
		<antcall target="grant-privs">
			<param name="user" value="${db.app.user}"/> <param name="type" value="TABLE"/> <param name="privilege" value="SELECT"/> 
		</antcall>	
		<antcall target="grant-privs">
			<param name="user" value="${db.app.user}"/> <param name="type" value="TABLE"/> <param name="privilege" value="INSERT"/> 
		</antcall>	
		<antcall target="grant-privs">
			<param name="user" value="${db.app.user}"/> <param name="type" value="TABLE"/> <param name="privilege" value="UPDATE"/> 
		</antcall>	
		<antcall target="grant-privs">
			<param name="user" value="${db.app.user}"/> <param name="type" value="TABLE"/> <param name="privilege" value="DELETE"/> 
		</antcall>	
		<antcall target="grant-privs">
			<param name="user" value="${db.app.user}"/> <param name="type" value="SEQUENCE"/> <param name="privilege" value="SELECT"/> 
		</antcall>	
		<antcall target="grant-privs">
			<param name="user" value="${db.app.user}"/> <param name="type" value="VIEW"/> <param name="privilege" value="SELECT"/> 
		</antcall>	
		<antcall target="grant-privs">
			<param name="user" value="${db.app.user}"/> <param name="type" value="PROCEDURE"/> <param name="privilege" value="EXECUTE"/> 
		</antcall>	
		<antcall target="grant-privs">
			<param name="user" value="${db.app.user}"/> <param name="type" value="FUNCTION"/> <param name="privilege" value="EXECUTE"/> 
		</antcall>	
		<antcall target="create-synonyms">     
			<param name="user" value="${db.app.user}"/> <param name="pass" value="${db.app.pass}"/> 
		</antcall>
	</target>

	<target name="update-privs-sup-user">
		<condition property="user.not.avaliable">
			<equals arg1="${db.sup.user}" arg2=""/>
		</condition>
		<antcall target="grant-privs">
			<param name="user" value="${db.sup.user}"/> <param name="type" value="TABLE"/> <param name="privilege" value="SELECT"/> 
		</antcall>
		<antcall target="grant-privs">
			<param name="user" value="${db.sup.user}"/> <param name="type" value="VIEW"/> <param name="privilege" value="SELECT"/> 
		</antcall>
		<antcall target="grant-privs">
			<param name="user" value="${db.sup.user}"/> <param name="type" value="FUNCTION"/> <param name="privilege" value="EXECUTE"/> 
		</antcall>
	</target>
	
	<target name="grant-privs" unless="user.not.avaliable">
        <sql driver="${db.driver}" url="${db.url}" encoding="${encoding}"
             userid="${db.user}" password="${db.pass}" print="true" classpathref="db.classpath" onerror="abort" delimitertype="row">
			DECLARE 
        	  CURSOR RECS IS 
        	  SELECT OBJECT_NAME FROM USER_OBJECTS WHERE OBJECT_TYPE = '${type}'
        	    AND OBJECT_NAME NOT IN (SELECT TABLE_NAME FROM USER_TAB_PRIVS WHERE GRANTEE='${user}' AND PRIVILEGE = '${privilege}') 
        	    AND OBJECT_NAME NOT LIKE 'BIN$%';
			BEGIN 
        	  FOR REC IN RECS LOOP
    	        EXECUTE IMMEDIATE 'GRANT ${privilege} ON ' || REC.OBJECT_NAME || ' TO ' || '${user}';  
        	  END LOOP; 
        	END;
        </sql>
	</target>			

	<target name="create-synonyms" unless="user.not.avaliable">
        <sql driver="${db.driver}" url="${db.url}" encoding="${encoding}"
             userid="${user}" password="${pass}" print="true" classpathref="db.classpath" onerror="abort" delimitertype="row">
        	DECLARE
        	  CURSOR RECS IS 
        	  SELECT OBJECT_NAME FROM ALL_OBJECTS WHERE OBJECT_TYPE IN ('TABLE','SEQUENCE','VIEW','PROCEDURE','FUNCTION')
        	    AND OWNER = '${db.user}' AND OBJECT_NAME NOT IN (SELECT SYNONYM_NAME FROM USER_SYNONYMS)
        	    AND OBJECT_NAME NOT LIKE 'BIN$%';
        	BEGIN 
        	  FOR REC IN RECS LOOP 
        	    EXECUTE IMMEDIATE 'CREATE SYNONYM ' || REC.OBJECT_NAME || ' FOR ' || '${db.user}.' || REC.OBJECT_NAME;
        	  END LOOP; 
        	END;
        </sql>
	</target>
	
	<target name="update-all" depends="update-database, create-extra, update-privs-synonyms"/>
</project>
