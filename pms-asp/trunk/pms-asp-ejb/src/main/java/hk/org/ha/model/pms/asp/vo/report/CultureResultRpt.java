package hk.org.ha.model.pms.asp.vo.report;

import hk.org.ha.model.pms.asp.vo.enquiry.PatientDemography;

import java.util.List;
public class CultureResultRpt {
	
	private String aspType;
	
	private PatientDemography patientDemography;
	
	private List<CultureResultRptDtl> manualList;
	
	private List<CultureResultRptLabTest> labTestList;


	public List<CultureResultRptDtl> getManualList() {
		return manualList;
	}

	public void setManualList(List<CultureResultRptDtl> manualList) {
		this.manualList = manualList;
	}


	public PatientDemography getPatientDemography() {
		return patientDemography;
	}

	public void setPatientDemography(PatientDemography patientDemography) {
		this.patientDemography = patientDemography;
	}

	public List<CultureResultRptLabTest> getLabTestList() {
		return labTestList;
	}

	public void setLabTestList(List<CultureResultRptLabTest> labTestList) {
		this.labTestList = labTestList;
	}

	public String getAspType() {
		return aspType;
	}

	public void setAspType(String aspType) {
		this.aspType = aspType;
	}
	
	
	
	
}
