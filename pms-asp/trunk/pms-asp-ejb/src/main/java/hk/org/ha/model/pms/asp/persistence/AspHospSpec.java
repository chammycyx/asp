package hk.org.ha.model.pms.asp.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name="ASP_HOSP_SPEC")
@IdClass(AspHospSpecPK.class)
public class AspHospSpec extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="HOSP_CODE", nullable = false, length = 3)
	private String hospCode;
	
	@Id
	@Column(name="SPEC_CODE", nullable = false, length = 4)
	private String specCode;
	
	@Column(name="VISIBLE_FLAG", nullable = false, length = 1)
	private Boolean visibleFlag;

	@Id
	@Converter(name = "AspHospSpec.aspType", converterClass = AspType.Converter.class)
    @Convert("AspHospSpec.aspType")
	@Column(name="ASP_TYPE", nullable = false)
	private AspType aspType;
	
    @ManyToOne
    @JoinColumn(name = "HOSP_CODE",nullable = false, insertable = false, updatable = false)
    private AspHosp aspHosp;
    
	public AspHosp getAspHosp() {
		return aspHosp;
	}

	public void setAspHosp(AspHosp aspHosp) {
		this.aspHosp = aspHosp;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public Boolean isVisibleFlag() {
		return visibleFlag;
	}
	
	public Boolean getVisibleFlag() {
		return visibleFlag;
	}
	
	public void setVisibleFlag(Boolean visibleFlag) {
		this.visibleFlag = visibleFlag;
	}

	public AspType getAspType() {
		return aspType;
	}

	public void setAspType(AspType type) {
		this.aspType = type;
	}
	
}
