package hk.org.ha.model.pms.asp.vo.enquiry;

import hk.org.ha.model.pms.asp.udt.Prescribing;
import hk.org.ha.model.pms.asp.udt.PrescribingSpecialty;
import hk.org.ha.model.pms.asp.udt.YesNoBlankFlag;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class Remark {
	@XmlElement
	private String remark;
	
	@XmlElement
	private Prescribing prescribing;
	
	@XmlElement
	private PrescribingSpecialty specialty;
	
	@XmlElement
	private String specialtyOthersValue;
	
	@XmlElement
	private Integer moPager;
	
	@XmlElement
	private Integer smoPager;
	
	@XmlElement
	private YesNoBlankFlag tlcDnrFlag;
	
	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public PrescribingSpecialty getSpecialty() {
		return specialty;
	}

	public void setSpecialty(PrescribingSpecialty specialty) {
		this.specialty = specialty;
	}
	
	public Integer getMoPager() {
		return moPager;
	}

	public void setMoPager(Integer moPager) {
		this.moPager = moPager;
	}

	public Integer getSmoPager() {
		return smoPager;
	}

	public void setSmoPager(Integer smoPager) {
		this.smoPager = smoPager;
	}

	public YesNoBlankFlag getTlcDnrFlag() {
		return tlcDnrFlag;
	}

	public void setTlcDnrFlag(YesNoBlankFlag tlcDnrFlag) {
		this.tlcDnrFlag = tlcDnrFlag;
	}

	public String getSpecialtyOthersValue() {
		return specialtyOthersValue;
	}

	public void setSpecialtyOthersValue(String specialtyOthersValue) {
		this.specialtyOthersValue = specialtyOthersValue;
	}

	public Prescribing getPrescribing() {
		return prescribing;
	}

	public void setPrescribing(Prescribing prescribing) {
		this.prescribing = prescribing;
	}

	
}
