package hk.org.ha.model.pms.vo.security;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "menu")
public class SysMenuDtl {

	private List<SysMenuDtl> menu;
	
	@XmlElement(name="displayName", required = true)
    private String displayName;
	    
    @XmlElement(name="url", required = true)
    private String	url;
    
    @XmlElement(name="target", required = true)
    private String target;
    
    public List<SysMenuDtl> getMenu() {
        if (menu == null) {
            menu = new ArrayList<SysMenuDtl>();
        }
        return this.menu;
    }
    
    public void setMenu(List<SysMenuDtl> menu) {
        this.menu = menu;
    }
    
	public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
        
    public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getTarget() {
		return target;
	} 
}
