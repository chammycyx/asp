package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum Indication implements StringValuedEnum {
	
	TIEN01HA("TIEN01HA", "Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure"),
	TIEN01Fever("TIEN01Fever", "Empirical therapy of neutropenic fever in high risk patients"),
	TIEN01ESBL("TIEN01ESBL", "Treatment of documented infections attributed to ESBL-producing bacteria"),
	TIEN01Path("TIEN01Path", "Treatment of documented infections due to pathogens that are resistant to other antibiotics"),
	TIEN01NECR("TIEN01NECR", "Treatment of necrotizing pancreatitis"),

	MERO01HA("MERO01HA", "Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure"),
	MERO01Fever("MERO01Fever", "Empirical therapy of neutropenic fever in high risk patients"),
	MERO01ESBL("MERO01ESBL", "Treatment of documented infections attributed to ESBL-producing bacteria"),
	MERO01Path("MERO01Path", "Treatment of documented infections due to pathogens that are resistant to other antibiotics"),
	MERO01Gram("MERO01Gram", "Treatment of meningitis due to gram-negative organism"),

	MERO02HA("MERO02HA", "Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure"),
	MERO02Fever("MERO02Fever", "Empirical therapy of neutropenic fever in high risk patients"),
	MERO02ESBL("MERO02ESBL", "Treatment of documented infections attributed to ESBL-producing bacteria"),
	MERO02Path("MERO02Path", "Treatment of documented infections due to pathogens that are resistant to other antibiotics"),
	MERO02Gram("MERO02Gram", "Treatment of meningitis due to gram-negative organism"),
	
	CEFT01Fever("CEFT01Fever", "Empirical therapy of neutropenic fever"),
	CEFT01Aeru("CEFT01Aeru", "Treatment of documented Pseudomonas aeruginosa infection sensitive to ceftazidim"),
	CEFT01Empi("CEFT01Empi", "Empirical treatment of peritonitis [Intraperitoneal Ceftazidime]"),
	CEFT01Burk("CEFT01Burk", "Treatment of infection by Burkholderia Pseudomallei [Ceftazidime only]"),
	
	CEFT02Fever("CEFT02Fever", "Empirical therapy of neutropenic fever"),
	CEFT02Aeru("CEFT02Aeru", "Treatment of documented Pseudomonas aeruginosa infection sensitive to ceftazidim"),
	CEFT02Empi("CEFT02Empi", "Empirical treatment of peritonitis [Intraperitoneal Ceftazidime]"),
	CEFT02Burk("CEFT02Burk", "Treatment of infection by Burkholderia Pseudomallei [Ceftazidime only]"),
	
	PIPE08HA("PIPE08Fever", "Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure"),
	PIPE08Fever("PIPE08Fever", "Empirical therapy of neutropenic fever"),
	PIPE08Gram("PIPE08Gram", "Treatment of documented gram-negative infections attributed to organisms that are resistant to 1st line antimicrobial agents (eg Unasyn, Augmentin, Cefuroxime)"),
	
	SULP31HA("SULP31HA", "Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure"),
	SULP31Fever("SULP31Fever", "Empirical therapy of neutropenic fever"),
	SULP31Gram("SULP31Gram", "Treatment of documented gram-negative infections attributed to organisms that are resistant to 1st line antimicrobial agents (eg Unasyn, Augmentin, Cefuroxime)"),
	SULP31Acin("SULP31Acin", "Treatment of infection by Acinetobacter baumannii"),
	
	CEFE03HA("CEFE03HA", "Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure"),
	CEFE03Fever("CEFE03Fever", "Empirical therapy of neutropenic fever"),
	CEFE03Gram("CEFE03Gram", "Treatment of documented gram-negative infections attributed to organisms that are resistant to 1st line antimicrobial agents (eg Unasyn, Augmentin, Cefuroxime)"),
	
	CEFE04HA("CEFE04HA", "Empirical therapy of hospital acquired infections with history of broad spectrum antibiotics exposure"),
	CEFE04Fever("CEFE04Fever", "Empirical therapy of neutropenic fever"),
	CEFE04Gram("CEFE04Gram", "Treatment of documented gram-negative infections attributed to organisms that are resistant to 1st line antimicrobial agents (eg Unasyn, Augmentin, Cefuroxime)"),
	
	VANC02MRSA("VANC02MRSA", "Treatment for serious infections caused by beta iV lactam resistant gram positive bacteria (e.g. MRSA, MRSE)"),
	VANC02Gram("VANC02Gram", "Treatment for infections due to gram-positive organisms in patient with SERIOUS beta-lactam allergy"),
	VANC02Card("VANC02Card", "Prophylaxis for endocarditis in high risk cardiac patient with beta-lactam allergy"),
	VANC02Coli("VANC02Coli", "For treatment of antibiotic-associated colitis in patient that has failed/is intolerant to metronidazole therapy"),
	VANC02Prop("VANC02Prop", "Prophylaxis in major surgical procedures involving the implantation of prosthetic devices in known carriers of MRSA"),
	
	VANC03MRSA("VANC02MRSA", "Treatment for serious infections caused by beta iV lactam resistant gram positive bacteria (e.g. MRSA, MRSE)"),
	VANC03Gram("VANC02Gram", "Treatment for infections due to gram-positive organisms in patient with SERIOUS beta-lactam allergy"),
	VANC03Card("VANC02Card", "Prophylaxis for endocarditis in high risk cardiac patient with beta-lactam allergy"),
	VANC03Coli("VANC02Coli", "For treatment of antibiotic-associated colitis in patient that has failed/is intolerant to metronidazole therapy"),
	VANC03Prop("VANC02Prop", "Prophylaxis in major surgical procedures involving the implantation of prosthetic devices in known carriers of MRSA"),
	
	TEIC01MRSA("TEIC01MRSA", "Treatment for serious infections caused by beta iV lactam resistant gram positive bacteria (e.g. MRSA, MRSE)"),
	TEIC01Gram("TEIC01Gram", "Treatment for infections due to gram-positive organisms in patient with SERIOUS beta-lactam allergy"),
	TEIC01Card("TEIC01Card", "Prophylaxis for endocarditis in high risk cardiac patient with beta-lactam allergy"),
	TEIC01Prop("TEIC01Prop", "Prophylaxis in major surgical procedures involving the implantation of prosthetic devices in known carriers of MRSA"),
	
	TEIC02MRSA("TEIC02MRSA", "Treatment for serious infections caused by beta iV lactam resistant gram positive bacteria (e.g. MRSA, MRSE)"),
	TEIC02Gram("TEIC02Gram", "Treatment for infections due to gram-positive organisms in patient with SERIOUS beta-lactam allergy"),
	TEIC02Card("TEIC02Card", "Prophylaxis for endocarditis in high risk cardiac patient with beta-lactam allergy"),
	TEIC02Prop("TEIC02Prop", "Prophylaxis in major surgical procedures involving the implantation of prosthetic devices in known carriers of MRSA"),
	
	LINE13Others("LINE13Others", "Others"),
	
	Others("Others", "Others");
	
	private final String dataValue;
	
	private final String displayValue;
	
	Indication(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static Indication dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(Indication.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<Indication> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<Indication> getEnumClass() {
    		return Indication.class;
    	}
    }
}
