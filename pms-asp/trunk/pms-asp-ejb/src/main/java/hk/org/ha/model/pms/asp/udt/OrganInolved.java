package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OrganInolved  implements StringValuedEnum{
	Lung("L", "Lung"),
	IntraAbdominal("I", "Intra-abdominal"),
	Urinary("U", "Urinary"),
	IVCatheter("V", "IV Catheter-related"),
	Bacteremia("B", "Bacteremia"),
	PD("P", "PD-related"),
	Soft("S", "Soft tissue"),
	CNS("C", "CNS"),
	Others("Others", "Others");
	
	private final String dataValue;
	
	private final String displayValue;
	
	OrganInolved(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static OrganInolved dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OrganInolved.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<OrganInolved> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<OrganInolved> getEnumClass() {
    		return OrganInolved.class;
    	}
    }
}
