package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum AdmissionSource implements StringValuedEnum {
	Home("H", "Home"),
	Acute("A", "OAH"),
	OtherAcute("U", "Other acute HA hospital"),
	Rehab("E", "Other rehab/extened care hospital"),
	TPH("T", "Transfer from private hospital"),
	Others("O", "Others");
	
	private final String dataValue;
	
	private final String displayValue;
	
	AdmissionSource(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static AdmissionSource dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AdmissionSource.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<AdmissionSource> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<AdmissionSource> getEnumClass() {
    		return AdmissionSource.class;
    	}
    }
}
