package hk.org.ha.model.pms.test;

import javax.ejb.Local;

@Local
public interface TestJpqlLocal {

	String getJpql();
	void setJpql(String jpql);
	
	String getJoinHints() ;
	void setJoinHints(String joinHints);
	
	String getResult();
	void setResult(String result);

	void executeJpql();
	
	boolean getHasResult();
	
	void destroy();
	
}
