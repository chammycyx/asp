package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum OutcomeMeasurementType implements StringValuedEnum {
	NotDone("N", "Not Done"),
	Undetermined("U", "Undetermined"),
	Pending("P", "Pending for Audit"),
	Appropriate("A", "Appropriate Prescription"),
	Inappropriate("I", "Inappropriate Prescription");
	
	
	private final String dataValue;
	
	private final String displayValue;
	
	OutcomeMeasurementType(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static OutcomeMeasurementType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(OutcomeMeasurementType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<OutcomeMeasurementType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<OutcomeMeasurementType> getEnumClass() {
    		return OutcomeMeasurementType.class;
    	}
    }
}
