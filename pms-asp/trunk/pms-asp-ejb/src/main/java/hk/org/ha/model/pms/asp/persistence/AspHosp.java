package hk.org.ha.model.pms.asp.persistence;

import hk.org.ha.fmk.pms.util.StringCollectionConverter;
import hk.org.ha.model.pms.persistence.VersionEntity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name="ASP_HOSP")
public class AspHosp extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="HOSP_CODE", nullable=false, length = 3)
	private String hospCode;
	
	@Column(name="G6PD_CODE", length = 8)
	private String g6pdCode;

	@PrivateOwned
	@OneToMany(mappedBy="aspHosp", cascade=CascadeType.ALL)
	private List<AspHospItem> aspHospItemList;

	@PrivateOwned
	@OneToMany(mappedBy="aspHosp", cascade=CascadeType.ALL)
	private List<AspHospSpec> aspHospSpecList;
	
	@Converter(name = "AspHosp.excludeWorkstores", converterClass = StringCollectionConverter.class)
	@Convert("AspHosp.excludeWorkstores")
	@Column(name="EXCLUDE_WORKSTORES")
	private List<String> excludeWorkstores;
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public List<AspHospItem> getAspHospItemList() {
		if (aspHospItemList == null) {
			aspHospItemList = new ArrayList<AspHospItem>();
		}
		return aspHospItemList;
	}

	public void setAspHospItemList(List<AspHospItem> aspHospItemList) {
		this.aspHospItemList = aspHospItemList;
	}

	public List<AspHospSpec> getAspHospSpecList() {
		if (aspHospSpecList == null) {
			aspHospSpecList = new ArrayList<AspHospSpec>();
		}
		return aspHospSpecList;
	}

	public void setAspHospSpecList(List<AspHospSpec> aspHospSpecList) {
		this.aspHospSpecList = aspHospSpecList;
	}
	
	public String getG6pdCode() {
		return g6pdCode;
	}

	public void setG6pdCode(String g6pdCode) {
		this.g6pdCode = g6pdCode;
	}

	public List<String> getExcludeWorkstores() {
		return excludeWorkstores;
	}

	public void setExcludeWorkstores(List<String> excludeWorkstores) {
		this.excludeWorkstores = excludeWorkstores;
	}
}
