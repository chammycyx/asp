package hk.org.ha.model.pms.asp.biz;

import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.cache.CacheResultCleanUp;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.persistence.AspItem;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpAspServiceJmsRemote;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
@AutoCreate
@Stateless
@Name("aspItemManager")
@MeasureCalls
public class AspItemManagerBean implements AspItemManagerLocal {

	@In
	CorpAspServiceJmsRemote corpAspServiceProxy;
	
	@Override
	@CacheResult(name="aspItemMap")
	public Map<String, AspItem> retrieveAspItemMap() {
		return corpAspServiceProxy.retrieveAspItemMap();
	}

	@Override
	@CacheResultCleanUp(name="aspItemMap")
	public void cleareAspItemMap(){
		
	}
	
	@Override
	public List<String> retrieveBGIndicationByItemCode(String itemCode) {
		Map<String, AspItem> aspItemMap = retrieveAspItemMap();
		
		AspItem aspItem = aspItemMap.get(itemCode);
		if(aspItem != null)
		{
			return aspItem.getIndications();
		}
		else
		{
			return new ArrayList<String>();
		}
	}

}
