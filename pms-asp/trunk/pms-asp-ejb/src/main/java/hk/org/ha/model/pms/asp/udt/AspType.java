package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum AspType implements StringValuedEnum {
	IV("IV", "IV Antibotics"),
	BG("BG", "Big Gun Antibotics"),
	NONE("-","");
	
	private final String dataValue;
	
	private final String displayValue;
	
	AspType(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static AspType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AspType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<AspType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<AspType> getEnumClass() {
    		return AspType.class;
    	}
    }
}
