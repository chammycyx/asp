package hk.org.ha.model.pms.biz.security;
import java.io.IOException;

import javax.ejb.Local;

@Local
public interface PostLogonServiceLocal {

	void setEnablePreLoadCacher(boolean enablePreLoadCacher);
	
	void postLogon(String debugHospCode, String debugWorkstoreCode) throws IOException;
	
	void destroy();
}
