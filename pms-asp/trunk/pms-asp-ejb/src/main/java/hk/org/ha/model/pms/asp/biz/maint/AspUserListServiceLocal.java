package hk.org.ha.model.pms.asp.biz.maint;

import hk.org.ha.model.pms.asp.persistence.AspUser;

import java.util.List;

import javax.ejb.Local;

@Local
public interface AspUserListServiceLocal {

	List<AspUser> retrieveAspUserList();
	
	void updateAspUserList(List<AspUser> aspUserList);

	void destroy();
}
