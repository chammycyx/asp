package hk.org.ha.model.pms.asp.vo.enquiry;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class Medication {

	@XmlElement(name="concurrent")
	private List<String> concurrentList;

	@XmlElement(name="previous")
	private List<String> previousList;

	@XmlElement(name="Checklist")
	private List<Checklist> checklistList;

	@XmlElement(name="wbcValue")
	private List<String> wbcValueList;


	public List<String> getConcurrentList() {
		return concurrentList;
	}
	public void setConcurrentList(List<String> concurrentList) {
		this.concurrentList = concurrentList;
	}
	public List<String> getPreviousList() {
		return previousList;
	}
	public void setPreviousList(List<String> previousList) {
		this.previousList = previousList;
	}
	public List<Checklist> getChecklistList() {
		return checklistList;
	}
	public void setChecklistList(List<Checklist> checklistList) {
		this.checklistList = checklistList;
	}
	public List<String> getWbcValueList() {
		return wbcValueList;
	}
	public void setWbcValueList(List<String> wbcValueList) {
		this.wbcValueList = wbcValueList;
	}

	public String getWbcValue1() {

		if(wbcValueList != null && !wbcValueList.isEmpty() ){
			return wbcValueList.get(0);
		}
		return "";
	}

	public String getWbcValue2() {

		if(wbcValueList != null && wbcValueList.size() >1 ){
			return wbcValueList.get(1);

		}
		return "";
	}
	public String getWbcValue3() {

		if(wbcValueList != null && wbcValueList.size() >2 ){
			return wbcValueList.get(2);

		}
		return "";
	}
	public String getWbcValue4() {

		if(wbcValueList != null && wbcValueList.size() >3 ){
			return wbcValueList.get(3);

		}
		return "";
	}
	public String getWbcValue5() {

		if(wbcValueList != null && wbcValueList.size() >4 ){
			return wbcValueList.get(4);

		}
		return "";
	}

	public Checklist getDay2Checklist(){
		if(checklistList != null){
			for(Checklist c : checklistList){
				if("day2".equals(c.getName())){
					return c;
				}

			}

		}
		return new Checklist();
	}

	public Checklist getDay3Checklist(){
		if(checklistList != null){
			for(Checklist c : checklistList){
				if("day3".equals(c.getName())){
					return c;
				}

			}

		}
		return new Checklist();
	}
	public Checklist getDay4Checklist(){
		if(checklistList != null){
			for(Checklist c : checklistList){
				if("day4".equals(c.getName())){
					return c;
				}

			}

		}
		return new Checklist();
	}
	
	public String getConcurrMedi1(){
		if(concurrentList != null && !concurrentList.isEmpty()){
			return concurrentList.get(0);
		}
		return "";
	}
	public String getConcurrMedi2(){
		if(concurrentList != null && concurrentList.size() > 1){
			return concurrentList.get(1);
		}
		return "";
	}
	public String getConcurrMedi3(){
		if(concurrentList != null && concurrentList.size() > 2){
			return concurrentList.get(2);
		}
		return "";
	}
	
	public String getPreviousMedi1(){
		if(previousList != null && !previousList.isEmpty()){
			return previousList.get(0);
		}
		return "";
	}
	public String getPreviousMedi2(){
		if(previousList != null && previousList.size() > 1){
			return previousList.get(1);
		}
		return "";
	}
	public String getPreviousMedi3(){
		if(previousList != null && previousList.size() > 2){
			return previousList.get(2);
		}
		return "";
	}
	
}
