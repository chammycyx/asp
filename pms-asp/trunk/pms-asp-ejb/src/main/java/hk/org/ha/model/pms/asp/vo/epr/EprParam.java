package hk.org.ha.model.pms.asp.vo.epr;

import hk.org.ha.model.pms.asp.udt.epr.EprParamType;

import java.io.Serializable;
import java.util.Date;

public class EprParam implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private EprParamType type;
	private String patKey;
	private Date startDate;
	private Date endDate;
	private String inClusterInd;
	private String inLatest;
	private String securityToken;
	
	public EprParamType getType() {
		return type;
	}

	public void setType(EprParamType type) {
		this.type = type;
	}
	
	public String getPatKey() {
		return patKey;
	}
	
	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public String getSecurityToken() {
		return securityToken;
	}
	
	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}

	public String getInClusterInd() {
		return inClusterInd;
	}

	public void setInClusterInd(String inClusterInd) {
		this.inClusterInd = inClusterInd;
	}

	public String getInLatest() {
		return inLatest;
	}

	public void setInLatest(String inLatest) {
		this.inLatest = inLatest;
	}
	
}
