package hk.org.ha.model.pms.asp.biz.alert;

import javax.ejb.Local;

import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.asp.vo.enquiry.AllergyHistory;
import hk.org.ha.model.pms.persistence.disp.Patient;

@Local
public interface AlertProfileManagerLocal {

	AllergyHistory retrieveAspRptAllergyData(Patient patient, String caseNum, String patHospCode, String g6pdCode) throws AlertProfileException;

}
