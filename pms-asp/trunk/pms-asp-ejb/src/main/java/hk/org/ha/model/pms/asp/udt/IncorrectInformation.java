package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum IncorrectInformation implements StringValuedEnum {
	Treatment("T", "Treatment"),
	Isolated("G", "Organism isolated"),
	Indication("I", "Indication"),
	Previous("P", "Previous Antibiotic Treatment"),
	Sensitivity("S", "Sensitivity"),
	Others("Others", "Others");
	
	private final String dataValue;
	
	private final String displayValue;
	
	IncorrectInformation(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    public String getDataValue() {
        return this.dataValue;
    }
    
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static IncorrectInformation dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(IncorrectInformation.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<IncorrectInformation> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<IncorrectInformation> getEnumClass() {
    		return IncorrectInformation.class;
    	}
    }
}
