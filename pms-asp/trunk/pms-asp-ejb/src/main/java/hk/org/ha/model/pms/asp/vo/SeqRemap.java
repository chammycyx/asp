package hk.org.ha.model.pms.asp.vo;

import java.io.Serializable;
import java.util.regex.Pattern;

public class SeqRemap implements Serializable {

	private static final long serialVersionUID = 8877248938510876364L;

	private String srcInstCode;
	
	private Pattern wardPattern;
	
	private String targetInstCode;

	public String getSrcInstCode() {
		return srcInstCode;
	}

	public void setSrcInstCode(String srcInstCode) {
		this.srcInstCode = srcInstCode;
	}

	public Pattern getWardPattern() {
		return wardPattern;
	}

	public void setWardPattern(Pattern wardPattern) {
		this.wardPattern = wardPattern;
	}

	public String getTargetInstCode() {
		return targetInstCode;
	}

	public void setTargetInstCode(String targetInstCode) {
		this.targetInstCode = targetInstCode;
	}
}
