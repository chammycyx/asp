package hk.org.ha.model.pms.asp.vo.enquiry;

import hk.org.ha.model.pms.asp.udt.AppropriatePrescription;
import hk.org.ha.model.pms.asp.udt.InappropriatePrescription;
import hk.org.ha.model.pms.asp.udt.IncorrectInformation;
import hk.org.ha.model.pms.asp.udt.YesNoBlankFlag;

import java.math.BigInteger;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class Miscellaneous {

	@XmlElement
	private YesNoBlankFlag accuracyInfoFlag;
	
	@XmlElement(name ="incorrectInformation")
	private List<IncorrectInformation> incorrectInfoList;
	
	@XmlElement
	private String incorrectInfoOtherValue;
	
	@XmlElement
	private YesNoBlankFlag approPresFlag;
	
	@XmlElement(name ="appropriatePrescription")
	private List<AppropriatePrescription> approPresList;
	
	@XmlElement
	private String approPresOtherValue;
	
	@XmlElement(name ="inappropriatePrescription")
	private List<InappropriatePrescription> inapproPresList;
	
	@XmlElement
	private String inapproPresOtherValue;
	
	@XmlElement
	private Integer durationIvDay;
	
	@XmlElement
	private BigInteger durationIvDose;
	
	@XmlElement
	private YesNoBlankFlag sameAntibioticFlag;
	
	@XmlElement
	private String otherAntibioticName;
	
	@XmlElement
	private Integer lengthOfStay;
	


	
	public String getIncorrectInfoOtherValue() {
		return incorrectInfoOtherValue;
	}
	public void setIncorrectInfoOtherValue(String incorrectInfoOtherValue) {
		this.incorrectInfoOtherValue = incorrectInfoOtherValue;
	}
	public String getApproPresOtherValue() {
		return approPresOtherValue;
	}
	public void setApproPresOtherValue(String approPresOtherValue) {
		this.approPresOtherValue = approPresOtherValue;
	}
	public Integer getDurationIvDay() {
		return durationIvDay;
	}
	public void setDurationIvDay(Integer durationIvDay) {
		this.durationIvDay = durationIvDay;
	}
	public BigInteger getDurationIvDose() {
		return durationIvDose;
	}
	public void setDurationIvDose(BigInteger durationIvDose) {
		this.durationIvDose = durationIvDose;
	}
	public String getOtherAntibioticName() {
		return otherAntibioticName;
	}
	public void setOtherAntibioticName(String otherAntibioticName) {
		this.otherAntibioticName = otherAntibioticName;
	}
	public Integer getLengthOfStay() {
		return lengthOfStay;
	}
	public void setLengthOfStay(Integer lengthOfStay) {
		this.lengthOfStay = lengthOfStay;
	}
	public YesNoBlankFlag getAccuracyInfoFlag() {
		return accuracyInfoFlag;
	}
	public void setAccuracyInfoFlag(YesNoBlankFlag accuracyInfoFlag) {
		this.accuracyInfoFlag = accuracyInfoFlag;
	}
	public YesNoBlankFlag getSameAntibioticFlag() {
		return sameAntibioticFlag;
	}
	public void setSameAntibioticFlag(YesNoBlankFlag sameAntibioticFlag) {
		this.sameAntibioticFlag = sameAntibioticFlag;
	}
	public String getInapproPresOtherValue() {
		return inapproPresOtherValue;
	}
	public void setInapproPresOtherValue(String inapproPresOtherValue) {
		this.inapproPresOtherValue = inapproPresOtherValue;
	}
	public List<AppropriatePrescription> getApproPresList() {
		return approPresList;
	}
	public void setApproPresList(List<AppropriatePrescription> approPresList) {
		this.approPresList = approPresList;
	}
	public List<InappropriatePrescription> getInapproPresList() {
		return inapproPresList;
	}
	public void setInapproPresList(List<InappropriatePrescription> inapproPresList) {
		this.inapproPresList = inapproPresList;
	}
	public List<IncorrectInformation> getIncorrectInfoList() {
		return incorrectInfoList;
	}
	public void setIncorrectInfoList(List<IncorrectInformation> incorrectInfoList) {
		this.incorrectInfoList = incorrectInfoList;
	}
	
	
	
	
	public YesNoBlankFlag getApproPresFlag() {
		return approPresFlag;
	}
	public void setApproPresFlag(YesNoBlankFlag approPresFlag) {
		this.approPresFlag = approPresFlag;
	}
	
	
	public String getIncorrectInfo() {
		StringBuilder sb = new StringBuilder();
		if(incorrectInfoList != null){
			for(IncorrectInformation o:incorrectInfoList){
				sb.append(o.getDataValue());
			}
		} else{
			return "";
		}
		return sb.toString();
	}
	
	public String getApproPres() {
		StringBuilder sb = new StringBuilder();
		if(approPresList != null){
			for(AppropriatePrescription o:approPresList){
				sb.append(o.getDataValue());
			}
		} else{
			return "";
		}
		
		return sb.toString();
	}
	
	public String getInapproPres() {
		StringBuilder sb = new StringBuilder();
		if(inapproPresList != null){
			for(InappropriatePrescription o:inapproPresList){
				sb.append(o.getDataValue());
			}
		} else{
			return "";
		}
		
		return sb.toString();
	}
	
	
	
	
	
}
