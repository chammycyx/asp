 package hk.org.ha.model.pms.asp.biz.enquiry;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.persistence.AspDispRpt;
import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.model.pms.asp.vo.enquiry.AspRpt;
import hk.org.ha.model.pms.asp.vo.enquiry.AspRptCriteria;
import hk.org.ha.model.pms.asp.vo.enquiry.CultureResult;
import hk.org.ha.model.pms.asp.vo.enquiry.Organism;
import hk.org.ha.model.pms.asp.vo.report.CultureResultRpt;
import hk.org.ha.model.pms.asp.vo.report.CultureResultRptDtl;
import hk.org.ha.model.pms.asp.vo.report.CultureResultRptLabTest;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.io.FileUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.document.ByteArrayDocumentData;
import org.jboss.seam.faces.Renderer;


@Stateful
@Scope(ScopeType.SESSION)
@Name("exportService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class ExportServiceBean implements ExportServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	private AspType rptType;
	
	private List<AspRpt> aspRptList = new  ArrayList<AspRpt>();
	
	private String password;
	
	private Boolean passwordFlag;
	
	@In(create=true)
	private Renderer renderer;
	@In
	private ReportProvider<JRDataSource> reportProvider;

	private JRBeanCollectionDataSource dataSource;

	@In(scope=ScopeType.SESSION, required=false)
	UamInfo uamInfo;

	@In
	private AuditLogger auditLogger;
	
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer("yyyy-MM-dd HH:mm:ss.SSS"), Date.class);
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	
	
	static final String BG_RPT = "report/BigGunRpt.jasper";
	static final String IV_RPT = "report/IvOralRpt.jasper";
	
	
	public void exportAspRpt(AspRptCriteria aspRptCriteriaIn){
		
		StringBuilder queryString = new StringBuilder();
		queryString.append("select o from AspDispRpt o "); 
		queryString.append(" where o.id in :idList ");	

		Query q = em.createQuery(queryString.toString())
		.setParameter("idList", aspRptCriteriaIn.getRptIdList() );
		
		@SuppressWarnings("unchecked")
		List<AspDispRpt> aspDispRptList = q.getResultList();
	
		if(!aspDispRptList.isEmpty()){
			
			aspRptList = new  ArrayList<AspRpt>();
			
			for(AspDispRpt adr : aspDispRptList){
				
				AspRpt a = adr.getAspRpt();
				a.setScopeOfRpt(aspRptCriteriaIn.getScopeOfRpt());
				List<CultureResultRpt> cList = new ArrayList<CultureResultRpt>();
				cList.add(createCultureResultRpt(a));
				a.setCultureResultRptList(cList);
				aspRptList.add(a);
				
				auditLogger.log("#0058",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),aspRptCriteriaIn.getScopeOfRpt(),serializer.deepSerialize(adr));
			}
			
			rptType = aspRptCriteriaIn.getReportType();
			password = aspRptCriteriaIn.getPassword();
			passwordFlag = aspRptCriteriaIn.getPasswordFlag();
			
		}
		
		
	}
	
	
	private CultureResultRpt createCultureResultRpt(AspRpt a) {
		CultureResultRpt c = new CultureResultRpt();
		c.setAspType(a.getAspType().getDataValue());
		c.setPatientDemography(a.getPatientDemography());
		
		List<CultureResultRptDtl> cdList = new ArrayList<CultureResultRptDtl>();
		if(a.getManualCultureResultList() != null){
			for(CultureResult o: a.getManualCultureResultList()){
				CultureResultRptDtl cd = new CultureResultRptDtl();
				cd.setSpecimen(o.getSpecimen());
				cd.setSpecimenDate(o.getSpecimenDate());
				cd.setRefDate(o.getRefDate());
				cd.setRptDate(o.getRptDate());
				cd.setLabNum(o.getLabNum());
				if(o.getOrganismList() != null){
					cd.setOrganism(o.getOrganismList().get(0).getOrganism());
					if(o.getOrganismList().get(0).getAntibioticList() != null){
						cd.setAntibiotics1(o.getOrganismList().get(0).getAntibioticList().get(0).getAntibiotic());
						cd.setSentivitive1(o.getOrganismList().get(0).getAntibioticList().get(0).getSensitive());
					}
				}
				if(!cd.isEmptyCultureResult()){
					cdList.add(cd);
				}
				
			}
			
		}
		
		c.setManualList(cdList);
		
		
		List<CultureResultRptLabTest> labTestList = new ArrayList<CultureResultRptLabTest>();
		if(a.getCultureResultList() != null){
			
			for(CultureResult o: a.getCultureResultList()){
				CultureResultRptLabTest labTest = new CultureResultRptLabTest();
				boolean headerFlag = true; 
				if(o.getOrganismList() != null){
					cdList =  new ArrayList<CultureResultRptDtl>(); 
					for(Organism organism : o.getOrganismList()){
						boolean organismHeaderFlag = true;
						if(organism.getAntibioticList() != null){
							for(int i = 0 ; i < organism.getAntibioticList().size(); i=i+2){
								CultureResultRptDtl cd = new CultureResultRptDtl();
								if(headerFlag){
									headerFlag = false;
									cd.setSpecimen(o.getSpecimen());
									cd.setSpecimenDate(o.getSpecimenDate());
									cd.setRefDate(o.getRefDate());
									cd.setRptDate(o.getRptDate());
									cd.setLabNum(o.getLabNum());
								}
								if(organismHeaderFlag){
									organismHeaderFlag = false;
									cd.setOrganism(organism.getOrganism());
								}
								cd.setAntibiotics1(organism.getAntibioticList().get(i).getAntibiotic());
								cd.setSentivitive1(organism.getAntibioticList().get(i).getSensitive());
								
								if(i+1 < organism.getAntibioticList().size() ){
									cd.setAntibiotics2(organism.getAntibioticList().get(i+1).getAntibiotic());
									cd.setSentivitive2(organism.getAntibioticList().get(i+1).getSensitive());
								}
								cdList.add(cd);
							}
						}else{
							CultureResultRptDtl cd = new CultureResultRptDtl();
							if(headerFlag){
								headerFlag = false;
								cd.setSpecimen(o.getSpecimen());
								cd.setSpecimenDate(o.getSpecimenDate());
								cd.setRefDate(o.getRefDate());
								cd.setRptDate(o.getRptDate());
								cd.setLabNum(o.getLabNum());
							}
							cd.setOrganism(organism.getOrganism());
							cdList.add(cd);
						}
					}
					
				}else{
					CultureResultRptDtl cd = new CultureResultRptDtl();
					if(headerFlag){
						headerFlag = false;
						cd.setSpecimen(o.getSpecimen());
						cd.setSpecimenDate(o.getSpecimenDate());
						cd.setRefDate(o.getRefDate());
						cd.setRptDate(o.getRptDate());
						cd.setLabNum(o.getLabNum());
					}
					cdList.add(cd);
				}
				labTest.setCultureResultRptDtlList(cdList);
				labTestList.add(labTest);
			}
		}
		
		
		c.setLabTestList(labTestList);
		return c;
	}
	
	public void exportRpt() throws IOException, ZipException{
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("SUBREPORT_DIR", "report/");
	
		dataSource = new JRBeanCollectionDataSource(aspRptList);
		
		String rptFile ="";
		
		if(AspType.BG.equals(rptType)){
			rptFile = BG_RPT;
		}else{
			rptFile = IV_RPT;
		}
		
		String contentId = reportProvider.generateReport(rptFile, parameters, dataSource); 
		if(passwordFlag){
			
			contentId = renderZipContentId(contentId, String.valueOf(System.currentTimeMillis()), password);
		}
		
		reportProvider.redirectReport(contentId);
	}
	
	public File generateZipFile(List<File> fileArrayList, String password, String folder) throws ZipException {
		
		File tempFile = createTempFile("", ".zip", folder);
		
		try {

			ZipFile zipFile = new ZipFile(tempFile);

			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

			parameters.setEncryptFiles(true);
			parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
			parameters.setPassword(password);

			zipFile.addFiles(new ArrayList<File>(fileArrayList), parameters);

			return tempFile;
			
		} catch (ZipException e) {
			
			tempFile.delete();
			throw e;
		}
	}
	
	private File createTempFile(String prefix, String suffix, String folder) {
		SecureRandom random = new SecureRandom();
		long n = random.nextLong();
		if (n == Long.MIN_VALUE) {
			n = 0;
		} else {
			n = Math.abs(n);
		}
		return new File(folder, prefix + Long.toString(n) + suffix);
	}
	
	
	public String renderZipContentId(String contentId, String filename, String password) throws IOException, ZipException {
        ByteArrayDocumentData dd = (ByteArrayDocumentData) reportProvider.getDocumentStore().getDocumentData(contentId);
        
        File tempDir = new File(System.getProperty("java.io.tmpdir") + System.currentTimeMillis());
        if (!tempDir.mkdirs()) {
        	throw new IOException("Cannot create directory : " + tempDir);
        }
        
        File f = new File(tempDir,filename + "." + dd.getDocumentType().getExtension());
        FileUtils.writeByteArrayToFile(f, dd.getData());

        ArrayList<File> fl = new ArrayList<File>();
        fl.add(f);
        File zipFile = generateZipFile(fl, password, tempDir.getAbsolutePath());
        
        String zipContentId = reportProvider.getDocumentStore().newId();
        
        ByteArrayDocumentData zipdd = new ByteArrayDocumentData(filename,ReportProvider.ZIP, FileUtils.readFileToByteArray(zipFile));        
        reportProvider.getDocumentStore().saveData(zipContentId, zipdd);
        
        if (!zipFile.delete()) {
           	throw new IOException("Cannot delete zip file : " + zipFile);
        }
        
        FileUtils.deleteDirectory(tempDir);
        
        return zipContentId;
	}
	
	
	
    @Remove
	public void destroy(){
    	aspRptList = null;
    	
	}

}
