package hk.org.ha.model.pms.biz.sys;

import javax.ejb.Local;

@Local
public interface SystemMessageServiceLocal {
	
	void retrieveSystemMessage(String messageCode, Object... params);
	
	void destroy();
}