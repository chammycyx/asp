package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PrescribingSpecialty implements StringValuedEnum {
	Resp("P", "Resp"),
	Hematology("H", "Hematology"),
	Renal("R", "Renal"),
	GI("I", "GI"),
	Endocrine("E", "Endocrine"),
	Geriatric("G", "Geriatric"),
	Cardiac("C", "Cardiac"),
	IDSMS("S", "ID/SMS"),
	Oncology("N", "Oncology"),
	Others("O", "Others");
	
	private final String dataValue;
	
	private final String displayValue;
	
	PrescribingSpecialty(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static PrescribingSpecialty dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PrescribingSpecialty.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PrescribingSpecialty> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PrescribingSpecialty> getEnumClass() {
    		return PrescribingSpecialty.class;
    	}
    }
}
