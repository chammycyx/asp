package hk.org.ha.model.pms.asp.biz.rpt;

import hk.org.ha.model.pms.asp.persistence.AspDispItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;

import java.util.List;

import javax.ejb.Local;

@Local
public interface AspDispRptManagerLocal {

	String generateNewAspDispRptList(List<AspDispItem> aspDispItemList, List<String> patHospCodeList, String g6pdCode) throws RuntimeException;

	String refreshAspDispRptList(List<Long> aspDispItemIdList, String g6pdCode) throws RuntimeException;
	
	void updateAspDispRptDischargeFlag(MedProfileOrder medProfileOrder);
}
