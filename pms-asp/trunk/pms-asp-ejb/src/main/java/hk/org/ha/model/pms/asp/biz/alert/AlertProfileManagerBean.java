package hk.org.ha.model.pms.asp.biz.alert;

import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.asp.udt.AlertProfileStatus;
import hk.org.ha.model.pms.asp.vo.enquiry.AllergyHistory;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.PatAdr;
import hk.org.ha.model.pms.vo.alert.PatAllergy;
import hk.org.ha.service.pms.asa.interfaces.alert.AlertServiceJmsRemote;

import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("alertProfileManager")
@MeasureCalls
public class AlertProfileManagerBean implements AlertProfileManagerLocal {
	
	private static final String DEFAULT_USER_ID = "PMS";
	
	private static final String DEFAULT_WORKSTATION = "server";
	
	@In(scope=ScopeType.SESSION, required=false)
	UamInfo uamInfo;
	
	@In
	private AlertServiceJmsRemote alertServiceProxy;	
	
	@Override
	public AllergyHistory retrieveAspRptAllergyData(Patient patient, String caseNum, String patHospCode, String g6pdCode) throws AlertProfileException
	{
		AllergyHistory allergyHistory = new AllergyHistory();
		allergyHistory.setAllergy("");
		
		AlertProfile alertProfile;
		String userId = "";
		String workstationId = "";
		String g6pdAlertDesc = "";
		if(uamInfo != null)
		{
			userId = uamInfo.getUserId();
			workstationId = uamInfo.getWorkstationId();
		}
		else
		{
			userId = DEFAULT_USER_ID;
			workstationId = DEFAULT_WORKSTATION;
		}

		alertProfile = alertServiceProxy.retrieveAlertProfile(patient, caseNum, patHospCode, userId, workstationId, g6pdCode);
		
		if(alertProfile == null)
		{
			return allergyHistory;
		}

		if(alertProfile.getStatus() != null)
		{
			allergyHistory.setStatus(AlertProfileStatus.dataValueOf(alertProfile.getStatus().getDataValue()));
		}
		
		if(AlertProfileStatus.Error.equals(allergyHistory.getStatus()))
		{
			allergyHistory.setErrMsg(alertProfile.getMessage());
		}
		else if(AlertProfileStatus.RecordExist.equals(allergyHistory.getStatus()))
		{
			String allergyDesc = getFormattedAllergyDesc(alertProfile.getPatAllergyList());
			String adrDesc = getFormattedAdrDesc(alertProfile.getPatAdrList());
			
			if(alertProfile.getG6pdAlert() != null)
			{
				g6pdAlertDesc = alertProfile.getG6pdAlert().getAlertDesc();			
			}
			
			StringBuilder allergyResult = new StringBuilder();
			
			if(!StringUtils.isBlank(allergyDesc))
			{
				allergyResult.append(allergyDesc);
			}
			
			if(!StringUtils.isBlank(adrDesc))
			{
				if(allergyResult.length() > 0)
				{
					allergyResult.append("; ");
				}
				
				allergyResult.append(adrDesc);
			}
			
			if(!StringUtils.isBlank(g6pdAlertDesc))
			{
				if(allergyResult.length() > 0)
				{
					allergyResult.append("; ");
				}
				
				allergyResult.append(g6pdAlertDesc);
			}
			
			allergyHistory.setAllergy(allergyResult.toString());
		}
		
		return allergyHistory;

	}
	
	private String getFormattedAllergyDesc(List<PatAllergy> patAllergyList)
	{
		StringBuilder sb = new StringBuilder();
		for(PatAllergy patAllergy:patAllergyList)
		{
			if(sb.length() > 0)
			{
				sb.append("; ");
			}
			
			String allergyText = patAllergy.getAllergenName();
			String manifestation = patAllergy.getClinicalManifestation();
			
			if(!StringUtils.isBlank(manifestation))
			{
				allergyText += "-" + manifestation;
			}
			sb.append(allergyText);
		}
		
		return sb.toString();
	}
	
	private String getFormattedAdrDesc(List<PatAdr> patAdrList)
	{
		StringBuilder sb = new StringBuilder();
		for(PatAdr patAdr:patAdrList)
		{
			if(sb.length() > 0)
			{
				sb.append("; ");
			}
			
			String adrText = patAdr.getDrugName();
			String reaction = patAdr.getReaction();
			
			if(!StringUtils.isBlank(reaction))
			{
				adrText += "-" + reaction;
			}
			sb.append(adrText);
		}
		
		return sb.toString();
	}
	
}
