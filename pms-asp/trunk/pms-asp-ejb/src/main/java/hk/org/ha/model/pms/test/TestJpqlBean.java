package hk.org.ha.model.pms.test;

import hk.org.ha.fmk.pms.remote.marshall.EclipseLinkXStreamMarshaller;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Stateful
@Scope(ScopeType.SESSION)
@Name("testJpql")
@MeasureCalls
public class TestJpqlBean implements TestJpqlLocal {

	@Logger
	private Log logger;

	private EclipseLinkXStreamMarshaller xmlMarshaller = new EclipseLinkXStreamMarshaller();

	@PersistenceContext
	private EntityManager em;
	
	private String jpql;
	
	private String joinHints;

	private String result;
	
	public String getJpql() {
		return jpql;
	}

	public void setJpql(String jpql) {
		this.jpql = jpql;
	}

	public String getJoinHints() {
		return joinHints;
	}

	public void setJoinHints(String joinHints) {
		this.joinHints = joinHints;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@SuppressWarnings("unchecked")
	public void executeJpql() {
		try {
			Query query = em.createQuery(jpql);
			
			String hintType = null;
			if (!StringUtils.isBlank(joinHints)) {
				for (String joinHint : joinHints.split("\\s+")) {
					if (joinHint.equalsIgnoreCase("BATCH")) {
						hintType = QueryHints.BATCH;
						continue;
					} else if (joinHint.equalsIgnoreCase("FETCH")) {
						hintType = QueryHints.FETCH;
						continue;
					}
					if (hintType != null) {
						query.setHint(hintType, joinHint);
						hintType = null;
					}
				}
			}

			List ret = query.getResultList();

			if (!StringUtils.isBlank(joinHints)) {
				for (Object object : ret) {
					for (String joinHint : joinHints.split("\\s+")) {
						Object o = object;
						int i = joinHint.indexOf(".");
						if (i > 0) {
							invoke(o, joinHint.substring(i+1));
						}
					}
				}
			}
								
			result = xmlMarshaller.toXML(ret);
			
		} catch (Exception e) {
			Writer writer = new StringWriter();
			e.printStackTrace(new PrintWriter(writer));
			result = writer.toString();
		}
	}
	
	private void invoke(Object o, String dotString) throws SecurityException, NoSuchMethodException {
		if (StringUtils.isNotBlank(dotString)) {
			int i = dotString.indexOf(".");
			if (i > 0) {
				invokeMethod(o, dotString.substring(0, i), dotString.substring(i+1));
			} else {
				invokeMethod(o, dotString, null);
			}
		}
	}	

	private void invokeMethod(Object o, String property, String reminder) {
		if (Collection.class.isAssignableFrom(o.getClass())) {
			Collection<?> col = (Collection<?>) o;
			for (Object _o : col) {
				_invokeMethod(_o, property, reminder);
			}
		} else {
			_invokeMethod(o, property, reminder);
		}
	}

	private void _invokeMethod(Object o, String property, String reminder) {
		try {
			Method m;
			try {
				m = o.getClass().getMethod("get" + WordUtils.capitalize(property));
			} catch (NoSuchMethodException e) {
				m = o.getClass().getMethod(property);
			}
			Object ret = m.invoke(o);
			if (ret != null) {
				if (Collection.class.isAssignableFrom(ret.getClass())) {
					((Collection<?>) ret).size();
				}
				invoke(ret, reminder);
			}
		} catch (Exception e) {
			logger.error("error", e);
		}
	}
	
	public boolean getHasResult() {
		return (!StringUtils.isBlank(result));
	}
	
	@Remove
	public void destroy() 
	{
	}	
}
