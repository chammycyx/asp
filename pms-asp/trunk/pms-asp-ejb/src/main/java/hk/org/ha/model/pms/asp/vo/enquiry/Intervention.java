package hk.org.ha.model.pms.asp.vo.enquiry;

import hk.org.ha.model.pms.asp.udt.YesNoBlankFlag;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class Intervention {

	@XmlElement
	private Date icfDate;
	
	@XmlElement
	private String antibioticName;
	
	@XmlElement
	private String dose;
	
	@XmlElement
	private String frequency;
	
	@XmlElement
	private YesNoBlankFlag possibleInteractionFlag;
	
	@XmlElement
	private String possibleInteractionValue;
	
	
	public Date getIcfDate() {
		return icfDate;
	}
	public void setIcfDate(Date icfDate) {
		this.icfDate = icfDate;
	}
	public String getAntibioticName() {
		return antibioticName;
	}
	public void setAntibioticName(String antibioticName) {
		this.antibioticName = antibioticName;
	}
	public String getDose() {
		return dose;
	}
	public void setDose(String dose) {
		this.dose = dose;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getPossibleInteractionValue() {
		return possibleInteractionValue;
	}
	public void setPossibleInteractionValue(String possibleInteractionValue) {
		this.possibleInteractionValue = possibleInteractionValue;
	}
	public YesNoBlankFlag getPossibleInteractionFlag() {
		return possibleInteractionFlag;
	}
	public void setPossibleInteractionFlag(YesNoBlankFlag possibleInteractionFlag) {
		this.possibleInteractionFlag = possibleInteractionFlag;
	}
	
}
