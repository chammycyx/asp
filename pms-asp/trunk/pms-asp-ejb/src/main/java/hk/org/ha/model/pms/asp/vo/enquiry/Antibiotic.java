package hk.org.ha.model.pms.asp.vo.enquiry;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class Antibiotic {
	
	
	@XmlElement
	private String antibiotic;
	@XmlElement
	private String sensitive;
	
	public String getAntibiotic() {
		return antibiotic;
	}
	public void setAntibiotic(String antibiotic) {
		this.antibiotic = antibiotic;
	}
	public String getSensitive() {
		return sensitive;
	}
	public void setSensitive(String sensitive) {
		this.sensitive = sensitive;
	}
	
	
	
	
}
