package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum WithoutImmeFeedback implements StringValuedEnum {
	Deteriorating("D", "Deteriorating patient condition"),
	NotApplicable("N", "Not applicable - patient transfer / discharge / death / treatment already stopped"),
	Intervention("I", "Other Intervention - Refer to microbiologist"),
	Others("O", "Others");
	
	private final String dataValue;
	
	private final String displayValue;
	
	WithoutImmeFeedback(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static WithoutImmeFeedback dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(WithoutImmeFeedback.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<WithoutImmeFeedback> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<WithoutImmeFeedback> getEnumClass() {
    		return WithoutImmeFeedback.class;
    	}
    }
}
