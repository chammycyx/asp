package hk.org.ha.model.pms.asp.persistence;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class AspUserHospPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String userCode;
	
	private String hospCode;

	public AspUserHospPK() {
		
	}
	
	public AspUserHospPK( String userCode, String hospCode)
	{
		this.hospCode = hospCode;
		this.userCode = userCode;
	}
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
