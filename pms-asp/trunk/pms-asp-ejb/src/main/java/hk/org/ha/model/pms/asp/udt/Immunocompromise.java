package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum Immunocompromise implements StringValuedEnum{
	Transplant("T", "Transplant"),
	Longterm("L", "On Long term steroid/immunosuppresants"),
	HIV("H", "HIV"),
	Chemotherapy("C", "Chemotherapy"),
	Others("Others", "Others");
	
	
	private final String dataValue;
	
	private final String displayValue;
	
	Immunocompromise(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static Immunocompromise dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(Immunocompromise.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<Immunocompromise> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<Immunocompromise> getEnumClass() {
    		return Immunocompromise.class;
    	}
    }
}
