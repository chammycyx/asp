package hk.org.ha.model.pms.asp.biz.epr;

import hk.org.ha.model.pms.asa.exception.epr.EprException;
import hk.org.ha.model.pms.asa.exception.epr.EprUnreachableException;
import hk.org.ha.model.pms.asp.vo.enquiry.AspRpt;
import hk.org.ha.model.pms.asp.vo.enquiry.CultureResult;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface EprManagerLocal {

	public AspRpt retrieveLabDataFromEpr(String patKey, Date batchDate) throws EprUnreachableException, EprException;

	public String retrieveRecentAdmFromEpr(String patKey, Date batchDate) throws EprUnreachableException, EprException;

	public List<CultureResult> retrieveCultureResultFromEpr(String patKey, Date batchDate) throws EprUnreachableException, EprException;

	public void destroy();
}
