package hk.org.ha.service.biz.pms.asp.interfaces;

import hk.org.ha.model.pms.persistence.disp.DispOrderItem;

import java.util.Date;
import java.util.List;

public interface AspBatchServiceJmsRemote {
	
	public void receiveLegacyDispOrderItemList(List<DispOrderItem> dispOrderItemList, List<String> patHospCodeList, String g6pdCode, Date batchDate) throws RuntimeException;
	
	public void generatePendingAspDispRpt(Date batchDate, String hospCode, String g6pdCode, List<String> patHospCodeList) throws RuntimeException;
	
}
