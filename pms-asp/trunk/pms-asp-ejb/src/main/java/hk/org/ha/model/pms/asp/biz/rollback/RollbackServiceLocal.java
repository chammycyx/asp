package hk.org.ha.model.pms.asp.biz.rollback;

import hk.org.ha.model.pms.asp.exception.RecordNotFoundException;
import hk.org.ha.model.pms.asp.persistence.AspDispRpt;
import hk.org.ha.model.pms.asp.udt.AspDispRptAdminStatus;
import hk.org.ha.model.pms.asp.vo.enquiry.AspRptCriteria;

import java.util.List;

import javax.ejb.Local;

@Local
public interface RollbackServiceLocal {
	public List<AspDispRpt> retrieveRollbackResultList(AspRptCriteria aspRptCriteriaIn);
	
	public AspDispRpt rollbackResultList(AspDispRptAdminStatus oriAdminStatus,List<AspDispRpt> aspRpttResultList) throws RecordNotFoundException;
	
	void destroy();
}
