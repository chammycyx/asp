package hk.org.ha.model.pms.asp.biz;

import hk.org.ha.model.pms.asp.udt.AspType;

import javax.ejb.Local;

@Local
public interface SeqTableManagerLocal {
	String retrieveAspDispRptSerialNum(String hospCode, AspType aspType);
}
