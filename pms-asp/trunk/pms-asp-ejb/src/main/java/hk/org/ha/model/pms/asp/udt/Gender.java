package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum Gender implements StringValuedEnum {

	Male("M", "M"),
	Female("F", "F"),
	Unknown("U", "U");
	
    private final String dataValue;
    private final String displayValue;

    Gender(final String dataValue, final String displayValue){
        this.dataValue = dataValue;
        this.displayValue = displayValue;
    }

    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static Gender dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(Gender.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<Gender> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<Gender> getEnumClass() {
    		return Gender.class;
    	}
    }
}



