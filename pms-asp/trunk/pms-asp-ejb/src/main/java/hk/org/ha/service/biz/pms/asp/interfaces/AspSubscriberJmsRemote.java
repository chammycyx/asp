package hk.org.ha.service.biz.pms.asp.interfaces;

import org.jboss.seam.annotations.async.Asynchronous;
import org.osoa.sca.annotations.OneWay;

public interface AspSubscriberJmsRemote {

	@OneWay
	@Asynchronous
	void initCache();	
	
	@OneWay
	@Asynchronous
	void clearAspHospList();
	
}
