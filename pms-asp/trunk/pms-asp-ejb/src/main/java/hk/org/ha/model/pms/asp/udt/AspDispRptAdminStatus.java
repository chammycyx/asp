package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AspDispRptAdminStatus implements StringValuedEnum {
	None("-", "None"),
	Locked("L", "Locked"),
	Submitted("S", "Submitted"),
	NotForReview("D", "Not Review");
	
	private final String dataValue;
	
	private final String displayValue;
	
	AspDispRptAdminStatus(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static AspDispRptAdminStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AspDispRptAdminStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<AspDispRptAdminStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<AspDispRptAdminStatus> getEnumClass() {
    		return AspDispRptAdminStatus.class;
    	}
    }
}
