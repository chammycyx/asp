package hk.org.ha.model.pms.asp.biz.operation;
import java.io.IOException;

import javax.ejb.Local;

import net.lingala.zip4j.exception.ZipException;

@Local
public interface RptServiceLocal {
	void generateOperationRpt() throws IOException, ZipException;
	
	void setRpt(String rptPath, String rptKey, String rptFileName,  Boolean passwordFlag, String password);
		
	void destroy();
}
