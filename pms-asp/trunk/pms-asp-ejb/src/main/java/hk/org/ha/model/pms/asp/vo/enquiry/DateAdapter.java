package hk.org.ha.model.pms.asp.vo.enquiry;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
 
import javax.xml.bind.DatatypeConverter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DateAdapter extends XmlAdapter<String, Date> implements Serializable {   
	
	private static final long serialVersionUID = 1L;
	
    public Date unmarshal(String v) {
        try {
	        return DatatypeConverter.parseDateTime(v).getTime();
        } catch (Exception e) {
        	return null;
        }
    }   
  
    public String marshal(Date v) {
    	
    	Calendar cal = new GregorianCalendar();
        cal.setTime(v);
        return DatatypeConverter.printDateTime(cal);
    }     
    
    
    
    
}  