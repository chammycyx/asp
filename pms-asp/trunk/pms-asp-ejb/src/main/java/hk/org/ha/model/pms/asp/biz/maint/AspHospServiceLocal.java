package hk.org.ha.model.pms.asp.biz.maint;

import hk.org.ha.model.pms.asp.persistence.AspHosp;

import javax.ejb.Local;

@Local
public interface AspHospServiceLocal {
	void updateAspHosp(AspHosp aspHosp, String originalScreen);
	
	void destroy(); 
	
}
