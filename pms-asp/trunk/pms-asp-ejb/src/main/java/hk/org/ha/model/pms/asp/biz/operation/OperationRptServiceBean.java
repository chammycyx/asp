package hk.org.ha.model.pms.asp.biz.operation;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.persistence.AspDispRpt;
import hk.org.ha.model.pms.asp.udt.AppropriatePrescription;
import hk.org.ha.model.pms.asp.udt.AspDispRptAdminStatus;
import hk.org.ha.model.pms.asp.udt.AspDispRptStatus;
import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.model.pms.asp.udt.InappropriatePrescription;
import hk.org.ha.model.pms.asp.udt.IncorrectInformation;
import hk.org.ha.model.pms.asp.udt.OrganInolved;
import hk.org.ha.model.pms.asp.udt.OutcomeMeasurementType;
import hk.org.ha.model.pms.asp.udt.WithImmeFeedback;
import hk.org.ha.model.pms.asp.udt.WithoutImmeFeedback;
import hk.org.ha.model.pms.asp.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.asp.vo.enquiry.ClinicalData;
import hk.org.ha.model.pms.asp.vo.enquiry.CultureResult;
import hk.org.ha.model.pms.asp.vo.enquiry.Miscellaneous;
import hk.org.ha.model.pms.asp.vo.enquiry.OutcomeMeasurement;
import hk.org.ha.model.pms.asp.vo.enquiry.PatientDemography;
import hk.org.ha.model.pms.asp.vo.enquiry.Remark;
import hk.org.ha.model.pms.asp.vo.operation.OperationRpt;
import hk.org.ha.model.pms.asp.vo.operation.OperationRptCriteria;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;


@Stateful
@Scope(ScopeType.SESSION)
@Name("operationRptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class OperationRptServiceBean implements OperationRptServiceLocal {

	@PersistenceContext
	private EntityManager em;

	@In(scope=ScopeType.SESSION, required=false)
	UamInfo uamInfo;
	
	@In
	private AuditLogger auditLogger;
	
	private List<OperationRpt> operationRptList; 
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static final String OTHERS = "Others";
	private static final String SEMICOLON = ";";
	private static final String DOT = ".";
	private static final String CROSS ="X";
	private static final String YES ="Y";
	
	public List<OperationRpt> retrieveOpertaionRptResult(OperationRptCriteria o) {
		
		StringBuilder queryString = new StringBuilder();
		queryString.append("select o from AspDispRpt o "); 
		queryString.append("where o.dispDate >= :dispDateFrom ");	
		queryString.append(" and o.dispDate <= :dispDateTo ");
		
		if(o.getHospCode() != null){
			queryString.append(" and o.hospCode = :hospCode ");
		}
		if(o.getRptType() != null){
			queryString.append(" and o.rptType = :rptType ");
		}
		
		
		Query q = em.createQuery(queryString.toString())
				.setParameter("dispDateFrom", o.getStartDate())
				.setParameter("dispDateTo", o.getEndDate());
		
		
		if(o.getHospCode() != null){
			q.setParameter("hospCode", o.getHospCode());
		}	
		
		if(o.getRptType() != null){
			if(AspType.BG.equals(o.getRptType())){
				q.setParameter("rptType", o.getRptType());
			}else if(AspType.IV.equals(o.getRptType())){
				q.setParameter("rptType", o.getRptType());
			}
		}
		
		@SuppressWarnings("unchecked")
		List<AspDispRpt> aspRptList = q.getResultList();
		operationRptList = new ArrayList<OperationRpt>();
		
		if(!aspRptList.isEmpty())
		{
			operationRptList =  convertOperationRptResult(aspRptList) ;
			Collections.sort(operationRptList, new TypeComparator());
		}
		
		auditLogger.log("#0065",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),o.getHospCode() == null? "All": o.getHospCode() ,o.getRptType() == null? "All": o.getRptType() ,sdf.format(o.getStartDate()),sdf.format(o.getEndDate()));
		
		return operationRptList;
	}
	
	
	public static class TypeComparator implements Comparator<OperationRpt>, Serializable
		{
			private static final long serialVersionUID = -1629369536048145337L;

			public int compare(OperationRpt c1, OperationRpt c2){
				return c1.getType()
				.compareTo( 
						c2.getType()
				);
			}						
		}
	
	
	private List<OperationRpt> convertOperationRptResult(
			List<AspDispRpt> aspRptList) {
		StringBuilder sb = new StringBuilder();
		StringBuilder sbd = new StringBuilder();
		List<OperationRpt> operationRptList = new ArrayList<OperationRpt>();
		for(AspDispRpt a : aspRptList){
			OperationRpt o = new OperationRpt();
			o.setRptDate(a.getRptDate());
			o.setHkid(a.getHkid());
			o.setPatientName(a.getPatName());	
			o.setPhsWard(a.getAspDispItem().getWardCode());
			o.setPhsSpecialty(a.getAspDispItem().getSpecCode());
			
			if(a.getAspRpt() != null){
				if(a.getAspRpt().getPatientDemography() != null){
					PatientDemography pd = a.getAspRpt().getPatientDemography();
					
					o.setSex(pd.getSex());
					o.setAge(pd.getAge());
					o.setIpasWard(pd.getIpasWard());
					o.setIpasSpecialty(pd.getIpasSpec());
					
				}
				
				if(a.getAspRpt().getClinicalData() != null){
					ClinicalData cd  = a.getAspRpt().getClinicalData();
					
					
					if(cd.getDrRank() != null){
						o.setDrRank(cd.getDrRank().getDisplayValue());
					}
				
						
					sb = new StringBuilder();
					if(cd.getOrganInolvedList() != null){
						for(OrganInolved oi : cd.getOrganInolvedList()){
							sb.append(oi.getDisplayValue()).append(SEMICOLON);
						}
						if(!StringUtils.isEmpty(cd.getOrganInolvedOthersValue())){
							sb.append(cd.getOrganInolvedOthersValue());
						}else{
							sb.substring(0, sb.length() -1);
						}
						
						o.setOrganSystemInvolved(sb.toString());
					}
					
					if(cd.getTreatment()!= null){
						o.setTreatment(cd.getTreatment().getDisplayValue());
						
					}
					
					Integer i = 1;
					sb = new StringBuilder();
					sbd = new StringBuilder(); 
					if(a.getAspRpt().getManualCultureResultList() != null){
						for(CultureResult c : a.getAspRpt().getManualCultureResultList()){
							if(!StringUtils.isEmpty(c.getSpecimen())){
								sb.append(i +DOT+ c.getSpecimen() + SEMICOLON);
								
							}
							if(!StringUtils.isEmpty(c.getOrganismList().get(0).getOrganism())){
								sbd.append(i +DOT+c.getOrganismList().get(0).getOrganism()+ SEMICOLON);
							}
							i++;
						}
						o.setSpecimen(sb.toString());
						o.setOrganismIsolated(sbd.toString());
					}
				
					o.setAntibioticName(cd.getAntibioticName());
					
					if(cd.getIndication() !=null){
						String indicationDesc ="";
						if(cd.getIndication()!= null){
							if(cd.getIndication() == 1){
								indicationDesc = cd.getIndication1Desc();
								
							}else if(cd.getIndication() == 2){
								
								indicationDesc = cd.getIndication2Desc();
							}else if(cd.getIndication() == 3){
								
								indicationDesc = cd.getIndication3Desc();
							}else if(cd.getIndication() == 4){
								
								indicationDesc = cd.getIndication4Desc();
							}else if(cd.getIndication() == 5){
								
								indicationDesc = cd.getIndication5Desc();
							}else if(cd.getIndication() == 6){
								
								indicationDesc = cd.getIndication6Desc();
							}
							
							if(OTHERS.equals(indicationDesc)){
								indicationDesc += SEMICOLON + cd.getIndicationOthersValue();
							}
							
							o.setIndication(indicationDesc);
							
						}
						
						
					}
					
				}
				
				
				
				if(a.getAspRpt().getOutcomeMeasurement()!=null){
					OutcomeMeasurement om = a.getAspRpt().getOutcomeMeasurement();
					
					
					if(om.getOutcomeMeasurementType() != null){
						if(OutcomeMeasurementType.Pending.equals(om.getOutcomeMeasurementType())){
							o.setOutcomesMeasures("Pending for Audit");
						}else{
							o.setOutcomesMeasures(om.getOutcomeMeasurementType().getDisplayValue());
						}
						
						if(AspDispRptStatus.Inactive.equals(a.getStatus())){
							o.setOutcomesMeasures("Old Report");
						}
						if(AspDispRptAdminStatus.NotForReview.equals(a.getAdminStatus())){
							o.setOutcomesMeasures("Not for Review");
						}
						
						
					}
					
					
					
					
					
					if(YesNoBlankFlag.Yes.equals(om.getImmeFeedbackFlag())){
						o.setImmediateConcurrentFeedback(om.getImmeFeedbackFlag().getDataValue());
						
						sb = new StringBuilder();
						sbd = new StringBuilder();
						
						if(om.getWithImmeFeedbackList() != null){
							for(WithImmeFeedback wi : om.getWithImmeFeedbackList()){
								sbd.append(wi.getDataValue());
								sb.append(wi.getDisplayValue()).append(SEMICOLON);
							}
						
							if(!StringUtils.isEmpty(om.getWithImmeFeedbackOtherValue())){
								sb.append(om.getWithImmeFeedbackOtherValue());
							}else{
								sb.substring(0, sb.length() -1);
								
							}
						}
						o.setImmediateYesCode(sbd.toString());
						o.setWithFeedbackReason(sb.toString());
						
					}else if(YesNoBlankFlag.No.equals(om.getImmeFeedbackFlag())){
						o.setImmediateConcurrentFeedback(om.getImmeFeedbackFlag().getDataValue());
						
						sb = new StringBuilder();
						sbd = new StringBuilder();
						
						if(om.getWithoutImmeFeedbackList() != null){
							for(WithoutImmeFeedback wi : om.getWithoutImmeFeedbackList()){
								sbd.append(wi.getDataValue());
								sb.append(wi.getDisplayValue()).append(SEMICOLON);
							}
							if(!StringUtils.isEmpty(om.getWithoutImmeFeedbackOtherValue())){
								sb.append(om.getWithoutImmeFeedbackOtherValue());
							}else{
								sb.substring(0, sb.length() -1);
							}
						
						}
						
						
						o.setImmediateNoCode(sbd.toString());
						o.setWithoutFeedbackReason(sb.toString());
					}
					o.setAuditBy(om.getAuditBy());
					
					if(om.getAuditRank() != null){
						o.setAuditRank(om.getAuditRank().getDisplayValue());
					}
					
				}
				
				
				
				if(a.getAspRpt().getMiscellaneous() != null){
					Miscellaneous m = a.getAspRpt().getMiscellaneous();
					
					if(m.getAccuracyInfoFlag() != null){
						
						o.setAccurancyInfoProvide(m.getAccuracyInfoFlag().getDataValue());
						
						if(YesNoBlankFlag.No.equals(m.getAccuracyInfoFlag())){
							if(m.getIncorrectInfoList() != null){
								sb =  new StringBuilder();
								sbd = new StringBuilder();
								
								for(IncorrectInformation ii : m.getIncorrectInfoList()){
									sbd.append(ii.getDataValue());
									sb.append(ii.getDisplayValue()).append(SEMICOLON);
								}
								if(!StringUtils.isEmpty(sbd.toString())){
									String incorrectInfoString = sbd.toString();
									o.setIncTreatment(incorrectInfoString.contains(IncorrectInformation.Treatment.getDataValue())? CROSS: null);
									o.setIncOrganism(incorrectInfoString.contains(IncorrectInformation.Isolated.getDataValue())? CROSS: null);
									o.setIncIndication(incorrectInfoString.contains(IncorrectInformation.Indication.getDataValue())? CROSS: null);
									o.setIncPreviousAntiTreatment(incorrectInfoString.contains(IncorrectInformation.Previous.getDataValue())? CROSS: null);
									o.setIncSensitivity(incorrectInfoString.contains(IncorrectInformation.Sensitivity.getDataValue())? CROSS: null);
									o.setIncOthers(incorrectInfoString.contains(IncorrectInformation.Others.getDataValue())? CROSS: null);
								}
								if(!StringUtils.isEmpty( m.getIncorrectInfoOtherValue())){
									sb.append(m.getInapproPresOtherValue());
								}else{
									sb.substring(0, sb.length() -1);
								}
								
								o.setIncorrectInfoReason(sb.toString());
							}
							
						
						}
					}
					
					
					
					sb = new StringBuilder();
					if(m.getApproPresList() != null){
						o.setAppropriatePresc(YES);
						for(AppropriatePrescription ap : m.getApproPresList()){
							sb.append(ap.getDisplayValue()).append(SEMICOLON);
						}
						
						if(!StringUtils.isEmpty(m.getApproPresOtherValue())){
							sb.append(m.getApproPresOtherValue());
						}else{
							
							sb.substring(0, sb.length() -1);
						}
						o.setReasonAppropriatePresc(sb.toString());
					}
					
					sb = new StringBuilder();
					if(m.getInapproPresList() != null){
						o.setInappropriatePresc(YES);
						for(InappropriatePrescription iap : m.getInapproPresList()){
							sb.append(iap.getDisplayValue()).append(SEMICOLON);
						}
						
						if(!StringUtils.isEmpty(m.getInapproPresOtherValue())){
							sb.append(m.getInapproPresOtherValue());
						}else{
								
							sb.substring(0, sb.length() -1);
						}
						o.setReasonInappropriatePresc(sb.toString());
					}
					
				}
				
				if(a.getAspRpt().getRemark() != null){
					Remark r = a.getAspRpt().getRemark();
					if(r.getPrescribing() != null){
						o.setSpecialtyGeneral(r.getPrescribing().getDisplayValue());
					}
					if(r.getTlcDnrFlag() != null){
						o.setTlcDNR(r.getTlcDnrFlag().getDataValue());
					}
					if(r.getMoPager() != null){
						o.setMoPager(r.getMoPager());
					}
					if(r.getSmoPager() != null){
						o.setSmoPager(r.getSmoPager());
					}
					if(r.getSpecialty()!= null){
						o.setPrescribingSpecialty(r.getSpecialty().getDisplayValue());
					}
				}
				
			}
			
			
			
			o.setHospital(a.getHospCode());
			o.setSerialNum(a.getSerialNum());
			
			if(a.getRptType()!= null){
				o.setType(a.getRptType().getDataValue());
			}
			
			operationRptList.add(o);
		}
		
		return operationRptList;
	}
	
	
	@Override
	public List<OperationRpt> getOperationRptList() {
		return operationRptList;
	}
	
    @Remove
	public void destroy(){
    	operationRptList = null;
	}


}
