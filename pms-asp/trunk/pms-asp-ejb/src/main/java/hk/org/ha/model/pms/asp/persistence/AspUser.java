package hk.org.ha.model.pms.asp.persistence;

import hk.org.ha.model.pms.persistence.VersionEntity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.PrivateOwned;

@Entity
@Table(name = "ASP_USER")
public class AspUser extends VersionEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="USER_CODE")
	private String userCode;

	@PrivateOwned
	@OneToMany(mappedBy="aspUser", cascade=CascadeType.ALL)
	private List<AspUserHosp> aspUserHospList;

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public List<AspUserHosp> getAspUserHospList() {
		return aspUserHospList;
	}

	public void setAspUserHospList(List<AspUserHosp> aspUserHospList) {
		this.aspUserHospList = aspUserHospList;
	}
}
