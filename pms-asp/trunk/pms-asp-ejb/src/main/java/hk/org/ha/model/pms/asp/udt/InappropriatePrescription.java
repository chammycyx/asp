package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum InappropriatePrescription implements StringValuedEnum {
	NoEvidence("N", "No evidence of infection/alternative Dx"),
	Use("U", "Use as prophylactic agent"),
	Colonization("C", "Colonization / contamination"),
	Spectrum("S", "Spectrum too broad"),
	Redundant("R", "Redundant combination"),
	InappCoverage("V", "Inappropriate coverage"),
	InappRoute("T", "Inappropriate route"),
	InappDosage("D", "Inappropriate dosage"),
	InappChoice("H", "Inappropriate choice"),
	Others("O", "Others");
	
	private final String dataValue;
	
	private final String displayValue;
	
	InappropriatePrescription(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static InappropriatePrescription dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(InappropriatePrescription.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<InappropriatePrescription> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<InappropriatePrescription> getEnumClass() {
    		return InappropriatePrescription.class;
    	}
    }
}
