package hk.org.ha.model.pms.asp.persistence;

import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.util.StringCollectionConverter;
import hk.org.ha.model.pms.asp.udt.AlertProfileStatus;
import hk.org.ha.model.pms.asp.udt.AspDispRptAdminStatus;
import hk.org.ha.model.pms.asp.udt.AspDispRptStatus;
import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.model.pms.asp.udt.Gender;
import hk.org.ha.model.pms.asp.vo.enquiry.AspRpt;
import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.drools.util.StringUtils;
import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name="ASP_DISP_RPT")
public class AspDispRpt extends VersionEntity
{
	private static final long serialVersionUID = 1L;

	private static final String JAXB_CONTEXT_RX = "hk.org.ha.model.pms.asp.vo.enquiry";

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aspDispRptSeq")
	@SequenceGenerator(name = "aspDispRptSeq", sequenceName = "SQ_ASP_DISP_RPT", initialValue = 100000000)
	private Long id;
	
	@Column(name = "HOSP_CODE", nullable = false, length = 3)
	private String hospCode;
	
	@Converter(name = "AspDispRpt.rptType", converterClass = AspType.Converter.class)
    @Convert("AspDispRpt.rptType")
	@Column(name = "RPT_TYPE", nullable = false)
	private AspType rptType;
	
	@Column(name = "SERIAL_NUM", nullable = false, length = 13)
	private String serialNum;
	
	@Column(name = "RPT_XML", nullable = false)
	private String rptXml;
	
	@Column(name = "RPT_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date rptDate;

	@Column(name = "HKID", nullable = false, length = 12)
	private String hkid;

	@Column(name = "PAS_SPEC_CODE", length = 4)
	private String pasSpecCode;

	@Column(name = "PAS_WARD_CODE", length = 4)
	private String pasWardCode;
	
	@Converter(name = "AspDispRpt.status", converterClass = AspDispRptStatus.Converter.class)
	@Convert("AspDispRpt.status")
	@Column(name = "STATUS", nullable = false)
	private AspDispRptStatus status;

	@Column(name = "PAT_KEY", length = 8)
	private String patKey;
	
	@Column(name = "PAT_NAME", length = 48)
	private String patName;

	@Column(name = "DRUG_NAME", length = 59)
	private String drugName;

	@Column(name = "DISP_DATE")
	@Temporal(TemporalType.DATE)
	private Date dispDate;
	
	@Column(name = "DOB")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dob;

	@Converter(name = "AspDispRpt.sex", converterClass = Gender.Converter.class)
    @Convert("AspDispRpt.sex")
	@Column(name = "SEX", length = 1)
	private Gender sex;
	
	@Column(name="WORKSTATION_ID", length = 19)
	private String workstationId;

	@Column(name="DISCHARGE_FLAG", nullable=false, length = 1)
	private Boolean dischargeFlag;
	
	@Converter(name = "AspDispRpt.adminStatus", converterClass = AspDispRptAdminStatus.Converter.class)
	@Convert("AspDispRpt.adminStatus")
	@Column(name="ADMIN_STATUS", nullable = false)
	private AspDispRptAdminStatus adminStatus;
	
	@Converter(name = "PatientEntity.ccCode", converterClass = StringCollectionConverter.class)
	@Convert("PatientEntity.ccCode")
	@Column(name = "CC_CODE", length = 100)
	private List<String> ccCode;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ASP_DISP_ITEM_ID", nullable = false)
	private AspDispItem aspDispItem;

	@Transient
	private Boolean selected;
	
	@Transient
	private AspRpt aspRpt;
	
	@Transient
	private Long aspDispItemId;
	
	@Transient
	private String updateUserId;

	@Transient
	private String freqDesc;
	
	@Transient
	private Date updateDateTime;
	
	@Transient
	private String wsErrorCode;
	
	@Transient
	private String itemCode;
	
	@Transient
	private String wardCode;
	
	@Transient
	private String specCode;
	
	@Transient
	private String caseNum;
	
	@Transient
	private String bedNum;
	
	@Transient
	private String freqCode;
	
	@Transient
	private String doseQty;
	
	@Transient
	private String doseUnit;
	
	@Transient 
	private String alertProfileErrorMsg;
	
	@Transient
	private AlertProfileStatus alertProfileStatus;
	
	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public AspDispRpt(Long id, String hospCode, AspType rptType, String serialNum, Date dispDate, 
			Date rptDate, String hkid, 
			AspDispRptStatus status, String caseNum, String patName, Long aspDispItemId, 
			AspDispRptAdminStatus adminStatus, String workstationId, String updateUserId) {
		super();
		this.id = id;
		this.hospCode = hospCode;
		this.rptType = rptType;
		this.serialNum = serialNum;
		this.rptDate = new Date(rptDate.getTime());
		this.hkid = hkid;
		this.status = status;
		this.caseNum = caseNum;
		this.patName = patName;
		this.dispDate = new Date(dispDate.getTime());
		this.aspDispItemId = aspDispItemId;
		this.adminStatus = adminStatus;
		this.workstationId= workstationId;
		this.updateUserId = updateUserId;
		
	}
	
	public AspDispRpt(Long id, String hospCode, AspType rptType, String serialNum,
			Date rptDate, String hkid, String itemCode, String pasSpecCode,
			AspDispRptStatus status, String caseNum, String patName,
			String pasWardCode, String bedNum, String drugName, String freqCode,
			String doseQty, String doseUnit, Date dispDate, Long aspDispItemId, 
			AspDispRptAdminStatus adminStatus, String workstationId, String updateUserId, 
			String freqDesc, Boolean dischargeFlag, Date updateDateTime, String wardCode, String rptXml ) {
		super();
		this.id = id;
		this.hospCode = hospCode;
		this.rptType = rptType;
		this.serialNum = serialNum;
		this.rptDate = new Date(rptDate.getTime());
		this.hkid = hkid;
		this.itemCode = itemCode;
		this.pasSpecCode = pasSpecCode;
		this.status = status;
		this.caseNum = caseNum;
		this.patName = patName;
		this.pasWardCode = pasWardCode;
		this.bedNum = bedNum;
		this.drugName = drugName;
		this.freqCode = freqCode;
		this.doseQty = doseQty;
		this.doseUnit = doseUnit;
		this.dispDate = new Date(dispDate.getTime());
		this.aspDispItemId = aspDispItemId;
		this.adminStatus = adminStatus;
		this.workstationId= workstationId;
		this.updateUserId = updateUserId;
		this.wardCode = wardCode;
		this.freqDesc = freqDesc;
		this.dischargeFlag = dischargeFlag;
		this.updateDateTime = updateDateTime;
		this.rptXml = rptXml;
	}

	public AspDispRpt() {
	}
	
	@PostLoad
    public void postLoad() {
		if ( !StringUtils.isEmpty(getRptXml()) ) {
			JaxbWrapper<AspRpt> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);
    		aspRpt = rxJaxbWrapper.unmarshall(rptXml) ;
    	}
    }
    
	@PrePersist
    @PreUpdate
	public void preSave() {
		JaxbWrapper<AspRpt> rxJaxbWrapper = JaxbWrapper.instance(JAXB_CONTEXT_RX);

		if (aspRpt != null) {
			rptXml = rxJaxbWrapper.marshall(aspRpt) ;
		}
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public String getPasWardCode() {
		return pasWardCode;
	}

	public void setPasWardCode(String pasWardCode) {
		this.pasWardCode = pasWardCode;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getFreqCode() {
		return freqCode;
	}

	public void setFreqCode(String freqCode) {
		this.freqCode = freqCode;
	}

	public String getDoseQty() {
		return doseQty;
	}

	public void setDoseQty(String doseQty) {
		this.doseQty = doseQty;
	}

	public String getDoseUnit() {
		return doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	public Date getDispDate() {
		return (dispDate != null)? new Date(dispDate.getTime()):null;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = new Date(dispDate.getTime());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public AspType getRptType() {
		return rptType;
	}

	public void setRptType(AspType rptType) {
		this.rptType = rptType;
	}

	public String getSerialNum() {
		return serialNum;
	}

	public void setSerialNum(String serialNum) {
		this.serialNum = serialNum;
	}

	public Date getRptDate() {
		return (rptDate != null)?new Date(rptDate.getTime()):null;
	}

	public void setRptDate(Date rptDate) {
		this.rptDate = new Date(rptDate.getTime());
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public AspDispRptStatus getStatus() {
		return status;
	}

	public void setStatus(AspDispRptStatus status) {
		this.status = status;
	}

	public String getRptXml() {
		return rptXml;
	}

	public void setRptXml(String rptXml) {
		this.rptXml = rptXml;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public AspRpt getAspRpt() {
		return aspRpt;
	}

	public void setAspRpt(AspRpt aspRpt) {
		this.aspRpt = aspRpt;
	}

	public Date getDob() {
		return (dob!=null)? new Date(dob.getTime()):null;
	}

	public void setDob(Date dob) {
		this.dob = (dob!=null)?new Date(dob.getTime()):null;
	}

	public Gender getSex() {
		return sex;
	}

	public void setSex(Gender sex) {
		this.sex = sex;
	}

	public void setSex(String sex) {
		this.sex = Gender.valueOf(sex);
	}
	
	public List<String> getCcCode() {
		return ccCode;
	}

	public void setCcCode(List<String> ccCode) {
		this.ccCode = ccCode;
	}

	public AspDispRptAdminStatus getAdminStatus() {
		return adminStatus;
	}

	public void setAdminStatus(AspDispRptAdminStatus adminStatus) {
		this.adminStatus = adminStatus;
	}

	public Boolean getDischargeFlag() {
		return dischargeFlag;
	}

	public void setDischargeFlag(Boolean dischargeFlag) {
		this.dischargeFlag = dischargeFlag;
	}

	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}

	public String getWorkstationId() {
		return workstationId;
	}

	public void setWorkstationId(String workstationId) {
		this.workstationId = workstationId;
	}

	public AspDispItem getAspDispItem() {
		return aspDispItem;
	}

	public void setAspDispItem(AspDispItem aspDispItem) {
		this.aspDispItem = aspDispItem;
	}

	public Long getAspDispItemId() {
		return aspDispItemId;
	}

	public void setAspDispItemId(Long aspDispItemId) {
		this.aspDispItemId = aspDispItemId;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public String getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(String updateUserId) {
		this.updateUserId = updateUserId;
	}

	public String getFreqDesc() {
		return freqDesc;
	}

	public void setFreqDesc(String freqDesc) {
		this.freqDesc = freqDesc;
	}

	public String getWsErrorCode() {
		return wsErrorCode;
	}

	public void setWsErrorCode(String wsErrorCode) {
		this.wsErrorCode = wsErrorCode;
	}

	public String getAlertProfileErrorMsg() {
		return alertProfileErrorMsg;
	}

	public void setAlertProfileErrorMsg(String alertProfileErrorMsg) {
		this.alertProfileErrorMsg = alertProfileErrorMsg;
	}

	public AlertProfileStatus getAlertProfileStatus() {
		return alertProfileStatus;
	}

	public void setAlertProfileStatus(AlertProfileStatus alertProfileStatus) {
		this.alertProfileStatus = alertProfileStatus;
	}


	
}