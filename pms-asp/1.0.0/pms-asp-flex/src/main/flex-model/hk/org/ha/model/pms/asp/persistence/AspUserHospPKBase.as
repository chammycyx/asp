/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (AspUserHospPK.as).
 */

package hk.org.ha.model.pms.asp.persistence {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.IExternalizable;

    [Bindable]
    public class AspUserHospPKBase implements IExternalizable {

        private var _hospCode:String;
        private var _userCode:String;

        public function set hospCode(value:String):void {
            _hospCode = value;
        }
        public function get hospCode():String {
            return _hospCode;
        }

        public function set userCode(value:String):void {
            _userCode = value;
        }
        public function get userCode():String {
            return _userCode;
        }

        public function readExternal(input:IDataInput):void {
            _hospCode = input.readObject() as String;
            _userCode = input.readObject() as String;
        }

        public function writeExternal(output:IDataOutput):void {
            output.writeObject(_hospCode);
            output.writeObject(_userCode);
        }
    }
}