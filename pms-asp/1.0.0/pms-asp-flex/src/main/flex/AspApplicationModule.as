package {
	
	import mx.logging.Log;
	import mx.logging.targets.TraceTarget;
	
	import hk.org.ha.control.pms.asp.maint.AspHospListServiceCtl;
	import hk.org.ha.control.pms.asp.maint.AspHospServiceCtl;
	import hk.org.ha.control.pms.asp.maint.AspUserServiceCtl;
	import hk.org.ha.control.pms.asp.maint.AspUserListServiceCtl;
	import hk.org.ha.control.pms.asp.enquiry.EnqServiceCtl;
	import hk.org.ha.control.pms.asp.rollback.RollbackServiceCtl;
	import hk.org.ha.control.pms.asp.operation.OperationRptServiceCtl;
	import hk.org.ha.control.pms.asp.operation.RptServiceCtl;
	import hk.org.ha.control.pms.asp.enquiry.ExportServiceCtl;
	import hk.org.ha.control.pms.exception.AccessDeniedExceptionHandler;
	import hk.org.ha.control.pms.exception.DefaultExceptionHandler;
	import hk.org.ha.control.pms.exception.IllegalStateExceptionHandler;
	import hk.org.ha.control.pms.exception.NotLoggedInExceptionHandler;
	import hk.org.ha.control.pms.exception.OptimisticLockExceptionHandler;
	import hk.org.ha.control.pms.exception.SecurityExceptionHandler;
	import hk.org.ha.control.pms.exception.LockTimeoutExceptionHandler;
	import hk.org.ha.control.pms.security.LogonCtl;
	import hk.org.ha.control.pms.sys.SystemMessageServiceCtl;
	import hk.org.ha.fmk.pms.flex.components.window.Window;
	
	import org.granite.tide.ITideModule;
	import org.granite.tide.Tide;
	import org.granite.tide.validators.ValidatorExceptionHandler;
	
	[Bindable]
	public class AspApplicationModule implements ITideModule 
	{
		
		public function init(tide:Tide):void 
		{
			var t:TraceTarget = new TraceTarget();
			t.filters = ["org.granite.*","hk.org.ha.*"];
			Log.addTarget(t);
			
			tide.addExceptionHandler(NotLoggedInExceptionHandler);
			tide.addExceptionHandler(AccessDeniedExceptionHandler);
			tide.addExceptionHandler(SecurityExceptionHandler);
			tide.addExceptionHandler(ValidatorExceptionHandler);
			tide.addExceptionHandler(IllegalStateExceptionHandler);
			tide.addExceptionHandler(OptimisticLockExceptionHandler);
			tide.addExceptionHandler(LockTimeoutExceptionHandler);
			tide.addExceptionHandler(DefaultExceptionHandler);
			
			tide.addComponent("window", Window);
			
			tide.addComponents(
				[	LogonCtl,
					SystemMessageServiceCtl, 
					AspHospListServiceCtl,
					AspHospServiceCtl,
					AspUserServiceCtl,
					AspUserListServiceCtl,
					EnqServiceCtl,
					RollbackServiceCtl,
					OperationRptServiceCtl,
					RptServiceCtl,
					ExportServiceCtl
				]); 
		}
	}
}
