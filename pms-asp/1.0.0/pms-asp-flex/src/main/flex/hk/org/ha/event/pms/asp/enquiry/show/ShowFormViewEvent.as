package hk.org.ha.event.pms.asp.enquiry.show
{
	
	import hk.org.ha.model.pms.asp.persistence.AspDispRpt;
	import org.granite.tide.events.AbstractTideEvent;
		
	public class ShowFormViewEvent extends AbstractTideEvent
	{
		private var _aspDispRpt:AspDispRpt;
		
		public function ShowFormViewEvent( aspDispRpt:AspDispRpt = null ):void 
		{
			super();
			_aspDispRpt = aspDispRpt;
		}
			
		public function get aspDispRpt():AspDispRpt {
			return _aspDispRpt;
		}
	}
}