package hk.org.ha.event.pms.asp.maint
{
	import org.granite.tide.events.AbstractTideEvent;
		
	public class RetrieveAspHospListEvent extends AbstractTideEvent
	{	
		private var _maintName:String;

		public function RetrieveAspHospListEvent(maintName:String=null):void 
		{
			super();
			_maintName = maintName;
		}
		
		public function get maintName():String {
			return _maintName;
		}

	}
}