package hk.org.ha.event.pms.asp.maint
{
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.asp.persistence.AspUser;

	import org.granite.tide.events.AbstractTideEvent;
	
	public class DeleteAspUserEvent extends AbstractTideEvent
	{
		private var _aspUser:AspUser;
		
		public function DeleteAspUserEvent(aspUser:AspUser):void 
		{
			super();
			_aspUser = aspUser;
		}
		
		public function get aspUser():AspUser {
			return _aspUser;
		}
		
	}
}

