package hk.org.ha.event.pms.asp.rollback.show
{
	import org.granite.tide.events.AbstractTideEvent;
		
	public class ShowRollbackRptViewEvent extends AbstractTideEvent
	{
		private var _clearMessages:Boolean;
				
		public function ShowRollbackRptViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
			
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}