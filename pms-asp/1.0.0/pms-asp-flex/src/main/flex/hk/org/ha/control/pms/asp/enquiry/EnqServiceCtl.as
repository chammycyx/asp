package hk.org.ha.control.pms.asp.enquiry
{		
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.asp.enquiry.NotForReviewAspDispRptListEvent;
	import hk.org.ha.event.pms.asp.enquiry.RefreshAspDispRptListEvent;
	import hk.org.ha.event.pms.asp.enquiry.RetrieveAspDispRptEvent;
	import hk.org.ha.event.pms.asp.enquiry.RetrieveAspDispRptListEvent;
	import hk.org.ha.event.pms.asp.enquiry.UpdateAspDispRptEvent;
	import hk.org.ha.event.pms.asp.enquiry.show.ShowFormViewEvent;
	import hk.org.ha.model.pms.asp.biz.enquiry.EnqServiceBean;
	import hk.org.ha.model.pms.asp.persistence.AspDispRpt;
	import hk.org.ha.model.pms.asp.udt.AppropriatePrescription;
	import hk.org.ha.model.pms.asp.udt.AspDispRptAdminStatus;
	import hk.org.ha.model.pms.asp.udt.AspDispRptStatus;
	import hk.org.ha.model.pms.asp.udt.Immunocompromise;
	import hk.org.ha.model.pms.asp.udt.InappropriatePrescription;
	import hk.org.ha.model.pms.asp.udt.IncorrectInformation;
	import hk.org.ha.model.pms.asp.udt.OrganInolved;
	import hk.org.ha.model.pms.asp.udt.OutcomeMeasurementType;
	import hk.org.ha.model.pms.asp.udt.PastMediHistory;
	import hk.org.ha.model.pms.asp.udt.WithImmeFeedback;
	import hk.org.ha.model.pms.asp.udt.WithoutImmeFeedback;
	import hk.org.ha.model.pms.asp.udt.AlertProfileStatus;
	import hk.org.ha.model.pms.asp.vo.enquiry.Antibiotic;
	import hk.org.ha.model.pms.asp.vo.enquiry.Checklist;
	import hk.org.ha.model.pms.asp.vo.enquiry.CultureResult;
	import hk.org.ha.model.pms.asp.vo.enquiry.LabTest;
	import hk.org.ha.model.pms.asp.vo.enquiry.Organism;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("EnqServiceCtl", restrict="false")]
	public class EnqServiceCtl
	{
		
		
		
		[In]
		public var enqService:EnqServiceBean;
		
		
		public var retrieveAspDispRptListEvent: RetrieveAspDispRptListEvent
		public var notForReviewAspDispRptListEvent: NotForReviewAspDispRptListEvent
		public var refreshAspDispRptListEvent: RefreshAspDispRptListEvent;
		public var retrieveAspDispRptEvent:RetrieveAspDispRptEvent;
		public var updateAspDispRptEvent:UpdateAspDispRptEvent;
		public var immunocompromise: Immunocompromise;
		public var labTest : LabTest;
		public var organInolved: OrganInolved;
		public var pastMediHistory:PastMediHistory;
		public var cultureResult:CultureResult;
		public var organism:Organism;
		public var outcomeMeasurementType:OutcomeMeasurementType;
		public var withImmeFeedback:WithImmeFeedback;
		public var withoutImmeFeedback:WithoutImmeFeedback;
		public var checkList:Checklist;
		public var antibiotic:Antibiotic;
		public var inappropriatePrescription:InappropriatePrescription;
		public var appropriatePrescription:AppropriatePrescription;
		public var incorrectInformation:IncorrectInformation;
		
		
		
		
		[Observer]
		public function retrieveAspDispRptList(evt:RetrieveAspDispRptListEvent):void
		{
			retrieveAspDispRptListEvent = evt;
			enqService.retrieveAspDispRptList(evt.aspRptCriteria, retrieveAspDispRptListResult);
		}
		
		public function retrieveAspDispRptListResult(evt:TideResultEvent):void
		{
			
			if (retrieveAspDispRptListEvent.callBackFunction != null) {
				retrieveAspDispRptListEvent.callBackFunction(evt.result as ArrayCollection);
			}
		}
		
		
		[Observer]
		public function notForReviewAspDispRptList(evt:NotForReviewAspDispRptListEvent):void
		{
			notForReviewAspDispRptListEvent = evt;
			enqService.updateAspDispRptListStatus(evt.aspRptCriteria, notForReviewAspDispRptListResult);
		}
		
		public function notForReviewAspDispRptListResult(evt:TideResultEvent):void
		{
			
			//check lock
			if(evt.result != null){
				notForReviewAspDispRptListEvent.lockFunction(evt.result as AspDispRpt);
			}else{
				notForReviewAspDispRptListEvent.callBackFunction();
			}
		}
		
		
		[Observer]
		public function refreshAspDispRptList(evt:RefreshAspDispRptListEvent):void
		{
			refreshAspDispRptListEvent = evt;
			enqService.refreshAspDispRptList(evt.aspRptCriteria, refreshAspDispRptListResult);
		}
		
		public function refreshAspDispRptListResult(evt:TideResultEvent):void
		{
			
			if(evt.result != null){
				var aspDispRpt:AspDispRpt = evt.result as AspDispRpt
					
				if(aspDispRpt.alertProfileStatus == AlertProfileStatus.Error){
					refreshAspDispRptListEvent.callBackFunction(aspDispRpt.alertProfileErrorMsg);
				}else if(aspDispRpt.wsErrorCode != ''){
					refreshAspDispRptListEvent.wsErrorFunction(aspDispRpt.wsErrorCode);
				}else{
					refreshAspDispRptListEvent.lockFunction(aspDispRpt);
				}
				
			}else{
				refreshAspDispRptListEvent.callBackFunction(null);
			}
			
			
		}
		
		[Observer]
		public function updateAspDispRpt(evt:UpdateAspDispRptEvent):void 
		{	
			
			updateAspDispRptEvent = evt;
			enqService.updateAspDispRpt(evt.aspDispRpt, updateAspDispRptResult);
		}	
		
		
		public function updateAspDispRptResult(evt:TideResultEvent):void 
		{	
			if(updateAspDispRptEvent.callbackFunc != null){
				updateAspDispRptEvent.callbackFunc(evt.result as String);
			}
			
		}
		
		
		
		[Observer]
		public function retrieveAspDispRpt(evt:RetrieveAspDispRptEvent):void 
		{	
			
			retrieveAspDispRptEvent = evt;
			enqService.retrieveAspDispRpt(evt.aspDispRptId, evt.workStationId, retrieveAspDispRptResult);
		}	
		
		
		public function retrieveAspDispRptResult(evt:TideResultEvent):void 
		{	
			var aspDispRpt :AspDispRpt =  evt.result as AspDispRpt;
			
			//check lock
			if(aspDispRpt.status.equals(AspDispRptStatus.Inactive)){
				retrieveAspDispRptEvent.lockFunction(aspDispRpt);
				
			}else if( aspDispRpt.adminStatus.equals(AspDispRptAdminStatus.Submitted) || aspDispRpt.adminStatus.equals(AspDispRptAdminStatus.NotForReview)){
				retrieveAspDispRptEvent.lockFunction(aspDispRpt);
				
			}else if( retrieveAspDispRptEvent.workStationId != aspDispRpt.workstationId && 
				aspDispRpt.adminStatus != AspDispRptAdminStatus.None ){
				retrieveAspDispRptEvent.lockFunction(aspDispRpt);
				
			}else{
				evt.context.dispatchEvent(new ShowFormViewEvent(aspDispRpt));
			}

		}
		
	}	
}
