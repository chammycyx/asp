package  hk.org.ha.event.pms.asp {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveAspHospListByUserEvent extends AbstractTideEvent 
	{
		
		private var _callBackFunction:Function;
		private var _originalScreen:String
		public function RetrieveAspHospListByUserEvent(originalScreen, callBackFunction:Function=null ):void 
		{
			super();
			_originalScreen =originalScreen 
			_callBackFunction= callBackFunction;
		}
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
		public function get originalScreen():String {
			return _originalScreen;
		}
		
	}
}
