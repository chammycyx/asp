package hk.org.ha.control.pms.exception {
	
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.model.pms.vo.sys.SysMsgMap;
	
	import mx.messaging.messages.ErrorMessage;
	
	import org.granite.tide.BaseContext;
	import org.granite.tide.IExceptionHandler;
	
	public class LockTimeoutExceptionHandler implements IExceptionHandler 
	{
		
		public static const LOCK_TIMEOUT:String = "Seam.LockTimeout";         
		
		[In]
		public var sysMsgMap:SysMsgMap
		
		public function accepts(emsg:ErrorMessage):Boolean 
		{
			return emsg.faultCode == LOCK_TIMEOUT;
		}
		
		public function handle(context:BaseContext, emsg:ErrorMessage):void 
		{
			var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp("0056");
			msgProp.setOkButtonOnly = true;
			context.dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
		}
	}
}
