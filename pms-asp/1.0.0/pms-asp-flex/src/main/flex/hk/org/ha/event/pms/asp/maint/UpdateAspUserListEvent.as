package hk.org.ha.event.pms.asp.maint
{
	import mx.collections.ArrayCollection;

	import org.granite.tide.events.AbstractTideEvent;
	
	public class UpdateAspUserListEvent extends AbstractTideEvent
	{
		private var _aspUserList:ArrayCollection;
		
		public function UpdateAspUserListEvent(aspUserList:ArrayCollection=null):void 
		{
			super();
			_aspUserList = aspUserList;
		}
		
		public function get aspUserList():ArrayCollection {
			return _aspUserList;
		}
		
	}
}

