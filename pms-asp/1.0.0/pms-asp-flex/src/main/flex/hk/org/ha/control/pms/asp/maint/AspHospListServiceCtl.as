package hk.org.ha.control.pms.asp.maint
{	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.asp.RetrieveAspHospListByUserEvent;
	import hk.org.ha.event.pms.asp.maint.RetrieveAspHospListEvent;
	import hk.org.ha.event.pms.asp.maint.SetAspHospListEvent;
	import hk.org.ha.model.pms.asp.biz.maint.AspHospListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;

	[Bindable]
	[Name("aspHospListServiceCtl", restrict="false")]
	public class AspHospListServiceCtl
	{

		[In]
		public var aspHospListService:AspHospListServiceBean;
		
		private var maintName:String;
		private var retrieveAspHospListByUserEvent: RetrieveAspHospListByUserEvent;
		
		[Observer]
		public function retrieveAspHospList(evt:RetrieveAspHospListEvent):void
		{
			maintName = evt.maintName;

			var screen:String = maintName =="itemMaint"? "Antibiotics List Maintenance" : "Specialty Maintenance";
			
			
			aspHospListService.retrieveAspHospList(screen, retrieveAspHospListResult);
		}
		
		public function retrieveAspHospListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new SetAspHospListEvent(maintName, evt.result as ArrayCollection));
		}
		
		
		[Observer]
		public function retrieveAspHospListByUser(evt:RetrieveAspHospListByUserEvent):void
		{
			retrieveAspHospListByUserEvent = evt;
			aspHospListService.retrieveAspHospListByUser(evt.originalScreen, retrieveAspHospListByUserResult);
		}
		
		public function retrieveAspHospListByUserResult(evt:TideResultEvent):void
		{
			if (retrieveAspHospListByUserEvent.callBackFunction != null){
				retrieveAspHospListByUserEvent.callBackFunction(evt.result as ArrayCollection);
			}
			
		}
		
		
		
		
	}
}