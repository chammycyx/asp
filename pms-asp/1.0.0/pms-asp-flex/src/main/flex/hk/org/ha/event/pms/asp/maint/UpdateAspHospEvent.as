package hk.org.ha.event.pms.asp.maint
{
	import mx.collections.ArrayCollection;
	import hk.org.ha.model.pms.asp.persistence.AspHosp;
	import org.granite.tide.events.AbstractTideEvent;
		
	public class UpdateAspHospEvent extends AbstractTideEvent
	{
		private var _maintName:String;
		
		private var _aspHosp:AspHosp;
		
		public function UpdateAspHospEvent(maintName:String=null,aspHosp:AspHosp=null):void 
		{
			super();
			_maintName = maintName;
			_aspHosp = aspHosp;
		}
		
		public function get maintName():String {
			return _maintName;
		}
		
		public function get aspHosp():AspHosp {
			return _aspHosp;
		}

	}
}