package hk.org.ha.event.pms.asp.enquiry
{
	import org.granite.tide.events.AbstractTideEvent;
		
	public class RetrieveAspDispRptEvent extends AbstractTideEvent
	{
		private var _workStationId:String;
		private var _aspDispRptId:Number;
		private var _lockFunction:Function;
		
		public function RetrieveAspDispRptEvent( aspDispRptId:Number,workStationId:String , lockFunction:Function):void 
		{
			super();
			_workStationId = workStationId;
			_aspDispRptId = aspDispRptId;
			_lockFunction= lockFunction;
		}
			
		public function get aspDispRptId():Number {
			return _aspDispRptId;
		}
		public function get workStationId():String {
			return _workStationId;
		}
		
		public function get lockFunction():Function {
			return _lockFunction;
		}
	}
}