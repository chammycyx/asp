package hk.org.ha.control.pms.asp.maint
{
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.asp.maint.RetrieveAspUserListEvent;
	import hk.org.ha.event.pms.asp.maint.RefreshUpdateAspUserListEvent;
	import hk.org.ha.event.pms.asp.maint.SetAspUserListEvent;
	import hk.org.ha.event.pms.asp.maint.UpdateAspUserListEvent;
	import hk.org.ha.model.pms.asp.biz.maint.AspUserListServiceBean;
	
	import org.granite.tide.events.TideResultEvent;

	[Bindable]
	[Name("aspUserListServiceCtl", restrict="false")]
	public class AspUserListServiceCtl
	{
		[In]
		public var aspUserListService:AspUserListServiceBean;
		
		[Observer]
		public function retrieveAspUserList(evt:RetrieveAspUserListEvent):void
		{
			aspUserListService.retrieveAspUserList(retrieveAspUserListResult);
		}
		
		public function retrieveAspUserListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new SetAspUserListEvent(evt.result as ArrayCollection));
		}
		
		[Observer]
		public function updateAspUserList(evt:UpdateAspUserListEvent):void
		{
			aspUserListService.updateAspUserList(evt.aspUserList as ArrayCollection, updateAspUserListResult);
		}
		
		public function updateAspUserListResult(evt:TideResultEvent):void
		{
			evt.context.dispatchEvent(new RefreshUpdateAspUserListEvent());
		}
	}
}