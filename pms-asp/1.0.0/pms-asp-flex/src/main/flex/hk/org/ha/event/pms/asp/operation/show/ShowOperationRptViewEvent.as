package hk.org.ha.event.pms.asp.operation.show
{
	import org.granite.tide.events.AbstractTideEvent;
		
	public class ShowOperationRptViewEvent extends AbstractTideEvent
	{
		private var _clearMessages:Boolean;
				
		public function ShowOperationRptViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
			
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}