package hk.org.ha.control.pms.asp.operation
{		
	import hk.org.ha.event.pms.asp.GenExcelDocumentEvent;
	import hk.org.ha.event.pms.asp.operation.RetrieveOperationRptListEvent;
	import hk.org.ha.model.pms.asp.biz.operation.OperationRptServiceBean;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("OperationRptServiceCtl", restrict="false")]
	public class OperationRptServiceCtl
	{
		
		[In]
		public var operationRptService:OperationRptServiceBean;
		public var retrieveOperationRptListEvent: RetrieveOperationRptListEvent
		
		
		[Observer]
		public function retrieveOpreationRptExcelEvent(evt:RetrieveOperationRptListEvent):void
		{
			retrieveOperationRptListEvent = evt;
			operationRptService.retrieveOpertaionRptResult(evt.operationRptCriteria, retrieveOpreationRptExcelEventResult);
		}
		
		public function retrieveOpreationRptExcelEventResult(evt:TideResultEvent):void
		{	var password:String = retrieveOperationRptListEvent.operationRptCriteria.password;
			var passwordFlag:Boolean = retrieveOperationRptListEvent.operationRptCriteria.passwordFlag
			var today:Date = new Date();
			var fileDate:String = (String)(today.getFullYear())+ ((today.getMonth()+1)<10?'0':'')+(today.getMonth()+1)+(today.getDate()<10?'0':'')+today.getDate();
			evt.context.dispatchEvent( new GenExcelDocumentEvent("/excelTemplate/operationRpt.xhtml", "OperationRpt", "operationRpt_"+fileDate , passwordFlag, password) );
		}
		
		
		
//	
		
		
		
	}	
}
