package hk.org.ha.control.pms.asp.rollback
{		
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.event.pms.asp.rollback.RetrieveRollbackResultListEvent;
	import hk.org.ha.event.pms.asp.rollback.RollbackRptListEvent;
	import hk.org.ha.model.pms.asp.biz.rollback.RollbackServiceBean;
	import hk.org.ha.model.pms.asp.persistence.AspDispRpt;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("RollbackServiceCtl", restrict="false")]
	public class RollbackServiceCtl
	{
		
		[In]
		public var rollbackService:RollbackServiceBean;
		public var retrieveRollbackResultListEvent: RetrieveRollbackResultListEvent;
		public var rollbackRptListEvent: RollbackRptListEvent;
		
		
		[Observer]
		public function retrieveAspDispRptList(evt:RetrieveRollbackResultListEvent):void
		{
			retrieveRollbackResultListEvent = evt;
			rollbackService.retrieveRollbackResultList(evt.aspRptCriteria, retrieveAspDispRptListResult);
		}
		
		public function retrieveAspDispRptListResult(evt:TideResultEvent):void
		{
			
			if (retrieveRollbackResultListEvent.callBackFunction != null) {
				retrieveRollbackResultListEvent.callBackFunction(evt.result as ArrayCollection);
			}
		}
		
		[Observer]
		public function rollbackRptList(evt:RollbackRptListEvent):void
		{
			rollbackRptListEvent = evt;
			rollbackService.rollbackResultList(evt.adminStatus, evt.aspRptList, rollbackRptListResult);
		}
		
		public function rollbackRptListResult(evt:TideResultEvent):void
		{
			
			if (rollbackRptListEvent.callBackFunction != null) {
				rollbackRptListEvent.callBackFunction(evt.result as AspDispRpt);
			}
		}
		
		
		
//	
	}	
}
