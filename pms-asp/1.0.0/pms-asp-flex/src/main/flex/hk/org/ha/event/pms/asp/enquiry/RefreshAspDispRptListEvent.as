package  hk.org.ha.event.pms.asp.enquiry {
	
	import hk.org.ha.model.pms.asp.vo.enquiry.AspRptCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RefreshAspDispRptListEvent extends AbstractTideEvent 
	{
		private var _wsErrorFunction:Function;
		private var _lockFunction:Function;
		private var _callBackFunction:Function;
		private var _aspRptCriteria:AspRptCriteria;
		
		public function RefreshAspDispRptListEvent( aspRptCriteria: AspRptCriteria, callBackFunction:Function=null , lockFunction:Function=null, wsErrorFunction:Function= null):void 
		{
			super();
			_wsErrorFunction= wsErrorFunction;
			_lockFunction= lockFunction;
			_callBackFunction= callBackFunction;
			_aspRptCriteria = aspRptCriteria;
		}
		
		
		public function get wsErrorFunction():Function {
			return _wsErrorFunction;
		}
		
		
		public function get lockFunction():Function {
			return _lockFunction;
		}
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
		
		
		public function get aspRptCriteria():AspRptCriteria
		{
			return _aspRptCriteria;
		}
		
	}
}
