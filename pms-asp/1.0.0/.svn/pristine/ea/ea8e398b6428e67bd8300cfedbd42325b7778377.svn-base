<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent 
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:util="hk.org.ha.pms.asp.util.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:mdcs="com.iwobanas.controls.*" 
	xmlns:mdgc="com.iwobanas.controls.dataGridClasses.*"
	xmlns:calendar="hk.org.ha.service.app.util.calendar.*"
	xmlns:rpt="hk.org.ha.view.pms.asp.enquiry.*" 
	width="100%" height="100%"  creationComplete="setupFocusViewportWatcher()">
	
	<fx:Metadata>
		[Name("formView")]
	</fx:Metadata>
	
	<fx:Declarations>
		<mx:DateFormatter id="dateFormatter" formatString="DD-MMM-YYYY"/>
		<mx:DateFormatter id="dateTimeFormatter" formatString="DD-MMM-YYYY JJ:NN"/>
		
	</fx:Declarations>
	
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.events.FlexMouseEvent;
			import mx.managers.PopUpManager;
			import mx.utils.ObjectUtil;
			
			import spark.core.IViewport;
			
			import hk.org.ha.event.pms.asp.enquiry.ExportAspDispRptListEvent;
			import hk.org.ha.event.pms.asp.enquiry.UpdateAspDispRptEvent;
			import hk.org.ha.event.pms.asp.enquiry.show.ShowEnqViewEvent;
			import hk.org.ha.event.pms.asp.enquiry.show.ShowFormViewEvent;
			import hk.org.ha.event.pms.main.show.ShowStartupViewEvent;
			import hk.org.ha.fmk.pms.flex.components.core.MenuItem;
			import hk.org.ha.fmk.pms.flex.components.loadingpopup.CloseLoadingPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.loadingpopup.ShowLoadingPopupEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.fmk.pms.flex.components.window.Window;
			import hk.org.ha.fmk.pms.flex.components.window.WindowCloseEvent;
			import hk.org.ha.fmk.pms.flex.components.window.WindowSwitchEvent;
			import hk.org.ha.model.pms.asp.persistence.AspDispRpt;
			import hk.org.ha.model.pms.asp.udt.AspDispRptAdminStatus;
			import hk.org.ha.model.pms.asp.udt.AspDispRptStatus;
			import hk.org.ha.model.pms.asp.udt.AspType;
			import hk.org.ha.model.pms.asp.udt.ScopeOfRpt;
			import hk.org.ha.model.pms.asp.vo.enquiry.AspRpt;
			import hk.org.ha.model.pms.asp.vo.enquiry.AspRptCriteria;
			import hk.org.ha.model.pms.asp.vo.enquiry.Intervention;
			import hk.org.ha.model.pms.asp.vo.enquiry.Miscellaneous;
			import hk.org.ha.model.pms.asp.vo.enquiry.OutcomeMeasurement;
			import hk.org.ha.model.pms.asp.vo.enquiry.Remark;
			import hk.org.ha.model.pms.vo.sys.SysMsgMap;
			
			
			
			
			
			[In][Bindable]
			public var sysMsgMap:SysMsgMap;
			
			[In]
			public var window:Window;
			
			[Bindable]
			public var indicationDescList:ArrayCollection = new  ArrayCollection();
			
			[Bindable]
			public var labCultureResultList:ArrayCollection = new  ArrayCollection();
			
			[Bindable]
			public var manualCultureResultList:ArrayCollection = new  ArrayCollection();
			
			
			[Bindable]
			private var miscellaneousVisibleFlag:Boolean = true;
			
			[Bindable]
			private var labCultureResultFlag:Boolean;
			
			[Bindable]
			private var labCultureResultPanelHeight:int;
			
			[Bindable]
			private var indicationDescFlag:Boolean;
			
			[Bindable]
			private var aspDispRpt:AspDispRpt;
			
			[Bindable]
			private var bgFlag:Boolean;
			
			[Bindable]
			private var aspRpt:AspRpt;
			
			private var originalAspRpt:AspRpt;
			
			private var msgProp:SystemMessagePopupProp;
			
			[Bindable]
			private var reportDisclaimerDate:Date;
			
			public function init(aspDispRptIn:AspDispRpt ):void{
				
				dispatchEvent(new CloseLoadingPopupEvent());
				
				aspDispRpt = aspDispRptIn as AspDispRpt;
				aspRpt = aspDispRpt.aspRpt as AspRpt;
				originalAspRpt =  ObjectUtil.clone(aspDispRpt.aspRpt) as AspRpt;
				bgFlag = aspDispRpt.rptType == AspType.BG ? true:false;
				
				if(aspRpt.outcomeMeasurement == null){
					aspRpt.outcomeMeasurement = new OutcomeMeasurement();
				}
				
				if(aspRpt.miscellaneous == null){
					aspRpt.miscellaneous = new Miscellaneous();
				}
				
				if(!bgFlag){
					if(aspRpt.intervention == null){
						aspRpt.intervention = new Intervention();
					}
				}
				
				if(aspRpt.remark == null){
					aspRpt.remark = new Remark();
				}
				
				labCultureResultList = new ArrayCollection();
				indicationDescList = new ArrayCollection();
				manualCultureResultList = new ArrayCollection();
				
				if( aspRpt.cultureResultList != null){
					labCultureResultList.addAll(aspRpt.cultureResultList);
					labCultureResultList.refresh();
				}
				
				if( aspRpt.clinicalData.indicationDescList != null){
					indicationDescList.addAll(aspRpt.clinicalData.indicationDescList);
					indicationDescList.refresh();
				}
				
				if(aspRpt.manualCultureResultList != null){
					manualCultureResultList.addAll(aspRpt.manualCultureResultList);
					manualCultureResultList.refresh();
				}
				
				labCultureResultFlag = labCultureResultListFlag();
				indicationDescFlag = indicationDescListFlag();
				labCultureResultPanelHeight = labCultureResultHeight();
				
				if(clinicalDataPanel!= null){
					if(aspRpt.clinicalData.inotrope == null){
						clinicalDataPanel.comboInotrope.selectedItem = null;
					}
					if(aspRpt.clinicalData.drRank == null){
						clinicalDataPanel.comboDrRank.selectedItem = null;
					}
					
					clinicalDataPanel.loadIndication();
					
				}
				
				if(outcomeMeasurementPanel != null){
					if(aspRpt.outcomeMeasurement.auditRank == null){
						outcomeMeasurementPanel.comboAuditRank.selectedItem = null;
						
					}
					outcomeMeasurementPanel.selectOutcomeMeasurement();
				}
				
				
				if(aspRpt.patientDemography.disclaimerDate == null ){
					reportDisclaimerDate = new Date();
				}else{
					reportDisclaimerDate = aspRpt.patientDemography.disclaimerDate;
					
				}
				
			} 
			
			
			public function showMessage(errorCode:String, oKfunc:Function=null , 
														  cancelFunc:Function = null, 
														  okResultOnlyFlag:Boolean = true,
														  messageParam:Array=null):void{
				msgProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageWinHeight = 200;
				msgProp.messageParams = messageParam;
				
				if (oKfunc != null && okResultOnlyFlag){
					msgProp.setOkCancelButton = true;
					msgProp.okHandler = oKfunc;
					msgProp.cancelHandler = cancelFunc;
				}else {
					msgProp.setOkButtonOnly = true;
					msgProp.okHandler = oKfunc;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			
			public function labCultureResultListFlag():Boolean {
				if(labCultureResultList == null || labCultureResultList.length == 0){
					return false;
				}else{
					return true;
				}
			}
			
			
			public function indicationDescListFlag():Boolean {
				if(indicationDescList == null || indicationDescList.length == 0){
					return false;
				}else{
					return true;
				}
			}
			
			public override function onShow():void{
				
				enableButton();
				var menuName:String ='';
				if(aspDispRpt.rptType == AspType.BG){
					menuName = "Big Guns Program Audit Form";
				}else{
					menuName = "IV-to-Oral Switch Program Audit Form";
				}
				
				var menuItem:MenuItem = new MenuItem(menuName , ShowFormViewEvent);
				dispatchEvent(new WindowSwitchEvent(menuItem, null, false));
				
				if(patientDemographyPanel != null){
					patientDemographyPanel.setFocus();
				}
				
				if(scroller != null){
					var vp:IViewport = scroller.viewport;
					vp.verticalScrollPosition = vgroupContainer.layout.getElementBounds(0).top;
				}
			}
			
			
			private function disableButton():void{
				btnSave.enabled = false;
				btnSubmit.enabled = false;
				btnCancel.enabled = false;
				btnClear.enabled = false;
			}
			private function enableButton():void{
				btnSave.enabled = true;
				btnSubmit.enabled = true;
				btnCancel.enabled = true;
				btnClear.enabled = true;
			}
			
			public function clear():void{
				showMessage("0010", confirmClear);
				
			}
			
			public function confirmClear(evt:Event):void{
				var temp:UIComponent = evt.currentTarget as UIComponent;
				PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);
				patientDemographyPanel.clear();
				clinicalDataPanel.clear();
				if(!bgFlag){
					medicationPanel.clearChecklist();
					interventionPanel.clear();
				}
				outcomeMeasurementPanel.clear();
				miscellaneousPanel.clear();
			}
			
			
			public function cancel():void{
				
				aspDispRpt.aspRpt = originalAspRpt;
				aspDispRpt.adminStatus = AspDispRptAdminStatus.None;
				aspDispRpt.workstationId = null;
				
				dispatchEvent(new UpdateAspDispRptEvent(aspDispRpt, cancelResult));
				
			}
			
			public function getEnquiryMenuItem():MenuItem{
				for(var i:int=0; i< window.menuItemList.length;i++){ 
					var mm:MenuItem = window.menuItemList[i] as MenuItem;
					if(mm.name == "Enquiry"){
						return mm;
					}
				}
				return null;
			}
			
			public function cancelResult(modifiedUser:String):void{
				
				//if(result){
				enableButton();
				patientDemographyPanel.cleanErrorString();
				clinicalDataPanel.cleanErrorString();
				outcomeMeasurementPanel.cleanErrorString();
				if(!bgFlag){
					interventionPanel.cleanErrorString();
					medicationPanel.cleanErrorString();
				}
				clinicalDataPanel.cleanManualLabTest();
				clinicalDataPanel.cleanIndication();
				window.closeWindowHandler(new WindowCloseEvent(WindowCloseEvent.CLOSE, ShowStartupViewEvent));
				var menuItem:MenuItem = getEnquiryMenuItem();
				dispatchEvent(new WindowSwitchEvent(menuItem, this, false));
				dispatchEvent(new ShowEnqViewEvent(true));
					
				//}
				
			}
			
			public function saveResult(modifiedUser:String):void{
				
				if(modifiedUser == null){
					export();
					clinicalDataPanel.cleanManualLabTest();
					clinicalDataPanel.cleanIndication();
					enableButton();
					showMessage("0022", showEnqViewFunc, null,false);
				}else{
					showMessage("0076", closeOptimisticLockPopupOkFunc, null,false, [modifiedUser]);
				}
			
			}
			
			public function submitResult(modifiedUser:String):void{
				
				if(modifiedUser == null){
					export();
					clinicalDataPanel.cleanManualLabTest();
					clinicalDataPanel.cleanIndication();
					enableButton();
					showMessage("0009", showEnqViewFunc, null,false);
				}else{
					showMessage("0076", closeOptimisticLockPopupOkFunc, null,false, [modifiedUser]);
				}
				
			}
			
			public function export():void{
				
				var aspRptCriteria:AspRptCriteria = new AspRptCriteria();
				if(bgFlag){
					aspRptCriteria.reportType = AspType.BG;
				}else{
					aspRptCriteria.reportType = AspType.IV;
				}
				
				var idList:ArrayCollection = new ArrayCollection();
				idList.addItem(aspDispRpt.id);
				aspRptCriteria.rptIdList = idList;
				aspRptCriteria.scopeOfRpt = ScopeOfRpt.Full;
				
				dispatchEvent(new ExportAspDispRptListEvent(aspRptCriteria));
				
			}
			
			public function closeOptimisticLockPopupOkFunc(evt:Event):void{
				enableButton();
				dispatchEvent(new CloseLoadingPopupEvent());
				var temp:UIComponent = evt.currentTarget as UIComponent;
				PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);
			}
			
			
			public function showEnqViewFunc(evt:Event):void{
				
				dispatchEvent(new CloseLoadingPopupEvent());
				
				var temp:UIComponent = evt.currentTarget as UIComponent;
				PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);
				window.closeWindowHandler(new WindowCloseEvent(WindowCloseEvent.CLOSE, ShowStartupViewEvent));
				var menuItem:MenuItem = getEnquiryMenuItem();
				dispatchEvent(new WindowSwitchEvent(menuItem, this, false));
				dispatchEvent(new ShowEnqViewEvent(true));
			}
			
			public function save():void{
				disableButton();
				dispatchEvent(new ShowLoadingPopupEvent("Processing ..."));
				
				bindAspRpt();
				aspDispRpt.adminStatus = AspDispRptAdminStatus.None;
				dispatchEvent(new UpdateAspDispRptEvent(aspDispRpt, saveResult));
					
			}
			
			public function submit():void{
			
				showMessage("0008", confirmSubmit);
			}
			
			public function confirmSubmit(evt:Event):void{
				var temp:UIComponent = evt.currentTarget as UIComponent;
				PopUpManager.removePopUp(temp.parentDocument as IFlexDisplayObject);
				
				disableButton();
				if(validate()){
					
					dispatchEvent(new ShowLoadingPopupEvent("Processing ..."));
					
					bindAspRpt();
					aspDispRpt.adminStatus = AspDispRptAdminStatus.Submitted;
					dispatchEvent(new UpdateAspDispRptEvent(aspDispRpt, submitResult));
				}else{
					enableButton();
				}
			}
			
			public function bindAspRpt():void{
				patientDemographyPanel.setPatientDemography();
				clinicalDataPanel.setClinicalData();
				medicationPanel.setMedication();
				if(!bgFlag){
					interventionPanel.setIntervention();
				}
				outcomeMeasurementPanel.setOutcomeMeasurement();
				miscellaneousPanel.setMiscellanousRemark();
				aspDispRpt.status = AspDispRptStatus.Edited;
				
			}
			
			
			public function validate():Boolean{
			
				var vp:IViewport = scroller.viewport;
				
				if(!patientDemographyPanel.validate()){
					vp.verticalScrollPosition = vgroupContainer.layout.getElementBounds(0).top;
					return false;
				}
				
				if(bgFlag){
					if(!clinicalDataPanel.validateBodyTemp()){
						vp.verticalScrollPosition = vgroupContainer.layout.getElementBounds(1).top;
						return false;
					}
				}
				
				if(!clinicalDataPanel.validateTreatment()){
					vp.verticalScrollPosition = vgroupContainer.layout.getElementBounds(1).top;
					return false;
				}
				
				
				if(!clinicalDataPanel.validateLabTestDate()){
					vp.verticalScrollPosition = vgroupContainer.layout.getElementBounds(1).top;
					return false;
				}
				
				
				var panelSeq:int = 3;
				if(!bgFlag){
					
					if(!medicationPanel.validate()){
						vp.verticalScrollPosition = vgroupContainer.layout.getElementBounds(panelSeq).top -100;
						return false;
					}
					
					if(!interventionPanel.validateICFDate()){
						vp.verticalScrollPosition = vgroupContainer.layout.getElementBounds(panelSeq).top;
						return false;
					}
					panelSeq = 4;
				}
				
				if( !outcomeMeasurementPanel.validate()){
					vp.verticalScrollPosition = vgroupContainer.layout.getElementBounds(panelSeq).top;
					return false;
				}
				
				if( !outcomeMeasurementPanel.validateCollectionAuditDate()){
					vp.verticalScrollPosition = vgroupContainer.layout.getElementBounds(panelSeq).top;
					return false;
				}
				
				return true;
				
			}
			
			public function labCultureResultHeight():int {
				var height:int = 0;
				var rowTitleHeight:int = 21;
				var panelTitleHeight :int= 25;
				var rowHeight:int = 37;
				var rowLine :int = 1;
				
				if(labCultureResultList == null || labCultureResultList.length == 0){
					return height + rowTitleHeight + panelTitleHeight;
				}else{
					for(var i:int = 0 ; i < labCultureResultList.length ;i++){
						var data:Object = labCultureResultList.getItemAt(i);
						if(data.organismList == null || data.organismList.length == 0){
							height += (rowHeight + rowLine);
						}else{
							
							for(var j:int = 0 ; j < data.organismList.length ;j++){
								var d:Object = data.organismList.getItemAt(j);
								if(d.antibioticList == null || d.antibioticList.length == 0){
									height += (rowHeight + rowLine);
								}else{
									height += d.antibioticList.length * (rowHeight + rowLine) ;
								}
							}
						}
					}
					return height + rowTitleHeight + panelTitleHeight;
				}
								
			}
			
			
			public function setupFocusViewportWatcher():void {
				addEventListener("focusIn", makeFocusedItemVisible);
			}
			
			
			public function customScrollerEvent(event:FlexMouseEvent):void{
				const vp:IViewport = scroller.viewport;
				if (event.isDefaultPrevented() || !vp || !vp.visible)
					return;
				
				var delta:Number = event.delta;
				var direction:Number = 0;
				var distance:Number = 50;
				
				if (delta < 0){
					direction = 1;
				} else if (delta == 0){
					direction = 0;
				} else {
					direction = -1;
				}
				
				vp.verticalScrollPosition += distance * direction;
				
				
				
				event.preventDefault();
				
			}
			
			public function makeFocusedItemVisible(event:FocusEvent):void {
				const vp:IViewport = scroller.viewport;
				var target:InteractiveObject = InteractiveObject(event.target);
				var originalTarget:InteractiveObject = 	InteractiveObject(focusManager.findFocusManagerComponent(target));
				var viewport:Rectangle = new Rectangle();
				do {
					if (target.parent is SkinnableContainer &&  originalTarget != null) {
						var viewportChanged:Boolean = false;
						var c:SkinnableContainer = target.parent as SkinnableContainer;
					
						viewport.x = c.left as Number;
						viewport.y = c.top as Number;
						viewport.width = 
							c.width / c.scaleX - (c.left as Number )- (c.right as Number);
						viewport.height = 
							c.height / c.scaleY - (c.top as Number) - (c.bottom as Number);
						
						var topLeft:Point = new Point(0, 0);
						var bottomRight:Point = 
							new Point(originalTarget.width, originalTarget.height);
						topLeft = originalTarget.localToGlobal(topLeft);
						topLeft = c.globalToLocal(topLeft);
						bottomRight = originalTarget.localToGlobal(bottomRight);
						bottomRight = c.globalToLocal(bottomRight);
						
						var delta:Number;
						
						if (bottomRight.x > viewport.right) {
							delta = bottomRight.x - viewport.right;
							vp.horizontalScrollPosition += delta;
							topLeft.x -= delta;
							viewportChanged = true;
						}
						
						if (topLeft.x < viewport.left) {
							vp.horizontalScrollPosition -= 
								viewport.left - topLeft.x + 50;
							viewportChanged = true;
						}
						
						if (bottomRight.y > viewport.bottom) {
							delta = bottomRight.y - viewport.bottom;
							vp.verticalScrollPosition += delta;
							topLeft.y -= delta;
							viewportChanged = true;
						}
						
						if (topLeft.y < viewport.top) {
							vp.verticalScrollPosition -= 
								viewport.top - topLeft.y + 50;
							viewportChanged = true;
						}
						
						if (viewportChanged) {
							c.validateNow();
						}
					}
					
					target = target.parent;
				}
					
				while (target != scrollerPanel);

			}
			
			
 			
		]]>
	</fx:Script>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/> 
	</fc:layout>
	<s:VGroup width="100%" height="100%" gap="10" paddingTop="10" paddingRight="10" paddingLeft="10" paddingBottom="10">
		<s:SkinnableContainer id="scrollerPanel" width="997" height="640">
			<s:Scroller id="scroller" width="100%" height="100%" verticalScrollPolicy="on"  mouseWheelChanging="customScrollerEvent(event)">
					<s:VGroup id="vgroupContainer" width="100%" height="100%"  >
						<rpt:PatientDemographyComponent id="patientDemographyPanel" patientDemography="{aspRpt.patientDemography}" 
														sysMsgMap="{sysMsgMap}"		aspRpt="{aspRpt}" bgFlag="{bgFlag}"		
														width="100%" />
						<rpt:ClinicalDataComponent id="clinicalDataPanel" clinicalData="{aspRpt.clinicalData}" 
												   labCultureResultList="{labCultureResultList}"
												   labCultureResultFlag ="{labCultureResultFlag}"
												   aspRpt="{aspRpt}" bgFlag="{bgFlag}" labCultureResultPanelHeight="{labCultureResultPanelHeight}"
												   indicationDescList ="{indicationDescList}"
												   indicationDescFlag ="{indicationDescFlag}" sysMsgMap="{sysMsgMap}"
												   width="100%" />
						<rpt:MedicationComponent id="medicationPanel" medication="{aspRpt.medication}" bgFlag="{bgFlag}" width="100%" sysMsgMap="{sysMsgMap}" />
						<rpt:InterventionComponent id="interventionPanel" intervention="{aspRpt.intervention}" bgFlag="{bgFlag}" width="100%" sysMsgMap="{sysMsgMap}" />
						<rpt:OutcomeMeasurementComponent id="outcomeMeasurementPanel" width="100%"  bgFlag="{bgFlag}"		
														 outcomeMeasurement="{aspRpt.outcomeMeasurement}" sysMsgMap="{sysMsgMap}" />
						<s:BorderContainer width="100%"  minHeight="0" visible="{!bgFlag}" height="{!bgFlag?50:0}" borderColor="0xFFFFFF">
							<s:VGroup  width="100%" height="100%"  >
									<s:Label text="*Specific indications include: Liver abscess and other inadequately drained abscesses, emphysema, cavitating pneumonia, meningitis, intracranial abscesses, endocarditis," />
									<s:Label text="mediastinitis, severe infections during chemotherapy-related neutropenia, severe necrotising soft tissue infections, Staphylococcus aureus / Pseudomonas aeruginosa" />
									<s:Label text="bacteraemia." />
							</s:VGroup>
						</s:BorderContainer>
						<s:BorderContainer width="100%"  height="35" borderColor="0xFFFFFF">
							<s:HGroup width="100%"  paddingRight="5" paddingTop="4" horizontalAlign="left" verticalAlign="middle">
								<s:Label text="Disclaimer: The information provided is as of" />
								<s:TextInput id="txtDisclaimerDate" text="{dateTimeFormatter.format(reportDisclaimerDate)}" width="300" enabled="false"  />
								<s:Label text="and intended for reference only " />
							</s:HGroup>
						</s:BorderContainer>
						<rpt:MiscellanousComponent id="miscellaneousPanel"   width="100%" bgFlag="{bgFlag}" 
												   miscellaneous="{aspRpt.miscellaneous}" 
												   remark="{aspRpt.remark}" />
						<s:BorderContainer width="100%"  height="35" borderColor="0xFFFFFF">
							<s:HGroup width="100%" paddingRight="0" paddingTop="4" horizontalAlign="left" verticalAlign="middle">
								<s:Button id="btnSave" content="Save" width="100" click="save()" />
								<s:Button id="btnSubmit" content="Submit" width="100" click="submit()"/>
								<s:Spacer width="550" />
								<s:Button id="btnClear" content="Clear " width="100" click="clear()" />
								<s:Button id="btnCancel" content="Cancel" width="100" click="cancel()" />
							</s:HGroup>
						</s:BorderContainer>
					</s:VGroup>
			</s:Scroller>
		</s:SkinnableContainer>
	</s:VGroup>
	
	
</fc:ExtendedNavigatorContent>