package hk.org.ha.model.pms.vo.security;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class PropMap implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Map<String, String> properties;
	
	public PropMap() {
		this.properties = new HashMap<String, String>();
	}
	
	public PropMap(Map<String, String> properties) {		
		this.properties = new HashMap<String, String>(properties);
	}

	public void add(String name, String value) {
		properties.put(name, value);
	}
    
    public boolean getValueAsBool(String name, boolean defaultBool) {
    	Boolean value = getValueAsBoolean(name);
    	return (value == null) ? defaultBool : value.booleanValue();
    }
    
    public int getValueAsInt(String name, int defaultInt) {
    	Integer value = getValueAsInteger(name);
    	return (value == null) ? defaultInt : value.intValue();
    }
    
    public Integer getValueAsInteger(String name) {
        String value = getValue(name);
        return !StringUtils.isEmpty(value) ? Integer.valueOf(value) : null;
    }
    
    public Boolean getValueAsBoolean(String name) {
        String value = getValue(name);
        return !StringUtils.isEmpty(value) ? asBoolean(value) : null;
    }
    
    private Boolean asBoolean(String value) {
    	if ("Y".equalsIgnoreCase(value) || Boolean.valueOf(value).booleanValue()) {
    		return Boolean.TRUE;
    	} else {
    		return Boolean.FALSE;
    	}
    }
        
    public String getValue(String name) {
    	return properties.get(name);
    }

	public Map<String, String> getProperties() {
		return properties;
	}    
}