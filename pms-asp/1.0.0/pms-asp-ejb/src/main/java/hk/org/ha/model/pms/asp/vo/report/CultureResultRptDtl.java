package hk.org.ha.model.pms.asp.vo.report;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

public class CultureResultRptDtl {
	private String specimen;

	private String labNum;

	private Date specimenDate;

	private Date refDate;

	private Date rptDate;

	private String organism;

	private String antibiotics1;

	private String sentivitive1;

	private String antibiotics2;

	private String sentivitive2;

	public String getSpecimen() {
		return specimen;
	}

	public void setSpecimen(String specimen) {
		this.specimen = specimen;
	}

	public String getLabNum() {
		return labNum;
	}

	public void setLabNum(String labNum) {
		this.labNum = labNum;
	}

	public Date getSpecimenDate() {
		return specimenDate;
	}

	public void setSpecimenDate(Date specimenDate) {
		this.specimenDate = specimenDate;
	}

	public Date getRefDate() {
		return refDate;
	}

	public void setRefDate(Date refDate) {
		this.refDate = refDate;
	}

	public Date getRptDate() {
		return rptDate;
	}

	public void setRptDate(Date rptDate) {
		this.rptDate = rptDate;
	}

	public String getOrganism() {
		return organism;
	}

	public void setOrganism(String organism) {
		this.organism = organism;
	}

	public String getAntibiotics1() {
		return antibiotics1;
	}

	public void setAntibiotics1(String antibiotics1) {
		this.antibiotics1 = antibiotics1;
	}

	public String getSentivitive1() {
		return sentivitive1;
	}

	public void setSentivitive1(String sentivitive1) {
		this.sentivitive1 = sentivitive1;
	}

	public String getAntibiotics2() {
		return antibiotics2;
	}

	public void setAntibiotics2(String antibiotics2) {
		this.antibiotics2 = antibiotics2;
	}

	public String getSentivitive2() {
		return sentivitive2;
	}

	public void setSentivitive2(String sentivitive2) {
		this.sentivitive2 = sentivitive2;
	}

	public boolean isEmptyCultureResult() {
		return StringUtils.isEmpty(specimen) &&
				StringUtils.isEmpty(labNum) &&
				this.specimenDate == null &&
				this.refDate == null &&
				this.rptDate == null &&
				StringUtils.isEmpty(organism) &&
				StringUtils.isEmpty(antibiotics1) &&
				StringUtils.isEmpty(sentivitive1) &&
				StringUtils.isEmpty(antibiotics2) &&
				StringUtils.isEmpty(sentivitive2);
	}



}
