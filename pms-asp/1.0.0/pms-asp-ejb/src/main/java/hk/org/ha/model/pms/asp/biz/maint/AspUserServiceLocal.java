package hk.org.ha.model.pms.asp.biz.maint;

import javax.ejb.Local;

import hk.org.ha.model.pms.asp.persistence.AspUser;

@Local
public interface AspUserServiceLocal {
	AspUser retrieveAspUser(String userCode);
	
	void deleteAspUser(AspUser aspUser);
	
	void destroy();
}
