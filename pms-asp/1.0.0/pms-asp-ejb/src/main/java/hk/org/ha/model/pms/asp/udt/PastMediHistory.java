package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum PastMediHistory implements StringValuedEnum{
	DM("D", "DM"),
	HT("H", "HT"),
	IHD("I", "IHD"),
	COAD("C", "COAD"),
	ESRF("E", "ERSF"),
	Others("O", "Others");
	
	
	private final String dataValue;
	
	private final String displayValue;
	
	PastMediHistory(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static PastMediHistory dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(PastMediHistory.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<PastMediHistory> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<PastMediHistory> getEnumClass() {
    		return PastMediHistory.class;
    	}
    }
}
