package hk.org.ha.model.pms.asp.persistence;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.util.StringCollectionConverter;
import hk.org.ha.model.pms.asp.udt.AspDispItemStatus;
import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.model.pms.persistence.VersionEntity;
import hk.org.ha.model.pms.asp.udt.Gender;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedProperty;

@Entity
@Table(name = "ASP_DISP_ITEM")
public class AspDispItem extends VersionEntity {

	private static final long serialVersionUID = 1L;

	private static final String DAY_UNIT = "day(s)";
	
	private static final String MONTH_UNIT = "month(s)";
	
	private static final String YEAR_UNIT = "year(s)";
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aspDispItemSeq")
	@SequenceGenerator(name = "aspDispItemSeq", sequenceName = "SQ_ASP_DISP_ITEM", initialValue = 100000000)
	private Long id;
	
	@Column(name = "CASE_NUM", length = 12)
	private String caseNum;
	
	@Column(name = "HOSP_CODE", nullable = false, length = 3)
	private String hospCode;
	
	@Column(name = "ITEM_CODE", nullable = false, length = 6)
	private String itemCode;
	
	@Column(name = "ITEM_NUM")
	private Integer itemNum;
	
	@Column(name = "HKID", nullable = false, length = 12)
	private String hkid;
	
	@Column(name = "PAT_KEY", length = 8)
	private String patKey;
	
	@Column(name = "DISP_QTY", precision=19, scale=4)
	private BigDecimal dispQty;
	
	@Column(name = "DISP_DATE")
	@Temporal(TemporalType.DATE)
	private Date dispDate;	

	@Column(name = "DRUG_NAME", length = 59)
	private String drugName;
	
	@Column(name = "STRENGTH", length = 59)
	private String strength;
	
	@Column(name = "FORM_CODE", length = 3)
	private String formCode;
	
	@Column(name = "FORM_DESC", length = 30)
	private String formDesc;
	
	@Column(name = "FREQ_CODE", length = 7)
	private String freqCode;
	
	@Column(name = "FREQ_DESC", length = 42)
	private String freqDesc;
	
	@Column(name = "DOSE_QTY", length=19)
	private String doseQty;
	
	@Column(name = "DOSE_UNIT", length = 15)
	private String doseUnit;
	
	@Column(name = "WARD_CODE", length = 4)
	private String wardCode;
	
	@Column(name = "SPEC_CODE", length = 4)
	private String specCode;
	
	@Column(name = "START_DATE")
	@Temporal(TemporalType.DATE)
	private Date startDate;
	
	@Column(name = "PAT_HOSP_CODE", length = 3)
	private String patHospCode;
	
	@Column(name = "DISP_ORDER_ID", precision = 19, scale = 0)
	private Long dispOrderId;
	
	@Column(name = "BATCH_DATE")
	@Temporal(TemporalType.DATE)
	private Date batchDate;
	
	@Converter(name = "AspDispItem.status", converterClass = AspDispItemStatus.Converter.class)
    @Convert("AspDispItem.status")
	@Column(name = "STATUS")
	private AspDispItemStatus status;
	
	@Converter(name = "AspDispItem.aspType", converterClass = AspType.Converter.class)
    @Convert("AspDispItem.aspType")
	@Column(name="ASP_TYPE", nullable = false)
	private AspType aspType;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ASP_DISP_RPT_ID")
	private AspDispRpt aspDispRpt;

	@Column(name = "PAT_NAME", length = 48)
	private String patName;
	
	@Column(name = "DOB")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dob;
	
	@Converter(name = "AspDispItem.sex", converterClass = Gender.Converter.class)
    @Convert("AspDispItem.sex")
	@Column(name = "SEX", length = 1)
	private Gender sex;
	
	@Converter(name = "AspDispItem.ccCode", converterClass = StringCollectionConverter.class)
	@Convert("AspDispItem.ccCode")
	@Column(name = "CC_CODE", length = 100)
	private List<String> ccCode;
	
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ADMISSION_DATE")
	private Date admissionDate;
	
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DISCHARGE_DATE")
    private Date dischargeDate;   
    
    @Column(name = "BED_NUM", length = 5)
	private String bedNum;
	
	@Column(name = "PAS_SPEC_CODE", length = 4)
	private String pasSpecCode;
	
	@Column(name = "PAS_WARD_CODE", length = 4)
	private String pasWardCode;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Integer getItemNum() {
		return itemNum;
	}

	public void setItemNum(Integer itemNum) {
		this.itemNum = itemNum;
	}

	public String getPatKey() {
		return patKey;
	}

	public void setPatKey(String patKey) {
		this.patKey = patKey;
	}

	public BigDecimal getDispQty() {
		return dispQty;
	}

	public void setDispQty(BigDecimal dispQty) {
		this.dispQty = dispQty;
	}

	public Date getDispDate() {
		return (dispDate != null) ? new Date(dispDate.getTime()) : null;
	}

	public void setDispDate(Date dispDate) {
		this.dispDate = new Date(dispDate.getTime());
	}

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getDrugName() {
		return drugName;
	}

	public void setDrugName(String drugName) {
		this.drugName = drugName;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getFormDesc() {
		return formDesc;
	}

	public void setFormDesc(String formDesc) {
		this.formDesc = formDesc;
	}

	public String getFreqCode() {
		return freqCode;
	}

	public void setFreqCode(String freqCode) {
		this.freqCode = freqCode;
	}

	public String getFreqDesc() {
		return freqDesc;
	}

	public void setFreqDesc(String freqDesc) {
		this.freqDesc = freqDesc;
	}

	public String getDoseQty() {
		return doseQty;
	}

	public void setDoseQty(String doseQty) {
		this.doseQty = doseQty;
	}

	public String getDoseUnit() {
		return doseUnit;
	}

	public void setDoseUnit(String doseUnit) {
		this.doseUnit = doseUnit;
	}

	public String getWardCode() {
		return wardCode;
	}

	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public AspType getAspType() {
		return aspType;
	}

	public void setAspType(AspType aspType) {
		this.aspType = aspType;
	}

	public AspDispRpt getAspDispRpt() {
		return aspDispRpt;
	}

	public void setAspDispRpt(AspDispRpt aspDispRpt) {
		this.aspDispRpt = aspDispRpt;
	}
	
	public Date getStartDate() {
		return (startDate != null)?new Date(startDate.getTime()):null;
	}

	public void setStartDate(Date startDate) {
		this.startDate = new Date(startDate.getTime());
	}

	public Date getBatchDate() {
		return (batchDate != null)?new Date(batchDate.getTime()):null;
	}

	public void setBatchDate(Date batchDate) {
		this.batchDate = new Date(batchDate.getTime());
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getPatHospCode() {
		return patHospCode;
	}

	public void setPatHospCode(String patHospCode) {
		this.patHospCode = patHospCode;
	}

	public Long getDispOrderId() {
		return dispOrderId;
	}

	public void setDispOrderId(Long dispOrderId) {
		this.dispOrderId = dispOrderId;
	}

	public AspDispItemStatus getStatus() {
		return status;
	}

	public void setStatus(AspDispItemStatus status) {
		this.status = status;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Gender getSex() {
		return sex;
	}

	public void setSex(Gender sex) {
		this.sex = sex;
	}

	public List<String> getCcCode() {
		return ccCode;
	}

	public void setCcCode(List<String> ccCode) {
		this.ccCode = ccCode;
	}

	public Date getAdmissionDate() {
		return admissionDate;
	}

	public void setAdmissionDate(Date admissionDate) {
		this.admissionDate = admissionDate;
	}

	public Date getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getPasSpecCode() {
		return pasSpecCode;
	}

	public void setPasSpecCode(String pasSpecCode) {
		this.pasSpecCode = pasSpecCode;
	}

	public String getPasWardCode() {
		return pasWardCode;
	}

	public void setPasWardCode(String pasWardCode) {
		this.pasWardCode = pasWardCode;
	}
	
	@ExternalizedProperty
	public String getAge() {
		String dateUnit = getDateUnit();
		if (dateUnit == null) {
			return null;
		}

		Calendar today = Calendar.getInstance();
		clearTimestamp(today);
		
		Calendar dobNormal = Calendar.getInstance();
		dobNormal.setTime(dob); 
		
		Integer yearDiff = today.get(Calendar.YEAR) - dobNormal.get(Calendar.YEAR);
		Integer monthDiff = today.get(Calendar.MONTH) - dobNormal.get(Calendar.MONTH);
		Integer dayDiff = today.get(Calendar.DATE) - dobNormal.get(Calendar.DATE);

		if (dayDiff < 0) {
			monthDiff = monthDiff - 1;
		}
		
		if (monthDiff < 0) {
			yearDiff = yearDiff - 1;
			monthDiff = monthDiff + 12;
		}

		if (DAY_UNIT.equals(dateUnit)) {
			return Long.toString((today.getTimeInMillis() - dobNormal.getTimeInMillis()) / (24 * 60 * 60 * 1000));
		}
		else if (MONTH_UNIT.equals(dateUnit)) {
			return Integer.toString((yearDiff * 12) + monthDiff);
		}
		else if (YEAR_UNIT.equals(dateUnit)) {
			return Integer.toString(yearDiff);
		}
		else {
			return null;
		}
	}
	
	@ExternalizedProperty
	public String getDateUnit() {
		if (dob == null) {
			return null;
		}
		
		Calendar dobAfter3m = Calendar.getInstance();
		dobAfter3m.setTime(dob);
		dobAfter3m.add(Calendar.MONTH, 3);
		clearTimestamp(dobAfter3m);

		Calendar today = Calendar.getInstance();
		clearTimestamp(today);
		
		Calendar dobAfter3y = Calendar.getInstance();
		dobAfter3y.setTime(dob);
		dobAfter3y.add(Calendar.YEAR, 3);
		clearTimestamp(dobAfter3y);
		
		if (dobAfter3m.after(today) || dobAfter3m.equals(today)) {
			return DAY_UNIT;
		} else if (dobAfter3m.before(today) && dobAfter3y.after(today)) {
			return MONTH_UNIT;
		} else {
			return YEAR_UNIT;
		}
	}
	
	private void clearTimestamp(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}
	
}
