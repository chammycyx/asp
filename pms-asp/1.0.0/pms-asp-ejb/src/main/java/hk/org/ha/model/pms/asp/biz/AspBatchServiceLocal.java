package hk.org.ha.model.pms.asp.biz;

import javax.ejb.Local;

import hk.org.ha.service.biz.pms.asp.interfaces.AspBatchServiceJmsRemote;

@Local
public interface AspBatchServiceLocal extends AspBatchServiceJmsRemote {

}
