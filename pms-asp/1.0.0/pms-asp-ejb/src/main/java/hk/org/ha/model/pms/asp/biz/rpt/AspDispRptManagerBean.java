package hk.org.ha.model.pms.asp.biz.rpt;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.epr.EprUnreachableException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.asp.biz.AspDispItemManagerLocal;
import hk.org.ha.model.pms.asp.biz.AspItemManagerLocal;
import hk.org.ha.model.pms.asp.biz.SeqRemapper;
import hk.org.ha.model.pms.asp.biz.SeqTableManagerLocal;
import hk.org.ha.model.pms.asp.biz.alert.AlertProfileManagerLocal;
import hk.org.ha.model.pms.asp.biz.epr.EprManagerLocal;
import hk.org.ha.model.pms.asp.persistence.AspDispItem;
import hk.org.ha.model.pms.asp.persistence.AspDispRpt;
import hk.org.ha.model.pms.asp.persistence.AspHospSpec;
import hk.org.ha.model.pms.asp.udt.AspDispItemStatus;
import hk.org.ha.model.pms.asp.udt.AspDispRptAdminStatus;
import hk.org.ha.model.pms.asp.udt.AspDispRptStatus;
import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.model.pms.asp.udt.Gender;
import hk.org.ha.model.pms.asp.vo.enquiry.AllergyHistory;
import hk.org.ha.model.pms.asp.vo.enquiry.AspRpt;
import hk.org.ha.model.pms.asp.vo.enquiry.ClinicalData;
import hk.org.ha.model.pms.asp.vo.enquiry.CultureResult;
import hk.org.ha.model.pms.asp.vo.enquiry.Medication;
import hk.org.ha.model.pms.asp.vo.enquiry.PatientDemography;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.Patient;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;
import hk.org.ha.model.pms.udt.medprofile.MedProfileOrderType;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpAspServiceJmsRemote;
import hk.org.ha.service.pms.asa.interfaces.pas.PasServiceJmsRemote;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

@AutoCreate
@Stateless
@Name("aspDispRptManager")
@MeasureCalls
public class AspDispRptManagerBean implements AspDispRptManagerLocal {

	private static final String MSG_CODE_HKPMI_NOT_AVAIL = "0045";
	private static final String MSG_CODE_EPR_NOT_AVAIL = "0046";
	private static final String DATE_PATTERN = "yyyyMMdd";
	private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern(DATE_PATTERN);
	private static final String HKPMI_DOB_FOR_CHECK = "19000101";
	
	@PersistenceContext(unitName="PMSCOR1_ASP")
	private EntityManager em;

	@In
	private SeqRemapper seqRemapper;
	
	@In
	private SeqTableManagerLocal seqTableManager;
	
	@In
	private AlertProfileManagerLocal alertProfileManager;
	
	@In
	private AspDispItemManagerLocal aspDispItemManager;
	
	@In
	private EprManagerLocal eprManager;
	
	@In
	private CorpAspServiceJmsRemote corpAspServiceProxy;
	
	@In
	private PasServiceJmsRemote pasServiceProxy;
	
	@In
	private AspItemManagerLocal aspItemManager;
	
	@Logger
	private Log logger;
	
	@Override
	public String refreshAspDispRptList(List<Long> aspDispItemIdList, String g6pdCode) throws RuntimeException
	{
		List<AspDispItem> aspDispItemList = aspDispItemManager.retrieveAspDispItemListById(aspDispItemIdList);
		List<String> patHospCodeList = new ArrayList<String>();
		if(aspDispItemList != null && !aspDispItemList.isEmpty())
		{
			patHospCodeList.addAll(corpAspServiceProxy.retrievePatHospCodeList(aspDispItemList.get(0).getHospCode()));
		}
		return generateNewAspDispRptList(aspDispItemList, patHospCodeList, g6pdCode);
	}
	
	private Patient retrievePasPatientByCaseNum(String patHospCode, String caseNum) throws UnreachableException
	{
		Patient patient = null;

		try {
			patient = pasServiceProxy.retrievePasPatientByCaseNum(patHospCode, caseNum);

		} catch (PasException e) {
		}
		return patient;
	}
	
	@Override
	public String generateNewAspDispRptList(List<AspDispItem> aspDispItemList, List<String> patHospCodeList, String g6pdCode) throws RuntimeException
	{
		try
		{
			List<AspHospSpec> newHospSpecList = new ArrayList<AspHospSpec>();
			
			logger.info("Generate asp report starts");
			for(AspDispItem aspDispItem : aspDispItemList)
			{
				Date today = new Date();
				String patKey = aspDispItem.getPatKey();
				String hkid = aspDispItem.getHkid();
				String itemCode = aspDispItem.getItemCode();
				Date dob = aspDispItem.getDob();
				List<String> ccCode = aspDispItem.getCcCode();
				String age = aspDispItem.getAge();
				String patName = aspDispItem.getPatName();
				String bedNum = aspDispItem.getBedNum();
				String pasSpecCode = aspDispItem.getPasSpecCode();
				String pasWardCode = aspDispItem.getPasWardCode();
				Date admDate = aspDispItem.getAdmissionDate();
				Date dischargeDate = aspDispItem.getDischargeDate();

				Gender sex = aspDispItem.getSex();
				String caseNum = aspDispItem.getCaseNum();
				String drugName = aspDispItem.getDrugName();
				String doseQty = aspDispItem.getDoseQty();
				String doseUnit = aspDispItem.getDoseUnit();
				String specCode = aspDispItem.getSpecCode();
				String wardCode = aspDispItem.getWardCode();
				String hospCode = aspDispItem.getHospCode();
				String patHospCode = aspDispItem.getPatHospCode();
				Date batchDate = aspDispItem.getBatchDate();
				AspType aspType = aspDispItem.getAspType();
				
				logger.info("Generate asp report for case: "+caseNum+ " hospCode: "+hospCode+" drugName: "+drugName+" disp order id: "+aspDispItem.getDispOrderId());

				String newSerialNum;
				
				String targetInstCode = seqRemapper.getTargetInstCode(hospCode, wardCode);
				if(!StringUtils.isBlank(targetInstCode))
				{
					newSerialNum = seqTableManager.retrieveAspDispRptSerialNum(targetInstCode, aspType);
				}
				else
				{
					newSerialNum = seqTableManager.retrieveAspDispRptSerialNum(hospCode, aspType);
				}
				
				AspDispRpt newDispRpt = new AspDispRpt();
				AspRpt newAspRpt = new AspRpt();
				PatientDemography patientDemo = new PatientDemography();

				Patient patient = null;
				if(!StringUtils.isBlank(caseNum))
				{
					if(StringUtils.isBlank(patHospCode))
					{
						if(patHospCodeList != null)
						{
							for(String tempHospCode:patHospCodeList)
							{
								patient = retrievePasPatientByCaseNum(tempHospCode, caseNum);
								if(patient != null)
								{
									patHospCode = tempHospCode;
									break;
								}
							}
						}
					}
					else
					{
						patient = retrievePasPatientByCaseNum(patHospCode, caseNum);
					}
				}
				
				if(patient != null)
				{
					hkid = patient.getHkid();
					dob = patient.getDob();
					ccCode = patient.getCcCode();
					if(dob!= null && DATE_FORMATTER.parseDateTime(HKPMI_DOB_FOR_CHECK).toDate().equals(dob))
					{
						age = "";
					}
					else
					{
						age = patient.getAge();
					}
					patName = patient.getName();
					bedNum = "";
					pasSpecCode = "";
					pasWardCode = "";
					admDate = null;
					dischargeDate = null;
					
					if(patient.getSex() != null)
					{
						sex = Gender.dataValueOf(patient.getSex().getDataValue());
					}
					else
					{
						sex = Gender.Unknown;
					}
		
					if(patient.getMedCaseList() != null && !patient.getMedCaseList().isEmpty())
					{
						MedCase medCase = patient.getMedCaseList().get(0);
						bedNum = medCase.getPasBedNum();
						pasSpecCode = medCase.getPasSpecCode();
						pasWardCode = medCase.getPasWardCode();
						admDate = medCase.getAdmissionDatetime();
						dischargeDate = medCase.getDischargeDatetime();
					}
				}
				else
				{
					patient = new Patient();
					patient.setHkid(hkid);
					patient.setCcCode(ccCode);
					patient.setDob(dob);
					patient.setName(patName);
					patient.setSex(hk.org.ha.model.pms.udt.Gender.dataValueOf(sex.getDataValue()));
				}

				if(dischargeDate != null)
				{
					newDispRpt.setDischargeFlag(Boolean.TRUE);
				}
				else
				{
					newDispRpt.setDischargeFlag(Boolean.FALSE);
				}
				
				AllergyHistory allergyHistory;
				if(!StringUtils.isBlank(caseNum))
				{
					allergyHistory = alertProfileManager.retrieveAspRptAllergyData(patient, caseNum, patHospCode, g6pdCode);
				}
				else
				{
					allergyHistory = new AllergyHistory();
				}

				String recentAdmin = eprManager.retrieveRecentAdmFromEpr(patKey, batchDate);
				
				AspRpt rptLabData = eprManager.retrieveLabDataFromEpr(patKey, batchDate);			
				ClinicalData clinicalData = rptLabData.getClinicalData();
				Medication medication = rptLabData.getMedication();
				
				List<CultureResult> cultureResultList = eprManager.retrieveCultureResultFromEpr(patKey, batchDate);

				if(AspType.BG.equals(aspType))
				{
					clinicalData.setIndicationDescList(aspItemManager.retrieveBGIndicationByItemCode(itemCode));
				}
				
				patientDemo.setSerialNum(newSerialNum);
				patientDemo.setHkid(hkid);
				patientDemo.setCaseNum(caseNum);
				patientDemo.setHospCode(hospCode);
				patientDemo.setSex(sex.getDataValue());
				patientDemo.setAge(age);
				patientDemo.setRptDate(today);
				patientDemo.setPatName(patName);
				patientDemo.setDob(dob);
				patientDemo.setAdmDate(admDate);
				patientDemo.setPhsSpec(specCode);
				patientDemo.setPhsWard(wardCode);
				patientDemo.setIpasSpec(pasSpecCode);
				patientDemo.setIpasWard(pasWardCode);
				patientDemo.setBedNum(bedNum);
				
				patientDemo.setAllergyHistory(allergyHistory.getAllergy());
				patientDemo.setAlertProfileErrMsg(allergyHistory.getErrMsg());
				if(allergyHistory.getStatus() != null)
				{
					patientDemo.setAlertProfileStatus(allergyHistory.getStatus());
				}
				
				patientDemo.setRecentAdmin(recentAdmin);
				
				patientDemo.setDisclaimerDate(new Date());
				
				clinicalData.setAntibioticName(drugName);
				clinicalData.setDosage(doseQty);
				clinicalData.setDosageUnit(doseUnit);
				clinicalData.setFrequency(aspDispItem.getFreqDesc());
				clinicalData.setStartDate(aspDispItem.getStartDate());
				
				newAspRpt.setAspType(aspType);
				newAspRpt.setPatientDemography(patientDemo);
				newAspRpt.setClinicalData(clinicalData);
				newAspRpt.setMedication(medication);
				newAspRpt.setCultureResultList(cultureResultList);
				newDispRpt.setAspRpt(newAspRpt);
				newDispRpt.setAspDispItem(aspDispItem);
				
				newDispRpt.setDischargeFlag(Boolean.FALSE);
				newDispRpt.setCcCode(ccCode);
				newDispRpt.setDispDate(aspDispItem.getDispDate());
				newDispRpt.setDob(dob);
				newDispRpt.setDrugName(drugName);
				newDispRpt.setHkid(hkid);
				newDispRpt.setHospCode(hospCode);
				newDispRpt.setPatKey(patKey);
				newDispRpt.setPatName(patName);
				newDispRpt.setRptDate(today);
				newDispRpt.setRptType(aspType);
				newDispRpt.setSex(sex);
				newDispRpt.setSerialNum(newSerialNum);
				newDispRpt.setPasSpecCode(pasSpecCode);
				newDispRpt.setStatus(AspDispRptStatus.New);
				newDispRpt.setAdminStatus(AspDispRptAdminStatus.None);
				newDispRpt.setPasWardCode(pasWardCode);
	
				AspDispRpt oriDispRpt = aspDispItem.getAspDispRpt();
				if(oriDispRpt != null)
				{
					oriDispRpt.setStatus(AspDispRptStatus.Inactive);
					em.merge(oriDispRpt);
				}

				aspDispItem.setPatHospCode(patHospCode);
				aspDispItem.setStatus(AspDispItemStatus.Active);
				aspDispItem.setAspDispRpt(newDispRpt);
				em.merge(aspDispItem);
				
				if(!StringUtils.isBlank(pasSpecCode))
				{
					AspHospSpec newSpec = new AspHospSpec();
					newSpec.setHospCode(hospCode);
					newSpec.setSpecCode(pasSpecCode);
					newSpec.setAspType(aspType);
					newSpec.setVisibleFlag(true);
					if(!newHospSpecList.contains(newSpec))
					{
						newHospSpecList.add(newSpec);
					}
				}

			}
			
			if(!newHospSpecList.isEmpty())
			{
				corpAspServiceProxy.updateAspHospSpec(newHospSpecList);
			}
			
			em.flush();
			em.clear();

			logger.info("Generate asp report ends");
			return "";
		}
		catch(UnreachableException e) 
		{
			return MSG_CODE_HKPMI_NOT_AVAIL;
		}
		catch(EprUnreachableException e) 
		{
			return MSG_CODE_EPR_NOT_AVAIL;
		}
		catch(Exception e)
		{
			logger.error("generateNewAspDispRptList Exception=#0",e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void updateAspDispRptDischargeFlag(MedProfileOrder medProfileOrder) {
		List<AspDispItem> aspDispItemList = aspDispItemManager.retrieveAspDispItemByCaseNum(medProfileOrder.getCaseNum(), medProfileOrder.getPatHospCode());
		for(AspDispItem aspDispItem:aspDispItemList)
		{
			AspDispRpt aspDispRpt = aspDispItem.getAspDispRpt();
			if(aspDispRpt != null)
			{
				if(MedProfileOrderType.Discharge.equals(medProfileOrder.getType()))
				{
					aspDispRpt.setDischargeFlag(Boolean.TRUE);
				}
				else
				{
					aspDispRpt.setDischargeFlag(Boolean.FALSE);
				}
			}
		}
		em.flush();
		logger.info("updateAspDispRptDischargeFlag inputCaseNum=#0,patHospCode=#1,orderType=#2",medProfileOrder.getCaseNum(),medProfileOrder.getPatHospCode(),medProfileOrder.getType());
	}
	
	@Remove
	public void destroy() {
	}
}
