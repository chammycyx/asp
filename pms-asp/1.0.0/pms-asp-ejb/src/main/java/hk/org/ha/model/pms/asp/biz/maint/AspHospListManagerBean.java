package hk.org.ha.model.pms.asp.biz.maint;

import hk.org.ha.fmk.pms.cache.CacheResult;
import hk.org.ha.fmk.pms.cache.CacheResultCleanUp;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.persistence.AspHosp;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpAspServiceJmsRemote;

import java.util.List;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("aspHospListManager")
@MeasureCalls
public class AspHospListManagerBean implements AspHospListManagerLocal {

	@In
	private CorpAspServiceJmsRemote corpAspServiceProxy;

	@Override
	@CacheResult(name="aspHospList")
	public List<AspHosp> retrieveAspHospList() {
		
		return corpAspServiceProxy.retrieveAspHospList();
		
	}

	@Override
	@CacheResultCleanUp(name="aspHospList")
	public void clearAspHospList(){
		
	}
	

}
