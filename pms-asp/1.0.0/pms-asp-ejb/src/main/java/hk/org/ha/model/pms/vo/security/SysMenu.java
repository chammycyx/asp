package hk.org.ha.model.pms.vo.security;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "applMenus")
public class SysMenu {

	@XmlElement(name="menu", required = true)
    private List<SysMenuDtl> menu;

	public List<SysMenuDtl> getMenu() {
		if (menu == null) {
            menu = new ArrayList<SysMenuDtl>();
        }
        return this.menu;
    }
	public void setMenu(List<SysMenuDtl> menu) {
		this.menu = menu;
	}
}