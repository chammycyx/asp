package hk.org.ha.model.pms.biz.sys;

import hk.org.ha.fmk.pms.sys.SysMsgResolver;

import javax.ejb.Local;

@Local
public interface SystemMessageManagerLocal extends SysMsgResolver{
}