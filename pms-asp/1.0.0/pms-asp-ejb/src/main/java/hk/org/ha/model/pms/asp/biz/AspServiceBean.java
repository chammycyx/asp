package hk.org.ha.model.pms.asp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.biz.rpt.AspDispRptManagerLocal;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("aspService")
@MeasureCalls
public class AspServiceBean implements AspServiceLocal {

	@In
	AspDispItemManagerLocal aspDispItemManager;
	
	@In
	AspDispRptManagerLocal aspDispRptManager;
	
	@In
	SystemMessageManagerLocal systemMessageManager;
	
	@Override
	public void receiveDispOrderItemList(List<DispOrderItem> dispOrderItemList, List<String> patHospCodeList, String g6pdCode, Date batchDate) throws RuntimeException
	{
		aspDispItemManager.constructAndSaveAspDispItem(dispOrderItemList, patHospCodeList, g6pdCode, batchDate);
	}

	@Override
	public void receivePatientTrx(MedProfileOrder medProfileOrder)
	{
		aspDispRptManager.updateAspDispRptDischargeFlag(medProfileOrder);
	}

}
