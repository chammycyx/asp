package hk.org.ha.model.pms.asp.vo.enquiry;

import hk.org.ha.model.pms.asp.udt.AdmissionSource;
import hk.org.ha.model.pms.asp.udt.AlertProfileStatus;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class PatientDemography {

	@XmlElement
	private String hkid;
	
	@XmlElement
	private String caseNum;
	
	@XmlElement
	private String serialNum;
	
	@XmlElement
	private String sex;
	
	@XmlElement
	private String age;
	
	@XmlElement
	private Date rptDate;
	
	@XmlElement
	private Date disclaimerDate;
	
	@XmlElement
	private String patName;
	
	@XmlElement
	private Date dob;
	
	@XmlElement
	private Date admDate;
	
	@XmlElement
	private String phsSpec;
	
	@XmlElement
	private String phsWard;
	
	@XmlElement
	private String ipasSpec;
	
	@XmlElement
	private String ipasWard;
	
	@XmlElement
	private String bedNum;
	
	@XmlElement( required = true)
	private String allergyHistory;
	
	@XmlElement
	private String alertProfileErrMsg;
	
	@XmlElement
	private AlertProfileStatus alertProfileStatus;
	
	@XmlElement
	private String recentAdmin;
	
	@XmlElement
	private String hospCode;
	
	@XmlElement
	private BigDecimal weight;
	
	@XmlElement
	private AdmissionSource admissionSource;
	
	@XmlElement
	private String admissionSourceOther;
	
	@XmlElement
	private String admissionDiagnosis;
	
	@XmlElement
	private String clinicalInformation;
	
	@XmlElement
	private String surgicalProcedure;
	
	@XmlTransient
	private Date dischargeDate;

	public String getHkid() {
		return hkid;
	}

	public void setHkid(String hkid) {
		this.hkid = hkid;
	}

	public String getCaseNum() {
		return caseNum;
	}

	public void setCaseNum(String caseNum) {
		this.caseNum = caseNum;
	}

	public String getSerialNum() {
		return serialNum;
	}

	public void setSerialNum(String serialNum) {
		this.serialNum = serialNum;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Date getRptDate() {
		return rptDate;
	}

	public void setRptDate(Date rptDate) {
		this.rptDate = rptDate;
	}

	public String getPatName() {
		return patName;
	}

	public void setPatName(String patName) {
		this.patName = patName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Date getAdmDate() {
		return admDate;
	}

	public void setAdmDate(Date admDate) {
		this.admDate = admDate;
	}

	public String getPhsSpec() {
		return phsSpec;
	}

	public void setPhsSpec(String phsSpec) {
		this.phsSpec = phsSpec;
	}

	public String getPhsWard() {
		return phsWard;
	}

	public void setPhsWard(String phsWard) {
		this.phsWard = phsWard;
	}

	public String getIpasSpec() {
		return ipasSpec;
	}

	public void setIpasSpec(String ipasSpec) {
		this.ipasSpec = ipasSpec;
	}

	public String getIpasWard() {
		return ipasWard;
	}

	public void setIpasWard(String ipasWard) {
		this.ipasWard = ipasWard;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public String getBedNum() {
		return bedNum;
	}

	public void setBedNum(String bedNum) {
		this.bedNum = bedNum;
	}

	public String getAllergyHistory() {
		return allergyHistory;
	}

	public void setAllergyHistory(String allergyHistory) {
		this.allergyHistory = allergyHistory;
	}

	public String getAlertProfileErrMsg() {
		return alertProfileErrMsg;
	}

	public void setAlertProfileErrMsg(String alertProfileErrMsg) {
		this.alertProfileErrMsg = alertProfileErrMsg;
	}

	public AlertProfileStatus getAlertProfileStatus() {
		return alertProfileStatus;
	}

	public void setAlertProfileStatus(AlertProfileStatus alertProfileStatus) {
		this.alertProfileStatus = alertProfileStatus;
	}

	public String getRecentAdmin() {
		return recentAdmin;
	}

	public void setRecentAdmin(String recentAdmin) {
		this.recentAdmin = recentAdmin;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public AdmissionSource getAdmissionSource() {
		return admissionSource;
	}

	public void setAdmissionSource(AdmissionSource admissionSource) {
		this.admissionSource = admissionSource;
	}

	public String getAdmissionSourceOther() {
		return admissionSourceOther;
	}

	public void setAdmissionSourceOther(String admissionSourceOther) {
		this.admissionSourceOther = admissionSourceOther;
	}

	public String getAdmissionDiagnosis() {
		return admissionDiagnosis;
	}

	public void setAdmissionDiagnosis(String admissionDiagnosis) {
		this.admissionDiagnosis = admissionDiagnosis;
	}

	public String getClinicalInformation() {
		return clinicalInformation;
	}

	public void setClinicalInformation(String clinicalInformation) {
		this.clinicalInformation = clinicalInformation;
	}

	public String getSurgicalProcedure() {
		return surgicalProcedure;
	}

	public void setSurgicalProcedure(String surgicalProcedure) {
		this.surgicalProcedure = surgicalProcedure;
	}

	public Date getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(Date dischargeDate) {
		this.dischargeDate = dischargeDate;
	}

	public Date getDisclaimerDate() {
		return disclaimerDate;
	}

	public void setDisclaimerDate(Date disclaimerDate) {
		this.disclaimerDate = disclaimerDate;
	}

	
}
