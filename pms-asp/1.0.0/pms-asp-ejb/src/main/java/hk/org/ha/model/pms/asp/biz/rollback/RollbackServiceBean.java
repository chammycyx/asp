package hk.org.ha.model.pms.asp.biz.rollback;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.exception.RecordNotFoundException;
import hk.org.ha.model.pms.asp.persistence.AspDispRpt;
import hk.org.ha.model.pms.asp.udt.AspDispRptAdminStatus;
import hk.org.ha.model.pms.asp.vo.enquiry.AspRptCriteria;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;


@Stateful
@Scope(ScopeType.SESSION)
@Name("rollbackService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class RollbackServiceBean implements RollbackServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In(scope=ScopeType.SESSION, required=false)
	UamInfo uamInfo;
	
	
	@In
	private AuditLogger auditLogger;
	
	private static final String ASP_DISP_RPT_CLASS_NAME = AspDispRpt.class.getName();
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer("yyyy-MM-dd HH:mm:ss.SSS"), Date.class);
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    
	public List<AspDispRpt> retrieveRollbackResultList(AspRptCriteria aspRptCriteriaIn){
		
		StringBuilder queryString = new StringBuilder();
		HashMap<String, Object> params = new HashMap<String, Object>();
		queryString.append("select new " +ASP_DISP_RPT_CLASS_NAME+  "( " +
				" o.id," +
				" o.hospCode," +
				" o.rptType," +
				" o.serialNum," +
				" o.dispDate, "+
				" o.rptDate," +
				" o.hkid," +
				" o.status," +
				" o.aspDispItem.caseNum," +
				" o.patName," +
				" o.aspDispItem.id, " +
				" o.adminStatus," +
				" o.workstationId," +
				" o.updateUser" +
				" ) from AspDispRpt o "); 
		queryString.append(" where o.adminStatus = :adminStatus "+	
						   " and o.dispDate >= :dispDateFrom "+
						   " and o.dispDate <= :dispDateTo ");
		
		params.put("adminStatus", aspRptCriteriaIn.getAdminStatus());
		params.put("dispDateFrom", aspRptCriteriaIn.getDispenseFromDate());
		params.put("dispDateTo", aspRptCriteriaIn.getDispenseToDate());
		
		Query query = em.createQuery(queryString.toString());
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		
		@SuppressWarnings("unchecked")
		List<AspDispRpt> aspDispRptList = query.getResultList();
	
		if(!aspDispRptList.isEmpty())
		{
			Collections.sort(aspDispRptList, new DispenseDateComparator());
		}
		else
		{
			aspDispRptList=null;
		}
		
		auditLogger.log("#0066",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),aspRptCriteriaIn.getAdminStatus(),sdf.format(aspRptCriteriaIn.getDispenseFromDate()),sdf.format(aspRptCriteriaIn.getDispenseToDate()));
				
		return aspDispRptList;
		
	}
	
	
	
	public AspDispRpt rollbackResultList(AspDispRptAdminStatus oriAdminStatus, List<AspDispRpt> aspDispRptList) throws RecordNotFoundException{
		AspDispRpt aspDispRpt;
		
		for(AspDispRpt a : aspDispRptList){
			aspDispRpt = em.find(AspDispRpt.class, a.getId());
			if(aspDispRpt != null){
				
				if(!oriAdminStatus.equals(aspDispRpt.getAdminStatus())){
					return aspDispRpt;
				}
				
			}else{
				throw new RecordNotFoundException("Record not find for AspRpt" + a.getId());
			}
		}
		
		for(AspDispRpt a : aspDispRptList){
			aspDispRpt = em.find(AspDispRpt.class, a.getId());
			if(aspDispRpt.getWorkstationId() != null){
				aspDispRpt.setWorkstationId(null);
			}
			
			aspDispRpt.setAdminStatus(AspDispRptAdminStatus.None);
			
			auditLogger.log("#0067",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),serializer.deepSerialize(aspDispRpt));
		}
		
		
		return null;
		
	}

    public static class DispenseDateComparator implements Comparator<AspDispRpt>, Serializable
	{
		private static final long serialVersionUID = -1629369536048145337L;

		public int compare(AspDispRpt c1, AspDispRpt c2){
			
			int a = c1.getHospCode().compareTo(c2.getHospCode());
			if(a != 0){
				return a;
			}
			
			return c2.getDispDate()
			.compareTo( 
					c1.getDispDate()
			);
		}						
	}
	
	
    @Remove
	public void destroy(){
	}

}
