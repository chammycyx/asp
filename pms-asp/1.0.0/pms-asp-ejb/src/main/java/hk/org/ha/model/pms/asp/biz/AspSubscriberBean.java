package hk.org.ha.model.pms.asp.biz;

import javax.ejb.Stateless;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.biz.maint.AspHospListManagerLocal;
import hk.org.ha.model.pms.asp.cacher.DmDrugCacherInf;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("aspSubscriber")
@MeasureCalls
public class AspSubscriberBean implements AspSubscriberLocal {

	@In
	private DmDrugCacherInf dmDrugCacher;
	
	@In
	private AspHospListManagerLocal aspHospListManager;
	
	@Override
	public void clearAspHospList() {
		aspHospListManager.clearAspHospList();
	}

	@Override
	public void initCache() {
		dmDrugCacher.clear();
		dmDrugCacher.getDrugList();
	}

}
