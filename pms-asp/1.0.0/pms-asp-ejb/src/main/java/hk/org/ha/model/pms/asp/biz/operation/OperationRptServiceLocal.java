package hk.org.ha.model.pms.asp.biz.operation;

import hk.org.ha.model.pms.asp.vo.operation.OperationRpt;
import hk.org.ha.model.pms.asp.vo.operation.OperationRptCriteria;

import java.util.List;

import javax.ejb.Local;

@Local
public interface OperationRptServiceLocal {
	
	public List<OperationRpt> retrieveOpertaionRptResult(OperationRptCriteria o);

	public List<OperationRpt> getOperationRptList();

	void destroy();
	
}
