package hk.org.ha.model.pms.asp.vo.enquiry;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class CultureResult {
	@XmlElement
	private String specimen;
	
	@XmlElement
	private String labNum;
	
	@XmlElement
	private Date specimenDate;
	
	@XmlElement
	private Date refDate;
	
	@XmlElement
	private Date rptDate;
	
	@XmlElement(name ="Organism")
	private List<Organism> organismList; 
	
	public String getSpecimen() {
		return specimen;
	}
	public void setSpecimen(String specimen) {
		this.specimen = specimen;
	}
	public String getLabNum() {
		return labNum;
	}
	public void setLabNum(String labNum) {
		this.labNum = labNum;
	}
	public Date getSpecimenDate() {
		return specimenDate;
	}
	public void setSpecimenDate(Date specimenDate) {
		this.specimenDate = specimenDate;
	}
	public Date getRefDate() {
		return refDate;
	}
	public void setRefDate(Date refDate) {
		this.refDate = refDate;
	}
	public Date getRptDate() {
		return rptDate;
	}
	public void setRptDate(Date rptDate) {
		this.rptDate = rptDate;
	}
	public List<Organism> getOrganismList() {
		return organismList;
	}
	public void setOrganismList(List<Organism> organismList) {
		this.organismList = organismList;
	}
	
	
}
