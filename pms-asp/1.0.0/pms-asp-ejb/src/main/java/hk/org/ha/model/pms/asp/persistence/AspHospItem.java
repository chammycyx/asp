package hk.org.ha.model.pms.asp.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.model.pms.persistence.VersionEntity;

@Entity
@Table(name="ASP_HOSP_ITEM")
@IdClass(AspHospItemPK.class)
public class AspHospItem extends VersionEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="HOSP_CODE", nullable=false, length = 3)
	private String hospCode;
	
	@Id
	@Column(name="ITEM_CODE", nullable=false, length = 6)
	private String itemCode;
	
	@Id
	@Converter(name = "AspHospItem.aspType", converterClass = AspType.Converter.class)
    @Convert("AspHospItem.aspType")
	@Column(name="ASP_TYPE", nullable = false)
	private AspType aspType;
	
	@Column(name="VISIBLE_FLAG", nullable=false, length = 1)
	private Boolean visibleFlag;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "ITEM_CODE",nullable = false, insertable = false, updatable = false)
    private AspItem aspItem;
    
    @ManyToOne
    @JoinColumn(name = "HOSP_CODE",nullable = false, insertable = false, updatable = false)
    private AspHosp aspHosp;

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public AspType getAspType() {
		return aspType;
	}

	public void setAspType(AspType aspType) {
		this.aspType = aspType;
	}

	public Boolean getVisibleFlag() {
		return visibleFlag;
	}

	public void setVisibleFlag(Boolean visibleFlag) {
		this.visibleFlag = visibleFlag;
	}
	
	public AspItem getAspItem() {
		return aspItem;
	}

	public void setAspItem(AspItem aspItem) {
		this.aspItem = aspItem;
	}

	
	
}
