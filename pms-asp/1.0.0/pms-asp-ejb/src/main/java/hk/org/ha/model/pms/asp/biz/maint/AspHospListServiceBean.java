package hk.org.ha.model.pms.asp.biz.maint;

import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.cacher.DmDrugCacher;
import hk.org.ha.model.pms.asp.persistence.AspHosp;
import hk.org.ha.model.pms.asp.persistence.AspHospItem;
import hk.org.ha.model.pms.asp.persistence.AspUser;
import hk.org.ha.model.pms.asp.persistence.AspUserHosp;
import hk.org.ha.model.pms.dms.persistence.DmDrug;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("aspHospListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AspHospListServiceBean implements AspHospListServiceLocal {

	@In
	AspHospListManagerLocal aspHospListManager;
	
	@In
	AspUserServiceLocal aspUserService;
	
	@In
	private DmDrugCacher dmDrugCacher;
	
	@In
	UamInfo uamInfo;

	@In
	private AuditLogger auditLogger;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public List<AspHosp> retrieveAspHospListByUser(String originalScreen)
	{
		
		AspUser aspUser = aspUserService.retrieveAspUser(uamInfo.getUserId());
		
		if(aspUser == null)
		{
			return new ArrayList<AspHosp>();
		}
		
		List<AspHosp> oriAspHospList = retrieveAspHospList(originalScreen);
		List<AspHosp> filteredList = new ArrayList<AspHosp>();
		
		for(AspHosp aspHosp:oriAspHospList)
		{
			for(AspUserHosp aspUserHosp:aspUser.getAspUserHospList())
			{
				
				if(aspUserHosp.getHospCode().equals(aspHosp.getHospCode()))
				{
					
					for(AspHospItem a : aspHosp.getAspHospItemList()){
						
						DmDrug dmDrug = dmDrugCacher.getDrugByItemCode(a.getAspItem().getItemCode());
						a.getAspItem().setItemDesc(dmDrug.getDrugName());
					}
					
					filteredList.add(aspHosp);
				}
			}
		}
		
		auditLogger.log("#0072",originalScreen,uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()));
		
		return filteredList;
	}
	
	public List<AspHosp> retrieveAspHospList(String originalScreen) {
		
		List<AspHosp> aspHospList = aspHospListManager.retrieveAspHospList();
		
		auditLogger.log("#0073",originalScreen,uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()));
		
		return aspHospList;
	}
	
	@Remove
	public void destroy(){
	}
	
	
}
