package hk.org.ha.model.pms.asp.vo.enquiry;

import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.model.pms.asp.udt.ScopeOfRpt;
import hk.org.ha.model.pms.asp.vo.report.CultureResultRpt;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlRootElement(name="AspRpt")
@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class AspRpt {
	
	
	@XmlElement(required=true)
	private AspType aspType;
	
	@XmlElement(name="PatientDemography", required=true)
	private PatientDemography patientDemography;
	
	@XmlElement(name="ClinicalData", required=true)
	private ClinicalData clinicalData;
	
	@XmlElement(name="ManualCultureResult", required=true)
	private List<CultureResult> manualCultureResultList;
	
	@XmlElement(name="CultureResult", required=true)
	private List<CultureResult> cultureResultList;
	
	@XmlElement(name="Medication", required=true)
	private Medication medication;

	@XmlElement(name="OutcomeMeasurement", required=true)
	private OutcomeMeasurement outcomeMeasurement;

	@XmlElement(name="Intervention")
	private Intervention intervention;
	
	@XmlElement(name="Miscellaneous", required=true)
	private Miscellaneous miscellaneous;
	
	@XmlElement(name="Remark", required=true)
	private Remark remark;
	
	@XmlTransient
	private ScopeOfRpt scopeOfRpt;
	
	@XmlTransient
	private List<CultureResultRpt> cultureResultRptList;
	
	
	public AspRpt(){
		
		
	}
	
	public AspType getAspType() {
		return aspType;
	}

	public void setAspType(AspType aspType) {
		this.aspType = aspType;
	}

	public PatientDemography getPatientDemography() {
		return patientDemography;
	}

	public void setPatientDemography(PatientDemography patientDemography) {
		this.patientDemography = patientDemography;
	}

	public ClinicalData getClinicalData() {
		return clinicalData;
	}

	public void setClinicalData(ClinicalData clinicalData) {
		this.clinicalData = clinicalData;
	}

	public Medication getMedication() {
		return medication;
	}

	public void setMedication(Medication medication) {
		this.medication = medication;
	}

	public OutcomeMeasurement getOutcomeMeasurement() {
		return outcomeMeasurement;
	}

	public void setOutcomeMeasurement(OutcomeMeasurement outcomeMeasurement) {
		this.outcomeMeasurement = outcomeMeasurement;
	}

	public Intervention getIntervention() {
		return intervention;
	}

	public void setIntervention(Intervention intervention) {
		this.intervention = intervention;
	}
	

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public List<CultureResult> getManualCultureResultList() {
		return manualCultureResultList;
	}

	public void setManualCultureResultList(
			List<CultureResult> manualCultureResultList) {
		this.manualCultureResultList = manualCultureResultList;
	}

	public List<CultureResult> getCultureResultList() {
		return cultureResultList;
	}

	public void setCultureResultList(List<CultureResult> cultureResultList) {
		Collections.sort(cultureResultList, new CultureResultComparator());
		this.cultureResultList = cultureResultList;
	}
	
	public Remark getRemark() {
		return remark;
	}

	public void setRemark(Remark remark) {
		this.remark = remark;
	}

	public ScopeOfRpt getScopeOfRpt() {
		return scopeOfRpt;
	}

	public void setScopeOfRpt(ScopeOfRpt scopeOfRpt) {
		this.scopeOfRpt = scopeOfRpt;
	}

	public List<CultureResultRpt> getCultureResultRptList() {
		return cultureResultRptList;
	}

	public void setCultureResultRptList(
			List<CultureResultRpt> cultureResultRptList) {
		this.cultureResultRptList = cultureResultRptList;
	}

	private static class CultureResultComparator implements Comparator<CultureResult>, Serializable
	{
		private static final long serialVersionUID = 5277759845327096800L;

		@Override
		public int compare(CultureResult c1, CultureResult c2) {
			Date specDate1 = c1.getSpecimenDate();
			Date specDate2 = c2.getSpecimenDate();
			if(specDate1 != null && specDate2 != null)
				return specDate1.compareTo(specDate2);
			else
				return 0;
		}
		
	}
	
	
}
