package hk.org.ha.model.pms.asp.biz.operation;

import hk.org.ha.fmk.pms.report.ReportProvider;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import net.sf.jasperreports.engine.JRDataSource;

import org.apache.commons.io.FileUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.document.ByteArrayDocumentData;
import org.jboss.seam.document.DocumentData;
import org.jboss.seam.document.DocumentStore;
import org.jboss.seam.faces.Renderer;

@Stateful
@Scope(ScopeType.SESSION)
@Name("rptService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class RptServiceBean implements RptServiceLocal, Serializable {

	private static final long serialVersionUID = 2673718947440942840L;

	@In(create=true)
	private Renderer renderer;
	
	private String rptKey; 
	
	private String rptPath;
	
	private String rptFileName;
	
	private String password;
	
	private Boolean passwordFlag;
	
	@In
	private ReportProvider<JRDataSource> reportProvider;

	public void setRpt(String rptPath, String rptKey, String rptFileName,  Boolean passwordFlag, String password){
		this.rptKey = rptKey;
		this.rptPath = rptPath;
		this.rptFileName = rptFileName;
		this.password = password;
		this.passwordFlag = passwordFlag;
	}

	public void generateOperationRpt() throws IOException, ZipException{
		
		renderer.render(rptPath);	
		
		DocumentStore documentStore = reportProvider.getDocumentStore();
		
		DocumentData docData = (DocumentData) Contexts.getEventContext().get(rptKey);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		docData.writeDataToStream(baos);

		ByteArrayDocumentData data = new ByteArrayDocumentData(
				rptFileName,
				new DocumentData.DocumentType("xls", "application/msexcel"),
				baos.toByteArray());

		
		String contentId = documentStore.newId();
		documentStore.saveData(contentId, data);
		
		if(passwordFlag){
			contentId = renderZipContentId(contentId, rptFileName , password);
			
		}
		
		reportProvider.redirectReport(contentId, rptFileName);

	}
	

	
	private File generateZipFile(List<File> fileArrayList, String password, String folder) throws ZipException {
		
		File tempFile = createTempFile("", ".zip", folder);
		
		try {

			ZipFile zipFile = new ZipFile(tempFile);

			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

			parameters.setEncryptFiles(true);
			parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
			parameters.setPassword(password);

			zipFile.addFiles(new ArrayList<File>(fileArrayList), parameters);

			return tempFile;
			
		} catch (ZipException e) {
			
			tempFile.delete();
			throw e;
		}
	}
	
	private File createTempFile(String prefix, String suffix, String folder) {
		SecureRandom random = new SecureRandom();
		long n = random.nextLong();
		if (n == Long.MIN_VALUE) {
			n = 0; 
		} else {
			n = Math.abs(n);
		}
		return new File(folder, prefix + Long.toString(n) + suffix);
	}
	
	
	private String renderZipContentId(String contentId, String filename, String password) throws IOException, ZipException {
        ByteArrayDocumentData dd = (ByteArrayDocumentData) reportProvider.getDocumentStore().getDocumentData(contentId);
        
        File tempDir = new File(System.getProperty("java.io.tmpdir") + System.currentTimeMillis());
        if (!tempDir.mkdirs()) {
        	throw new IOException("Cannot create directory : " + tempDir);
        }
        
        File f = new File(tempDir,filename + "." + dd.getDocumentType().getExtension());
        FileUtils.writeByteArrayToFile(f, dd.getData());

        ArrayList<File> fl = new ArrayList<File>();
        fl.add(f);
        File zipFile = generateZipFile(fl, password, tempDir.getAbsolutePath());
        
        String zipContentId = reportProvider.getDocumentStore().newId();
        
        ByteArrayDocumentData zipdd = new ByteArrayDocumentData(filename,ReportProvider.ZIP, FileUtils.readFileToByteArray(zipFile));        
        reportProvider.getDocumentStore().saveData(zipContentId, zipdd);
        
        if (!zipFile.delete()) {
           	throw new IOException("Cannot delete zip file : " + zipFile);
        }
        
        FileUtils.deleteDirectory(tempDir);
        
        return zipContentId;
	}
	
	
	@Remove
	public void destroy(){
		
	}

	

}
