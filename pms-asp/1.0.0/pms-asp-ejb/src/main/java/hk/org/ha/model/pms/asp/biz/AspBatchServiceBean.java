package hk.org.ha.model.pms.asp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.biz.rpt.AspDispRptManagerLocal;
import hk.org.ha.model.pms.asp.persistence.AspDispItem;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("aspBatchService")
@MeasureCalls
public class AspBatchServiceBean implements AspBatchServiceLocal {

	@In
	AspDispItemManagerLocal aspDispItemManager;
	
	@In
	AspDispRptManagerLocal aspDispRptManager;
	
	@In
	SystemMessageManagerLocal systemMessageManager;
	
	@Override
	public void generatePendingAspDispRpt(Date batchDate, String hospCode, String g6pdCode, List<String> patHospCodeList) throws RuntimeException
	{		
		List<AspDispItem> aspDispItemList = aspDispItemManager.retrievePendingItemList(batchDate, hospCode);
		
		String errCode = aspDispRptManager.generateNewAspDispRptList(aspDispItemList, patHospCodeList, g6pdCode);
		
		if(!StringUtils.isBlank(errCode))
		{
			String errMsg = systemMessageManager.retrieveMessageDesc(errCode);
			throw new RuntimeException(errMsg);
		}

	}
	
	@Override
	public void receiveLegacyDispOrderItemList(List<DispOrderItem> dispOrderItemList, List<String> patHospCodeList, String g6pdCode, Date batchDate) throws RuntimeException
	{
		aspDispItemManager.constructAndSaveAspDispItem(dispOrderItemList, patHospCodeList, g6pdCode, batchDate);
	}

}
