package hk.org.ha.model.pms.asp.biz.enquiry;

import hk.org.ha.model.pms.asp.vo.enquiry.AspRptCriteria;

import java.io.IOException;

import javax.ejb.Local;

import net.lingala.zip4j.exception.ZipException;

@Local
public interface ExportServiceLocal {
	
	public void exportAspRpt(AspRptCriteria aspRptCriteriaIn);
		
	public void exportRpt() throws IOException, ZipException;
	
	void destroy();
}
