package hk.org.ha.model.pms.asp.biz.maint;

import hk.org.ha.model.pms.asp.persistence.AspHosp;

import java.util.List;

import javax.ejb.Local;

@Local
public interface AspHospListManagerLocal {
	List<AspHosp> retrieveAspHospList();
	
	void clearAspHospList(); 
}
