package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum Treatment implements StringValuedEnum {
	Prophylaxis("P", "Prophylaxis"),
	Empirical("E", "Empirical"),
	KnownPathogen("K", "KnownPathogen");
	
	private final String dataValue;
	
	private final String displayValue;
	
	Treatment(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static Treatment dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(Treatment.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<Treatment> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<Treatment> getEnumClass() {
    		return Treatment.class;
    	}
    }
}
