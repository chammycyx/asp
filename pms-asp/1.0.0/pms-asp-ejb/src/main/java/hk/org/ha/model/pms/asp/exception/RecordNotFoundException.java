package hk.org.ha.model.pms.asp.exception;

import hk.org.ha.fmk.pms.exception.SerializableException;

public class RecordNotFoundException extends SerializableException{
	private static final long serialVersionUID = 1L;
	
	public RecordNotFoundException() {
		super("");
	}
	
	public RecordNotFoundException(String msg) {
		super(msg);
	}
	
	public RecordNotFoundException(Throwable source) {
		super(source);
	}
}
