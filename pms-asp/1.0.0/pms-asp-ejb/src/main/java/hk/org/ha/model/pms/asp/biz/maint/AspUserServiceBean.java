package hk.org.ha.model.pms.asp.biz.maint;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.persistence.AspUser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("aspUserService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AspUserServiceBean implements AspUserServiceLocal {
	
	@PersistenceContext
	private EntityManager em;
	
	@In(scope=ScopeType.SESSION, required=false)
	UamInfo uamInfo;

	@In
	private AuditLogger auditLogger;
	
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer("yyyy-MM-dd HH:mm:ss.SSS"), Date.class);
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	
	@Override
	public void deleteAspUser(AspUser aspUser) {
		
		
		em.remove(em.merge(aspUser));
		em.flush();
		
		auditLogger.log("#0071",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),serializer.deepSerialize(aspUser));

	}

	@Remove
	public void destroy() {
	}

	@SuppressWarnings("unchecked")
	@Override
	public AspUser retrieveAspUser(String userCode) {
		List<AspUser> aspUserList = em.createQuery(
				"select o from AspUser o "+
				"where o.userCode = :userCode"
				)
				.setParameter("userCode", userCode)
				.getResultList();
		if(aspUserList.isEmpty())
		{
			return null;
		}
		else
		{
			auditLogger.log("#0068",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()));

			return aspUserList.get(0);
		}
	}

}
