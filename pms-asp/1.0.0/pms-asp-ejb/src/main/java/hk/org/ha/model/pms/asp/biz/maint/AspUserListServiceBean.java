package hk.org.ha.model.pms.asp.biz.maint;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.persistence.AspUser;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.QueryHints;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("aspUserListService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AspUserListServiceBean implements AspUserListServiceLocal {
	
	@PersistenceContext
	private EntityManager em;

	@In(scope=ScopeType.SESSION, required=false)
	UamInfo uamInfo;

	@In
	private AuditLogger auditLogger;
	
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer("yyyy-MM-dd HH:mm:ss.SSS"), Date.class);
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	
	@SuppressWarnings("unchecked")
	@Override
	public List<AspUser> retrieveAspUserList() {

		List<AspUser> aspUserList = em.createQuery(
				"select o from AspUser o "+
				"order by o.userCode "
				)
				.setHint(QueryHints.BATCH, "o.aspUserHospList")
				.getResultList();
		
		for(AspUser aspUser : aspUserList)
		{
			aspUser.getAspUserHospList().size();
		}
		
		auditLogger.log("#0069",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()));

		return aspUserList;
	}

	@Override
	public void updateAspUserList(List<AspUser> aspUserList) {
		
		for(AspUser aspUser: aspUserList)
		{
			if(StringUtils.isBlank(aspUser.getCreateUser()))
			{
				em.persist(aspUser);		
				em.flush();
			}
			else
			{
				
				em.merge(aspUser);				
				em.flush();
			}
			auditLogger.log("#0070",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),serializer.deepSerialize(aspUser));
		}

		em.clear();
	}
	
	@Remove
	public void destroy(){
	}


}
