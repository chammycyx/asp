package hk.org.ha.model.pms.asp.vo.enquiry;

import hk.org.ha.model.pms.asp.udt.AspDispRptAdminStatus;
import hk.org.ha.model.pms.asp.udt.AspDispRptStatus;
import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.model.pms.asp.udt.ScopeOfRpt;
import hk.org.ha.model.pms.asp.udt.YesNoBlankFlag;

import java.util.Date;
import java.util.List;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@ExternalizedBean(type=DefaultExternalizer.class)
public class AspRptCriteria {

	private List<Long> rptIdList;
	
	private List<Long> aspDispItemIdList;
	
	private String hospCode;
	
	private Date dispenseFromDate;
	
	private Date dispenseToDate;
	
	private String hkid;
	
	private String serialNum;
	
	private AspType reportType;
	
	private AspDispRptStatus status;
	
	private Boolean passwordFlag;
	
	private String password;
	
	private ScopeOfRpt scopeOfRpt;
	
	private Boolean includeOldRpt;
	
	private List<String> specialtyList;
	
	private List<String> itemCodeList;
	
	private String wardCode;
	
	private String updateUser;
	
	private String g6dpCode;
	
	private AspDispRptAdminStatus adminStatus;
	
	private YesNoBlankFlag allSpecFlag;
	
	private YesNoBlankFlag allDrugFlag;
	
	public AspDispRptStatus getStatus() {
		return status;
	}
	public void setStatus(AspDispRptStatus status) {
		this.status = status;
	}
	public String getHospCode() {
		return hospCode;
	}
	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}
	public Date getDispenseFromDate() {
		return dispenseFromDate;
	}
	public void setDispenseFromDate(Date dispenseFromDate) {
		this.dispenseFromDate = dispenseFromDate;
	}
	public Date getDispenseToDate() {
		return dispenseToDate;
	}
	public void setDispenseToDate(Date dispenseToDate) {
		this.dispenseToDate = dispenseToDate;
	}
	public String getHkid() {
		return hkid;
	}
	public void setHkid(String hkid) {
		this.hkid = hkid;
	}
	public String getSerialNum() {
		return serialNum;
	}
	public void setSerialNum(String serialNum) {
		this.serialNum = serialNum;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public AspType getReportType() {
		return reportType;
	}
	public void setReportType(AspType reportType) {
		this.reportType = reportType;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getWardCode() {
		return wardCode;
	}
	public void setWardCode(String wardCode) {
		this.wardCode = wardCode;
	}
	public List<Long> getRptIdList() {
		return rptIdList;
	}
	public void setRptIdList(List<Long> rptIdList) {
		this.rptIdList = rptIdList;
	}
	public ScopeOfRpt getScopeOfRpt() {
		return scopeOfRpt;
	}
	public void setScopeOfRpt(ScopeOfRpt scopeOfRpt) {
		this.scopeOfRpt = scopeOfRpt;
	}
	public Boolean getIncludeOldRpt() {
		return includeOldRpt;
	}
	public void setIncludeOldRpt(Boolean includeOldRpt) {
		this.includeOldRpt = includeOldRpt;
	}
	public String getG6dpCode() {
		return g6dpCode;
	}
	public void setG6dpCode(String g6dpCode) {
		this.g6dpCode = g6dpCode;
	}
	public Boolean getPasswordFlag() {
		return passwordFlag;
	}
	public void setPasswordFlag(Boolean passwordFlag) {
		this.passwordFlag = passwordFlag;
	}
	public List<Long> getAspDispItemIdList() {
		return aspDispItemIdList;
	}
	public void setAspDispItemIdList(List<Long> aspDispItemIdList) {
		this.aspDispItemIdList = aspDispItemIdList;
	}
	public AspDispRptAdminStatus getAdminStatus() {
		return adminStatus;
	}
	public void setAdminStatus(AspDispRptAdminStatus adminStatus) {
		this.adminStatus = adminStatus;
	}
	public List<String> getSpecialtyList() {
		return specialtyList;
	}
	public void setSpecialtyList(List<String> specialtyList) {
		this.specialtyList = specialtyList;
	}
	public YesNoBlankFlag getAllSpecFlag() {
		return allSpecFlag;
	}
	public void setAllSpecFlag(YesNoBlankFlag allSpecFlag) {
		this.allSpecFlag = allSpecFlag;
	}
	public YesNoBlankFlag getAllDrugFlag() {
		return allDrugFlag;
	}
	public void setAllDrugFlag(YesNoBlankFlag allDrugFlag) {
		this.allDrugFlag = allDrugFlag;
	}
	public List<String> getItemCodeList() {
		return itemCodeList;
	}
	public void setItemCodeList(List<String> itemCodeList) {
		this.itemCodeList = itemCodeList;
	}



}
