package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum AuditRank implements StringValuedEnum {
	Physician("D", "Physician"),
	Pharmacist("P", "Pharmacist"),
	ICN("I", "ICN");
	
	
	private final String dataValue;
	
	private final String displayValue;
	
	AuditRank(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static AuditRank dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AuditRank.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<AuditRank> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<AuditRank> getEnumClass() {
    		return AuditRank.class;
    	}
    }
}
