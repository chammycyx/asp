package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AspRptStatus implements StringValuedEnum {
	Active("A", "Active"),
	SysDeleted("X", "System deleted"),
	Locked("L", "Locked"),
	Submitted("S", "Submitted"),
	NotForReview("N", "Not for Review");
	
	private final String dataValue;
	
	private final String displayValue;
	
	AspRptStatus(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static AspRptStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AspRptStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<AspRptStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<AspRptStatus> getEnumClass() {
    		return AspRptStatus.class;
    	}
    }
}
