package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum Prescribing implements StringValuedEnum {
	Specialty("S", "Specialty"),
	General("G", "General");
	
	private final String dataValue;
	
	private final String displayValue;
	
	Prescribing(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static Prescribing dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(Prescribing.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<Prescribing> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<Prescribing> getEnumClass() {
    		return Prescribing.class;
    	}
    }
}
