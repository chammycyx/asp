package hk.org.ha.model.pms.asp.persistence;

import hk.org.ha.model.pms.persistence.VersionEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="ASP_USER_HOSP")
@IdClass(AspUserHospPK.class)
public class AspUserHosp extends VersionEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="USER_CODE", nullable = false)
	private String userCode;
	
	@Id
	@Column(name="HOSP_CODE", nullable = false)
	private String hospCode;

	@ManyToOne
	@JoinColumn(name="USER_CODE", nullable = false, insertable = false, updatable = false)
	private AspUser aspUser;
	
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}
}
