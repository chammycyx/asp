package hk.org.ha.model.pms.asp.biz.mock;

import java.util.ArrayList;
import java.util.List;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;

import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.asa.exception.alert.drug.DrugException;
import hk.org.ha.model.pms.asa.exception.alert.druginfo.DrugInfoException;
import hk.org.ha.model.pms.asa.exception.alert.mds.MdsException;
import hk.org.ha.model.pms.dms.vo.AllergenIngredient;
import hk.org.ha.model.pms.dms.vo.DrugMds;
import hk.org.ha.model.pms.persistence.PatientEntity;
import hk.org.ha.model.pms.vo.alert.AlertProfile;
import hk.org.ha.model.pms.vo.alert.AlertProfileHistory;
import hk.org.ha.model.pms.vo.alert.PatAdr;
import hk.org.ha.model.pms.vo.alert.PatAlert;
import hk.org.ha.model.pms.vo.alert.PatAllergy;
import hk.org.ha.model.pms.vo.alert.druginfo.ClassificationNode;
import hk.org.ha.model.pms.vo.alert.druginfo.DrugInteraction;
import hk.org.ha.model.pms.vo.alert.druginfo.FoodInteraction;
import hk.org.ha.model.pms.vo.alert.druginfo.PatientEducation;
import hk.org.ha.model.pms.vo.alert.mds.MdsCriteria;
import hk.org.ha.model.pms.vo.alert.mds.MdsResult;
import hk.org.ha.service.pms.asa.interfaces.alert.AlertServiceJmsRemote;

@Startup
@Name("alertServiceProxy")
@Scope(ScopeType.APPLICATION)
public class MockAlertServiceProxy implements AlertServiceJmsRemote {

	@Override
	public AlertProfile retrieveAlertProfile(PatientEntity patientEntity,
			String caseNum, String patHospCode, String userId,
			String workstationId, String g6pdCode) throws AlertProfileException {
		AlertProfile alertProfile = new AlertProfile();
		
		List<PatAdr> patAdrList = new ArrayList<PatAdr>();
		
		List<PatAllergy> patAllergyList = new ArrayList<PatAllergy>();
		PatAllergy pat1 = new PatAllergy();
		pat1.setAllergenName("CLOXACIN [CLOXACILLIN SODIUM]");
		pat1.setClinicalManifestation("Allergic contact dermatitis;Asthma;Eczema");
		
		PatAllergy pat2 = new PatAllergy();
		pat2.setAllergenName("METOPROLOL");
		pat2.setClinicalManifestation("Allergic contact dermatitis");
		
		patAllergyList.add(pat1);
		patAllergyList.add(pat2);

		PatAdr adr1 = new PatAdr();
		
		adr1.setDrugName("CLOXACIN [CLOXACILLIN SODIUM]");
		adr1.setReaction("Heartburn; Nausea");
		
		PatAdr adr2 = new PatAdr();
		adr2.setDrugName("ASPIRIN");
		adr2.setReaction("Headache Disorder");
		
		patAdrList.add(adr1);
		patAdrList.add(adr2);
		

		
		PatAlert alert1 = new PatAlert();
		alert1.setAlertDesc("Fr 7, 24cm Lt. JJ stent inserted on 1/12/2010 ");
		
		alertProfile.setPatAdrList(patAdrList);
		alertProfile.setPatAllergyList(patAllergyList);
		alertProfile.setG6pdAlert(alert1);

		return alertProfile;
	}

	@Override
	public AlertProfileHistory retrieveAlertProfileHistory(
			PatientEntity patientEntity, String caseNum, String patHospCode,
			String userId, String workstationId) throws AlertProfileException {
		return null;
	}

	@Override
	public MdsResult mdsCheck(MdsCriteria mdsCriteria) throws MdsException {
		return null;
	}

	@Override
	public List<ClassificationNode> retrieveDrugClassificationList(
			Integer gcnSeqNum, Integer rdfgenId, Integer rgenId,
			String patHospCode, List<DrugMds> drugMdsList)
			throws DrugInfoException {

		return null;
	}

	@Override
	public String retrieveSideEffect(Integer gcnSeqNum, Integer rdfgenId,
			Integer rgenId, String patHospCode, List<DrugMds> drugMdsList)
			throws DrugInfoException {

		return null;
	}

	@Override
	public String retrieveContraindication(Integer gcnSeqNum, Integer rdfgenId,
			Integer rgenId, String patHospCode, List<DrugMds> drugMdsList)
			throws DrugInfoException {

		return null;
	}

	@Override
	public String retrievePrecaution(Integer gcnSeqNum, Integer rdfgenId,
			Integer rgenId, String patHospCode, List<DrugMds> drugMdsList)
			throws DrugInfoException {

		return null;
	}

	@Override
	public List<DrugInteraction> retrieveDrugInteraction(Integer gcnSeqNum,
			Integer rdfgenId, Integer rgenId, String patHospCode,
			List<DrugMds> drugMdsList) throws DrugInfoException {

		return null;
	}

	@Override
	public List<FoodInteraction> retrieveFoodInteraction(Integer gcnSeqNum,
			Integer rdfgenId, Integer rgenId, String patHospCode,
			List<DrugMds> drugMdsList) throws DrugInfoException {

		return null;
	}

	@Override
	public List<PatientEducation> retrievePatientEducation(Integer gcnSeqNum,
			Integer rdfgenId, Integer rgenId, String patHospCode,
			List<DrugMds> drugMdsList) throws DrugInfoException {

		return null;
	}

	@Override
	public String retrieveCommonOrder(Integer gcnSeqNum, Integer rdfgenId,
			Integer rgenId, String patHospCode, List<DrugMds> drugMdsList)
			throws DrugInfoException {

		return null;
	}

	@Override
	public String retrieveDosageRange(Integer gcnSeqNum, Integer rdfgenId,
			Integer rgenId, String patHospCode, Integer age, String ageUnit,
			List<DrugMds> drugMdsList, Integer selectedStrengthIndex)
			throws DrugInfoException {

		return null;
	}

	@Override
	public String retrieveMonograph(Integer monoId, Integer monoType,
			String monoVersion, String patHospCode) throws DrugInfoException {

		return null;
	}

	@Override
	public Integer updateAllergenIngredient(
			List<AllergenIngredient> allergenIngredientList)
			throws DrugException {

		return null;
	}

}
