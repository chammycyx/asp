package hk.org.ha.model.pms.asp.biz.enquiry;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.util.JaxbWrapper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.biz.SeqRemapper;
import hk.org.ha.model.pms.asp.biz.rpt.AspDispRptManagerLocal;
import hk.org.ha.model.pms.asp.exception.RecordNotFoundException;
import hk.org.ha.model.pms.asp.persistence.AspDispItem;
import hk.org.ha.model.pms.asp.persistence.AspDispRpt;
import hk.org.ha.model.pms.asp.udt.AlertProfileStatus;
import hk.org.ha.model.pms.asp.udt.AspDispRptAdminStatus;
import hk.org.ha.model.pms.asp.udt.AspDispRptStatus;
import hk.org.ha.model.pms.asp.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.asp.vo.enquiry.AspRpt;
import hk.org.ha.model.pms.asp.vo.enquiry.AspRptCriteria;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;


@Stateful
@Scope(ScopeType.SESSION)
@Name("enqService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class EnqServiceBean implements EnqServiceLocal {

	@In(scope=ScopeType.SESSION, required=false)
	UamInfo uamInfo;

	@PersistenceContext
	private EntityManager em;

	@In
	private SeqRemapper seqRemapper;
	
	@In
	AspDispRptManagerLocal aspDispRptManager;

	@In
	private AuditLogger auditLogger;
	
	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer("yyyy-MM-dd HH:mm:ss.SSS"), Date.class);
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Out(required = false)
	public Integer refreshMaxSelect;

	private static final JaxbWrapper<AspRpt> JAXB_WRAPPER = new JaxbWrapper<AspRpt>("hk.org.ha.model.pms.asp.vo.enquiry");

	private static final String ASP_DISP_RPT_CLASS_NAME = AspDispRpt.class.getName();


	public AspDispRpt refreshAspDispRptList(AspRptCriteria aspRptCriteriaIn) throws RuntimeException, RecordNotFoundException{

		String errorCode ="";


		ArrayList<Long> aspDispItemIdList = new ArrayList<Long>();

		for(Long id: aspRptCriteriaIn.getRptIdList()){

			AspDispRpt aspDispRpt = em.find(AspDispRpt.class, id);
			if(aspDispRpt != null){

				if(aspDispRpt.getStatus().equals(AspDispRptStatus.Inactive)){
					return aspDispRpt;
				}

				if( ! aspDispRpt.getAdminStatus().equals(AspDispRptAdminStatus.None)){
					return aspDispRpt;
				}

				aspDispItemIdList.add(aspDispRpt.getAspDispItem().getId());

			}else{

				throw new RecordNotFoundException("Record not find for AspRpt, id : " + id);
			}

		}


		errorCode =	aspDispRptManager.refreshAspDispRptList(aspDispItemIdList,aspRptCriteriaIn.getG6dpCode());
		
		if(!StringUtils.isEmpty(errorCode)){
			AspDispRpt aspDispRpt = new AspDispRpt();
			aspDispRpt.setWsErrorCode(errorCode);
			return aspDispRpt;
			
		}else{
			AspDispRpt aspDispRpt = null; 
			String alertProfileErrorDetail = "";
			
			for(Long id: aspDispItemIdList){
				
				AspDispItem aspDispItem = em.find(AspDispItem.class, id);
				auditLogger.log("#0059",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),aspRptCriteriaIn.getG6dpCode(),serializer.deepSerialize(aspDispItem.getAspDispRpt()));
			
				AspRpt aspRpt = JAXB_WRAPPER.unmarshall(aspDispItem.getAspDispRpt().getRptXml());
				
				if(AlertProfileStatus.Error.equals(aspRpt.getPatientDemography().getAlertProfileStatus())){
					aspDispRpt = new AspDispRpt();
					aspDispRpt.setAlertProfileStatus(AlertProfileStatus.Error);
					alertProfileErrorDetail += aspDispRpt.getPatName() + "," + aspDispRpt.getHkid() + "\n";
				}
			
			}
			if(!"".equals(alertProfileErrorDetail)){
				aspDispRpt.setAlertProfileErrorMsg(alertProfileErrorDetail);
				return aspDispRpt;
			}else {
				return null;
			}
				
			
		}


	}

	public String updateAspDispRpt(AspDispRpt aspDispRptIn) throws RecordNotFoundException {

		AspDispRpt aspDispRpt = em.find(AspDispRpt.class, aspDispRptIn.getId());
		if(aspDispRpt != null){

			if (aspDispRptIn.getVersion() != null && aspDispRptIn.getVersion().compareTo(aspDispRpt.getVersion())!=0) {
				return aspDispRpt.getUpdateUser();
			}
			
			if(aspDispRptIn.getAspRpt() == null){
				
				aspDispRpt.setAdminStatus(AspDispRptAdminStatus.None);
				aspDispRpt.setWorkstationId(null);
				em.merge(aspDispRpt);
				
				auditLogger.log("#0064",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),serializer.deepSerialize(aspDispRpt));
				
			}else{
				
				String previousStr = JAXB_WRAPPER.marshall(aspDispRpt.getAspRpt());
				String newStr = JAXB_WRAPPER.marshall(aspDispRptIn.getAspRpt());
				
				aspDispRpt.setUpdateDate(new Date());

				if( !previousStr.equals(newStr) || AspDispRptAdminStatus.Submitted.equals(aspDispRptIn.getAdminStatus())){
					aspDispRpt.setAspRpt(aspDispRptIn.getAspRpt());
					aspDispRpt.setStatus(aspDispRptIn.getStatus());
					aspDispRpt.setAdminStatus(aspDispRptIn.getAdminStatus());
					aspDispRpt.setWorkstationId(aspDispRptIn.getWorkstationId());
					
				}else{
					aspDispRpt.setAdminStatus(AspDispRptAdminStatus.None);
					aspDispRpt.setWorkstationId(null);
				}
				
				em.merge(aspDispRpt);
				
				if(AspDispRptAdminStatus.Submitted.equals(aspDispRptIn.getAdminStatus())){
					auditLogger.log("#0063",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),serializer.deepSerialize(aspDispRpt));
				}else if(!previousStr.equals(newStr)){
					auditLogger.log("#0062",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),serializer.deepSerialize(aspDispRpt));
				}else{
					auditLogger.log("#0064",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),serializer.deepSerialize(aspDispRpt));
				}
				
			}
			
			return null;

		}else{

			throw new RecordNotFoundException("Record not find for AspRpt, id : " + aspDispRptIn.getId());
		}

	}


	public AspDispRpt updateAspDispRptListStatus(AspRptCriteria aspRptCriteriaIn) throws RecordNotFoundException{

		for(Long id : aspRptCriteriaIn.getRptIdList()){
			AspDispRpt aspDispRpt = em.find(AspDispRpt.class, id);
			if(aspDispRpt != null){
		
				if(aspDispRpt.getStatus().equals(AspDispRptStatus.Inactive)){
					return aspDispRpt;
				}

				if(aspDispRpt.getWorkstationId() != null && !aspDispRpt.getWorkstationId().equals(uamInfo.getWorkstationId()) 
						&& ! aspDispRpt.getAdminStatus().equals(AspDispRptAdminStatus.None)){
					return aspDispRpt;

				}
				
			}else{
				
				throw new RecordNotFoundException("Record not find for AspRpt, id : " + id);
			}
		}
		
		
		for(Long id : aspRptCriteriaIn.getRptIdList()){
			AspDispRpt aspDispRpt = em.find(AspDispRpt.class, id);
			if(aspRptCriteriaIn.getAdminStatus().equals(AspDispRptAdminStatus.Submitted) || 
			   aspRptCriteriaIn.getAdminStatus().equals(AspDispRptAdminStatus.NotForReview)){

				aspDispRpt.setWorkstationId(uamInfo.getWorkstationId());

			}else{

				aspDispRpt.setWorkstationId(null);
			}

			aspDispRpt.setAdminStatus(aspRptCriteriaIn.getAdminStatus());
			em.merge(aspDispRpt);
			
			auditLogger.log("#0060",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),serializer.deepSerialize(aspDispRpt));
			
		
		}

		return null;
	}


	public AspDispRpt retrieveAspDispRpt(Long aspDispRptId,String workStationId) throws RecordNotFoundException{

		AspDispRpt aspDispRpt = em.find(AspDispRpt.class, aspDispRptId);
		if(aspDispRpt != null){

			if(aspDispRpt.getStatus().equals(AspDispRptStatus.Inactive)){
				return aspDispRpt;
			}

			if( aspDispRpt.getAdminStatus().equals(AspDispRptAdminStatus.Submitted) || aspDispRpt.getAdminStatus().equals(AspDispRptAdminStatus.NotForReview)){
				return aspDispRpt;

			}else if( aspDispRpt.getAdminStatus().equals(AspDispRptAdminStatus.Locked) 
					&& aspDispRpt.getWorkstationId() != null && !aspDispRpt.getWorkstationId().equals(workStationId)){

				return aspDispRpt;
			}

			aspDispRpt.setAdminStatus(AspDispRptAdminStatus.Locked);
			aspDispRpt.setWorkstationId(uamInfo.getWorkstationId());
			em.merge(aspDispRpt);
			em.flush();
			em.clear();
			
			auditLogger.log("#0061",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),serializer.deepSerialize(aspDispRpt));
			aspDispRpt = em.find(AspDispRpt.class, aspDispRptId);
			
			return aspDispRpt;

		}else{

			throw new RecordNotFoundException("Record not find for AspRpt, id : " + aspDispRptId);
		}

	}


	public List<AspDispRpt> retrieveAspDispRptList(AspRptCriteria aspRptCriteriaIn){

		if(YesNoBlankFlag.Blank.equals(aspRptCriteriaIn.getAllSpecFlag()) && aspRptCriteriaIn.getSpecialtyList().isEmpty()){
			return null;
		}
		
		if(YesNoBlankFlag.Blank.equals(aspRptCriteriaIn.getAllDrugFlag()) && aspRptCriteriaIn.getItemCodeList().isEmpty()){
			return null;
		}
		
		StringBuilder sb = new StringBuilder();
		HashMap<String, Object> params = new HashMap<String, Object>();
		sb.append(
				"select new " + ASP_DISP_RPT_CLASS_NAME + " (" +
				" o.id," +
				" o.hospCode," +
				" o.rptType," +
				" o.serialNum," +
				" o.rptDate," +
				" o.hkid," +
				" o.aspDispItem.itemCode," +
				" o.pasSpecCode," +
				" o.status," +
				" o.aspDispItem.caseNum," +
				" o.patName," +
				" o.pasWardCode," +
				" o.aspDispItem.bedNum," +
				" o.drugName," +
				" o.aspDispItem.freqCode," +
				" o.aspDispItem.doseQty," +
				" o.aspDispItem.doseUnit," +
				" o.dispDate," +
				" o.aspDispItem.id," +
				" o.adminStatus," +
				" o.workstationId," +
				" o.updateUser," +
				" o.aspDispItem.freqDesc," +
				" o.dischargeFlag," +
				" o.updateDate," +
				" o.aspDispItem.wardCode," +
				" o.rptXml" +
				") from AspDispRpt o");
		
		sb.append(
				" where o.hospCode = :hospCode " +
				" and o.dispDate >= :dispDateFrom " +
				" and o.dispDate <= :dispDateTo ");		
		params.put("hospCode", aspRptCriteriaIn.getHospCode());
		params.put("dispDateFrom", aspRptCriteriaIn.getDispenseFromDate());
		params.put("dispDateTo", aspRptCriteriaIn.getDispenseToDate());
		
		if(!checkIncludeOldReport(aspRptCriteriaIn)){
			sb.append(" and o.status <> :status ");
			sb.append(" and o.adminStatus in :adminStatusList ");
			params.put("status", AspDispRptStatus.Inactive);
			params.put("adminStatusList", AspDispRptAdminStatus.None_Locked);
		}
		
		if(!isEmptyHKID(aspRptCriteriaIn)){
			sb.append(" and o.hkid like :hkid ");
			params.put("hkid", "%" + aspRptCriteriaIn.getHkid() +"%");
		}

		if(!isEmptySerialNum(aspRptCriteriaIn)){
			sb.append(" and o.serialNum like :serialNum ");
			params.put("serialNum", "%" + aspRptCriteriaIn.getSerialNum() +"%");
		}
		
		if(!isAllSpecialty(aspRptCriteriaIn)){
			if(YesNoBlankFlag.Blank.equals(aspRptCriteriaIn.getAllSpecFlag())){
				sb.append(" and (o.pasSpecCode in :pasSpecCodeList or o.pasSpecCode is null )");
			}else{
				sb.append(" and o.pasSpecCode in :pasSpecCodeList ");
			}
			params.put("pasSpecCodeList", aspRptCriteriaIn.getSpecialtyList());
		}

		if(!isAllDrugName(aspRptCriteriaIn)){
			sb.append(" and o.aspDispItem.itemCode in :itemCodeList ");
			params.put("itemCodeList", aspRptCriteriaIn.getItemCodeList());
		}

		if(!isEmptyReportType(aspRptCriteriaIn)){
			sb.append(" and o.rptType = :rptType ");
			params.put("rptType", aspRptCriteriaIn.getReportType());
		}

		Query query = em.createQuery(sb.toString());
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		@SuppressWarnings("unchecked")
		List<AspDispRpt> aspDispRptList = query.getResultList();

		if(aspDispRptList != null ){
			em.clear();
			Collections.sort(aspDispRptList, new AspDispReportComparator());
			for(AspDispRpt aspDispRpt : aspDispRptList)
			{
				String targetInstCode = seqRemapper.getTargetInstCode(aspDispRpt.getHospCode(), aspDispRpt.getWardCode());
				String oriPasWardCode = aspDispRpt.getPasWardCode();
				
				if(!StringUtils.isBlank(targetInstCode) && !StringUtils.isBlank(oriPasWardCode))
				{
					aspDispRpt.setPasWardCode(oriPasWardCode+"("+targetInstCode+")");
				}
				
				AspRpt aspRpt = JAXB_WRAPPER.unmarshall(aspDispRpt.getRptXml());
				aspDispRpt.setAlertProfileStatus(aspRpt.getPatientDemography().getAlertProfileStatus());

				if(AlertProfileStatus.Error.equals(aspRpt.getPatientDemography().getAlertProfileStatus())){
					aspDispRpt.setAlertProfileErrorMsg(aspRpt.getPatientDemography().getAlertProfileErrMsg());
				}
				
			}
		}

		auditLogger.log("#0057",uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),serializer.deepSerialize(aspRptCriteriaIn));
		
		return aspDispRptList;

	}

	public static class AspDispReportComparator implements Comparator<AspDispRpt>, Serializable
	{
		private static final long serialVersionUID = -1629369536048145337L;

		@Override
		public int compare(AspDispRpt c1, AspDispRpt c2) {

			int a = c2.getDispDate().compareTo(c1.getDispDate());
			if(a != 0){
				return a;
			}

			if( c1.getPasSpecCode() != null && c2.getPasSpecCode() != null){
				a = c1.getPasSpecCode().compareTo(c2.getPasSpecCode());
				if(a != 0){
					return a;
				}
			}

			if(c1.getPasWardCode() != null && c2.getPasWardCode() != null){
				a = c1.getPasWardCode().compareTo(c2.getPasWardCode());
				if(a != 0){
					return a;
				}
			}

			if(c1.getBedNum() != null && c2.getBedNum() != null){
				a = c1.getBedNum().compareTo(c2.getBedNum());
				if(a != 0){
					return a;
				}
			}

			if(c1.getHkid() != null && c2.getHkid() != null){
				a = c1.getHkid().compareTo(c2.getHkid());
				if(a != 0){
					return a;
				}
			}

			a = c2.getItemCode().compareTo(c1.getItemCode());
			if(a != 0){
				return a;
			}

			String[] strArr = {"N","E","I","S","D"};
			int y = 0;
			int z = 0;

			for(int j = 0 ; j<strArr.length; j++){

				if(strArr[j].equals(c1.getStatus().getDataValue())){
					y = j+1;
				}
				if(strArr[j].equals(c1.getAdminStatus().getDataValue())){
					y = j+1;
				}
				if(strArr[j].equals(c2.getStatus().getDataValue())){
					z = j+1;
				}
				if(strArr[j].equals(c2.getAdminStatus().getDataValue())){
					z = j+1;
				}
			}
			if("L".equals(c1.getAdminStatus().getDataValue())){
				y = 0;
			}
			if("L".equals(c2.getAdminStatus().getDataValue())){
				z = 0;
			}

			if(y == z){
				return c1.getSerialNum().compareTo(c2.getSerialNum());
			}else{
				return y>z? 1:-1;
			}

		}						
	}


	private boolean checkIncludeOldReport(AspRptCriteria aspRptCriteriaIn) {
		return aspRptCriteriaIn.getIncludeOldRpt();
	}


	private boolean isEmptyReportType(AspRptCriteria aspRptCriteriaIn) {
		return aspRptCriteriaIn.getReportType() == null;
	}

	private boolean isAllDrugName(AspRptCriteria aspRptCriteriaIn) {
		return YesNoBlankFlag.Yes.equals(aspRptCriteriaIn.getAllDrugFlag())  ;
	}


	private boolean isAllSpecialty(AspRptCriteria aspRptCriteriaIn) {
		return YesNoBlankFlag.Yes.equals(aspRptCriteriaIn.getAllSpecFlag()) ;
	}


	private boolean isEmptySerialNum(AspRptCriteria aspRptCriteriaIn) {
		return StringUtils.isEmpty(aspRptCriteriaIn.getSerialNum());
	}


	private boolean isEmptyHKID(AspRptCriteria aspRptCriteriaIn) {
		return StringUtils.isEmpty(aspRptCriteriaIn.getHkid());
	}


	@Remove
	public void destroy(){
	}



}
