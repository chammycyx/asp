create table ASP_USER
(
	USER_CODE	varchar2(12) not null,
	CREATE_USER	varchar2(12) not null,
	CREATE_DATE	timestamp not null,
	UPDATE_USER	varchar2(12) not null,
	UPDATE_DATE	timestamp not null,
	VERSION		varchar2(19) not null,
	constraint PK_ASP_USER primary key (USER_CODE) using index tablespace ASP_INDX_01
)
tablespace ASP_DATA_01;

comment on column ASP_USER.USER_CODE is 'User code';
comment on column ASP_USER.CREATE_USER is 'Created user';
comment on column ASP_USER.CREATE_DATE is 'Created date';
comment on column ASP_USER.UPDATE_USER is 'Last updated user';
comment on column ASP_USER.UPDATE_DATE is 'Last updated date';
comment on column ASP_USER.VERSION is 'Version to serve as optimistic lock value';

create table ASP_USER_HOSP
(
	USER_CODE varchar2(12) not null,
	HOSP_CODE varchar2(3) not null,
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,
	constraint PK_ASP_USER_HOSP primary key (USER_CODE,HOSP_CODE) using index tablespace ASP_INDX_01
)
tablespace ASP_DATA_01;

comment on column ASP_USER_HOSP.USER_CODE is 'User code';
comment on column ASP_USER_HOSP.HOSP_CODE is 'Hospital code';
comment on column ASP_USER_HOSP.CREATE_USER is 'Created user';
comment on column ASP_USER_HOSP.CREATE_DATE is 'Created date';
comment on column ASP_USER_HOSP.UPDATE_USER is 'Last updated user';
comment on column ASP_USER_HOSP.UPDATE_DATE is 'Last updated date';
comment on column ASP_USER_HOSP.VERSION is 'Version to serve as optimistic lock value';

create table ASP_DISP_ITEM
	(
	ID number(19,0) not null,
	CASE_NUM varchar2(12),
	HOSP_CODE varchar2(3) not null,
	ITEM_CODE varchar2(6) not null,
	ITEM_NUM number(10,0),
	HKID varchar2(12),
	PAT_KEY varchar2(8),
	DISP_QTY number(19,4),
	DISP_DATE date,
	WARD_CODE varchar2(4),
	SPEC_CODE varchar2(4),
	DRUG_NAME varchar2(59),
	STRENGTH varchar2(59),
	FORM_CODE varchar2(3),
	FORM_DESC varchar2(30),
	FREQ_CODE varchar2(7),
	FREQ_DESC varchar2(42),
	DOSE_QTY varchar2(19),
	DOSE_UNIT varchar2(15),
	START_DATE date,
	BATCH_DATE date,
	ASP_TYPE varchar2(2) not null,
	STATUS varchar2(1) not null,
	ASP_DISP_RPT_ID number(19,0),
	PAT_HOSP_CODE varchar2(3),
	DISP_ORDER_ID number(19,0),
	PAT_NAME varchar2(48),
	DOB timestamp(6),
	SEX varchar2(1),
	CC_CODE varchar2(100),
	BED_NUM	varchar2(5),
	ADMISSION_DATE timestamp(6),
	DISCHARGE_DATE timestamp(6),
	PAS_SPEC_CODE varchar2(4),
	PAS_WARD_CODE varchar2(4),
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,	
	constraint PK_ASP_DISP_ITEM primary key (ID) using index tablespace ASP_INDX_01
	)
	partition by list (HOSP_CODE)
	(
		partition P_ASP_DATA_AHN values ('AHN') tablespace P_ASP_DATA_AHN,
		partition P_ASP_DATA_CMC values ('CMC') tablespace P_ASP_DATA_CMC,
		partition P_ASP_DATA_KH values ('KH') tablespace P_ASP_DATA_KH,
		partition P_ASP_DATA_KWH values ('KWH') tablespace P_ASP_DATA_KWH,
		partition P_ASP_DATA_NDH values ('NDH') tablespace P_ASP_DATA_NDH,
		partition P_ASP_DATA_PMH values ('PMH') tablespace P_ASP_DATA_PMH,
		partition P_ASP_DATA_PWH values ('PWH') tablespace P_ASP_DATA_PWH,
		partition P_ASP_DATA_PYN values ('PYN') tablespace P_ASP_DATA_PYN,
		partition P_ASP_DATA_QEH values ('QEH') tablespace P_ASP_DATA_QEH,
		partition P_ASP_DATA_QMH values ('QMH') tablespace P_ASP_DATA_QMH,
		partition P_ASP_DATA_RH values ('RH') tablespace P_ASP_DATA_RH,
		partition P_ASP_DATA_TKO values ('TKO') tablespace P_ASP_DATA_TKO,
		partition P_ASP_DATA_TMH values ('TMH') tablespace P_ASP_DATA_TMH,
		partition P_ASP_DATA_TWH values ('TWH') tablespace P_ASP_DATA_TWH,
		partition P_ASP_DATA_UCH values ('UCH') tablespace P_ASP_DATA_UCH,
		partition P_ASP_DATA_YCH values ('YCH') tablespace P_ASP_DATA_YCH,
		partition P_ASP_DATA_DEFAULT values (DEFAULT) tablespace P_ASP_DATA_DEFAULT
	);

comment on table ASP_DISP_ITEM is 'ASP Dispensing order items';
comment on column ASP_DISP_ITEM.ID is 'ID';
comment on column ASP_DISP_ITEM.CASE_NUM is 'Medication case number';
comment on column ASP_DISP_ITEM.HOSP_CODE is 'Disp. hospital code';
comment on column ASP_DISP_ITEM.ITEM_CODE is 'Item code';
comment on column ASP_DISP_ITEM.ITEM_NUM is 'Item number';
comment on column ASP_DISP_ITEM.HKID is 'HKID';
comment on column ASP_DISP_ITEM.PAT_KEY is 'Patient key';
comment on column ASP_DISP_ITEM.DISP_QTY is 'Dispensed quantity';
comment on column ASP_DISP_ITEM.DISP_DATE is 'Dispensing date';
comment on column ASP_DISP_ITEM.WARD_CODE is 'Ward code';
comment on column ASP_DISP_ITEM.SPEC_CODE is 'Specialty code';
comment on column ASP_DISP_ITEM.DRUG_NAME is 'Drug name';
comment on column ASP_DISP_ITEM.STRENGTH is 'Strength';
comment on column ASP_DISP_ITEM.FORM_CODE is 'Form code';
comment on column ASP_DISP_ITEM.FORM_DESC is 'Form description';
comment on column ASP_DISP_ITEM.FREQ_CODE is 'Frequency code';
comment on column ASP_DISP_ITEM.FREQ_DESC is 'Frequency description';
comment on column ASP_DISP_ITEM.DOSE_QTY is 'Dose quantity';
comment on column ASP_DISP_ITEM.DOSE_UNIT is 'Dose unit';
comment on column ASP_DISP_ITEM.START_DATE is 'Start Date';
comment on column ASP_DISP_ITEM.BATCH_DATE is 'Batch Date';
comment on column ASP_DISP_ITEM.ASP_TYPE is 'ASP Type (BG,IV)';
comment on column ASP_DISP_ITEM.STATUS is 'Status';
comment on column ASP_DISP_ITEM.ASP_DISP_RPT_ID is 'Asp Report Id';
comment on column ASP_DISP_ITEM.PAT_HOSP_CODE is 'Patient hospital code';
comment on column ASP_DISP_ITEM.DISP_ORDER_ID is 'Disp order id';
comment on column ASP_DISP_ITEM.CREATE_USER is 'Created user';
comment on column ASP_DISP_ITEM.CREATE_DATE is 'Created date';
comment on column ASP_DISP_ITEM.UPDATE_USER is 'Last updated user';
comment on column ASP_DISP_ITEM.UPDATE_DATE is 'Last updated date';
comment on column ASP_DISP_ITEM.VERSION is 'Version to serve as optimistic lock value';
comment on column ASP_DISP_ITEM.PAT_NAME is 'Patient name';
comment on column ASP_DISP_ITEM.DOB is 'Date of birth';
comment on column ASP_DISP_ITEM.SEX is 'Sex';
comment on column ASP_DISP_ITEM.CC_CODE is 'Cc code';
comment on column ASP_DISP_ITEM.BED_NUM is 'Bed number';
comment on column ASP_DISP_ITEM.ADMISSION_DATE is 'Admission date';
comment on column ASP_DISP_ITEM.DISCHARGE_DATE is 'Discharge date';
comment on column ASP_DISP_ITEM.PAS_SPEC_CODE is 'Pas specialty code';
comment on column ASP_DISP_ITEM.PAS_WARD_CODE is 'Pas ward code';

create table ASP_DISP_RPT
	(
	ID number(19,0) not null,
	HOSP_CODE varchar2(3) not null,
	RPT_TYPE varchar2(2) not null,
	SERIAL_NUM varchar2(13) not null,
	RPT_XML clob,
	RPT_DATE timestamp not null,
	HKID varchar2(12),
	PAS_SPEC_CODE varchar2(4),
	PAS_WARD_CODE varchar2(4),
	STATUS varchar2(1) not null,
	PAT_NAME varchar2(48),
	DRUG_NAME varchar2(59),
	DISP_DATE date not null,
	DOB timestamp(6),
	SEX varchar2(1),
	CC_CODE varchar2(100),
	PAT_KEY varchar2(8),
	ADMIN_STATUS varchar2(1) not null,
	DISCHARGE_FLAG number(1) not null,
	WORKSTATION_ID varchar2(19),
	ASP_DISP_ITEM_ID number(19,0) not null,
	CREATE_USER varchar2(12) not null,
	CREATE_DATE timestamp not null,
	UPDATE_USER varchar2(12) not null,
	UPDATE_DATE timestamp not null,
	VERSION varchar2(19) not null,	
	constraint PK_ASP_DISP_RPT primary key (ID) using index tablespace ASP_INDX_01
	)
partition by list (HOSP_CODE)
	(
		partition P_ASP_DATA_AHN values ('AHN') tablespace P_ASP_DATA_AHN,
		partition P_ASP_DATA_CMC values ('CMC') tablespace P_ASP_DATA_CMC,
		partition P_ASP_DATA_KH values ('KH') tablespace P_ASP_DATA_KH,
		partition P_ASP_DATA_KWH values ('KWH') tablespace P_ASP_DATA_KWH,
		partition P_ASP_DATA_NDH values ('NDH') tablespace P_ASP_DATA_NDH,
		partition P_ASP_DATA_PMH values ('PMH') tablespace P_ASP_DATA_PMH,
		partition P_ASP_DATA_PWH values ('PWH') tablespace P_ASP_DATA_PWH,
		partition P_ASP_DATA_PYN values ('PYN') tablespace P_ASP_DATA_PYN,
		partition P_ASP_DATA_QMH values ('QMH') tablespace P_ASP_DATA_QMH,
		partition P_ASP_DATA_QEH values ('QEH') tablespace P_ASP_DATA_QEH,
		partition P_ASP_DATA_RH values ('RH') tablespace P_ASP_DATA_RH,
		partition P_ASP_DATA_TKO values ('TKO') tablespace P_ASP_DATA_TKO,
		partition P_ASP_DATA_TWH values ('TWH') tablespace P_ASP_DATA_TWH,
		partition P_ASP_DATA_TMH values ('TMH') tablespace P_ASP_DATA_TMH,
		partition P_ASP_DATA_UCH values ('UCH') tablespace P_ASP_DATA_UCH,
		partition P_ASP_DATA_YCH values ('YCH') tablespace P_ASP_DATA_YCH,
		partition P_ASP_DATA_DEFAULT values (DEFAULT) tablespace P_ASP_DATA_DEFAULT
	);

comment on column ASP_DISP_RPT.ID is 'ID';
comment on column ASP_DISP_RPT.HOSP_CODE is 'Hospital code';
comment on column ASP_DISP_RPT.RPT_TYPE is 'ASP Type (BG,IV)';
comment on column ASP_DISP_RPT.SERIAL_NUM is 'serial number';
comment on column ASP_DISP_RPT.RPT_XML is 'report details in xml';
comment on column ASP_DISP_RPT.RPT_DATE is 'report date';
comment on column ASP_DISP_RPT.HKID is 'HKID';
comment on column ASP_DISP_RPT.PAS_SPEC_CODE is 'pas specialty code';
comment on column ASP_DISP_RPT.PAS_WARD_CODE is 'pas ward code';
comment on column ASP_DISP_RPT.STATUS is 'status';
comment on column ASP_DISP_RPT.PAT_NAME is 'patient name';
comment on column ASP_DISP_RPT.DRUG_NAME is 'drug name';
comment on column ASP_DISP_RPT.DISP_DATE is 'Dispensing date';
comment on column ASP_DISP_RPT.DOB is 'Date of birth';
comment on column ASP_DISP_RPT.SEX is 'Sex';
comment on column ASP_DISP_RPT.CC_CODE is 'HKID CC code';
comment on column ASP_DISP_RPT.PAT_KEY is 'Patient key';
comment on column ASP_DISP_RPT.ADMIN_STATUS is 'admin status';
comment on column ASP_DISP_RPT.DISCHARGE_FLAG is 'discharge flag';
comment on column ASP_DISP_RPT.WORKSTATION_ID is 'workstation id';
comment on column ASP_DISP_RPT.ASP_DISP_ITEM_ID is 'Asp Disp Item Id';
comment on column ASP_DISP_RPT.CREATE_USER is 'Created user';
comment on column ASP_DISP_RPT.CREATE_DATE is 'Created date';
comment on column ASP_DISP_RPT.UPDATE_USER is 'Last updated user';
comment on column ASP_DISP_RPT.UPDATE_DATE is 'Last updated date';
comment on column ASP_DISP_RPT.VERSION is 'Version to serve as optimistic lock value';

alter table ASP_USER_HOSP add constraint FK_ASP_USER_HOSP_01 foreign key (USER_CODE) references ASP_USER (USER_CODE);
alter table ASP_DISP_ITEM add constraint FK_ASP_DISP_ITEM_01 foreign key (ASP_DISP_RPT_ID) references ASP_DISP_RPT (ID);
alter table ASP_DISP_RPT add constraint FK_ASP_DISP_RPT_01 foreign key (ASP_DISP_ITEM_ID) references ASP_DISP_ITEM (ID);

create index FK_ASP_USER_HOSP_01 on ASP_USER_HOSP (USER_CODE) tablespace ASP_INDX_01;
create index FK_ASP_DISP_ITEM_01 on ASP_DISP_ITEM (ASP_DISP_RPT_ID) tablespace ASP_INDX_01;
create index FK_ASP_DISP_RPT_01 on ASP_DISP_RPT (ASP_DISP_ITEM_ID) tablespace ASP_INDX_01;

--//@UNDO
drop index FK_ASP_DISP_RPT_01;
drop index FK_ASP_DISP_ITEM_01;
drop index FK_ASP_USER_HOSP_01;

drop table ASP_DISP_RPT cascade constraints;
drop table ASP_DISP_ITEM cascade constraints;
drop table ASP_USER_HOSP;
drop table ASP_USER;
--//
