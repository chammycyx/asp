create sequence SQ_ASP_DISP_ITEM increment by 50 start with 10000000000049;
create sequence SQ_ASP_DISP_RPT increment by 50 start with 10000000000049;

--//@UNDO
drop sequence SQ_ASP_DISP_ITEM;
drop sequence SQ_ASP_DISP_RPT;
--//