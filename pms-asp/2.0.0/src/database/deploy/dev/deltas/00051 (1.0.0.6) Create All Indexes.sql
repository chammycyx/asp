create index I_ASP_DISP_RPT_01 on ASP_DISP_RPT (HOSP_CODE, DISP_DATE) online;
create index I_ASP_DISP_RPT_02 on ASP_DISP_RPT (ADMIN_STATUS, DISP_DATE) online;

create index I_ASP_DISP_ITEM_01 on ASP_DISP_ITEM (HOSP_CODE, STATUS, BATCH_DATE) online;
create index I_ASP_DISP_ITEM_02 on ASP_DISP_ITEM (PAT_HOSP_CODE, CASE_NUM) online;


--//@UNDO
drop index I_ASP_DISP_RPT_01;
drop index I_ASP_DISP_RPT_02;

drop index I_ASP_DISP_ITEM_01;
drop index I_ASP_DISP_ITEM_02;
--//