alter table ASP_DISP_ITEM add last_disp_date DATE null;

comment on column ASP_DISP_ITEM.last_disp_date is 'Last dispense date. Update when item is dispensed within date range.';

--//@UNDO
alter table ASP_DISP_ITEM set unused column last_disp_date;
--//