@echo off
if "%DATABASE_ID%" == "" (
	set DATABASE_ID=%WORKSTATION_ID%
) 
@echo on

call mvn clean install %*

if %ERRORLEVEL% neq 0 goto END

@echo off
for %%* in (.) do set CURR_FOLDER=%%~n*
@echo on

cd pms-asp-ear
call mvn weblogic:deploy %*
cd ..

:END