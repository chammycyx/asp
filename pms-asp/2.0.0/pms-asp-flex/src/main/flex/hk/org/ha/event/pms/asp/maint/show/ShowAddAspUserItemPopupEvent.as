package hk.org.ha.event.pms.asp.maint.show {
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class ShowAddAspUserItemPopupEvent extends AbstractTideEvent 
	{
		private var _popupOkHandler:Function;
		private var _popupCancelHandler:Function;
		
		public function ShowAddAspUserItemPopupEvent(popupOkHandler:Function,popupCancelHandler:Function):void 
		{
			super();
			_popupOkHandler = popupOkHandler;
			_popupCancelHandler = popupCancelHandler;
		}
		
		public function get popupOkHandler():Function
		{
			return _popupOkHandler;
		}
		
		public function get popupCancelHandler():Function
		{
			return _popupCancelHandler;
		}
	}
}