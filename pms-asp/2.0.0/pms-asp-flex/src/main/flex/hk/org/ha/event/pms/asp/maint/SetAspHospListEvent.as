package hk.org.ha.event.pms.asp.maint
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class SetAspHospListEvent extends AbstractTideEvent
	{
		
		private var _maintName:String;
	
		private var _aspHospList:ArrayCollection;
		
		public function SetAspHospListEvent(maintName:String=null, aspHospList:ArrayCollection=null):void 
		{
			super();
			_maintName = maintName;
			_aspHospList = aspHospList;
		}
		
		public function get maintName():String {
			return _maintName;
		}
		
		public function get aspHospList():ArrayCollection {
			return _aspHospList;
		}

	}
}