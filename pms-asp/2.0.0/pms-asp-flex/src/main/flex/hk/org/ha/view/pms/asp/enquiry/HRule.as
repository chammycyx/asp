package hk.org.ha.view.pms.asp.enquiry
{
	import mx.controls.HRule;
	
	public class HRule extends mx.controls.HRule
	{
		public function HRule()
		{
			super();
		}
		
		override protected function measure():void
		{
			super.measure();
			measuredWidth = 1;
		}
	}
}