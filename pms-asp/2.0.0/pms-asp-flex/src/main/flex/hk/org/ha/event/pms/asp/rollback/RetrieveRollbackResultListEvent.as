package  hk.org.ha.event.pms.asp.rollback {
	
	import hk.org.ha.model.pms.asp.vo.enquiry.AspRptCriteria;
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveRollbackResultListEvent extends AbstractTideEvent 
	{
		
		private var _callBackFunction:Function;
		private var _aspRptCriteria:AspRptCriteria;
		
		public function RetrieveRollbackResultListEvent(aspRptCriteria: AspRptCriteria, callBackFunction:Function):void 
		{
			super();
			_callBackFunction= callBackFunction;
			_aspRptCriteria = aspRptCriteria;
		}
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
		
		
		public function get aspRptCriteria():AspRptCriteria
		{
			return _aspRptCriteria;
		}
	}
}
