package hk.org.ha.event.pms.asp.maint
{
	import mx.collections.ArrayCollection;
	
	import org.granite.tide.events.AbstractTideEvent;
		
	public class SetAspUserListEvent extends AbstractTideEvent
	{

		private var _aspUserList:ArrayCollection;
		
		public function SetAspUserListEvent(aspUserList:ArrayCollection=null):void 
		{
			super();
			_aspUserList = aspUserList;
		}
		
		public function get aspUserList():ArrayCollection {
			return _aspUserList;
		}

	}
}