package  hk.org.ha.event.pms.asp.enquiry {
	
	import hk.org.ha.model.pms.asp.vo.enquiry.AspRptCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class NotForReviewAspDispRptListEvent extends AbstractTideEvent 
	{
		
		private var _lockFunction:Function;
		private var _callBackFunction:Function;
		private var _aspRptCriteria:AspRptCriteria;
		
		public function NotForReviewAspDispRptListEvent( aspRptCriteria: AspRptCriteria, callBackFunction:Function=null, lockFunction:Function=null):void 
		{
			super();
			_lockFunction= lockFunction;
			_callBackFunction= callBackFunction;
			_aspRptCriteria = aspRptCriteria;
		}
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
		
		public function get lockFunction():Function {
			return _lockFunction;
		}
		
		public function get aspRptCriteria():AspRptCriteria
		{
			return _aspRptCriteria;
		}
		
	}
}
