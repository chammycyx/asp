package hk.org.ha.event.pms.asp.maint.show
{
	import org.granite.tide.events.AbstractTideEvent;
		
	public class ShowItemMaintViewEvent extends AbstractTideEvent
	{
		private var _clearMessages:Boolean;
				
		public function ShowItemMaintViewEvent(clearMessages:Boolean=true):void 
		{
			super();
			_clearMessages = clearMessages;
		}
			
		public function get clearMessages():Boolean {
			return _clearMessages;
		}
	}
}