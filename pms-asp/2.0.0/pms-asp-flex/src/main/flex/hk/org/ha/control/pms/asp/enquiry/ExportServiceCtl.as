package hk.org.ha.control.pms.asp.enquiry
{		
	import mx.messaging.config.ServerConfig;
	
	import hk.org.ha.event.pms.asp.enquiry.ExportAspDispRptListEvent;
	import hk.org.ha.model.pms.asp.biz.enquiry.ExportServiceBean;
	import hk.org.ha.model.pms.asp.persistence.ExtendedExternalInterface;
	
	import org.granite.tide.events.TideResultEvent;
	
	
	[Bindable]
	[Name("ExportServiceCtl", restrict="false")]
	public class ExportServiceCtl
	{
		
		[In]
		public var exportService:ExportServiceBean;
		
		[Observer]
		public function exportReport(evt:ExportAspDispRptListEvent):void
		{
			exportService.exportAspRpt(evt.aspRptCriteria, exportRptResult);
			
		}
		
		public function exportRptResult(evt:TideResultEvent):void
		{
			
			var url:String = "exportRpt.seam";
			
			var winName:String ="excel_win";
			
			var argStr:String = "";
			
			if (ServerConfig.getChannel("my-graniteamf").uri.search("{server.name}") == -1) {
				url = ServerConfig.getChannel("my-graniteamf").uri.replace("graniteamf/amf", "") + url;
			}							
			ExtendedExternalInterface.call("window.open", url, winName, argStr );  
		}	
	}	
}
