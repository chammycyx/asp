package  hk.org.ha.event.pms.asp.enquiry {
	
	import hk.org.ha.model.pms.asp.vo.enquiry.AspRptCriteria;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RetrieveAspDispRptListEvent extends AbstractTideEvent 
	{
		
		private var _callBackFunction:Function;
		private var _aspRptCriteria:AspRptCriteria;
		
		public function RetrieveAspDispRptListEvent(aspRptCriteria: AspRptCriteria, callBackFunction:Function=null):void 
		{
			super();
			_callBackFunction= callBackFunction;
			_aspRptCriteria = aspRptCriteria;
		}
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
		
		
		public function get aspRptCriteria():AspRptCriteria
		{
			return _aspRptCriteria;
		}
	}
}
