package hk.org.ha.control.pms.sys
{
	import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
	import hk.org.ha.fmk.pms.flex.components.message.ShowSystemMessagePopupEvent;
	import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
	import hk.org.ha.model.pms.biz.sys.SystemMessageServiceBean;
	import hk.org.ha.fmk.pms.sys.entity.SystemMessage;
	
	import mx.core.UIComponent;
	
	import org.granite.tide.events.TideResultEvent;
	
	[Bindable]
	[Name("systemMessageServiceCtl", restrict="false")]
	public class SystemMessageServiceCtl
	{
		[In]
		public var systemMessageService:SystemMessageServiceBean;
		
		[In]
		public var systemMessage:SystemMessage; 
		
		private var sysMsgProp:SystemMessagePopupProp;
				
		[Observer]
		public function retrieveSystemMessage(evt:RetrieveSystemMessageEvent):void
		{		
			sysMsgProp = evt.sysMsgProp;				
			systemMessageService.retrieveSystemMessage(sysMsgProp.messageCode, sysMsgProp.messageParams, retrieveSystemMessageResult);
		}
		
		public function retrieveSystemMessageResult(evt:TideResultEvent):void 
		{				
			sysMsgProp.messageCode = systemMessage.messageCode;
			sysMsgProp.displayDesc = systemMessage.displayDesc;
			sysMsgProp.messageTitle = "Message Code: "+systemMessage.applicationId + "-" + systemMessage.severityCode + "-" + systemMessage.messageCode;
			
			if (sysMsgProp.functionId != null && sysMsgProp.functionId != "") {
				sysMsgProp.messageTitle += "-" + sysMsgProp.functionId;
			}
			evt.context.dispatchEvent(new ShowSystemMessagePopupEvent(sysMsgProp));
		}		
	}
}