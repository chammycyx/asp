package  hk.org.ha.event.pms.asp.rollback {
	
	import mx.collections.ArrayCollection;
	
	import hk.org.ha.model.pms.asp.udt.AspDispRptAdminStatus;
	
	import org.granite.tide.events.AbstractTideEvent;
	
	public class RollbackRptListEvent extends AbstractTideEvent 
	{
		
		private var _callBackFunction:Function;
		private var _adminStatus : AspDispRptAdminStatus;
		private var _aspRptList:ArrayCollection;
		
		public function RollbackRptListEvent(aspRptList: ArrayCollection, adminStatus:AspDispRptAdminStatus, callBackFunction:Function):void 
		{
			super();
			_callBackFunction= callBackFunction;
			_adminStatus=  adminStatus;
			_aspRptList = aspRptList;
		}
		
		public function get callBackFunction():Function {
			return _callBackFunction;
		}
		
		public function get adminStatus():AspDispRptAdminStatus {
			return _adminStatus;
		}
		
		public function get aspRptList():ArrayCollection
		{
			return _aspRptList;
		}
	}
}
