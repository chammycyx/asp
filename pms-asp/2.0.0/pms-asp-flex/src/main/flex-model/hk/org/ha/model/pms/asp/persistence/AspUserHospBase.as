/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR. INSTEAD, EDIT THE INHERITED CLASS (AspUserHosp.as).
 */

package hk.org.ha.model.pms.asp.persistence {

    import flash.utils.IDataInput;
    import flash.utils.IDataOutput;
    import flash.utils.getQualifiedClassName;
    import hk.org.ha.model.pms.persistence.VersionEntity;
    import mx.core.IUID;
    import mx.data.utils.Managed;
    import mx.utils.UIDUtil;
    import org.granite.collections.IPersistentCollection;
    import org.granite.meta;
    import org.granite.tide.IEntityManager;
    import org.granite.tide.IPropertyHolder;

    use namespace meta;

    [Managed]
    public class AspUserHospBase extends VersionEntity {

        [Transient]
        meta var entityManager:IEntityManager = null;

        private var __initialized:Boolean = true;
        private var __detachedState:String = null;

        private var _aspUser:AspUser;
        private var _hospCode:String;
        private var _userCode:String;

        meta override function isInitialized(name:String = null):Boolean {
            if (!name)
                return __initialized;

            var property:* = this[name];
            return (
                (!(property is AspUserHosp) || (property as AspUserHosp).meta::isInitialized()) &&
                (!(property is IPersistentCollection) || (property as IPersistentCollection).isInitialized())
            );
        }
        
        [Bindable(event="dirtyChange")]
		public function get meta_dirty():Boolean {
			return Managed.getProperty(this, "meta_dirty", false);
		}

        public function set hospCode(value:String):void {
            _hospCode = value;
        }
        [Id]
        public function get hospCode():String {
            return _hospCode;
        }

        public function set userCode(value:String):void {
            _userCode = value;
        }
        public function get userCode():String {
            return _userCode;
        }

        public function set uid(value:String):void {
            // noop...
        }
        public function get uid():String {
            if (!_hospCode && !_userCode)
                return UIDUtil.createUID();
            return getQualifiedClassName(this) + "#[" + String(_hospCode) + "," + String(_userCode) + "]";
        }

        meta override function merge(em:IEntityManager, obj:*):void {
            var src:AspUserHospBase = AspUserHospBase(obj);
            __initialized = src.__initialized;
            __detachedState = src.__detachedState;
            if (meta::isInitialized()) {
                super.meta::merge(em, obj);
               em.meta_mergeExternal(src._aspUser, _aspUser, null, this, 'aspUser', function setter(o:*):void{_aspUser = o as AspUser}, false);
               em.meta_mergeExternal(src._hospCode, _hospCode, null, this, 'hospCode', function setter(o:*):void{_hospCode = o as String}, false);
               em.meta_mergeExternal(src._userCode, _userCode, null, this, 'userCode', function setter(o:*):void{_userCode = o as String}, false);
            }
            else {
               em.meta_mergeExternal(src._hospCode, _hospCode, null, this, 'hospCode', function setter(o:*):void{_hospCode = o as String});
               em.meta_mergeExternal(src._userCode, _userCode, null, this, 'userCode', function setter(o:*):void{_userCode = o as String});
            }
        }

        public override function readExternal(input:IDataInput):void {
            __initialized = input.readObject() as Boolean;
            __detachedState = input.readObject() as String;
            if (meta::isInitialized()) {
                super.readExternal(input);
                _aspUser = input.readObject() as AspUser;
                _hospCode = input.readObject() as String;
                _userCode = input.readObject() as String;
            }
            else {
                var id:AspUserHospPK = input.readObject() as AspUserHospPK;
                if (id) {
                    _hospCode = id.hospCode;
                    _userCode = id.userCode;
            	}
            }
        }

        public override function writeExternal(output:IDataOutput):void {
            output.writeObject(__initialized);
            output.writeObject(__detachedState);
            if (meta::isInitialized()) {
                super.writeExternal(output);
                output.writeObject((_aspUser is IPropertyHolder) ? IPropertyHolder(_aspUser).object : _aspUser);
                output.writeObject((_hospCode is IPropertyHolder) ? IPropertyHolder(_hospCode).object : _hospCode);
                output.writeObject((_userCode is IPropertyHolder) ? IPropertyHolder(_userCode).object : _userCode);
            }
            else {
                var id:AspUserHospPK = new AspUserHospPK();
                id.hospCode = _hospCode;
                id.userCode = _userCode;
                output.writeObject(id);
            }
        }
    }
}
