/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.asp.vo.enquiry {

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.asp.vo.enquiry.Medication")]
    public class Medication extends MedicationBase {
		
		
		public function getDayChecklist(day:int):Checklist{
		
			if(this.checklistList != null){
			
				for(var i:int=0 ; i < this.checklistList.length; i++){
					var checklist:Checklist = this.checklistList.getItemAt(i) as Checklist;
					var dayN:String = "day"+day.toString();
					if(checklist.name.indexOf(dayN) > -1 ){
						return checklist;
					}
				
				}
			
			}
			
			return new Checklist();
		
		}
		
		
		public function getConcurrentList(i:int):String{
			
			if(this.concurrentList != null && this.concurrentList.length >= i ){
				var concurrent:String = this.concurrentList.getItemAt(i) as String;
				return concurrent;
			}else{
				return "";
			}
			
		}
		
		public function getPreviousList(i:int):String{
			
			if(this.previousList != null && this.previousList.length >= i ){
				var previous:String = this.previousList.getItemAt(i) as String;
				return previous;
			}else{
				return "";
			}
			
		}
		
		
		public function getWBCList(i:int):String{
			
			if(this.wbcValueList != null){
				if(this.wbcValueList.length > i){
					var wbc:String = this.wbcValueList.getItemAt(i) as String;
					return wbc;
				}
			}
			
			return "";
			
		}
		
    }
}