/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * WARNING: DO NOT CHANGE THIS FILE. IT MAY BE OVERWRITTEN EACH TIME YOU USE
 * THE GENERATOR.
 */

package hk.org.ha.model.pms.asp.udt {

    import org.granite.util.Enum;

    [Bindable]
    [RemoteClass(alias="hk.org.ha.model.pms.asp.udt.PastMediHistory")]
    public class PastMediHistory extends Enum {

        public static const DM:PastMediHistory = new PastMediHistory("DM", _, "D", "DM");
        public static const HT:PastMediHistory = new PastMediHistory("HT", _, "H", "HT");
        public static const IHD:PastMediHistory = new PastMediHistory("IHD", _, "I", "IHD");
        public static const COAD:PastMediHistory = new PastMediHistory("COAD", _, "C", "COAD");
        public static const ESRF:PastMediHistory = new PastMediHistory("ESRF", _, "E", "ERSF");
        public static const Others:PastMediHistory = new PastMediHistory("Others", _, "O", "Others");

        function PastMediHistory(value:String = null, restrictor:* = null, dataValue:String = null, displayValue:String = null) {
            super((value || DM.name), restrictor, (dataValue || DM.dataValue), (displayValue || DM.displayValue));
        }

        protected override function getConstants():Array {
            return constants;
        }

        public static function get constants():Array {
            return [DM, HT, IHD, COAD, ESRF, Others];
        }

        public static function valueOf(name:String):PastMediHistory {
            return PastMediHistory(DM.constantOf(name));
        }

        public static function dataValueOf(dataValue:String):PastMediHistory {
            return PastMediHistory(DM.dataConstantOf(dataValue));
        }
    }
}