/**
 * Generated by Gas3 v2.2.0 (Granite Data Services).
 *
 * NOTE: this file is only generated if it does not exist. You may safely put
 * your custom code here.
 */

package hk.org.ha.model.pms.biz.security {

    [RemoteClass(alias="hk.org.ha.model.pms.biz.security.PostLogonServiceBean")]
    public class PostLogonServiceBean extends PostLogonServiceBeanBase {
    }
    
}
