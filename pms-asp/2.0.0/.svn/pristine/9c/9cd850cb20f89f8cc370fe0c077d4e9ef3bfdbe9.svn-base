package hk.org.ha.model.pms.asp.udt.epr;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum EprLabItemType implements StringValuedEnum {
	
	WBC("1298", "WBC"),
	Neu("943", "Neu"),
	ANC("945", "ANC"),
	Plt("1042", "Plt"),
	ESR("608", "ESR"),
	ALT("48", "ALT"),
	ALP("70", "ALP"),
	Bil("231", "Bil"),
	CRP("505", "CRP"),
	Ur("1261", "Ur"),
	Cr("509", "Cr");
	
	private final String dataValue;
	
	private final String displayValue;

	EprLabItemType(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
	@Override
	public String getDataValue() {
		return dataValue;
	}

	@Override
	public String getDisplayValue() {
		return displayValue;
	}

    public static EprLabItemType dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(EprLabItemType.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<EprLabItemType> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<EprLabItemType> getEnumClass() {
    		return EprLabItemType.class;
    	}
    }
}
