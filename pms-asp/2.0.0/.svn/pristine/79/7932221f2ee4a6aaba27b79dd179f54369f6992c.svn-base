package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum AntibioticStatus implements StringValuedEnum {
	NotPrevious("N", "Not on Antibiotic Previously"),
	Switch("S", "Switch From");
	
	private final String dataValue;
	
	private final String displayValue;
	
	AntibioticStatus(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static AntibioticStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AntibioticStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<AntibioticStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<AntibioticStatus> getEnumClass() {
    		return AntibioticStatus.class;
    	}
    }
}
