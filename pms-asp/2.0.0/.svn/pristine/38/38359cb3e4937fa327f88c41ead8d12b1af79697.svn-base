<?xml version="1.0" encoding="utf-8"?>
<fc:ExtendedNavigatorContent
	xmlns:fc="hk.org.ha.fmk.pms.flex.components.core.*"
	xmlns:util="hk.org.ha.pms.asp.util.*"
	xmlns:fx="http://ns.adobe.com/mxml/2009" 			   
	xmlns:s="library://ns.adobe.com/flex/spark"			   
	xmlns:mx="library://ns.adobe.com/flex/mx"
	xmlns:mdcs="com.iwobanas.controls.*"
	xmlns:mdgc="com.iwobanas.controls.dataGridClasses.*"
	xmlns:calendar="hk.org.ha.service.app.util.calendar.*"
	width="100%" height="100%">
	
	<fx:Metadata>
		[Name("itemMaint")]
	</fx:Metadata>
	
	<fx:Declarations>
		<mx:DateFormatter id="dateFormatter" formatString="DD-MMM-YYYY"/>
		<s:ArrayCollection id="aspHospItemBGList" list="{aspHospItemList}"
						   filterFunction="{filterAspHospItemBGList}"/>
		<s:ArrayCollection id="aspHospItemIVList" list="{aspHospItemList}"
						   filterFunction="{filterAspHospItemIVList}"/>
	</fx:Declarations>
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.collections.ArrayList;
			import mx.core.IFlexDisplayObject;
			import mx.core.UIComponent;
			import mx.managers.PopUpManager;
			
			import spark.events.IndexChangeEvent;
			
			import hk.org.ha.event.pms.asp.maint.RetrieveAspHospListEvent;
			import hk.org.ha.event.pms.asp.maint.UpdateAspHospEvent;
			import hk.org.ha.fmk.pms.flex.components.message.RetrieveSystemMessageEvent;
			import hk.org.ha.fmk.pms.flex.components.message.SystemMessagePopupProp;
			import hk.org.ha.model.pms.asp.persistence.AspHosp;
			import hk.org.ha.model.pms.asp.persistence.AspHospItem;
			import hk.org.ha.model.pms.asp.persistence.AspHospSpec;
			import hk.org.ha.model.pms.asp.udt.AspType;

			private var dummyAspHospSpec:AspHospSpec;
			private var dummyAspHospItem:AspHospItem;
			
			[Bindable]
			public var aspHospList:ArrayCollection;
			
			[Bindable]
			public var aspHospItemList:ArrayCollection;

			[Bindable]
			public var cbxEnableBG:Boolean = false;

			[Bindable]
			public var cbxEnableIV:Boolean = false;
			
			[Bindable]
			public var selectAllBG:Boolean = false;
			
			[Bindable]
			public var selectAllIV:Boolean = false;
			
			private var initScreen:Boolean = true;
			
			public override function onShowLater():void
			{
				if(!pmsToolbar.hasInited())
				{
					pmsToolbar.retrieveFunc = retrieveHandler;
					pmsToolbar.clearFunc = clearHandler;
					pmsToolbar.saveYesFunc = saveYesHandler;
					pmsToolbar.init();
				}
				
				if(initScreen) {
					retrieveAspHospList();
					initScreen = false;
					pmsToolbar.saveButton.enabled = false;
					pmsToolbar.retrieveButton.enabled = false;
					cbxEnableBG = false;
					cbxEnableIV = false;
					selectAllBG = false;
					selectAllIV = false;
					aspHospListDdl.setFocus();
				}
			}
			
			public function setAspHospList(aspHospResultList:ArrayCollection):void {
				aspHospList = aspHospResultList;
				aspHospListDdl.dataProvider = aspHospList;
				aspHospListDdl.setFocus();
			}
			
			public function aspHospDropDownLabelFunc(item:Object):String {
				if(item == null)
					return "";
				else
					return item.hospCode;
			}
			
			private function filterAspHospItemBGList(item:Object):Boolean
			{
				return item.aspType == AspType.BG;
			}
			
			private function filterAspHospItemIVList(item:Object):Boolean
			{
				return item.aspType == AspType.IV;
			}
			
			public function retrieveHandler():void {
				var currentHosp:AspHosp = aspHospListDdl.selectedItem;
				aspHospItemList = currentHosp.aspHospItemList as ArrayCollection;
				
				aspHospItemBGList.filterFunction = filterAspHospItemBGList;
				aspHospItemBGList.refresh();
				
				aspHospItemIVList.filterFunction = filterAspHospItemIVList;
				aspHospItemIVList.refresh();
				
				BGItemGrid.dataProvider = aspHospItemBGList;
				IVItemGrid.dataProvider = aspHospItemIVList;
				
				aspHospListDdl.enabled = false;
				pmsToolbar.saveButton.enabled = true;
				pmsToolbar.retrieveButton.enabled = false;
				BGItemGrid.setFocus();
				
				if(aspHospItemBGList.length > 0)
				{
					cbxEnableBG = true;
					updateBGVisibleAll();
				}
				else
				{
					cbxEnableBG = false;
					selectAllBG = false;
				}
				
				if(aspHospItemIVList.length > 0)
				{
					cbxEnableIV = true;
					updateIVVisibleAll();
				}
				else
				{
					cbxEnableIV = false;
					selectAllIV = false;
				}
				
			}
			
			public function retrieveAspHospList():void {
				dispatchEvent(new RetrieveAspHospListEvent("itemMaint"));
			}
			
			public function refreshUpdateItemList(evt:MouseEvent):void {
				PopUpManager.removePopUp(((evt.currentTarget as UIComponent).parentDocument) as IFlexDisplayObject);
				clearHandler();
			}
			
			public function clearHandler():void {
				BGItemGrid.dataProvider = new ArrayList();
				IVItemGrid.dataProvider = new ArrayList();
				aspHospListDdl.enabled = true;
				pmsToolbar.saveButton.enabled = false;
				pmsToolbar.retrieveButton.enabled = false;
				cbxEnableBG = false;
				cbxEnableIV = false;
				selectAllBG = false;
				selectAllIV = false;
				retrieveAspHospList();
			}
			
			public function saveYesHandler():void {
				dispatchEvent(new UpdateAspHospEvent("itemMaint",aspHospListDdl.selectedItem));
			}
			
			public function showSystemMessage(errorCode:String, params:Array=null, oKfunc:Function=null):void
			{
				var msgProp:SystemMessagePopupProp = new SystemMessagePopupProp();
				msgProp.messageCode = errorCode;  
				msgProp.messageParams = params;
				msgProp.messageWinHeight = 200;
				msgProp.setOkButtonOnly = true;
				
				if (oKfunc != null)
				{
					msgProp.okHandler = oKfunc;
				}
				
				dispatchEvent(new RetrieveSystemMessageEvent(msgProp));
			}
			
			protected function selectedHospChangeHandler(event:IndexChangeEvent):void
			{
				if(aspHospListDdl.selectedIndex > -1)
				{
					pmsToolbar.retrieveButton.enabled = true;
				}
				else
				{
					pmsToolbar.retrieveButton.enabled = false;
				}
			}
			
			private function dateTimeLabelFunc(item:Object, column:DataGridColumn):String {
				return dateTimeFormatter.format(item[column.dataField]);
			}
			
			public function updateBGVisibleAll():void {
				selectAllBG = true;
				for each(var aspHospItem:AspHospItem in aspHospItemBGList)
				{
					if(!aspHospItem.visibleFlag)
					{
						selectAllBG = false;
						break;
					}
				}
			}
			
			public function updateIVVisibleAll():void {
				selectAllIV = true;
				for each(var aspHospItem:AspHospItem in aspHospItemIVList)
				{
					if(!aspHospItem.visibleFlag)
					{
						selectAllIV = false;
						break;
					}
				}
			}
			
		]]>
	</fx:Script>
	
	<fc:layout>
		<s:VerticalLayout gap="0"/> 
	</fc:layout>
	<util:Toolbar id="pmsToolbar" width="100%"/>
	<s:HGroup width="100%" height="5%" gap="10" paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10" verticalAlign="middle">
		<s:Label text="Hospital" />
		<fc:ExtendedDropDownList id="aspHospListDdl" width="95" labelFunction="aspHospDropDownLabelFunc" change="selectedHospChangeHandler(event)" />
	</s:HGroup>
	<s:HGroup width="100%" height="95%" gap="10" paddingLeft="10" paddingRight="10" paddingTop="10" paddingBottom="10">
		<s:VGroup width="50%" height="100%" >
			<s:Panel title="Items for Big Gun" width="100%" height="100%" >
				<s:layout>
					<s:VerticalLayout gap="3" paddingBottom="10" paddingTop="10" paddingLeft="10" paddingRight="10"/>
				</s:layout>
				<fc:ExtendedDataGrid id="BGItemGrid" width="100%" height="100%"
											 resizableColumns="false" draggableColumns="false" sortableColumns="false">
					<fc:columns>
						<mx:DataGridColumn headerText="Item Code" dataField="itemCode" width="{BGItemGrid.width*0.25}"/>
						<mx:DataGridColumn headerText="Visible" dataField="visibleFlag" width="{BGItemGrid.width*0.15}" >
							<mx:headerRenderer>
								<fx:Component>
									<s:MXDataGridItemRenderer autoDrawBackground="false">
										<s:layout>
											<s:HorizontalLayout horizontalAlign="left" paddingLeft="5"/>
										</s:layout>
										<fx:Script>
											<![CDATA[
												import hk.org.ha.model.pms.asp.persistence.AspHosp;
												import hk.org.ha.model.pms.asp.persistence.AspHospItem;
												import mx.collections.ArrayCollection;
												
												protected function selectAllUpdate():void {
													for each(var aspHospItem:AspHospItem in outerDocument.aspHospItemBGList)
													{
														aspHospItem.visibleFlag = bgVisibleAll.selected;
													}
													outerDocument.selectAllBG = bgVisibleAll.selected;
												}
											]]>
										</fx:Script>
										<s:CheckBox label="Visible" id="bgVisibleAll" selected="{outerDocument.selectAllBG}" click="selectAllUpdate()" 
													enabled="{outerDocument.cbxEnableBG}"
													/>
									</s:MXDataGridItemRenderer>
								</fx:Component>
							</mx:headerRenderer>
							<mx:itemRenderer>
								<fx:Component>
									<s:MXDataGridItemRenderer autoDrawBackground="false">
										<s:layout>
											<s:HorizontalLayout horizontalAlign="center"/>
										</s:layout>
										<s:CheckBox id="visibleBGCbx" selected="@{data.visibleFlag}" click="outerDocument.updateBGVisibleAll()"/>
									</s:MXDataGridItemRenderer>
								</fx:Component>
							</mx:itemRenderer>
						</mx:DataGridColumn>
						<mx:DataGridColumn headerText="Update User" dataField="updateUser" width="{BGItemGrid.width*0.25}" />
						<mx:DataGridColumn headerText="Last Update Date" dataField="updateDate" width="{BGItemGrid.width*0.35}" labelFunction="dateTimeLabelFunc"/>
					</fc:columns>
				</fc:ExtendedDataGrid>
			</s:Panel>
		</s:VGroup>
		<s:VGroup width="50%" height="100%" >
			<s:Panel title="Items for IV-to-Oral Switch" width="100%" height="100%" >
				<s:layout>
					<s:VerticalLayout gap="3" paddingBottom="10" paddingTop="10" paddingLeft="10" paddingRight="10"/>
				</s:layout>
				<fc:ExtendedDataGrid id="IVItemGrid" width="100%" height="100%"
											 resizableColumns="false" draggableColumns="false" sortableColumns="false">
					<fc:columns>
						<mx:DataGridColumn headerText="Item Code" dataField="itemCode" width="{IVItemGrid.width*0.25}" />
						<mx:DataGridColumn headerText="Visible" dataField="visibleFlag" width="{IVItemGrid.width*0.15}" >
							<mx:headerRenderer>
								<fx:Component>
									<s:MXDataGridItemRenderer autoDrawBackground="false">
										<s:layout>
											<s:HorizontalLayout horizontalAlign="left" paddingLeft="5"/>
										</s:layout>
										<fx:Script>
											<![CDATA[
												import hk.org.ha.model.pms.asp.persistence.AspHosp;
												import hk.org.ha.model.pms.asp.persistence.AspHospItem;
												import mx.collections.ArrayCollection;
												
												protected function selectAllUpdate():void {
													for each(var aspHospItem:AspHospItem in outerDocument.aspHospItemIVList)
													{
														aspHospItem.visibleFlag = ivVisibleAll.selected;
													}
													outerDocument.selectAllIV = ivVisibleAll.selected;
												}
											]]>
										</fx:Script>
										<s:CheckBox label="Visible" id="ivVisibleAll" selected="{outerDocument.selectAllIV}" click="selectAllUpdate()" 
													enabled="{outerDocument.cbxEnableIV}"
													/>
									</s:MXDataGridItemRenderer>
								</fx:Component>
							</mx:headerRenderer>
							<mx:itemRenderer>
								<fx:Component>
									<s:MXDataGridItemRenderer autoDrawBackground="false">
										<s:layout>
											<s:HorizontalLayout horizontalAlign="center"/>
										</s:layout>
										<s:CheckBox id="visibleIVCbx" selected="@{data.visibleFlag}" click="outerDocument.updateIVVisibleAll()"/>
									</s:MXDataGridItemRenderer>
								</fx:Component>
							</mx:itemRenderer>
						</mx:DataGridColumn>
						<mx:DataGridColumn headerText="Update User" dataField="updateUser" width="{IVItemGrid.width*0.25}" />
						<mx:DataGridColumn headerText="Last Update Date" dataField="updateDate" width="{IVItemGrid.width*0.35}" labelFunction="dateTimeLabelFunc" />
					</fc:columns>
				</fc:ExtendedDataGrid>
			</s:Panel>
		</s:VGroup>
	</s:HGroup>
</fc:ExtendedNavigatorContent>