package hk.org.ha.model.pms.asp.biz.maint;

import java.text.SimpleDateFormat;
import java.util.Date;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;
import hk.org.ha.fmk.pms.audit.AuditLogger;
import hk.org.ha.fmk.pms.security.UamInfo;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.persistence.AspHosp;
import hk.org.ha.service.biz.pms.corp.interfaces.CorpAspServiceJmsRemote;


import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

@AutoCreate
@Stateful
@Scope(ScopeType.SESSION)
@Name("aspHospService")
@Restrict("#{identity.loggedIn}")
@RemoteDestination
@MeasureCalls
public class AspHospServiceBean implements AspHospServiceLocal {

	@In
	CorpAspServiceJmsRemote corpAspServiceProxy;

	private static final JSONSerializer serializer = new JSONSerializer().transform(new DateTransformer("yyyy-MM-dd HH:mm:ss.SSS"), Date.class);
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@In
	UamInfo uamInfo;

	@In
	private AuditLogger auditLogger;
	
	
	
	
	@Override
	public void updateAspHosp(AspHosp aspHosp, String originalScreen) {
		
		corpAspServiceProxy.updateAspHosp(aspHosp);
		
		auditLogger.log("#0074",originalScreen,uamInfo.getUserId(),uamInfo.getWorkstationId(),sdf.format(new Date()),serializer.deepSerialize(aspHosp));
		
	}

	@Remove
	public void destroy(){
	}

}
