package hk.org.ha.model.pms.asp.persistence;

import hk.org.ha.fmk.pms.entity.UpdateDate;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SEQ_TABLE")
public class SeqTable implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "SEQ_NAME", nullable = false, length=100)
	private String seqName;
	
	@Column(name = "MIN_NUM", nullable = false)
	private Long minNum;

	@Column(name = "MAX_NUM", nullable = false)
	private Long maxNum;

	@Column(name = "CURR_NUM", nullable = false)
	private Long currNum;

	@UpdateDate
	@Column(name = "UPDATE_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date updateDate;

	public String getSeqName() {
		return seqName;
	}

	public void setSeqName(String seqName) {
		this.seqName = seqName;
	}

	public Long getMinNum() {
		return minNum;
	}

	public void setMinNum(Long minNum) {
		this.minNum = minNum;
	}

	public Long getMaxNum() {
		return maxNum;
	}

	public void setMaxNum(Long maxNum) {
		this.maxNum = maxNum;
	}

	public Long getCurrNum() {
		return currNum;
	}

	public void setCurrNum(Long currNum) {
		this.currNum = currNum;
	}
	
	public Date getUpdateDate() {
		return cloneDate(updateDate);
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = cloneDate(updateDate);
	}
	
	private Date cloneDate(Date date) {
		return (date != null) ? new Date(date.getTime()) : null;
	}
}
