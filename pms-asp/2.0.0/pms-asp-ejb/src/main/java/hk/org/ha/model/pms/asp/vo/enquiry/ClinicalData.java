package hk.org.ha.model.pms.asp.vo.enquiry;
import hk.org.ha.model.pms.asp.udt.AntibioticStatus;
import hk.org.ha.model.pms.asp.udt.CategoryInfection;
import hk.org.ha.model.pms.asp.udt.DrRank;
import hk.org.ha.model.pms.asp.udt.Immunocompromise;
import hk.org.ha.model.pms.asp.udt.Inotrope;
import hk.org.ha.model.pms.asp.udt.OrganInolved;
import hk.org.ha.model.pms.asp.udt.PastMediHistory;
import hk.org.ha.model.pms.asp.udt.Treatment;
import hk.org.ha.model.pms.asp.udt.YesNoBlankFlag;
import hk.org.ha.model.pms.asp.udt.epr.EprLabItemType;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;


@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class ClinicalData {

	
	@XmlElement(name ="pastMediHistory")
	private List<PastMediHistory> pastMediHistoryList;
	
	
	@XmlElement
	private String pastMediHistoryOthersValue;
	
	@XmlElement
	private YesNoBlankFlag immunoFlag;
	
	@XmlElement(name ="immunocompromise")
	private List<Immunocompromise> immunocompromiseList;
	
	
	@XmlElement
	private String immunoOthersValue;
	@XmlElement
	private String admissionDiagnosis;
	@XmlElement
	private String clinicalInformation;
	@XmlElement
	private BigDecimal bodyTemp;
	@XmlElement
	private YesNoBlankFlag ventilatorFlag;
	@XmlElement
	private Inotrope inotrope;
	@XmlElement
	private YesNoBlankFlag septicShockFlag;
	@XmlElement
	private CategoryInfection categoryInfection;
	@XmlElement
	private String categoryInfectionOthersValue;
	
	
	@XmlElement(name ="LabTest")
	private List<LabTest> labTestList;
	@XmlElement
	private String drName;
	@XmlElement
	private DrRank drRank;
	@XmlElement
	private String drCode;
	
	@XmlElement(name ="organInolved")
	private List<OrganInolved> organInolvedList;
	
	@XmlElement
	private String organInolvedOthersValue;
	
	@XmlElement
	private Treatment treatment;
	@XmlElement
	private AntibioticStatus antibioticStatus;
	@XmlElement
	private String antibioticStatusValue;
	
	@XmlElement
	private String calCrCl;
	
	
	@XmlElement
	private String antibioticName;
	@XmlElement
	private String dosage;
	@XmlElement
	private String dosageUnit;
	@XmlElement
	private String frequency;
	@XmlElement
	private Date startDate;
	
	
	@XmlElement
	private Integer intendedDuration;
	@XmlElement
	private Integer indication;
	@XmlElement
	private String indicationOthersValue;
	@XmlElement(name="indicationDesc")
	private List<String> indicationDescList;
	
	public List<String> getIndicationDescList() {
		return indicationDescList;
	}
	public void setIndicationDescList(List<String> indicationDescList) {
		this.indicationDescList = indicationDescList;
	}
	public String getPastMediHistoryOthersValue() {
		return pastMediHistoryOthersValue;
	}
	public void setPastMediHistoryOthersValue(String pastMediHistoryOthersValue) {
		this.pastMediHistoryOthersValue = pastMediHistoryOthersValue;
	}
	public YesNoBlankFlag getImmunoFlag() {
		return immunoFlag;
	}
	public void setImmunoFlag(YesNoBlankFlag immunoFlag) {
		this.immunoFlag = immunoFlag;
	}
	public String getImmunoOthersValue() {
		return immunoOthersValue;
	}
	public void setImmunoOthersValue(String immunoOthersValue) {
		this.immunoOthersValue = immunoOthersValue;
	}
	public String getAdmissionDiagnosis() {
		return admissionDiagnosis;
	}
	public void setAdmissionDiagnosis(String admissionDiagnosis) {
		this.admissionDiagnosis = admissionDiagnosis;
	}
	public String getClinicalInformation() {
		return clinicalInformation;
	}
	public void setClinicalInformation(String clinicalInformation) {
		this.clinicalInformation = clinicalInformation;
	}
	public BigDecimal getBodyTemp() {
		return bodyTemp;
	}
	public void setBodyTemp(BigDecimal bodyTemp) {
		this.bodyTemp = bodyTemp;
	}
	
	public Inotrope getInotrope() {
		return inotrope;
	}
	public void setInotrope(Inotrope inotrope) {
		this.inotrope = inotrope;
	}
	
	public CategoryInfection getCategoryInfection() {
		return categoryInfection;
	}
	public void setCategoryInfection(CategoryInfection categoryInfection) {
		this.categoryInfection = categoryInfection;
	}
	public String getDrName() {
		return drName;
	}
	public void setDrName(String drName) {
		this.drName = drName;
	}
	public DrRank getDrRank() {
		return drRank;
	}
	public void setDrRank(DrRank drRank) {
		this.drRank = drRank;
	}
	public String getDrCode() {
		return drCode;
	}
	public void setDrCode(String drCode) {
		this.drCode = drCode;
	}
	public String getOrganInolvedOthersValue() {
		return organInolvedOthersValue;
	}
	public void setOrganInolvedOthersValue(String organInolvedOthersValue) {
		this.organInolvedOthersValue = organInolvedOthersValue;
	}
	public Treatment getTreatment() {
		return treatment;
	}
	public void setTreatment(Treatment treatment) {
		this.treatment = treatment;
	}
	public AntibioticStatus getAntibioticStatus() {
		return antibioticStatus;
	}
	public void setAntibioticStatus(AntibioticStatus antibioticStatus) {
		this.antibioticStatus = antibioticStatus;
	}
	public String getAntibioticStatusValue() {
		return antibioticStatusValue;
	}
	public void setAntibioticStatusValue(String antibioticStatusValue) {
		this.antibioticStatusValue = antibioticStatusValue;
	}
	public String getAntibioticName() {
		return antibioticName;
	}
	public void setAntibioticName(String antibioticName) {
		this.antibioticName = antibioticName;
	}
	public String getDosage() {
		return dosage;
	}
	public void setDosage(String dosage) {
		this.dosage = dosage;
	}
	public String getDosageUnit() {
		return dosageUnit;
	}
	public void setDosageUnit(String dosageUnit) {
		this.dosageUnit = dosageUnit;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frrequency) {
		this.frequency = frrequency;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Integer getIntendedDuration() {
		return intendedDuration;
	}
	public void setIntendedDuration(Integer intendedDuration) {
		this.intendedDuration = intendedDuration;
	}
	public String getCategoryInfectionOthersValue() {
		return categoryInfectionOthersValue;
	}
	public void setCategoryInfectionOthersValue(String categoryInfectionOthersValue) {
		this.categoryInfectionOthersValue = categoryInfectionOthersValue;
	}
	public List<LabTest> getLabTestList() {
		return labTestList;
	}
	public void setLabTestList(List<LabTest> labTestList) {
		this.labTestList = labTestList;
	}
	public String getIndicationOthersValue() {
		return indicationOthersValue;
	}
	public void setIndicationOthersValue(String indicationOthersValue) {
		this.indicationOthersValue = indicationOthersValue;
	}
	public YesNoBlankFlag getVentilatorFlag() {
		return ventilatorFlag;
	}
	public void setVentilatorFlag(YesNoBlankFlag ventilatorFlag) {
		this.ventilatorFlag = ventilatorFlag;
	}
	public YesNoBlankFlag getSepticShockFlag() {
		return septicShockFlag;
	}
	public void setSepticShockFlag(YesNoBlankFlag septicShockFlag) {
		this.septicShockFlag = septicShockFlag;
	}
	public List<OrganInolved> getOrganInolvedList() {
		return organInolvedList;
	}
	public void setOrganInolvedList(List<OrganInolved> organInolvedList) {
		this.organInolvedList = organInolvedList;
	}
	
	public List<PastMediHistory> getPastMediHistoryList() {
		return pastMediHistoryList;
	}
	public void setPastMediHistoryList(List<PastMediHistory> pastMediHistoryList) {
		this.pastMediHistoryList = pastMediHistoryList;
	}
	public List<Immunocompromise> getImmunocompromiseList() {
		return immunocompromiseList;
	}
	public void setImmunocompromiseList(List<Immunocompromise> immunocompromiseList) {
		this.immunocompromiseList = immunocompromiseList;
	}
	public Integer getIndication() {
		return indication;
	}
	public void setIndication(Integer indication) {
		this.indication = indication;
	}
	
	public String getImmunocompromise() {
		if(immunocompromiseList != null){
			StringBuilder sb = new StringBuilder();
			for(Immunocompromise i:immunocompromiseList){
				sb.append(i.getDataValue());
			}
			return sb.toString();
		}else{
			
			return "";
		}
	}
	
	public String getOrganInolved() {
		if(organInolvedList != null){
			StringBuilder sb = new StringBuilder();
			for(OrganInolved o:organInolvedList){
				sb.append(o.getDataValue());
			}
			return sb.toString();
		}else{
			return "";
		}
	}
	
	public String getPastMediHistory() {
		if(pastMediHistoryList != null){
			StringBuilder sb = new StringBuilder();
			for(PastMediHistory o:pastMediHistoryList){
				sb.append(o.getDataValue());
			}
			return sb.toString();
		}else{
			return "";
		}
	}
	
	public LabTest getLabTestWBC(){
		if(labTestList != null){
			for(LabTest labTest : labTestList){
				if(EprLabItemType.WBC.getDisplayValue().equals(labTest.getItem())){
					return labTest;
				}
				
			}
			
		}
		return new LabTest();
	} 
	
	public LabTest getLabTestANC(){
		if(labTestList != null){
			for(LabTest labTest : labTestList){
				if(EprLabItemType.ANC.getDisplayValue().equals(labTest.getItem())){
					return labTest;
				}
				
			}
			
		}
		return new LabTest();
	}
	public LabTest getLabTestNeu(){
		if(labTestList != null){
			for(LabTest labTest : labTestList){
				if(EprLabItemType.Neu.getDisplayValue().equals(labTest.getItem())){
					return labTest;
				}
				
			}
			
		}
		return new LabTest();
	}
	public LabTest getLabTestESR(){
		if(labTestList != null){
			for(LabTest labTest : labTestList){
				if(EprLabItemType.ESR.getDisplayValue().equals(labTest.getItem())){
					return labTest;
				}
				
			}
			
		}
		return new LabTest();
	}
	public LabTest getLabTestALT(){
		if(labTestList != null){
			for(LabTest labTest : labTestList){
				if(EprLabItemType.ALT.getDisplayValue().equals(labTest.getItem())){
					return labTest;
				}
				
			}
			
		}
		return new LabTest();
	}
	public LabTest getLabTestALP(){
		if(labTestList != null){
			for(LabTest labTest : labTestList){
				if(EprLabItemType.ALP.getDisplayValue().equals(labTest.getItem())){
					return labTest;
				}
				
			}
			
		}
		return new LabTest();
	}
	public LabTest getLabTestBil(){
		if(labTestList != null){
			for(LabTest labTest : labTestList){
				if(EprLabItemType.Bil.getDisplayValue().equals(labTest.getItem())){
					return labTest;
				}
				
			}
			
		}
		return new LabTest();
	}
	public LabTest getLabTestCRP(){
		if(labTestList != null){
			for(LabTest labTest : labTestList){
				if(EprLabItemType.CRP.getDisplayValue().equals(labTest.getItem())){
					return labTest;
				}
				
			}
			
		}
		return new LabTest();
	}
	public LabTest getLabTestUr(){
		if(labTestList != null){
			for(LabTest labTest : labTestList){
				if(EprLabItemType.Ur.getDisplayValue().equals(labTest.getItem())){
					return labTest;
				}
				
			}
			
		}
		return new LabTest();
	}
	public LabTest getLabTestCr(){
		if(labTestList != null){
			for(LabTest labTest : labTestList){
				if(EprLabItemType.Cr.getDisplayValue().equals(labTest.getItem())){
					return labTest;
				}
				
			}
			
		}
		return new LabTest();
	}
	public LabTest getLabTestPlt(){
		if(labTestList != null){
			for(LabTest labTest : labTestList){
				if(EprLabItemType.Plt.getDisplayValue().equals(labTest.getItem())){
					return labTest;
				}
				
			}
			
		}
		return new LabTest();
	}
	
	public String getIndication1Desc(){
		if(indicationDescList != null && !indicationDescList.isEmpty()){
			return indicationDescList.get(0).isEmpty()?null:indicationDescList.get(0);
		}
		return null;
	}
	
	public String getIndication2Desc(){
		if(indicationDescList != null && indicationDescList.size() > 1){
			return indicationDescList.get(1).isEmpty()?null:indicationDescList.get(1);
			
		}
		return null;
	}
	public String getIndication3Desc(){
		if(indicationDescList != null && indicationDescList.size() > 2){
			return indicationDescList.get(2).isEmpty()?null:indicationDescList.get(2);
			
		}
		return null;
	}
	public String getIndication4Desc(){
		if(indicationDescList != null && indicationDescList.size() > 3){
			return indicationDescList.get(3).isEmpty()?null:indicationDescList.get(3);
			
		}
		return null;
	}
	public String getIndication5Desc(){
		if(indicationDescList != null && indicationDescList.size() > 4){
			return indicationDescList.get(4).isEmpty()?null:indicationDescList.get(4);
			
		}
		return null;
	}
	public String getIndication6Desc(){
		if(indicationDescList != null && indicationDescList.size() > 5){
			return indicationDescList.get(5).isEmpty()?null:indicationDescList.get(5);
			
		}
		return null;
	}
	
	public String getCalCrCl() {
		return calCrCl;
	}
	
	public void setCalCrCl(String calCrCl) {
		this.calCrCl = calCrCl;
	}
	
	
}
