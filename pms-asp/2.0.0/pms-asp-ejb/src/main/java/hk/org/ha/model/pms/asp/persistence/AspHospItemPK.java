package hk.org.ha.model.pms.asp.persistence;

import hk.org.ha.model.pms.asp.udt.AspType;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class AspHospItemPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String hospCode;
	
	private String itemCode;
	
	private AspType aspType;
	
	public AspHospItemPK()
	{
	}
	
	public AspHospItemPK(String hospCode, String itemCode, AspType aspType)
	{
		this.hospCode = hospCode;
		this.itemCode = itemCode;
		this.aspType = aspType;
	}

	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public AspType getAspType() {
		return aspType;
	}

	public void setAspType(AspType aspType) {
		this.aspType = aspType;
	}

	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
}
