package hk.org.ha.service.biz.pms.asp.interfaces;

import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.medprofile.MedProfileOrder;

import java.util.Date;
import java.util.List;

import org.osoa.sca.annotations.OneWay;

public interface AspServiceJmsRemote {
	
	@OneWay
	public void receiveDispOrderItemList(List<DispOrderItem> dispOrderItemList, List<String> patHospCodeList, String g6pdCode, Date batchDate) throws RuntimeException;

	@OneWay
	public void receivePatientTrx(MedProfileOrder medProfileOrder);
}
