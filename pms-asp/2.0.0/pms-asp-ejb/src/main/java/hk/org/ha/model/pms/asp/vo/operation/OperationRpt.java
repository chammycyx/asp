package hk.org.ha.model.pms.asp.vo.operation;

import java.io.Serializable;
import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@ExternalizedBean(type=DefaultExternalizer.class)
public class OperationRpt implements Serializable {
	private static final long serialVersionUID = -6410140013822002536L;
	
	private Date rptDate;
	private String hkid;
	private String sex;
	private String age;
	private String patientName;
	private String phsWard;
	private String phsSpecialty;
	private String ipasWard;
	private String ipasSpecialty;
	private String drRank;
	private String organSystemInvolved;
	private String treatment;
	private String specimen;
	private String organismIsolated;
	private String antibioticName;
	private String indication;
	private String outcomesMeasures;
	private String immediateConcurrentFeedback;
	private String immediateYesCode;
	private String withFeedbackReason;
	private String immediateNoCode;
	private String withoutFeedbackReason;
	private String auditBy;
	private String auditRank;
	private String accurancyInfoProvide;
	private String incorrectInfoReason;
	private String incTreatment;
	private String incOrganism;
	private String incIndication;
	private String incPreviousAntiTreatment;
	private String incSensitivity;
	private String incOthers;
	private String appropriatePresc;
	private String reasonAppropriatePresc;
	private String inappropriatePresc;
	private String reasonInappropriatePresc;
	private String specialtyGeneral;
	private String prescribingSpecialty;
	private String tlcDNR;
	private Integer moPager;
	private Integer smoPager;
	private String hospital;
	private String serialNum;
	private String type;
	
	
	public Date getRptDate() {
		return rptDate;
	}
	public void setRptDate(Date rptDate) {
		this.rptDate = rptDate;
	}
	public String getHkid() {
		return hkid;
	}
	public void setHkid(String hkid) {
		this.hkid = hkid;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getPhsWard() {
		return phsWard;
	}
	public void setPhsWard(String phsWard) {
		this.phsWard = phsWard;
	}
	public String getPhsSpecialty() {
		return phsSpecialty;
	}
	public void setPhsSpecialty(String phsSpecialty) {
		this.phsSpecialty = phsSpecialty;
	}
	public String getIpasWard() {
		return ipasWard;
	}
	public void setIpasWard(String ipasWard) {
		this.ipasWard = ipasWard;
	}
	public String getIpasSpecialty() {
		return ipasSpecialty;
	}
	public void setIpasSpecialty(String ipasSpecialty) {
		this.ipasSpecialty = ipasSpecialty;
	}
	public String getDrRank() {
		return drRank;
	}
	public void setDrRank(String drRank) {
		this.drRank = drRank;
	}
	public String getOrganSystemInvolved() {
		return organSystemInvolved;
	}
	public void setOrganSystemInvolved(String organSystemInvolved) {
		this.organSystemInvolved = organSystemInvolved;
	}
	public String getTreatment() {
		return treatment;
	}
	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}
	public String getSpecimen() {
		return specimen;
	}
	public void setSpecimen(String specimen) {
		this.specimen = specimen;
	}
	public String getOrganismIsolated() {
		return organismIsolated;
	}
	public void setOrganismIsolated(String organismIsolated) {
		this.organismIsolated = organismIsolated;
	}
	public String getIndication() {
		return indication;
	}
	public void setIndication(String indication) {
		this.indication = indication;
	}
	
	public String getOutcomesMeasures() {
		return outcomesMeasures;
	}
	public void setOutcomesMeasures(String outcomesMeasures) {
		this.outcomesMeasures = outcomesMeasures;
	}
	public String getImmediateYesCode() {
		return immediateYesCode;
	}
	public void setImmediateYesCode(String immediateYesCode) {
		this.immediateYesCode = immediateYesCode;
	}
	public String getWithFeedbackReason() {
		return withFeedbackReason;
	}
	public void setWithFeedbackReason(String withFeedbackReason) {
		this.withFeedbackReason = withFeedbackReason;
	}
	public String getImmediateNoCode() {
		return immediateNoCode;
	}
	public void setImmediateNoCode(String immediateNoCode) {
		this.immediateNoCode = immediateNoCode;
	}
	public String getWithoutFeedbackReason() {
		return withoutFeedbackReason;
	}
	public void setWithoutFeedbackReason(String withoutFeedbackReason) {
		this.withoutFeedbackReason = withoutFeedbackReason;
	}
	public String getAuditBy() {
		return auditBy;
	}
	public void setAuditBy(String auditBy) {
		this.auditBy = auditBy;
	}
	public String getAuditRank() {
		return auditRank;
	}
	public void setAuditRank(String auditRank) {
		this.auditRank = auditRank;
	}
	public String getAccurancyInfoProvide() {
		return accurancyInfoProvide;
	}
	public void setAccurancyInfoProvide(String accurancyInfoProvide) {
		this.accurancyInfoProvide = accurancyInfoProvide;
	}
	public String getIncorrectInfoReason() {
		return incorrectInfoReason;
	}
	public void setIncorrectInfoReason(String incorrectInfoReason) {
		this.incorrectInfoReason = incorrectInfoReason;
	}
	public String getIncTreatment() {
		return incTreatment;
	}
	public void setIncTreatment(String incTreatment) {
		this.incTreatment = incTreatment;
	}
	public String getIncOrganism() {
		return incOrganism;
	}
	public void setIncOrganism(String incOrganism) {
		this.incOrganism = incOrganism;
	}
	public String getIncIndication() {
		return incIndication;
	}
	public void setIncIndication(String incIndication) {
		this.incIndication = incIndication;
	}
	public String getIncPreviousAntiTreatment() {
		return incPreviousAntiTreatment;
	}
	public void setIncPreviousAntiTreatment(String incPreviousAntiTreatment) {
		this.incPreviousAntiTreatment = incPreviousAntiTreatment;
	}
	public String getIncSensitivity() {
		return incSensitivity;
	}
	public void setIncSensitivity(String incSensitivity) {
		this.incSensitivity = incSensitivity;
	}
	public String getIncOthers() {
		return incOthers;
	}
	public void setIncOthers(String incOthers) {
		this.incOthers = incOthers;
	}
	public String getAppropriatePresc() {
		return appropriatePresc;
	}
	public void setAppropriatePresc(String appropriatePresc) {
		this.appropriatePresc = appropriatePresc;
	}
	public String getReasonAppropriatePresc() {
		return reasonAppropriatePresc;
	}
	public void setReasonAppropriatePresc(String reasonAppropriatePresc) {
		this.reasonAppropriatePresc = reasonAppropriatePresc;
	}
	public String getInappropriatePresc() {
		return inappropriatePresc;
	}
	public void setInappropriatePresc(String inappropriatePresc) {
		this.inappropriatePresc = inappropriatePresc;
	}
	public String getReasonInappropriatePresc() {
		return reasonInappropriatePresc;
	}
	public void setReasonInappropriatePresc(String reasonInappropriatePresc) {
		this.reasonInappropriatePresc = reasonInappropriatePresc;
	}
	public String getSpecialtyGeneral() {
		return specialtyGeneral;
	}
	public void setSpecialtyGeneral(String specialtyGeneral) {
		this.specialtyGeneral = specialtyGeneral;
	}
	public String getPrescribingSpecialty() {
		return prescribingSpecialty;
	}
	public void setPrescribingSpecialty(String prescribingSpecialty) {
		this.prescribingSpecialty = prescribingSpecialty;
	}
	public String getTlcDNR() {
		return tlcDNR;
	}
	public void setTlcDNR(String tlcDNR) {
		this.tlcDNR = tlcDNR;
	}
	public Integer getMoPager() {
		return moPager;
	}
	public void setMoPager(Integer moPager) {
		this.moPager = moPager;
	}
	public Integer getSmoPager() {
		return smoPager;
	}
	public void setSmoPager(Integer smoPager) {
		this.smoPager = smoPager;
	}
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	public String getSerialNum() {
		return serialNum;
	}
	public void setSerialNum(String serialNum) {
		this.serialNum = serialNum;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAntibioticName() {
		return antibioticName;
	}
	public void setAntibioticName(String antibioticName) {
		this.antibioticName = antibioticName;
	}
	public String getImmediateConcurrentFeedback() {
		return immediateConcurrentFeedback;
	}
	public void setImmediateConcurrentFeedback(String immediateConcurrentFeedback) {
		this.immediateConcurrentFeedback = immediateConcurrentFeedback;
	}
	
	
	
	
}
