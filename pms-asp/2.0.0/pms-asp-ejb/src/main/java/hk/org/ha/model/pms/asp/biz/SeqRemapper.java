package hk.org.ha.model.pms.asp.biz;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import hk.org.ha.fmk.pms.util.PropertiesHelper;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.vo.SeqRemap;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("seqRemapper")
@Scope(ScopeType.APPLICATION)
@MeasureCalls
public class SeqRemapper {

	private static final String APPLICATION_PROPERTIES = "application.properties";
	
	private static final String SEQ_REMAP = "report.sequence.remap";
	
	Map<String, List<SeqRemap>> seqMap = new HashMap<String, List<SeqRemap>>();
	
	public String getTargetInstCode(String srcInstCode, String phsWardCode)
	{
		try 
		{
			List<SeqRemap> seqRemapList = null;
			String seqRemapProp = PropertiesHelper.getProperties(APPLICATION_PROPERTIES).getProperty(SEQ_REMAP);
		
			if(StringUtils.isBlank(seqRemapProp))
			{
				return "";
			}
			
			synchronized (this) {				
				seqRemapList = seqMap.get(seqRemapProp);			
				if (seqRemapList == null) {
					seqRemapList = new ArrayList<SeqRemap>();
					for (String propPerInstCode : seqRemapProp.split("[|]+")) {
						if(!StringUtils.isBlank(propPerInstCode))
						{
							String[] seqRemapStr = propPerInstCode.split("[,\\s]+");
							SeqRemap seqRemap = new SeqRemap();
							seqRemap.setSrcInstCode(seqRemapStr[0]);
							seqRemap.setWardPattern(Pattern.compile(wildcardToRegex(seqRemapStr[1])));
							seqRemap.setTargetInstCode(seqRemapStr[2]);
							seqRemapList.add(seqRemap);
						}
					}
					seqMap.put(seqRemapProp, seqRemapList);
				}
			}
			
			for (SeqRemap seqRemap : seqRemapList) {
				if (seqRemap.getSrcInstCode().equals(srcInstCode) && seqRemap.getWardPattern().matcher(phsWardCode).matches()) {
					return seqRemap.getTargetInstCode();
				}
			}
			return "";
		}
		catch(IOException e)
		{
			return "";
		}
	}
	
    private String wildcardToRegex(String wildcard){
        StringBuffer s = new StringBuffer(wildcard.length());
        s.append('^');
        for (int i = 0, is = wildcard.length(); i < is; i++) {
            char c = wildcard.charAt(i);
            switch(c) {
                case '*':
                    s.append(".*");
                    break;
                case '?':
                    s.append(".");
                    break;
                case '(': case ')': case '[': case ']': case '$':
                case '^': case '.': case '{': case '}': case '|':
                case '\\':
                    s.append("\\");
                    s.append(c);
                    break;
                default:
                    s.append(c);
                    break;
            }
        }
        s.append('$');
        return s.toString();
    }
}
