package hk.org.ha.model.pms.asp.biz;

import hk.org.ha.fmk.pms.util.PropertiesHelper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@AutoCreate
@Name("applicationProp")
@Scope(ScopeType.APPLICATION)
public class ApplicationProp {

	private static final String APPLICATION_PROPERTIES = "application.properties";
	
	private static final String PREV_DATE_RANGE_FOR_CHECK = "batch.disp.check-date-range";
	
	private static final String BATCH_DELAY_HOSP_LIST = "batch.delay-hosp-code-list";
	
	private static final String BATCH_DELAY_ITEM_LIST = "batch.delay-item-code-list";
	
	public Properties getProperties() {
		try {
			return PropertiesHelper.getProperties(APPLICATION_PROPERTIES);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<String> getDelayHospList()
	{
		return Arrays.asList(getProperties().getProperty(BATCH_DELAY_HOSP_LIST).split(","));
	}
	
	public List<String> getDelayItemList()
	{
		return Arrays.asList(getProperties().getProperty(BATCH_DELAY_ITEM_LIST).split(","));
	}
	
	public int getDateRangeForCheck()
	{
		return Integer.parseInt(StringUtils.trim(getProperties().getProperty(PREV_DATE_RANGE_FOR_CHECK)));
	}
}
