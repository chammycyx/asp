package hk.org.ha.model.pms.asp.vo.operation;

import hk.org.ha.model.pms.asp.udt.AspType;

import java.util.Date;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@ExternalizedBean(type=DefaultExternalizer.class)
public class OperationRptCriteria {

	
	private Date startDate;
	private Date endDate;
	private AspType rptType;
	private String hospCode;
	
	private String password;
	
	private Boolean passwordFlag;
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getHospCode() {
		return hospCode;
	}
	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}
	public AspType getRptType() {
		return rptType;
	}
	public void setRptType(AspType rptType) {
		this.rptType = rptType;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Boolean getPasswordFlag() {
		return passwordFlag;
	}
	public void setPasswordFlag(Boolean passwordFlag) {
		this.passwordFlag = passwordFlag;
	}
	
	
	
}
