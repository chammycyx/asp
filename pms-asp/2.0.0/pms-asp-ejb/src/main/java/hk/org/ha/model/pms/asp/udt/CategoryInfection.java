package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum CategoryInfection implements StringValuedEnum {
	Community("C", "Community Acquired"),
	Hospital("H", "Hospital Acquired"),
	Others("O", "Others");
	
	private final String dataValue;
	
	private final String displayValue;
	
	CategoryInfection(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static CategoryInfection dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(CategoryInfection.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<CategoryInfection> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<CategoryInfection> getEnumClass() {
    		return CategoryInfection.class;
    	}
    }
}
