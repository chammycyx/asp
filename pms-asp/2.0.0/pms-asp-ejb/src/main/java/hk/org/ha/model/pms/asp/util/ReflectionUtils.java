package hk.org.ha.model.pms.asp.util;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang3.ClassUtils;

public class ReflectionUtils {

	@SuppressWarnings("unchecked")
	public static void nullifyStrings(Object o) {
		try {
			Package pkg = o.getClass().getPackage();
			
			for ( Field f : o.getClass().getDeclaredFields() ) {
				f.setAccessible(true);
				if (Collection.class.isAssignableFrom(f.getType())) {
					Collection<Object> col = (Collection<Object>) f.get(o);
					if (col != null) {
						for (Object entry : col) {
							if (entry != null) {
								if (entry.getClass().getPackage().equals(pkg)) {
									nullifyStrings(entry);
								}
							}
						}  
					}
				} else if (f.getType().equals(String.class)) {
					String value = (String) f.get(o);
					if (value != null && value.trim().isEmpty()) {
						f.set(o, null);
					}
				} else if (!ClassUtils.isPrimitiveOrWrapper(f.getType()) &&
						!Date.class.isAssignableFrom(f.getType()) &&
						!Number.class.isAssignableFrom(f.getType()) &&
						!Enum.class.isAssignableFrom(f.getType())) {
					Object co = f.get(o);
					if (co != null) {
						if (co.getClass().getPackage().equals(pkg)) {
							nullifyStrings(co);
						}
					}
				}
			}
		}catch ( Exception e ) { 
			throw new RuntimeException(e);
		}
			
	}			
}		

