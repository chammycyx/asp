package hk.org.ha.model.pms.asp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.biz.rpt.AspDispRptManagerLocal;
import hk.org.ha.model.pms.asp.persistence.AspDispItem;
import hk.org.ha.model.pms.asp.udt.AspDispItemStatus;
import hk.org.ha.model.pms.asp.udt.AspType;
import hk.org.ha.model.pms.asp.udt.Gender;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrder;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;
import hk.org.ha.model.pms.persistence.disp.MedCase;
import hk.org.ha.model.pms.persistence.disp.MedOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrder;
import hk.org.ha.model.pms.persistence.disp.PharmOrderItem;
import hk.org.ha.model.pms.vo.rx.Dose;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("aspDispItemManager")
@MeasureCalls
public class AspDispItemManagerBean implements AspDispItemManagerLocal {
	
	@PersistenceContext(unitName="PMSCOR1_ASP")
	private EntityManager em;

	@In
	ApplicationProp applicationProp;
	
	@In
	AspDispRptManagerLocal aspDispRptManager;
	
	@In
	SystemMessageManagerLocal systemMessageManager;
	
	@Logger
	private Log logger;

	@SuppressWarnings("unchecked")
	private boolean isDuplicateAspDispItem(DispOrderItem dispOrderItem, AspType aspType)
	{
		DispOrder dispOrder = dispOrderItem.getDispOrder();
		List<AspDispItem> duplicateList = em.createQuery(
				" select o from AspDispItem o"+
				" where o.hospCode = :hospCode "+
				" and o.dispDate = :dispDate "+
				" and o.dispOrderId = :dispOrderId "+
				" and o.itemNum = :itemNum " +
				" and o.itemCode = :itemCode "+
				" and o.aspType = :aspType "+
				" and o.status <> :aspDispItemStatus "
				)
				.setParameter("hospCode", dispOrder.getWorkstore().getHospCode())
				.setParameter("dispDate", dispOrder.getDispDate())
				.setParameter("dispOrderId", dispOrder.getId())
				.setParameter("itemNum", dispOrderItem.getItemNum())
				.setParameter("itemCode", dispOrderItem.getPharmOrderItem().getItemCode())
				.setParameter("aspType", aspType)
				.setParameter("aspDispItemStatus", AspDispItemStatus.Deleted)
				.getResultList();
		
		logger.debug("isDuplicateAspDispItem: "+(!duplicateList.isEmpty()));
		
		return !duplicateList.isEmpty();
	}
	
	@SuppressWarnings("unchecked")
	private List<AspDispItem> retrieveLatestAspDispItem(DispOrderItem dispOrderItem, AspType aspType)
	{
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		PharmOrder pharmOrder = pharmOrderItem.getPharmOrder();
				
		List<AspDispItem> existList = em.createQuery(
				" select o from AspDispItem o "+
				" where o.hospCode = :hospCode "+
				" and (o.hkid = :hkid "+
				" or o.patKey = :patKey) "+
				" and (o.itemCode = :itemCode "+
				" or (o.drugName = :drugName "+
				" and o.formCode = :formCode)) "+
				" and o.lastDispDate is not null "+
				" and o.aspType = :aspType "+
				" and o.status <> :aspDispItemStatus "+
				" order by o.id "
				)
				.setParameter("hkid", pharmOrder.getHkid())
				.setParameter("patKey", pharmOrder.getPatKey())
				.setParameter("hospCode", dispOrderItem.getDispOrder().getWorkstore().getHospCode())
				.setParameter("itemCode", pharmOrderItem.getItemCode())
				.setParameter("drugName", pharmOrderItem.getDrugName())
				.setParameter("formCode", pharmOrderItem.getFormCode())
				.setParameter("aspType", aspType)
				.setParameter("aspDispItemStatus", AspDispItemStatus.Deleted)
				.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
				.getResultList();
		
		return existList;
	}
	
	private AspDispItem constructAspDispItem(DispOrderItem dispOrderItem, Date batchDate, AspType aspType, AspDispItemStatus itemStatus)
	{
		DispOrder dispOrder = dispOrderItem.getDispOrder();
		PharmOrderItem pharmOrderItem = dispOrderItem.getPharmOrderItem();
		Dose dose = pharmOrderItem.getRegimen().getDoseGroupList().get(0).getDoseList().get(0);
		PharmOrder pharmOrder = pharmOrderItem.getPharmOrder();
		MedCase medCase = pharmOrder.getMedCase();
		MedOrder medOrder = pharmOrder.getMedOrder();
		String hkid = pharmOrder.getHkid();
		String patKey = pharmOrder.getPatKey();
		String hospCode = dispOrder.getHospCode();

		if(StringUtils.isBlank(hospCode))
		{
			hospCode = dispOrder.getWorkstore().getHospCode();
		}
		
		String patHospCode = medOrder.getPatHospCode();
		String itemCode = pharmOrderItem.getItemCode();
		String drugName = pharmOrderItem.getDrugName();
		String formCode = pharmOrderItem.getFormCode();
		
		AspDispItem aspDispItem = new AspDispItem();
		Gender sex = Gender.Unknown;

		if(medCase != null)
		{
			aspDispItem.setCaseNum(medCase.getCaseNum());
			aspDispItem.setBedNum(medCase.getPasBedNum());
			aspDispItem.setPasSpecCode(medCase.getPasSpecCode());
			aspDispItem.setPasWardCode(medCase.getPasWardCode());
			aspDispItem.setAdmissionDate(medCase.getAdmissionDate());
			aspDispItem.setDischargeDate(medCase.getDischargeDate());
		}
		
		aspDispItem.setPatName(pharmOrder.getName());
		aspDispItem.setCcCode(pharmOrder.getCcCode());
		aspDispItem.setDob(pharmOrder.getDob());
		
		if(pharmOrder.getSex() != null)
		{
			sex = Gender.dataValueOf(pharmOrder.getSex().getDataValue());
		}
		
		aspDispItem.setHospCode(hospCode);
		aspDispItem.setItemNum(dispOrderItem.getItemNum());
		aspDispItem.setPatKey(patKey);
		aspDispItem.setHkid(hkid);
		aspDispItem.setDispQty(dispOrderItem.getDispQty());
		aspDispItem.setDispDate(dispOrder.getDispDate());
		aspDispItem.setItemCode(itemCode);
		aspDispItem.setDrugName(drugName);
		aspDispItem.setStrength(pharmOrderItem.getStrength());
		aspDispItem.setFormCode(formCode);
		aspDispItem.setFormDesc(pharmOrderItem.getFormLabelDesc());
		aspDispItem.setDispOrderId(dispOrder.getId());
		aspDispItem.setFreqCode(dose.getDailyFreq().getCode());
		aspDispItem.setSex(sex);
		
		String freqDesc = dose.getDailyFreq().getDesc();
		if(StringUtils.isBlank(freqDesc))
		{
			freqDesc = dose.getDmDailyFrequency().getLabelFreqBlk1Eng();
		}
		aspDispItem.setFreqDesc(freqDesc);
		if(pharmOrderItem.getDoseQty() == null)
		{
			aspDispItem.setDoseQty(dose.getLegacyDosage());
			aspDispItem.setDoseUnit(dose.getDosageUnit());
		}
		else
		{
			aspDispItem.setDoseQty(pharmOrderItem.getDoseQty().toString());
			aspDispItem.setDoseUnit(pharmOrderItem.getDoseUnit());
		}
		
		aspDispItem.setWardCode(dispOrder.getWardCode());
		aspDispItem.setSpecCode(dispOrder.getSpecCode());
		aspDispItem.setStartDate(dispOrderItem.getStartDate());
		aspDispItem.setBatchDate(batchDate);
		aspDispItem.setPatHospCode(patHospCode);
		aspDispItem.setAspType(aspType);
		aspDispItem.setStatus(itemStatus);
		aspDispItem.setLastDispDate(dispOrder.getDispDate());
	
		return aspDispItem;
	}
	
	private void createAspDispItemAndRpt(DispOrderItem dispOrderItem, Date batchDate, AspType aspType, AspDispItemStatus itemStatus, List<AspDispItem> genRptList)
	{
		AspDispItem aspDispItem = constructAspDispItem(dispOrderItem, batchDate, aspType, itemStatus);
		em.persist(aspDispItem);

		if(AspDispItemStatus.None.equals(aspDispItem.getStatus()))
		{
			genRptList.add(aspDispItem);
		}
	}
	
	private void constructAspDispItemAndPrepareRpt(DispOrderItem dispOrderItem, Date batchDate, AspType aspType, AspDispItemStatus itemStatus, List<AspDispItem> genRptList)
	{
		int dateRangeForCheck = applicationProp.getDateRangeForCheck();
		DateMidnight batchDateTime = new DateMidnight(batchDate);
		Date startBatchDate = batchDateTime.minusDays(dateRangeForCheck).toDate();
		logger.debug("constructAspDispItemAndPrepareRpt - startBatchDate: #0 ", startBatchDate);
		if(!isDuplicateAspDispItem(dispOrderItem, aspType))
		{
			List<AspDispItem> existItemList = retrieveLatestAspDispItem(dispOrderItem, aspType);
			logger.debug("constructAspDispItemAndPrepareRpt - existItemList: #0 ", existItemList.size());
			if(existItemList.isEmpty())
			{
				createAspDispItemAndRpt(dispOrderItem, batchDate, aspType, itemStatus, genRptList);
			}
			else
			{
				Date dispDate = dispOrderItem.getDispOrder().getDispDate();

				//keep one record having lastDispDate only 
				for(int i =1;i<existItemList.size();i++)
				{
					AspDispItem aspDispItem = existItemList.get(i);
					aspDispItem.setLastDispDate(null);
					em.merge(aspDispItem);
				}
				
				AspDispItem existItem = existItemList.get(0);
				Date dispDateForCheck = new DateMidnight(dispDate).toDate();
				Date lastDispDateForCheck = new DateMidnight(existItem.getLastDispDate()).toDate();
				
				logger.debug("constructAspDispItemAndPrepareRpt - lastDispDate for check (without time): #0 disp date for check: #1 ", lastDispDateForCheck, dispDateForCheck);
				logger.debug("constructAspDispItemAndPrepareRpt - lastDispDateForCheck.before(startBatchDate): #0  lastDispDateForCheck.compareTo(dispDateForCheck): #1", 
						lastDispDateForCheck.before(startBatchDate),
						( lastDispDateForCheck.compareTo(dispDateForCheck) == 0));
				
				if(lastDispDateForCheck.before(startBatchDate) || lastDispDateForCheck.compareTo(dispDateForCheck) == 0)
				{
					existItem.setLastDispDate(null);
					createAspDispItemAndRpt(dispOrderItem, batchDate, aspType, itemStatus, genRptList);
				}
				else
				{
					existItem.setLastDispDate(dispDate);
				}
				
				em.merge(existItem);
			}
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Override
	public void checkToGenAspDispItemAndRpt(List<DispOrderItem> dispOrderItemList, List<String> patHospCodeList, String g6pdCode, Date batchDate) throws RuntimeException
	{
		try
		{
			List<String> delayHospList = applicationProp.getDelayHospList();
			List<String> delayItemList = applicationProp.getDelayItemList();
			
			List<AspDispItem> genRptList = new ArrayList<AspDispItem>();
			for(DispOrderItem dispOrderItem : dispOrderItemList)
			{
				DispOrder dispOrder = dispOrderItem.getDispOrder();
				logger.info("checkToGenAspDispItemAndRpt for hosp: #0, dispOrder id: #1, itemNum: #2 ",
						dispOrder.getHospCode(),
						dispOrder.getId(),
						dispOrderItem.getItemNum());
				
				AspType aspItemType = AspType.dataValueOf(dispOrderItem.getAspItemType());
				AspDispItemStatus itemStatus;
				
				if(delayHospList.contains(dispOrderItem.getDispOrder().getHospCode()) && delayItemList.contains(dispOrderItem.getPharmOrderItem().getItemCode()))
				{
					itemStatus = AspDispItemStatus.Pending;
				}
				else
				{
					itemStatus = AspDispItemStatus.None;
				}
				
				if(AspType.NONE.equals(aspItemType))
				{
					constructAspDispItemAndPrepareRpt(dispOrderItem, batchDate, AspType.BG, itemStatus, genRptList);
					constructAspDispItemAndPrepareRpt(dispOrderItem, batchDate, AspType.IV, itemStatus, genRptList);
				}
				else
				{
					constructAspDispItemAndPrepareRpt(dispOrderItem, batchDate, aspItemType, itemStatus, genRptList);
				}
			}
			
			em.flush();
			em.clear();
	
			String errCode = "";
			
			logger.info("Generate asp reports for #0 records ",genRptList.size());
			
			if(!genRptList.isEmpty())
			{
				errCode = aspDispRptManager.genNewAspDispRptList(genRptList, patHospCodeList, g6pdCode);
			}
			
			if(!StringUtils.isBlank(errCode))
			{
				String errMsg = systemMessageManager.retrieveMessageDesc(errCode);
				logger.error("checkToGenAspDispItemAndRpt systemMessage=#0",errMsg);
				throw new RuntimeException(errMsg);
			}
		}
		catch(Exception e)
		{
			logger.error("checkToGenAspDispItemAndRpt Exception=#0",e);
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AspDispItem> retrieveAspDispItemListById(List<Long> aspDispItemIdList ) {
		
		return em.createQuery(
				"select o from AspDispItem o "+ // 20170524 index check : ASP_DISP_ITEM.id : PK_ASP_DISP_ITEM
				" where o.id in :idList"+
				" and o.status <> :status "
				)
				.setParameter("idList", aspDispItemIdList)
				.setParameter("status", AspDispItemStatus.Deleted)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Long> retrievePendingItemIdList(Date batchDate, String hospCode) {

		DateMidnight prevBatchDateStart = new DateMidnight(batchDate);
		prevBatchDateStart = prevBatchDateStart.minusDays(1);
		DateTime prevBatchDateEnd = new DateMidnight(prevBatchDateStart).toDateTime().plusDays(1).minus(1);

		List<Long> resultList = em.createQuery(
				"select o.id from AspDispItem o "+ // 20170524 index check : AspDispItem.hospCode,status,batchDate : I_ASP_DISP_ITEM_01
				" where o.hospCode = :hospCode "+
				" and o.status = :status "+
				" and o.batchDate between :prevBatchDateStart and :prevBatchDateEnd "+
				" order by o.id "
				)
				.setParameter("hospCode", hospCode)
				.setParameter("status", AspDispItemStatus.Pending)
				.setParameter("prevBatchDateStart", prevBatchDateStart.toDate())
				.setParameter("prevBatchDateEnd", prevBatchDateEnd.toDate())
				.getResultList();
		
		return resultList;
				
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AspDispItem> retrieveAspDispItemByCaseNum(String caseNum, String patHospCode) {

		return em.createQuery(
				"select o from AspDispItem o"+ // 20170524 index check : AspDispItem.patHospCode,caseNum : I_ASP_DISP_ITEM_02
				" where o.caseNum = :caseNum"+
				" and o.patHospCode = :patHospCode"+
				" and o.status <> :status "
				)
				.setParameter("caseNum", caseNum)
				.setParameter("patHospCode", patHospCode)
				.setParameter("status", AspDispItemStatus.Deleted)
				.getResultList();
	}

}
