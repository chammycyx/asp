package hk.org.ha.model.pms.asp.biz.epr;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asa.exception.epr.EprException;
import hk.org.ha.model.pms.asa.exception.epr.EprUnreachableException;
import hk.org.ha.model.pms.asp.udt.epr.EprParamType;
import hk.org.ha.model.pms.asp.vo.enquiry.AspRpt;
import hk.org.ha.model.pms.asp.vo.enquiry.CultureResult;
import hk.org.ha.model.pms.asp.vo.epr.EprParam;
import hk.org.ha.service.pms.asa.interfaces.epr.EprServiceJmsRemote;

import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateless;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.joda.time.DateTime;

@AutoCreate
@Stateless
@Name("eprManager")
@MeasureCalls
public class EprManagerBean implements EprManagerLocal {

	@In
	EprServiceJmsRemote eprServiceProxy;

	private static final String CLUSTER_IND = "0";
	private static final String LAB_LATEST_IND = "5";
	private static final String CULTURE_LATEST_IND = "0";
	private static final String HKPMI_LATEST_IND = "0";
	
	private int hkpmiSearchRange = 180;
	private int cultureSearchRange = 14;
	private int labSearchRange = 14;
	private String eprSecurityToken = "";
	
	@Override
	public AspRpt retrieveLabDataFromEpr(String patKey, Date batchDate) throws EprUnreachableException, EprException 
	{		
		DateTime batchDateTime = new DateTime(batchDate);
		Date startDate = batchDateTime.minusDays(labSearchRange).toDate();
		EprParam eprParam = getEprParam(EprParamType.PHS_ASP_LIS, patKey, startDate, batchDateTime, LAB_LATEST_IND);
		
		
		return eprServiceProxy.convertEprLabDataToLabResult(eprParam);
	}
	
	@Override
	public String retrieveRecentAdmFromEpr(String patKey, Date batchDate) throws EprUnreachableException, EprException 
	{
		DateTime batchDateTime = new DateTime(batchDate);
		Date startDate = batchDateTime.minusDays(hkpmiSearchRange).toDate();
		EprParam eprParam = getEprParam(EprParamType.PHS_ASP_HKPMI, patKey, startDate, batchDateTime, HKPMI_LATEST_IND);
		
		return eprServiceProxy.retrieveRecentAdmFromEpr(eprParam);
	}

	@Override
	public List<CultureResult> retrieveCultureResultFromEpr(String patKey, Date batchDate) throws EprUnreachableException, EprException 
	{
		DateTime batchDateTime = new DateTime(batchDate);
		Date startDate = batchDateTime.minusDays(cultureSearchRange).toDate();

		EprParam eprParam = getEprParam(EprParamType.PHS_ASP_C_ST, patKey, startDate, batchDateTime, CULTURE_LATEST_IND);
		
		return eprServiceProxy.retrieveCultureResultFromEpr(eprParam);
	}
	
	private EprParam getEprParam(EprParamType type, String patKey, Date startDate, DateTime batchDateTime, String inLatest)
	{
		EprParam eprParam = new EprParam();
		DateTime endDateTime = batchDateTime.withTime(6, 0, 0, 0);
		
		eprParam.setPatKey(patKey);
		eprParam.setStartDate(startDate);
		eprParam.setEndDate(endDateTime.toDate());
		eprParam.setType(type);
		eprParam.setInClusterInd(CLUSTER_IND);
		eprParam.setInLatest(inLatest);
		eprParam.setSecurityToken(eprSecurityToken);

		return eprParam;
	}
	@Remove
	public void destroy() {
	}


}
