package hk.org.ha.model.pms.asp.biz.enquiry;

import hk.org.ha.model.pms.asa.exception.UnreachableException;
import hk.org.ha.model.pms.asa.exception.alert.AlertProfileException;
import hk.org.ha.model.pms.asa.exception.epr.EprException;
import hk.org.ha.model.pms.asa.exception.pas.PasException;
import hk.org.ha.model.pms.asp.exception.RecordNotFoundException;
import hk.org.ha.model.pms.asp.persistence.AspDispRpt;
import hk.org.ha.model.pms.asp.vo.enquiry.AspRptCriteria;

import java.util.List;

import javax.ejb.Local;

@Local
public interface EnqServiceLocal {
	
	public List<AspDispRpt> retrieveAspDispRptList(AspRptCriteria aspRptCriteriaIn);
	public AspDispRpt refreshAspDispRptList(AspRptCriteria aspRptCriteriaIn) throws PasException, UnreachableException, AlertProfileException, EprException, RecordNotFoundException;
	public String updateAspDispRpt(AspDispRpt aspDispRptIn) throws RecordNotFoundException;
	public AspDispRpt updateAspDispRptListStatus(AspRptCriteria aspRptCriteriaIn) throws RecordNotFoundException;
	public AspDispRpt retrieveAspDispRpt(Long aspDispRptId,String workStationId) throws RecordNotFoundException;
	void destroy();
}
