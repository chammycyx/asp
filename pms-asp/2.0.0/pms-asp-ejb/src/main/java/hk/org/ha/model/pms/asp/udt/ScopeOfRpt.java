package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum ScopeOfRpt implements StringValuedEnum {
	Full("F", "Full"),
	Standard("D", "Standard"),
	Simple("S", "Simple");
	
	
	private final String dataValue;
	
	private final String displayValue;
	
	ScopeOfRpt(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static ScopeOfRpt dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(ScopeOfRpt.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<ScopeOfRpt> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<ScopeOfRpt> getEnumClass() {
    		return ScopeOfRpt.class;
    	}
    }
}
