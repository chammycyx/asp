package hk.org.ha.model.pms.asp.biz;

import hk.org.ha.model.pms.asp.persistence.AspDispItem;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface AspDispItemManagerLocal {
	
	List<AspDispItem> retrieveAspDispItemByCaseNum(String caseNum, String patHospCode);
	
	List<Long> retrievePendingItemIdList(Date batchDate, String hospCode);
	
	List<AspDispItem> retrieveAspDispItemListById(List<Long> aspDispItemIdList);
	
	void checkToGenAspDispItemAndRpt(List<DispOrderItem> dispOrderItemList, List<String> patHospCodeList, String g6pdCode, Date batchDate) throws RuntimeException;

}
