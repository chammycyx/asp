package hk.org.ha.model.pms.asp.vo.enquiry;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class Organism {
	@XmlElement
	private String organism;
	
	@XmlElement(name ="Antibiotic")
	private List<Antibiotic> antibioticList;
	
	@XmlTransient
	private String seqId;
	
	public String getOrganism() {
		return organism;
	}
	public void setOrganism(String organism) {
		this.organism = organism;
	}
	public List<Antibiotic> getAntibioticList() {
		return antibioticList;
	}
	public void setAntibioticList(List<Antibiotic> antibioticList) {
		this.antibioticList = antibioticList;
	}
	public String getSeqId() {
		return seqId;
	}
	public void setSeqId(String seqId) {
		this.seqId = seqId;
	}
	
	
}
