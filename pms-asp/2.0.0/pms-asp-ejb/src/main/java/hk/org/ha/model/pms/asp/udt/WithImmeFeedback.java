package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum WithImmeFeedback implements StringValuedEnum {
	Follow("F", "Recommendation followed (e.g. switch to suggested antibiotic, dose, etc)"),
	Change("C", "Change prescription but not follow specific recommendation"),
	NotFollow("R", "Recommendations not followed, i.e. no change of antibiotic, dose, etc."),
	NotApplicable("N", "Not applicable-patient transfer / discharge / death / treatment already stopped"),
	ModifyNotFollow("O", "Modify concurrent antibiotics; recommendation not followed"),
	ModifyFollow("M", "Modify concurrent antibiotics; recommendation followed"),
	Others("P", "Others");
	
	private final String dataValue;
	
	private final String displayValue;
	
	WithImmeFeedback(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static WithImmeFeedback dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(WithImmeFeedback.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<WithImmeFeedback> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<WithImmeFeedback> getEnumClass() {
    		return WithImmeFeedback.class;
    	}
    }
}
