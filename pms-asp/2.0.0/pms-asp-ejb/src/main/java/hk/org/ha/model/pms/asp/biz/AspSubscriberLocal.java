package hk.org.ha.model.pms.asp.biz;

import javax.ejb.Local;

import hk.org.ha.service.biz.pms.asp.interfaces.AspSubscriberJmsRemote;

@Local
public interface AspSubscriberLocal extends AspSubscriberJmsRemote {

}
