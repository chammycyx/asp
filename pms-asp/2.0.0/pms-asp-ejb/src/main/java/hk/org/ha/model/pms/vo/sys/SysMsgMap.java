package hk.org.ha.model.pms.vo.sys;

import hk.org.ha.fmk.pms.sys.entity.SystemMessage;

import java.io.Serializable;
import java.util.Map;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@ExternalizedBean(type=DefaultExternalizer.class)
public class SysMsgMap implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Map<String, SystemMessage> msg;
	
	public void setMsg(Map<String, SystemMessage> msg) {
		this.msg = msg;
	}

	public Map<String, SystemMessage> getMsg() {
		return msg;
	}
}