package hk.org.ha.model.pms.biz.sys;

import hk.org.ha.fmk.pms.sys.entity.SystemMessage;
import hk.org.ha.fmk.pms.util.Interpolator;
import hk.org.ha.fmk.pms.web.MeasureCalls;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;

/**
 * Session Bean implementation class SystemMessageService
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("systemMessageService")
@RemoteDestination
@MeasureCalls
public class SystemMessageServiceBean implements SystemMessageServiceLocal {

	@PersistenceContext
	private EntityManager em;

	@Out(required = false)
	private SystemMessage systemMessage;
	
	public void retrieveSystemMessage(String messageCode, Object... params) {
		systemMessage = em.find(SystemMessage.class, messageCode);
		if (systemMessage != null) {
			systemMessage.setDisplayDesc(convertHtmlFormat(params));
		}
	}

	private String convertHtmlFormat(Object... params){
		StringBuilder resultString = new StringBuilder();

		if (systemMessage != null) {
			resultString.append("<font size='16'><b>");
			resultString.append(systemMessage.getMainMsg());
			resultString.append("</b></font><br><br>");
			if (systemMessage.getSupplMsg() != null
					&& !systemMessage.getSupplMsg().equals("")) {
				resultString.append(systemMessage.getSupplMsg());
			}
		}
		return Interpolator.instance().interpolate(resultString.toString(), params);
	}

	@Remove
	public void destroy() {
	}

}