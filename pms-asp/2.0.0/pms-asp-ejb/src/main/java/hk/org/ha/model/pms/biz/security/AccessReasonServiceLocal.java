package hk.org.ha.model.pms.biz.security;

import javax.ejb.Local;

@Local
public interface AccessReasonServiceLocal
{
   void saveLogonReason(String reason);
}
