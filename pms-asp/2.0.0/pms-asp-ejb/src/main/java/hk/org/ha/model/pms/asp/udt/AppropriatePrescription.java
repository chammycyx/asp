package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AppropriatePrescription  implements StringValuedEnum{
	
	According("A", "According to ST"),
	Immunocompromised("I", "Immunocompromised"),
	Nosocomial("N", "Nosocomial Infection"),
	Empirical("E", "Empirical Treatment for Neutopenic Fever"),
	CAPD("C", "CAPD Peritonitis"),
	Recommended("R", "Recommended by Microbiologist / ID Physicians"),
	History("H", "Allergy History"),
	Severe("S", "Severe Clinical Infection"),
	Failure("F", "Failure of 1st Line Antibiotics"),
	OralIntake("U", "Oral Intake / Absorption Unreliable / Impossible"),
	Others("O", "Others");
	
	private final String dataValue;
	
	private final String displayValue;
	
	AppropriatePrescription(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    public String getDataValue() {
        return this.dataValue;
    }

    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static AppropriatePrescription dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AppropriatePrescription.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<AppropriatePrescription> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<AppropriatePrescription> getEnumClass() {
    		return AppropriatePrescription.class;
    	}
    }
}
