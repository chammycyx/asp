package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum Inotrope implements StringValuedEnum {
	DOBU("DOBU", "Dobutamine"),
	DOPA("DOPA", "Dopamine"),
	NORA("NORA", "Noradrenaline"),
	ADRE("ADRE", "Adrenaline"),
	Others("Others", "Others");
	
	
	private final String dataValue;
	
	private final String displayValue;
	
	Inotrope(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static Inotrope dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(Inotrope.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<Inotrope> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<Inotrope> getEnumClass() {
    		return Inotrope.class;
    	}
    }
}
