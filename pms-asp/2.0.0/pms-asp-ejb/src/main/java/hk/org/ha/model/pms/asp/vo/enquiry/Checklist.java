package hk.org.ha.model.pms.asp.vo.enquiry;

import hk.org.ha.model.pms.asp.udt.YesNoBlankFlag;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class Checklist {
	@XmlElement
	private String name;
	
	@XmlElement
	private Boolean noSpecIndication;
	
	@XmlElement
	private Boolean afebrile;
	
	@XmlElement
	private Boolean wbc;
	
	@XmlElement
	private Boolean sign;
	
	@XmlElement
	private Boolean neutropenic;
	
	@XmlElement
	private Boolean drugByMouth;
	
	@XmlElement
	private Boolean noSuctioning;
	
	@XmlElement
	private Boolean noSevere;
	
	@XmlElement
	private Boolean noMalabsorption;
	
	@XmlElement
	private Boolean noPancreatitis;
	
	@XmlElement
	private YesNoBlankFlag meetCriteriaFlag;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getNoSpecIndication() {
		return noSpecIndication;
	}
	public void setNoSpecIndication(Boolean noSpecIndication) {
		this.noSpecIndication = noSpecIndication;
	}
	public Boolean getAfebrile() {
		return afebrile;
	}
	public void setAfebrile(Boolean afebrile) {
		this.afebrile = afebrile;
	}
	public Boolean getWbc() {
		return wbc;
	}
	public void setWbc(Boolean wbc) {
		this.wbc = wbc;
	}
	public Boolean getSign() {
		return sign;
	}
	public void setSign(Boolean sign) {
		this.sign = sign;
	}
	public Boolean getNeutropenic() {
		return neutropenic;
	}
	public void setNeutropenic(Boolean neutropenic) {
		this.neutropenic = neutropenic;
	}
	public Boolean getDrugByMouth() {
		return drugByMouth;
	}
	public void setDrugByMouth(Boolean drugByMouth) {
		this.drugByMouth = drugByMouth;
	}
	public Boolean getNoSuctioning() {
		return noSuctioning;
	}
	public void setNoSuctioning(Boolean noSuctioning) {
		this.noSuctioning = noSuctioning;
	}
	public Boolean getNoSevere() {
		return noSevere;
	}
	public void setNoSevere(Boolean noSevere) {
		this.noSevere = noSevere;
	}
	public Boolean getNoMalabsorption() {
		return noMalabsorption;
	}
	public void setNoMalabsorption(Boolean noMalabsorption) {
		this.noMalabsorption = noMalabsorption;
	}
	public Boolean getNoPancreatitis() {
		return noPancreatitis;
	}
	public void setNoPancreatitis(Boolean noPancreatitis) {
		this.noPancreatitis = noPancreatitis;
	}
	public YesNoBlankFlag getMeetCriteriaFlag() {
		return meetCriteriaFlag;
	}
	public void setMeetCriteriaFlag(YesNoBlankFlag meetCriteriaFlag) {
		this.meetCriteriaFlag = meetCriteriaFlag;
	}


}
