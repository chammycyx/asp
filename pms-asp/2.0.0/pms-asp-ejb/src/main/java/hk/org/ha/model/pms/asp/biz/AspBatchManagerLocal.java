package hk.org.ha.model.pms.asp.biz;

import hk.org.ha.model.pms.persistence.disp.DispOrderItem;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

@Local
public interface AspBatchManagerLocal {
	void checkToGenAspDispItemAndRpt(List<DispOrderItem> dispOrderItemList, List<String> patHospCodeList, String g6pdCode, Date batchDate) throws RuntimeException;
	
	void genPendingAspDispRpt(List<Long> aspDispItemIdList, List<String> patHospCodeList, String g6pdCode) throws RuntimeException;
}
