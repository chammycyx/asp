package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;

public enum DrRank implements StringValuedEnum {
	House("H", "House Officer"),
	Resident("R", "Resident/MO"),
	SMO("S", "SMO/AC"),
	Consultant("C", "Consultant");
	
	
	private final String dataValue;
	
	private final String displayValue;
	
	DrRank(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static DrRank dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(DrRank.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<DrRank> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<DrRank> getEnumClass() {
    		return DrRank.class;
    	}
    }
}
