package hk.org.ha.model.pms.biz.security;

import hk.org.ha.fmk.pms.security.Uam;
import hk.org.ha.fmk.pms.sys.SysProfile;
import hk.org.ha.fmk.pms.sys.entity.SystemMessage;
import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.cacher.DmDrugCacher;
import hk.org.ha.model.pms.vo.security.PropMap;
import hk.org.ha.model.pms.vo.sys.SysMsgMap;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.granite.messaging.service.annotations.RemoteDestination;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.contexts.Contexts;

/**
 * Session Bean implementation class PostLogonServiceBean
 */
@Stateful
@Name("postLogonService")
@Scope(ScopeType.SESSION)
@RemoteDestination
@MeasureCalls
public class PostLogonServiceBean implements PostLogonServiceLocal {

	@PersistenceContext
	private EntityManager em;
	
	
	@In 
	private AccessControlLocal accessControl;
	
	@In
	private DmDrugCacher dmDrugCacher;

	@Out
	private SysMsgMap sysMsgMap = new SysMsgMap();
	
	@In @Out
	private SysProfile sysProfile;	
	
	@Out(required = false)
	private PropMap permissionMap;

	private boolean enablePreLoadCacher = false;
	
	private boolean isEnablePreLoadCacher() {
		return enablePreLoadCacher;
	}

	public void setEnablePreLoadCacher(boolean enablePreLoadCacher) {
		this.enablePreLoadCacher = enablePreLoadCacher;
	}

	public void postLogon(String debugHospCode, String debugWorkstoreCode) throws IOException {			
		
		if (accessControl.retrieveUserAccessControl(debugHospCode, debugWorkstoreCode)) 
		{
			if (this.isEnablePreLoadCacher()) {
				dmDrugCacher.load();
			}
			
			this.loadSessionData();
			retrieveSystemMessage();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void retrieveSystemMessage() {

		List<SystemMessage> systemMessageList = em.createQuery(
				"select o from SystemMessage o") // 20170524 index check : none
				.getResultList();
		
		Map<String, SystemMessage> properties = new HashMap<String, SystemMessage>();
		for (SystemMessage systemMessage : systemMessageList) {
			
			properties.put(systemMessage.getMessageCode(), systemMessage);
		}
		
		sysMsgMap.setMsg(properties);
		Contexts.getSessionContext().set("sysMsgMap", sysMsgMap);
	}
	
	
	private void loadSessionData() {
		
		Uam uam = (Uam) Component.getInstance(Uam.class, true);   
		
    	permissionMap = new PropMap(uam.getUamInfo().getPermissionMap());
		
	}
	
	@Remove
	public void destroy() {
		if (permissionMap != null) {
			permissionMap = null;
		}
		if (sysProfile != null) {
			sysProfile = null;
		}
	}
}
