package hk.org.ha.model.pms.asp.vo.enquiry;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;

@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class LabTest {
	
	@XmlElement
	private String item;
	
	@XmlElement
	private String hospCode;
	
	@XmlElement
	private String unit;
	
	@XmlElement
	private String value;
	
	@XmlElement
	private Date resultDate;
	
	@XmlTransient
	private Date resultDatetime;
	
	public String getHospCode() {
		return hospCode;
	}
	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}
	
	public Date getResultDate() {
		return resultDate;
	}
	public void setResultDate(Date resultDate) {
		this.resultDate = resultDate;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	
	public Date getResultDatetime() {
		return resultDatetime;
	}
	
	public void setResultDatetime(Date resultDatetime) {
		this.resultDatetime = new Date(resultDatetime.getTime());
	}
	
	
	
	
	
	
}
