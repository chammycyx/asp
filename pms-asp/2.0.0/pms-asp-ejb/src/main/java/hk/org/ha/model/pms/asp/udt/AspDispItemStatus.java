package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AspDispItemStatus implements StringValuedEnum {

	None("-", "None"),
	Pending("P", "Pending"),
	Active("A", "Active"),
	Deleted("D", "Deleted");
	
	private final String dataValue;
	
	private final String displayValue;
	
	AspDispItemStatus(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static AspDispItemStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AspDispItemStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<AspDispItemStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<AspDispItemStatus> getEnumClass() {
    		return AspDispItemStatus.class;
    	}
    }
}
