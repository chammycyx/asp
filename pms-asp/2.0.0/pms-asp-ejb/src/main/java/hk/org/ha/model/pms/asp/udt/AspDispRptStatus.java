package hk.org.ha.model.pms.asp.udt;

import hk.org.ha.fmk.pms.util.StringValuedEnum;
import hk.org.ha.fmk.pms.util.StringValuedEnumConverter;
import hk.org.ha.fmk.pms.util.StringValuedEnumReflect;

public enum AspDispRptStatus implements StringValuedEnum {
	New("N", "New"),
	Edited("E", "Edited"),
	Inactive("I", "Inactive");
	

	private final String dataValue;
	
	private final String displayValue;
	
	AspDispRptStatus(final String dataValue, final String displayValue)
	{
		this.dataValue = dataValue;
		this.displayValue = displayValue;
	}
	
    @Override
    public String getDataValue() {
        return this.dataValue;
    }

    @Override
    public String getDisplayValue() {
        return this.displayValue;
    }   
    
    public static AspDispRptStatus dataValueOf(String dataValue) {
		return StringValuedEnumReflect.getObjectFromValue(AspDispRptStatus.class, dataValue);
	}

	public static class Converter extends StringValuedEnumConverter<AspDispRptStatus> {

		private static final long serialVersionUID = 1L;

		@Override
    	public Class<AspDispRptStatus> getEnumClass() {
    		return AspDispRptStatus.class;
    	}
    }
}
