package hk.org.ha.model.pms.asp.biz;

import hk.org.ha.model.pms.asp.persistence.AspItem;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

@Local
public interface AspItemManagerLocal {
	Map<String, AspItem> retrieveAspItemMap();
	
	List<String> retrieveBGIndicationByItemCode(String itemCode);

	void cleareAspItemMap();
}
