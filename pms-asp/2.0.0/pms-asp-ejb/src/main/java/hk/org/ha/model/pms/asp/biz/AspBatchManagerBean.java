package hk.org.ha.model.pms.asp.biz;

import java.util.Date;
import java.util.List;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.biz.rpt.AspDispRptManagerLocal;
import hk.org.ha.model.pms.biz.sys.SystemMessageManagerLocal;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("aspBatchManager")
@MeasureCalls
public class AspBatchManagerBean implements AspBatchManagerLocal 
{
	@Logger
	private Log logger;
	
	@In
	private AspDispRptManagerLocal aspDispRptManager;
	
	@In
	private AspDispItemManagerLocal aspDispItemManager;
	
	@In
	SystemMessageManagerLocal systemMessageManager;
	
	private int dayEndBatchSize = 50;
	
	@PersistenceContext(unitName="PMSCOR1_ASP")
	private EntityManager em;
	
	@Override
	public void checkToGenAspDispItemAndRpt(List<DispOrderItem> dispOrderItemList, List<String> patHospCodeList, String g6pdCode, Date batchDate) throws RuntimeException {
		
		for(int start = 0, end = 0;start<dispOrderItemList.size(); start = end)
		{
			end = start + dayEndBatchSize;
			if(end > dispOrderItemList.size())
			{
				end = dispOrderItemList.size();
			}
			
			List<DispOrderItem> dispOrderItemSubList = dispOrderItemList.subList(start, end);

			logger.info("checkToGenAspDispItemAndRpt process dispOrderItem sub-list from list index = #0 to index = #1, count = #2 ", start, (end-1), dispOrderItemSubList.size());
			
			aspDispItemManager.checkToGenAspDispItemAndRpt(dispOrderItemSubList, patHospCodeList, g6pdCode, batchDate);
		}
	}

	@Override
	public void genPendingAspDispRpt(List<Long> aspDispItemIdList, List<String> patHospCodeList, String g6pdCode) throws RuntimeException 
	{
		for(int start = 0, end = 0;start<aspDispItemIdList.size(); start = end)
		{
			end = start + dayEndBatchSize;
			if(end > aspDispItemIdList.size())
			{
				end = aspDispItemIdList.size();
			}
			
			List<Long> aspDispItemIdSubList = aspDispItemIdList.subList(start, end);
			
			logger.info("genPendingAspDispRpt process AspDispItem sub-list from list index = #0 to index = #1, count = #2 ", start, (end-1), aspDispItemIdSubList.size());

			String errCode = aspDispRptManager.genBatchAspDispRptList(aspDispItemIdSubList, patHospCodeList, g6pdCode);
			
			if(!StringUtils.isBlank(errCode))
			{
				String errMsg = systemMessageManager.retrieveMessageDesc(errCode);
				throw new RuntimeException(errMsg);
			}
		}
	}
}
