@XmlJavaTypeAdapters({   
    @XmlJavaTypeAdapter(value=DateAdapter.class, type=Date.class)
})  

package hk.org.ha.model.pms.asp.vo.enquiry;


import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
   