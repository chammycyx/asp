package hk.org.ha.model.pms.asp.persistence;

import hk.org.ha.fmk.pms.util.StringCollectionConverter;
import hk.org.ha.model.pms.persistence.VersionEntity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.eclipse.persistence.annotations.Convert;
import org.eclipse.persistence.annotations.Converter;

@Entity
@Table(name="ASP_ITEM")
public class AspItem extends VersionEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ITEM_CODE", nullable = false)
	private String itemCode;

	@Transient
	private String itemDesc;

	@Converter(name = "AspItem.indications", converterClass = StringCollectionConverter.class)
	@Convert("AspItem.indications")
	@Column(name="INDICATIONS")
	private List<String> indications;
	
	@OneToMany(mappedBy = "aspItem")
	private List<AspHospItem> aspHospItemList;
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public List<AspHospItem> getAspHospItemList() {
		return aspHospItemList;
	}

	public void setAspHospItemList(List<AspHospItem> aspHospItemList) {
		this.aspHospItemList = aspHospItemList;
	}

	public List<String> getIndications() {
		return indications;
	}

	public void setIndications(List<String> indications) {
		this.indications = indications;
	}

	public String getItemDesc() {
		return itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	
}
