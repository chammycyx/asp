package hk.org.ha.model.pms.asp.vo.enquiry;

import hk.org.ha.model.pms.asp.udt.AuditRank;
import hk.org.ha.model.pms.asp.udt.OutcomeMeasurementType;
import hk.org.ha.model.pms.asp.udt.WithImmeFeedback;
import hk.org.ha.model.pms.asp.udt.WithoutImmeFeedback;
import hk.org.ha.model.pms.asp.udt.YesNoBlankFlag;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.granite.messaging.amf.io.util.externalizer.DefaultExternalizer;
import org.granite.messaging.amf.io.util.externalizer.annotation.ExternalizedBean;
@XmlAccessorType(XmlAccessType.FIELD)
@ExternalizedBean(type=DefaultExternalizer.class)
public class OutcomeMeasurement {
	
	@XmlElement
	private OutcomeMeasurementType outcomeMeasurementType;
	
	@XmlElement
	private String collectBy;
	
	@XmlElement
	private Date collectDate;
	
	@XmlElement
	private String auditBy;
	
	@XmlElement
	private AuditRank auditRank;
	
	@XmlElement
	private Date auditDate;
	
	@XmlElement
	private YesNoBlankFlag immeFeedbackFlag;
	
	@XmlElement(name ="withImmeFeedback")
	private List<WithImmeFeedback> withImmeFeedbackList;
	
	@XmlElement
	private String withImmeFeedbackOtherValue;
	
	@XmlElement(name ="withoutImmeFeedback")
	private List<WithoutImmeFeedback> withoutImmeFeedbackList;
	
	@XmlElement
	private String withoutImmeFeedbackOtherValue;
	
	public String getCollectBy() {
		return collectBy;
	}
	public void setCollectBy(String collectedBy) {
		this.collectBy = collectedBy;
	}
	public Date getCollectDate() {
		return collectDate;
	}
	public void setCollectDate(Date collectDate) {
		this.collectDate = collectDate;
	}
	public String getAuditBy() {
		return auditBy;
	}
	public void setAuditBy(String auditBy) {
		this.auditBy = auditBy;
	}
	public Date getAuditDate() {
		return auditDate;
	}
	public void setAuditDate(Date auditDate) {
		this.auditDate = auditDate;
	}
	public String getWithImmeFeedbackOtherValue() {
		return withImmeFeedbackOtherValue;
	}
	public void setWithImmeFeedbackOtherValue(String withImmeFeedbackOtherValue) {
		this.withImmeFeedbackOtherValue = withImmeFeedbackOtherValue;
	}
	public String getWithoutImmeFeedbackOtherValue() {
		return withoutImmeFeedbackOtherValue;
	}
	public void setWithoutImmeFeedbackOtherValue(
			String withoutImmeFeedbackOtherValue) {
		this.withoutImmeFeedbackOtherValue = withoutImmeFeedbackOtherValue;
	}
	public OutcomeMeasurementType getOutcomeMeasurementType() {
		return outcomeMeasurementType;
	}
	public void setOutcomeMeasurementType(
			OutcomeMeasurementType outcomeMeasurementType) {
		this.outcomeMeasurementType = outcomeMeasurementType;
	}
	public YesNoBlankFlag getImmeFeedbackFlag() {
		return immeFeedbackFlag;
	}
	public void setImmeFeedbackFlag(YesNoBlankFlag immeFeedbackFlag) {
		this.immeFeedbackFlag = immeFeedbackFlag;
	}
	public AuditRank getAuditRank() {
		return auditRank;
	}
	public void setAuditRank(AuditRank auditRank) {
		this.auditRank = auditRank;
	}
	
	public String getAuditRankDisplayValue() {
		if(auditRank != null){
			return auditRank.getDisplayValue();
		}else{
			return "";
		}
	}
	
	public List<WithImmeFeedback> getWithImmeFeedbackList() {
		return withImmeFeedbackList;
	}
	public void setWithImmeFeedbackList(List<WithImmeFeedback> withImmeFeedbackList) {
		this.withImmeFeedbackList = withImmeFeedbackList;
	}
	public List<WithoutImmeFeedback> getWithoutImmeFeedbackList() {
		return withoutImmeFeedbackList;
	}
	public void setWithoutImmeFeedbackList(
			List<WithoutImmeFeedback> withoutImmeFeedbackList) {
		this.withoutImmeFeedbackList = withoutImmeFeedbackList;
	}
	
	public String getWithImmeFeedback() {
		StringBuilder sb = new StringBuilder();
		if(withImmeFeedbackList!= null){
			for(WithImmeFeedback o:withImmeFeedbackList){
				sb.append(o.getDataValue());
			}
			
		}
		
		return sb.toString();
	}
	
	public String getWithoutImmeFeedback() {
		StringBuilder sb = new StringBuilder();
		if(withoutImmeFeedbackList != null){
			for(WithoutImmeFeedback o:withoutImmeFeedbackList){
				sb.append(o.getDataValue());
			}
			
		}
		
		return sb.toString();
	}
	
}
