package hk.org.ha.model.pms.asp.persistence;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import hk.org.ha.model.pms.asp.udt.AspType;

public class AspHospSpecPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String hospCode;
	
	private String specCode;
	
	private AspType aspType;

	public AspHospSpecPK()
	{
	}
	
	public AspHospSpecPK(String hospCode, String specCode, AspType aspType)
	{
		this.hospCode = hospCode;
		this.specCode = specCode;
		this.aspType = aspType;
	}
	
	public String getHospCode() {
		return hospCode;
	}

	public void setHospCode(String hospCode) {
		this.hospCode = hospCode;
	}

	public String getSpecCode() {
		return specCode;
	}

	public void setSpecCode(String specCode) {
		this.specCode = specCode;
	}

	public AspType getAspType() {
		return aspType;
	}

	public void setAspType(AspType aspType) {
		this.aspType = aspType;
	}

	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}
	
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}
	
}
