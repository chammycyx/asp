package hk.org.ha.model.pms.asp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.asp.persistence.SeqTable;
import hk.org.ha.model.pms.asp.udt.AspType;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.PessimisticLock;
import org.eclipse.persistence.config.QueryHints;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;

@AutoCreate
@Stateless
@Name("seqTableManager")
@MeasureCalls
public class SeqTableManagerBean implements SeqTableManagerLocal {

	private static final String ASP_DISP_RPT_SN_PREFIX = "RPT_SERIAL_NUM";
	private static final String DELIMITER = "_";
	private static final int SN_DIGIT_LEN = 8;
	private static final char SN_PAD_CHAR = '0';
	@PersistenceContext
	EntityManager em;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String retrieveAspDispRptSerialNum(String hospCode, AspType aspType) {
		
		String seqName = ASP_DISP_RPT_SN_PREFIX+DELIMITER+hospCode.toUpperCase()+DELIMITER+aspType.getDataValue();
		
		SeqTable rptSerialNumSeq = this.retrieveSeqTable(seqName);

		if (rptSerialNumSeq.getCurrNum() >= rptSerialNumSeq.getMaxNum()) {
			rptSerialNumSeq.setCurrNum(rptSerialNumSeq.getMinNum());
		}
		else {
			rptSerialNumSeq.setCurrNum(rptSerialNumSeq.getCurrNum() + 1);
		}
		
		return hospCode+aspType.getDataValue()+StringUtils.leftPad(rptSerialNumSeq.getCurrNum().toString(), SN_DIGIT_LEN, SN_PAD_CHAR);
	}

	private SeqTable retrieveSeqTable(String seqName) 
	{
		return (SeqTable) em.createQuery(
			"select o from SeqTable o " + // 20170524 index check : SeqTable.seqName : PK_SEQ_TABLE
			" where o.seqName = :seqName" +
			" and o.seqName is not null")
			.setHint(QueryHints.PESSIMISTIC_LOCK, PessimisticLock.Lock)
			.setParameter("seqName", seqName)
			.getSingleResult();
	}
}
