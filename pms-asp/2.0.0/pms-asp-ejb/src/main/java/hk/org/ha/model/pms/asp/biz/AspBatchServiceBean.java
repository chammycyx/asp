package hk.org.ha.model.pms.asp.biz;

import hk.org.ha.fmk.pms.web.MeasureCalls;
import hk.org.ha.model.pms.persistence.disp.DispOrderItem;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@AutoCreate
@Stateless
@Name("aspBatchService")
@MeasureCalls
public class AspBatchServiceBean implements AspBatchServiceLocal {

	@In
	AspDispItemManagerLocal aspDispItemManager;
	
	@In
	AspBatchManagerLocal aspBatchManager;
	
	@PersistenceContext(unitName="PMSCOR1_ASP")
	private EntityManager em;
	
	@Logger
	private Log logger;

	@Override
	public void generatePendingAspDispRpt(Date batchDate, String hospCode, String g6pdCode, List<String> patHospCodeList) throws RuntimeException
	{		
		List<Long> aspDispItemIdList = aspDispItemManager.retrievePendingItemIdList(batchDate, hospCode);

		logger.info("generatePendingAspDispRpt - total number of records: #0", aspDispItemIdList.size());
		
		if(aspDispItemIdList != null && !aspDispItemIdList.isEmpty())
		{
			aspBatchManager.genPendingAspDispRpt(aspDispItemIdList, patHospCodeList, g6pdCode);
		}

	}
	
	@Override
	public void receiveLegacyDispOrderItemList(List<DispOrderItem> dispOrderItemList, List<String> patHospCodeList, String g6pdCode, Date batchDate) throws RuntimeException
	{
		logger.info("receiveDispOrderItemList - total number of records: #0", dispOrderItemList.size());
		
		if(dispOrderItemList!=null && !dispOrderItemList.isEmpty())
		{
			aspBatchManager.checkToGenAspDispItemAndRpt(dispOrderItemList, patHospCodeList, g6pdCode, batchDate);
		}
	}

}
